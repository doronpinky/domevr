﻿/*******************************************************
 * Copyright (C) 2017 Doron Weiss  - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of unity license.
 * 
 * See https://abnormalcreativity.wixsite.com/home for more info
 *******************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DomeVR.JoystickCom;

namespace Dweiss
{
    [System.Serializable]
    public class Settings : ASettings
    {
        public bool printDebug;

        [Header("Settings")]
        public string ip;
        public int port;
        public int trackerStartId = 0;
        public float timeBetweenDataSend = 0;

        #region Singleton
        private static bool m_ShuttingDown = false;

        public bool dontDestoryOnLoad;

        private static Settings _instance;
        public static Settings S { get { return Instance; } }

        public static Settings S_AutoInit
        {
            get
            {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(Settings) +
                        "' already destroyed. Returning null.");
                    return null;
                }
                var ret = Instance;
                if (ret == null)
                {
                    var go = new GameObject(typeof(Settings).Name, typeof(Settings));
                    ret = Instance = go.GetComponent<Settings>();
                }
                return ret;
            }
        }
        //Assumes access via main thread only
        public static Settings Instance
        {
            get
            {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(Settings) +
                        "' already destroyed. Returning null.");
                    return null;
                }

                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<Settings>();
                }
                return _instance;
            }
            set
            {
                _instance = value;
                //Debug.Log("Singleton " + typeof(T).Name + " set as " + (_instance == null?"null":value.transform.FullName()));
            }
        }

        protected override void Awake()
        {
            base.Awake();

            //Debug.Log("Singeltong " + transform.FullName() + " with instance " + (_instance == null ?"NULL" : _instance.transform.FullName()));
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                throw new System.NotSupportedException("Multiple singelton instances " + this + ", " + _instance);
            }
            else
            {

                Instance = this as Settings;

                if (dontDestoryOnLoad)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
            Setup();

        }

        private void OnApplicationQuit()
        {
            m_ShuttingDown = true;
        }

        protected virtual void OnDestroy()
        {
            if (_instance == this)
                Instance = null;
        }

        #endregion

        private void Setup()
        {
            //UDP
            var allGo = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            var udpWrapper = allGo.First(a => a.GetComponent<UDPWrapper>()).GetComponent<UDPWrapper>();
            udpWrapper.externalIp = ip;
            udpWrapper.externalPort = port;
            udpWrapper.gameObject.SetActive(true);


            //Trackers
            var trackers = GameObject.FindObjectsOfType<SendTracker>();
            SendTracker.id = trackerStartId;
            for (int i = 0; i < trackers.Length; i++)
            {
                trackers[i].timeBetweenSend = timeBetweenDataSend;
                trackers[i].enabled = true;
            }

#if UNITY_EDITOR

#endif

        }





    }
}