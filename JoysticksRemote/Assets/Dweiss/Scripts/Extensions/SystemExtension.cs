﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class StandardLibExtenstion
{
    public static string ElementAtOrDefault(this string[] arr, int index, string defaultStr = null)
    {
        if (arr.Length <= index || index < 0) return defaultStr;
        return arr[index];
    }
}


namespace System
{
    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
    public static class SystemExtension {
        public static string SpaceCamelCase(this string that)
        {

            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(that, " ");
        }

        public static string[] SplitCamelCase(this string source)
        {
            return Regex.Split(source, @"(?<!^)(?=[A-Z])");
        }
    }
}

namespace System
{
    public static class MathfExt
    {
        public static float Fraction(this float floatNumber)
        {
            return (float)(floatNumber - System.Math.Truncate(floatNumber));
        }
    }
}

namespace System.Collections.Generic {
    public static class SystemCollectionsGenericExtension
    {

        public static void AddRangeCast<T,E>(this IList<T> list, IEnumerable<E> other) where T : class
        {
            foreach(var toAdd in other)
            {
                list.Add(toAdd as T);
            }
        }

        public static int IncreamentOrCreate<T>(this IDictionary<T, int> that, T newKey)
        {
            int value = 0;
            //Debug.Log(that + " search for " + (newKey == null ? "NULL" : newKey.ToString()) );
            if(that.TryGetValue(newKey, out value))
            {
                ++value;
                that[newKey] = value;
                return value;
            }

            that[newKey] = 1;
            return 1;
        }
        public static int Decreament<T>(this IDictionary<T, int> that, T newKey)
        {
            int value = 0;
            if(that.TryGetValue(newKey, out value))
            {
                --value;
                that[newKey] = value;
                return value;
            }
            that[newKey] = -1;
            return -1;

        }

        public static V GetOrInit<K,V>(this IDictionary<K,V> that, K key) where V : new()
        {
            V val = default(V);
            if(that.TryGetValue(key, out val))
            {
                return val;
            }
            val = new V();
            that[key] = val;
            return val;
        }
       
        public static void AddRange<T>(this HashSet<T> that, IEnumerable<T> toAdd)
        {
            foreach(var newAdded in toAdd)
            {
                that.Add(newAdded);
            }
        }

        public static void Foreach<T>(this IEnumerable<T> that, Action<T> func)
        {
            foreach (var t in that)
            {
                func(t);
            }
        }
    }
}
