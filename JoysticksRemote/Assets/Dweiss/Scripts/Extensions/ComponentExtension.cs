﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Dweiss;

public static class ComponentExtension {


    public static string ToFormatedString(this Vector3 that, string seperator, string floatToStrFormat = null)
    {
        return floatToStrFormat == null? string.Format("{0}{3}{1}{3}{2}", that.x, that.y, that.z, seperator) :
            string.Format("{0}{3}{1}{3}{2}", that.x.ToString(floatToStrFormat), that.y.ToString(floatToStrFormat), that.z.ToString(floatToStrFormat), seperator);
    }

    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        return IsVisibleFrom(renderer.bounds, camera);
    }

    public static bool IsVisibleFrom(Bounds bounds, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, bounds);
    }

    public static Quaternion Inverse(this Quaternion that)
    {
        return Quaternion.Inverse(that);
    }


    public static float Range(this System.Random rnd, float a, float b)
    {
        return (float)rnd.NextDouble()*(b-a) + a;
    }

    public static Vector3 RandomPointInBounds(this Bounds bounds, System.Random rnd = null)
    {
        if (rnd == null) rnd = new System.Random();
        return new Vector3(
            rnd.Range(bounds.min.x, bounds.max.x),
            rnd.Range(bounds.min.y, bounds.max.y),
            rnd.Range(bounds.min.z, bounds.max.z)
        );
    }

    public delegate void SimpleMethod();

    public static Vector3[] GetPoints(this Bounds b)
    {
        var c = b.center;
        var ex = b.extents;
        var points = new Vector3[] {
                     new Vector3(c.x + ex.x, c.y + ex.y, c.z - ex.z)
                    ,new Vector3(c.x + ex.x, c.y + ex.y, c.z + ex.z)
                    ,new Vector3(c.x + ex.x, c.y - ex.y, c.z - ex.z)
                    ,new Vector3(c.x + ex.x, c.y - ex.y, c.z + ex.z)

                    ,new Vector3(c.x - ex.x, c.y - ex.y, c.z + ex.z)
                    ,new Vector3(c.x - ex.x, c.y - ex.y, c.z - ex.z)
                    ,new Vector3(c.x - ex.x, c.y + ex.y, c.z + ex.z)
                    ,new Vector3(c.x - ex.x, c.y + ex.y, c.z - ex.z)
                };
        return points;
    }

     public static Vector3[] GetCenterPoints(this Bounds b)
    {
        var c = b.center;
        var ex = b.extents;
        var points = new Vector3[] {
                     new Vector3(c.x + ex.x, c.y, c.z)
                    ,new Vector3(c.x - ex.x, c.y, c.z)
                    ,new Vector3(c.x, c.y + ex.y, c.z)
                    ,new Vector3(c.x, c.y - ex.y, c.z)
                    ,new Vector3(c.x, c.y, c.z + ex.z)
                    ,new Vector3(c.x, c.y, c.z - ex.z)
                };
        return points;
    }



    public static void Enable(this Behaviour[] bList, bool active)
    { for (int i = 0; i < bList.Length; ++i) bList[i].enabled = active; }
    public static void Enable(this Renderer[] bList, bool active)
    { for (int i = 0; i < bList.Length; ++i) bList[i].enabled = active; }
    public static void Enable(this Collider[] bList, bool active)
    { for (int i = 0; i < bList.Length; ++i) bList[i].enabled = active; }

    public static void Enable(this IList<Behaviour> bList, bool active)
    { for (int i = 0; i < bList.Count; ++i) bList[i].enabled = active; }
    public static void Enable(this IList<Renderer> bList, bool active)
    { for (int i = 0; i < bList.Count; ++i) bList[i].enabled = active; }
    public static void Enable(this IList<Collider> bList, bool active)
    { for (int i = 0; i < bList.Count; ++i) bList[i].enabled = active; }

    public static void SetActive(this GameObject[] goList, bool active)
    {
        for (int i = 0; i < goList.Length; ++i)
        {
            goList[i].SetActive(active);
        }
    }
    public static void SetActive(this System.Collections.Generic.IList<GameObject> goList, bool active)
    {
        for (int i = 0; i < goList.Count; ++i)
        {
            goList[i].SetActive(active);
        }
    }

    public static void SetActive(this System.Collections.Generic.IList<Transform> goList, bool active)
    {
        for (int i = 0; i < goList.Count; ++i)
        {
            goList[i].gameObject.SetActive(active);
        }
    }

    public static void PendingAnimation(this MonoBehaviour that, Animation anim, SimpleMethod  f)
	{
		that.StartCoroutine (WaitForAnimation (anim, f));
	}

    public static IEnumerator WaitForAnimation ( Animation animation, SimpleMethod  f )
	{
		do
		{
			yield return null;
		} while ( animation.isPlaying );
			f ();
	}
	
	public static Coroutine WaitForSeconds(this MonoBehaviour that, float seconds, SimpleMethod  f)
	{
        return that.StartCoroutine (WaitForSecondsHelper (seconds, f));
	}
    public static Coroutine WaitForSecondsFixedTime(this MonoBehaviour that, float seconds, SimpleMethod f)
    {
        return that.StartCoroutine(WaitForSecondsHelperFixedTime(seconds, f));
    }
    public static Coroutine WaitForSecondsRealTime(this MonoBehaviour that, float seconds, SimpleMethod f)
    {
        return that.StartCoroutine(WaitForSecondsHelperRealTime(seconds, f));
    }
    public static IEnumerator WaitForSecondsHelperRealTime(float seconds, SimpleMethod f)
    {
        yield return new WaitForSecondsRealtime(seconds);
        f();
    }
    public static IEnumerator WaitForSecondsHelperFixedTime(float seconds, SimpleMethod f)
    {
        yield return new WaitForSeconds(seconds);
        yield return new WaitForFixedUpdate();
        f();
    }
    public static Coroutine SimpleCoroutine(this MonoBehaviour that, System.Func<float> f)
    {
        return that.StartCoroutine(CoroutineBetweenFrame(f, -1));
    }

    public static Coroutine SimpleCoroutine(this MonoBehaviour that, float waitToBegin, System.Func<float> f)
    {
        return that.StartCoroutine(CoroutineBetweenFrame(f, waitToBegin));
    }

    public static IEnumerator CoroutineBetweenFrame(System.Func<float> f, float waitToBegin)
    {
        if(waitToBegin >= 0)
            yield return new WaitForSeconds(waitToBegin);

        while (true)
        {
            var call = f();
            if (float.IsPositiveInfinity(call)) break;
            yield return new WaitForSeconds(call);
        }

    }


    public static IEnumerator WaitForSecondsHelper ( float seconds, SimpleMethod  f )
	{
        if(seconds >= 0)
		    yield return new WaitForSeconds(seconds);
		
		f ();
	}

    public static T GetOrAddComponent<T>(this GameObject go) where T:Component
    {
        var ret = go.GetComponent<T>();
        if (ret == null) ret = go.AddComponent<T>();
        return ret;
    }

    public static List<Transform> GetChildrens(this Transform t, bool includeDisable = true)
    {
        var children = new List<Transform>(includeDisable? t.childCount : t.childCount/4);
        for(int i=0; i < t.childCount; ++i)
        {
            var c = t.GetChild(i);
            if (includeDisable || c.gameObject.activeSelf)
                children.Add(c);
        }
        return children;
    }

    public static Transform RecGetTransformByHeirarchyName(this Transform current, string name, char seperator = '.')
    {
        var nameArr = name.SplitOnce(seperator);
        var children = current.GetChildrens();
        for (int i=0; i < children.Count; ++i)
        {
            if(children[i].name == nameArr[0])
            {
                if (string.IsNullOrEmpty(nameArr[1])) return children[i];

                return RecGetTransformByHeirarchyName(children[i], nameArr[1], seperator);
            }
            //.name == nameArr[0])
        }
        return null;
    }

    public static List<T> FindObjectsOfType<T>(bool includeInactive = true)
    {
        List<T> results = new List<T>();
        for(int i=0; i < UnityEngine.SceneManagement.SceneManager.sceneCount; ++i) {
            var sc = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);
            //foreach (var sc in UnityEngine.SceneManagement.SceneManager.GetAllScenes()) { 
            sc.GetRootGameObjects().ToList().ForEach(g => results.AddRange(g.GetComponentsInChildren<T>(includeInactive)));
        }
        return results;
    }

    public static GameObject[] GetActiveSceneRootObject()
    {
        return UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
    }

    public static T FindFirstInActiveScene<T>(System.Func<GameObject, T> filterFromRoot) where T : class
    {
        var gos = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();

        for (int i = 0; i < gos.Length; i++)
        {
            var ret = filterFromRoot(gos[i]);
            if (ret != null) return ret;
        }
        return null;
    }

    public static List<T> FindAllInActiveScene<T>(System.Func<GameObject, List<T>> filterFromRoot) where T : class
    {
        var gos = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        var ret = new List<T>();
        for (int i = 0; i < gos.Length; i++)
        {
            var list = filterFromRoot(gos[i]);
            if (list != null && list.Count != 0) ret.AddRange(list);
        }
        return ret;
    }
    public static T FindObjectOfType<T>(bool includeInactive = true) where T : class
    {
        List<T> results = FindObjectsOfType<T>(includeInactive);

        return results.Count > 0 ? results[0] : null;
    }

    public static string FullName(this MonoBehaviour mono, char seperator = '.')
    {
        var ret = "";
        Transform t = mono.transform;
        while (t != null)
        {
            ret = t.name + (string.IsNullOrEmpty(ret)? "" : (seperator + ret));
            t = t.parent;
        }
        return ret;
    }

    public static string FullName(this Transform t, char seperator = '.')
    {
        var ret = "";
        while (t != null)
        {
            ret = t.name + (string.IsNullOrEmpty(ret) ? "" : (seperator + ret));
            t = t.parent;
        }
        return ret;
    }

    public static bool IsInBounds(this Collider[] cldrs, Vector3 point)
    {
        for (int i = 0; i < cldrs.Length; i++)
        {
            //if((cldrs[i].ClosestPoint(point) - point).sqrMagnitude <= 0.01f) //Point in bounds
            if(cldrs[i].bounds.Contains(point))
            {
                return true;
            }
        }
        return false;
    }

    //public static bool IsInCollider(this Collider[] cldrs, Vector3 point)
    //{
    //    for (int i = 0; i < cldrs.Length; i++)
    //    {
    //        if((cldrs[i].ClosestPoint(point) - point).sqrMagnitude <= 0.01f) //Point in bounds
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}
}
