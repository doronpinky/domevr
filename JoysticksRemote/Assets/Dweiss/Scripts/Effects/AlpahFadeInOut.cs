using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dweiss
{

    public class AlpahFadeInOut : MonoBehaviour
    {

        public float fadeTime = 1f;
        public float startAlpha = 0, endAlpha = 1;
        public EventEmpty onBlackScreen;

        private Coroutine fader;
        private Image rndr;

        private void Awake()
        {
            if (rndr == null) rndr = GetComponent<Image>();
        }

        private void OnEnable()
        {
            fader = null;
            StopAllCoroutines();

            var clr = GetColor();
            clr.a = startAlpha;
            SetColor(clr);
        }

        private Color GetColor()
        {
            return rndr.color;
        }

        private void SetColor(Color clr)
        {
            rndr.color = (clr);
        }


        private bool ActiveSelf { get { return enabled && gameObject.activeSelf; } }

        [ContextMenu("Fade")]
        public void StartFade()
        {
            if (ActiveSelf == false)
            {
                Debug.LogWarning("Not active cant fade");
                return;
            }

            if (fader == null)
                fader = StartCoroutine(Fade());
        }



        public void StartFade(System.Action onFadeBlack)
        {
            if (ActiveSelf == false)
            {
                Debug.LogWarning("Not active cant fade");
                return;
            }

            if (fader == null)
                fader = StartCoroutine(Fade(onFadeBlack));
        }
        private IEnumerator Fade(System.Action onFadeBlack = null)
        {
            yield return FadePhase(fadeTime / 2, startAlpha, endAlpha, ()=> ReachBlackScreen(onFadeBlack));
            yield return FadePhase(fadeTime / 2, endAlpha, startAlpha);
            fader = null;
        }


        private void ReachBlackScreen(System.Action onFadeBlack = null)
        {
            if (onFadeBlack != null) onFadeBlack.Invoke();
            onBlackScreen.Invoke();
        }

        public void FadeIn()
        {
            if (ActiveSelf == false)
            {
                Debug.LogWarning("Not active cant fade");
                return;
            }

            StopAllCoroutines();
            fader = StartCoroutine(FadePhase(fadeTime / 2, startAlpha, endAlpha, 
                () => { ReachBlackScreen(null); fader = null; }));
        }
        public void FadeOut()
        {
            if (ActiveSelf == false)
            {
                Debug.LogWarning("Not active cant fade");
                return;
            }

            StopAllCoroutines();
            fader = StartCoroutine(FadePhase(fadeTime / 2, endAlpha, startAlpha, ()=> { fader = null; }));
        }

        private IEnumerator FadePhase(float length, float startAlpha, float endAlpha, System.Action action = null)
        {
            var startTime = Time.time;
            var endTime = Time.time + length;
            var clr = GetColor();

            while (Time.time < endTime)
            {
                var percent = (Time.time - startTime) / length;
                clr.a = Mathf.Lerp(startAlpha, endAlpha, percent);// * (endAlpha - startAlpha) + startAlpha;
                SetColor(clr);
                yield return 0;
            }

            clr.a = endAlpha;
            SetColor(clr);
            if (action != null) action();
        }
    }
}
