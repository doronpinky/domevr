﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveFor : MonoBehaviour {
    public float activeTime;
	
    private void OnEnable()
    {
        this.Invoke("Disable", activeTime);
    }
    private void Disable()
    {
        gameObject.SetActive( false);
    }
}
