﻿using UnityEngine;


namespace Dweiss
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool m_ShuttingDown = false;

		public bool dontDestoryOnLoad;

        private static T _instance;
        public static T S { get { return Instance; } }

        public static T S_AutoInit { get {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                        "' already destroyed. Returning null.");
                    return null;
                }
                var ret = Instance;
                if (ret == null)
                {
                    var go = new GameObject(typeof(T).Name, typeof(T));
                    ret= Instance = go.GetComponent<T>();
                }
                return ret;
            } }
        //Assumes access via main thread only
        public static T Instance { get {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                        "' already destroyed. Returning null.");
                    return null;
                }

                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<T>();
                }
                return _instance;
            } set {
                _instance = value;
                //Debug.Log("Singleton " + typeof(T).Name + " set as " + (_instance == null?"null":value.transform.FullName()));
            }
        }

        protected virtual void Awake()
        {
            
            //Debug.Log("Singeltong " + transform.FullName() + " with instance " + (_instance == null ?"NULL" : _instance.transform.FullName()));
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                throw new System.NotSupportedException("Multiple singelton instances " + this + ", " + _instance);
            }
            else
            {
               
                Instance = this as T;

                if (dontDestoryOnLoad)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
        }

        private void OnApplicationQuit()
        {
            m_ShuttingDown = true;
        }

        protected virtual void OnDestroy()
        {
            if(_instance == this)
                Instance = null;
        }
    }
    
}