using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Dweiss
{

    public class DestroyOnRealBuild : MonoBehaviour
    {
        public RuntimePlatform[] platformsToDestroyOn = new RuntimePlatform[] { RuntimePlatform.WindowsEditor };
        public bool removeInEditorAsWell;

        void Awake()
        {

            if (enabled == false) return;
            if (platformsToDestroyOn.ToList().Contains(Application.platform)
                ||
                (removeInEditorAsWell && platformsToDestroyOn.Contains(EditorPlatform))
                )
            {
                Debug.Log("DestroyOnRealBuild " + name + " in platform " + Application.platform);
                Destroy(gameObject);
            }
        }


        private void Start() { }


        public static RuntimePlatform EditorPlatform
        {
            get
            {

#if UNITY_EDITOR
#if UNITY_ANDROID
                return RuntimePlatform.Android;
#elif UNITY_IOS
                return RuntimePlatform.IPhonePlayer;
#elif UNITY_STANDALONE_OSX
                return RuntimePlatform.OSXPlayer;
#elif UNITY_STANDALONE_WIN
                return RuntimePlatform.WindowsPlayer;
#endif
#else
                return RuntimePlatform.WindowsEditor; // Will do nothing in real build
#endif
            }
        }

    }
}
