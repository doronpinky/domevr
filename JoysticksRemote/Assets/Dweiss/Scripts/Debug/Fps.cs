﻿using UnityEngine;
using System.Collections;

namespace Dweiss
{
    //[RequireComponent(typeof(TextMesh))]
    public class Fps : MonoBehaviour
    {
        public bool hideInBuild;

        public bool onGuiDraw;
        public bool debugLog;

        public TextMesh textMesh;
        public UnityEngine.UI.Text textUi;

        private void SetText(float fps)
        {
            var txt = fps.ToSingleDotStr() + "FPS"; ;
            if (textMesh) textMesh.text = txt;
            if (textUi) textUi.text = txt;
        }

        float deltaTime = 0.0f;


        private void Start()
        {
#if UNITY_EDITOR == false
            if (hideInBuild)
            {
                Destroy(gameObject);
            }
#endif
            deltaTime = Time.deltaTime;
        }


        void Update()
        {
            deltaTime += (Time.deltaTime - deltaTime) * 0.2f;

            if(Random.value < .2f) SetText(FpsCount);

            if (debugLog) Debug.Log("FPS: " + FpsCount + " dlta" + Time.deltaTime + " t " + Time.time);
        }


        //private void Reset()
        //{
        //    tMesh = GetComponent<TextMesh>();
        //}

        public int FpsCount
        {
            get { return (int)(1/deltaTime); }
        }



        void OnGUI()
        {
            if (onGuiDraw == false) return;

            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 50;
            style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
            //float fps = Time.time / deltaTimeCount;
            float msec = deltaTime * 1000.0f;

            Rect rect = new Rect(w * guiOnScreenPosFactor.x, h * guiOnScreenPosFactor.y, w, h);
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, FpsCount);
            GUI.Label(rect, text, style);
        }

        public Vector2 guiOnScreenPosFactor = new Vector2(.5f, .03f);
    }
}