﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Dweiss.Msg
{
    [CustomEditor(typeof(OnMsgEnum))]
    public class OnMsgSpecialEnumInspector : Editor
    {
        private static MsgEnum GetMsgEnum()
        {
            var obj = GameObject.FindObjectOfType<MsgEnum>();
            if (obj == null)
            {
                var go = new GameObject("MessgeEnumOptions");
                obj = go.AddComponent<MsgEnum>();
            }
            return obj;
        }
        public static List<string> GetOptionList()
        {
            return GetMsgEnum().MsgOptions;
        }

        public static void SelectOptionList()
        {
            GetOptionList();
            var obj = GameObject.FindObjectOfType<MsgEnum>();
            UnityEditor.Selection.activeGameObject = obj.gameObject;
        }


        private bool showPosition = false;
        public override void OnInspectorGUI()
        {
            var script = ((OnMsgEnum)target);

            var serializedObject = new SerializedObject(target);

            if (script.msgId == null) script.msgId = new List<int>();
            EditorGUILayout.BeginHorizontal();
            // var obj = GameObject.FindObjectOfType<MsgEnum>();
            showPosition = EditorGUILayout.Foldout(showPosition,

                (script.msgId == null) ? " no msg" :
                (
                "Msg Id (#" + script.msgId.Count + " - " +
                (script.msgId.Count > 0 ? MsgEnum.GetNameById(script.msgId[0]) : "") + " ...) "));

            EditorGUILayout.EndHorizontal();

            if (showPosition)
            {
                EditorGUI.indentLevel++;
                //DrawField(serializedObject, script, "msgId");
                var allOptions = GetOptionList();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Size");
                var size = EditorGUILayout.IntField(script.msgId.Count);
                EditorGUILayout.EndHorizontal();

                while (size > script.msgId.Count)
                {
                    script.msgId.Add(script.msgId.Count > 0 ? script.msgId[script.msgId.Count - 1] : 0);
                }
                while (size < script.msgId.Count)
                {
                    script.msgId.RemoveAt(script.msgId.Count - 1);
                }
                for (int i = 0; i < script.msgId.Count; ++i)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("#" + i);
                    script.msgId[i] = EditorGUILayout.Popup(script.msgId[i], allOptions.ToArray());

                    if (GUILayout.Button("+", GUILayout.MaxWidth(24)))
                    {
                        SelectOptionList();
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel--;
            }


            DrawField(serializedObject, script, "delay");
            DrawField(serializedObject, script, "runOnDisable");
            DrawField(serializedObject, script, "objectAttachedType");

            switch (script.objectAttachedType)
            {
                case OnMsgGeneric<int>.EventType.Bool:
                    ClearOtherListeners(script, script.onEventBool);
                    DrawField(serializedObject, script, "onEventBool"); break;
                case OnMsgGeneric<int>.EventType.Int:
                    ClearOtherListeners(script, script.onEventInt);
                    DrawField(serializedObject, script, "onEventInt"); break;
                case OnMsgGeneric<int>.EventType.Float:
                    ClearOtherListeners(script, script.onEventFloat);
                    DrawField(serializedObject, script, "onEventFloat"); break;
                case OnMsgGeneric<int>.EventType.String:
                    ClearOtherListeners(script, script.onEventString);
                    DrawField(serializedObject, script, "onEventString"); break;
                case OnMsgGeneric<int>.EventType.None:
                    ClearOtherListeners(script, script.onEventVoid);
                    DrawField(serializedObject, script, "onEventVoid"); break;
                case OnMsgGeneric<int>.EventType.Object:
                    ClearOtherListeners(script, script.onEventObject);
                    DrawField(serializedObject, script, "onEventObject"); break;
                case OnMsgGeneric<int>.EventType.UnityObject:
                    ClearOtherListeners(script, script.onEventUnityObject);
                    DrawField(serializedObject, script, "onEventUnityObject"); break;
                case OnMsgGeneric<int>.EventType.Component:
                    ClearOtherListeners(script, script.onEventComponent);
                    DrawField(serializedObject, script, "onEventComponent"); break;
                case OnMsgGeneric<int>.EventType.GameObject:
                    ClearOtherListeners(script, script.onEventGameObject);
                    DrawField(serializedObject, script, "onEventGameObject"); break;

                default: Debug.LogError(name + " Invoke Not supported " + script.objectAttachedType); break;
            }
            UnityEditor.EditorUtility.SetDirty(script);

        }

        private void ClearOtherListeners(OnMsgEnum script, UnityEngine.Events.UnityEventBase ev)
        {
            RemoveListenersIfNeeded(ev, script.onEventVoid);
            RemoveListenersIfNeeded(ev, script.onEventObject);
            RemoveListenersIfNeeded(ev, script.onEventUnityObject);
            RemoveListenersIfNeeded(ev, script.onEventComponent);
            RemoveListenersIfNeeded(ev, script.onEventGameObject);
            RemoveListenersIfNeeded(ev, script.onEventBool);
            RemoveListenersIfNeeded(ev, script.onEventInt);
            RemoveListenersIfNeeded(ev, script.onEventFloat);
            RemoveListenersIfNeeded(ev, script.onEventString);
        }

        private void RemoveListenersIfNeeded(UnityEngine.Events.UnityEventBase ev, UnityEngine.Events.UnityEventBase toRemove)
        {
            //TODO remove all other listeners
            //if (ev != toRemove && toRemove.GetPersistentEventCount() > 0)
            //{
            //    Debug.Log("Clear listeners (ev != toRemove " + (ev != toRemove));
            //    toRemove.RemoveAllListeners();
                
            //}
        }

        private void DrawField<T>(SerializedObject serializedObject, OnMsgGeneric<T> script, string name)
        {
            var property = serializedObject.FindProperty(name);
            serializedObject.Update();
            EditorGUILayout.PropertyField(property, true);
            serializedObject.ApplyModifiedProperties();

            //TODO fix state saving 

            //var js JsonUtility.ToJson(property);

            //UnityEditor.EditorUtility.SetDirty(script); //TODO bug in inspector?
        }

    }
    
}