﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss.Msg
{
    public class OnMsgGenericVoid<T> : MonoBehaviour
    {

        public List<T> msgId;
        public float delay = -1;
        public bool runOnDisable = false;


        public EventEmpty onEventVoid;

        private void Start()
        {
            if (runOnDisable) Register(true);
        }
        private void OnDestroy()
        {
            if (runOnDisable) Register(false);
        }

        private void OnEnable()
        {
            if (runOnDisable == false) Register(true);
        }
        private void OnDisable()
        {
            if (runOnDisable == false) Register(false);
        }

        private void Register(bool on)
        {
            if (on) foreach (var m in msgId) Register(m);
            else if (MsgSystem.S) foreach (var m in msgId) Unregister(m);
        }

        private void Register(T msg)
        {
            MsgSystem.Get<T>().Register(msg, (System.Action)Action);
        }
        private void Unregister(T msg)
        {
            MsgSystem.Get<T>().Unregister(msg, (System.Action)Action);
        }

        private void Action()
        {
            if (delay < 0)
            {
                onEventVoid.Invoke();
            }
            else
            {
                this.WaitForSeconds(delay, () => onEventVoid.Invoke());
            }
        }

        private void Invoke()
        {
            onEventVoid.Invoke();
        }

        //private void Action(bool v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(int v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(float v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(string v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(System.Object v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(UnityEngine.Object v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(Component v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
        //private void Action(GameObject v) { if (delay < 0) Invoke(v); else this.WaitForSeconds(delay, () => Invoke(v)); }
    }
}