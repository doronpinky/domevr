﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss.Msg;

public class DemoListenToEvent : MonoBehaviour {

    private void OnEnable()
    {
        MsgSystem.MsgInt.Register(1, MsgRegisterFromScriptStr);
        MsgSystem.MsgInt.Register(1, MsgRegisterFromScriptComp);
        MsgSystem.MsgInt.Register<object>(1, MsgRegisterFromScriptObj);
    }

    private void OnDisable()
    {
        if (MsgSystem.S)
        {
            MsgSystem.MsgInt.Unregister(MsgEnum.GetId("MyEnum1"), MsgRegisterFromScriptStr);
            MsgSystem.MsgInt.Unregister(MsgEnum.GetId("MyEnum1"), MsgRegisterFromScriptComp);
            MsgSystem.MsgInt.Unregister<object>(MsgEnum.GetId("MyEnum1"), MsgRegisterFromScriptObj);
        }
    }


    private void MsgRegisterFromScriptStr( string str)
    {
        Debug.Log(name + " MsgRegisterFromScriptStr " + str);
    }

    private void MsgRegisterFromScriptComp(Component comp)
    {
        Debug.Log(name + " MsgRegisterFromScriptComp " + comp.GetType() + " GO: " + comp.name);
    }

    private void MsgRegisterFromScriptObj(object obj)
    {
        Debug.Log(name + " MsgRegisterFromScript " + obj.GetType());
    }

    public void OnMsgEnumCall()
    {
        Debug.Log(name + " OnMsgEnumCall");
    }
}
