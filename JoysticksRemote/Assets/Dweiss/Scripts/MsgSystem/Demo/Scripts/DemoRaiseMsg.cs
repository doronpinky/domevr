﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using Dweiss.Msg;

public class DemoRaiseMsg : MonoBehaviour {

    public bool raiseMessageFromScript;
    public bool callFunc;
    public EventEmpty funcEvent;

    [System.Serializable]
    public class ObjToSend { }

	void Update () {
        if (callFunc)
        {
            funcEvent.Invoke();
            callFunc = false;
        }
        if (raiseMessageFromScript)
        {
            MsgSystem.MsgInt.Raise(1, "string example");// Will also raise simple event
            MsgSystem.MsgInt.Raise(1, this);// Will also raise simple event
            MsgSystem.MsgInt.Raise<object>(1, new ObjToSend());// Will also raise simple event
            raiseMessageFromScript = false;
        }	
	}
}
