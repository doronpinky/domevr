﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class LookAtCamera : MonoBehaviour
{
    private Transform cam, t;
    private void OnEnable()
    {
        cam = Camera.main.transform;
        t = transform;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        var dir = transform.position - Camera.main.transform.position;
        Gizmos.DrawRay(Camera.main.transform.position, dir * 300);
    }

    void Update()
    {
#if UNITY_EDITOR
        if(t) t.LookAt(cam);
#else
        t.LookAt(cam);    
#endif
    }
}
