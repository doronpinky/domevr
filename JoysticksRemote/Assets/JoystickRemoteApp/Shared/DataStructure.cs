﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR.JoystickCom
{
    public class DataStructure
    {
        private const int SizeOfIdInBytes = 1, SizeOfEularInBytes = 3 * sizeof(float), SizeOfPosInBytes = 3 * sizeof(float);

        public byte id;
        public Vector3 localEular;
        public Vector3 localPos;

        private readonly float[] VectorFloat = new float[3];
        private readonly byte[] Data = new byte[SizeOfIdInBytes + SizeOfEularInBytes + SizeOfPosInBytes];


        public void SetData(int id, Transform t)
        {
            this.id = (byte)id;
            localEular = t.localEulerAngles;
            localPos = t.localPosition;
        }
        public void SetTransform(Transform t)
        {
            t.localEulerAngles = localEular;
            t.localPosition = localPos;
        }

        
        private float[] FillVectorFloat(Vector3 v)
        {
            VectorFloat[0] = v.x;
            VectorFloat[1] = v.y;
            VectorFloat[2] = v.z;
            return VectorFloat;
        }

        private Vector3 ReadArrToVector3(byte[] arr, int index)
        {
            System.BitConverter.ToSingle(arr, index);
            var ret = Vector3.zero;
            ret.x = BitConverter.ToSingle(arr, index); ;
            ret.y = BitConverter.ToSingle(arr, index + sizeof(float)); ;
            ret.z = BitConverter.ToSingle(arr, index + sizeof(float)*2); ;
            return ret;
        }
        private Vector3 ReadArrToVector3(float[] arr, int index)
        {
            var ret = Vector3.zero;
            ret.x = arr[index + 0];
            ret.y = arr[index + 1];
            ret.z = arr[index + 2];
            return ret;
        }

        public byte[] ToBytes()
        {
            Data[0] = id;

            Buffer.BlockCopy(FillVectorFloat(localEular), 0, Data, SizeOfIdInBytes, SizeOfEularInBytes);
            Buffer.BlockCopy(FillVectorFloat(localPos), 0, Data, SizeOfIdInBytes + SizeOfEularInBytes, SizeOfPosInBytes);

            return Data;
        }

        public void FromBytes(byte[] bytes)
        {
            id = bytes[0];
            localEular = ReadArrToVector3(bytes, SizeOfIdInBytes);
            localPos = ReadArrToVector3(bytes, SizeOfIdInBytes + SizeOfEularInBytes);
        }
    }
}