﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR.JoystickCom {
    public class SendTracker : MonoBehaviour
    {
        public static int id;

        public UDPWrapper udp;
        private Transform t;

        private int myId;
        private DataStructure myData = new DataStructure();

        public float timeBetweenSend;
        void Start()
        {
            t = transform;
            myId = id++;

        }


        private void OnEnable()
        {
            StartCoroutine(RepeatSend());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        IEnumerator RepeatSend()
        {
            while (true)
            {
                yield return new WaitForSeconds(timeBetweenSend);
                try
                {
                    myData.SetData(id, t);
                    udp.Send(myData.ToBytes());
                }
                catch (System.Exception e) { Debug.LogError(id + " Error " + e); }
            }
        }
    }
}