﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR.JoystickCom
{
    public class RecieveTracker : MonoBehaviour
    {
        public static int id;

        public UDPWrapper udp;
        private DataStructure formatedData = new DataStructure();

        public Transform[] trackers;
        void Start()
        {
            udp.onMainThreadData += OnDataRecieve;
        }
        private void OnDataRecieve(byte[] data)
        {
            formatedData.FromBytes(data);
            formatedData.SetTransform(trackers[formatedData.id]);
        }
    }
}