Question	Correct	Wrong	Wrong	Wrong	VoiceOverQ	QuestionMovie	InfoTextAfter	InfoAudioAfter	GroupID
What is the estimated rise in temperature by the year 2100?	18 to 40 °C	0 to 50 °C	1 to 18 °C	55 to 75 °C					
Which of the following are consequences associated with climate change?	All answers are correct	Global sea level rising	Extreme weather	Melting ice sheets					
How many centimetres are global sea levels expected to rise at least by 2100?	30 cm	10 cm	5 cm	50 cm					
What was the warmest year on record so far? 	2016	2000	1958	1983					
What was agreed to in the Paris Climate Agreement regarding global temperature rise?	Keep below 20 °C	Keep below 30 °C	Keep below  40 °C	Keep below 50 °C					
How many people were displaced due to environmental disasters in 2016?	24 million	10 million	7 million	40 million					
How many football fields worth of forest do we lose every minute?	27	1	5	59					
Which climate change related weather event causes the increased spread of mosquitoes?	All answers are correct	Temperature rise	Windstorms	Floods					
Which combination of technologies has the ability to analyse the state of tree health?  	Drones & Artificial Intelligence	 Augmented Reality & Robotics	Blockchain & Virtual Reality	 3D Printing & Virtual Reality					
How can the Internet of Things (IoT) help combat climate change?	All answers are correct	Decarbonise our energy system	Manage our infrastructure	 Predictive analytics					
What technology can help to create 3D climate risk maps?	Drones	Internet of Things 	Artificial Intelligence 	3D printing					
How can you adapt your diet to reduce its climate impact?	All answers are correct	Donâ€™t waste food	Eat meat-free meals	Eat regional food					
Which technologies can be used for data collection to design climate change strategies?	All answers are correct	Satellite remote sensors	Drones gathering data	IoT environmental sensors					
How many people could the renewable energy sector employ by 2030?	20 million	40 million	10 million	3 million					
Climate change impacts coastal zones. Which example is an effect of climate change?	All answers are correct	More frequent flooding	Coastal erosion	Intrusion of salt into groundwater					
What is MethaneSAT?	A satellite to identify methane emissions	A non-profit environmental group	A satellite to absorb methane	A TV station dedicated to climate change					
