﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class TextureFromCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject imageFrame;
    [SerializeField]
    private GameObject rawImagePhoto;
    [SerializeField]
    private float ratio = 0.25f;
    
    public RawImage sourceOfCamera;

    private Texture2D texture;
    private WebCamTexture mCamera;

    private void Start()
    {
        mCamera = new WebCamTexture();
        sourceOfCamera.texture = mCamera;
        mCamera.Play();
    }

    public void TakePicture()
    {
        rawImagePhoto.SetActive(false);
        StartCoroutine(CaptureScreen());
        rawImagePhoto.SetActive(true);
    }

    public void SaveImage()
    {
        byte[] bytes = texture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/../Image/SavedScreen.png", bytes);
    }

    IEnumerator CaptureScreen()
    {
        RectTransform frameTransform = imageFrame.GetComponent<RectTransform>();
        Rect framing = frameTransform.rect;
        Vector2 pivot = frameTransform.pivot;
        Vector2 origin = frameTransform.anchorMin;
        origin.x *= Screen.width;
        origin.y *= Screen.height;
        float xOffset = pivot.x * framing.width;
        origin.x += xOffset;
        float yOffset = pivot.y * framing.height;
        origin.y += yOffset;
        framing.x += origin.x;
        framing.y += origin.y;
        texture = new Texture2D((int)framing.width, (int)framing.height);

        yield return new WaitForEndOfFrame();
        texture.ReadPixels(framing, 0, 0);
        texture.Apply();
        Vector3 photoScale = new Vector3(framing.width * ratio, framing.height * ratio, 1);
        rawImagePhoto.GetComponent<RectTransform>().localScale = photoScale;
        rawImagePhoto.GetComponent<RawImage>().texture = texture;
    }
}