﻿using System;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace GetValueFromArduino
{
    public class ArduinoValue
    {
        public int serialPort;
        public string SerialCOM;
        public delegate void DataReceived(string data);
        public static event DataReceived onDataReceived;
        

        private static SerialPort mySerialPort;
        private string State;
        private bool _START = false;
        public Thread thread;
        private bool applicationQuit = false;

        public ArduinoValue(int port, string com)
        {
            this.serialPort = port;
            this.SerialCOM = com;
            //Connect();
            //thread = new Thread(new ThreadStart(UpdateController));
            //thread.Start();
        }

        public void UpdateController()
        {
            while (!applicationQuit)
            {
                if (_START == true)
                {
                    try
                    {
                        string data = mySerialPort.ReadLine();
                        if (onDataReceived != null)
                        {
                            onDataReceived(data);

                        }
                        //Debug.Log(data);
                    }
                    catch (Exception e)
                    {
                        State = "Data Received Error: ";
                    }
                }
                Thread.Sleep(5);
            }
        }

        public void Connect()
        {
            try
            {
                mySerialPort = new SerialPort(SerialCOM, serialPort); //9600 115200
                mySerialPort.Parity = Parity.None;
                mySerialPort.StopBits = StopBits.One;
                mySerialPort.DataBits = 8;
                mySerialPort.Handshake = Handshake.None;
                mySerialPort.ReadBufferSize = 18;
                mySerialPort.WriteBufferSize = 1;
                mySerialPort.ReadTimeout = 100;
                mySerialPort.WriteTimeout = 100;
                mySerialPort.Open();
                State = "Door open successfully.";
                if (onDataReceived != null)
                {
                    onDataReceived(State);
                }
                    
                //Debug.Log(State);
                Thread.Sleep(300);
                mySerialPort.DiscardInBuffer();
                _START = true;
            }
            catch (Exception e)
            {
                State = "Error opening port: ";
                //Debug.Log(State + e.ToString());
            }
            //Debug.Log(mySerialPort);
        }

        void OnApplicationQuit()
        {
            applicationQuit = true;
            if (thread.IsAlive) thread.Abort();
            if (mySerialPort.IsOpen)
            {
                mySerialPort.Close();
            }
        }
    }
}
