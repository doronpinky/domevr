
#define MAX_OUT_CHARS 48
char   buffer[MAX_OUT_CHARS + 1];

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(3,INPUT);
  pinMode(6,INPUT);
}

void loop() {
  
  sprintf(buffer,"#3 - %d | #6 - %d", digitalRead(3),  digitalRead(6));   
  Serial.println(buffer);
  
}
