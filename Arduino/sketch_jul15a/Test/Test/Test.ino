
const int length = 2;
int buttons[length] = {A0, A2};


#define MAX_OUT_CHARS 24
char   buffer[MAX_OUT_CHARS + 1];

void setup() {
  Serial.begin(9600);

pinMode(A0,INPUT);
pinMode(A2,INPUT);
  
//   for(int i=0; i < length; ++i){
//    pinMode(buttons[i],INPUT);
//  }
}

void ReadState(int id){
    int digital = digitalRead(id);
    int sensorValue = analogRead(id);
    float voltage= ((float)sensorValue) * (5.0 / 1023.0);
    String value = String(voltage);
    
    sprintf(buffer ,"#%d:%d|%d|%s > ", id, digital, sensorValue, value.c_str());  
    Serial.print(buffer);
}

void loop() {
  

ReadState(A0);
ReadState(A2);
 
//  for(int i=0; i < length; ++i){
//    ReadState(buttons[i]);
//  }
    
    Serial.println();
  delay(222);
}
