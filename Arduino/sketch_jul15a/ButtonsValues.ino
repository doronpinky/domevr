const int buttonPin1 = 2;
const int buttonPin2 = 4;
const int buttonPin3 = 7;
const int buttonPin4 = 8;
int buttonState = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;
bool canPrint = true;
bool canPrint2 = true;
bool canPrint3 = true;
bool canPrint4 = true;

void setup() {
  // put your setup code here, to run once:
 
    Serial.begin(9600);
    pinMode(buttonPin1, INPUT);
    pinMode(buttonPin2, INPUT);
    pinMode(buttonPin3, INPUT);
    pinMode(buttonPin4, INPUT);
}

void loop() {
 
  buttonState = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  buttonState3 = digitalRead(buttonPin3);
  buttonState4 = digitalRead(buttonPin4);
  
   if (buttonState == HIGH ) {
    if(canPrint){
       Serial.println(1);
       canPrint = false;
    }           
  } else{
    canPrint = true;
  }

  if (buttonState2 == HIGH ) {
    if(canPrint2){
       Serial.println(2);
       canPrint2 = false;
    }           
  } else{
    canPrint2 = true;
  }

  if (buttonState3 == HIGH ) {
    if(canPrint3){
       Serial.println(3);
       canPrint3 = false;
    }           
  } else{
    canPrint3 = true;
  } 

  if (buttonState4 == HIGH ) {
    if(canPrint4){
       Serial.println(4);
       canPrint4 = false;
    }           
  } else{
    canPrint4 = true;
  }
  
}
