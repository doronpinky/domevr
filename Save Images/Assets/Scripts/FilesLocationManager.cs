﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class FilesLocationManager : MonoBehaviour
{

    public StrEvent errorOnMainPath;
    [System.Serializable]
    public class StrEvent : UnityEngine.Events.UnityEvent<string> { }

    [System.Serializable]
    public class UnityIntEvent : UnityEngine.Events.UnityEvent<string> { }

    public const int LIMIT_OF_PICTURES = 10;
    public UnityAction IndexOfFolderChanged;
    public List<Texture2D> teamPicture = new List<Texture2D>();

    public UnityIntEvent onDirectoryIdChanged;

    public int daysToSaveImageFolder = 2;
    
    private bool endOfFolders;
    public string mainFolderPath = @"C:\PhotoBooth\";
    public string secondaryFolderPath = @"C:\PhotoBooth\";
    [SerializeField]
    private TextureFromCamera textureFromCamera;
    
    public int IndexOfFolder { get; private set; }
    public int NumberOfPic { get; private set; }

    void Start()
    {
        //var lastIndex = Directory.GetDirectories(PATH).Select(a => SimpleParseToInt(a)).Max();
        //Debug.Log("last index: " + lastIndex);
        //Directory.CreateDirectory(PATH + (lastIndex + 1).ToString());
        //PathToCurrentFolder = PATH + (lastIndex + 1).ToString();

        IndexOfFolder = 0;
        NumberOfPic = 0;
        textureFromCamera.TextureSaved += AddImage;

        TryCatch((folder) => {
            if (Directory.Exists(folder) == false)
            {
                Directory.CreateDirectory(folder);
            }
        });
        FindNameOfNewFolder();
    }
    
    private void TryCatch(System.Action<string> a)
    {
        try
        {
            a(mainFolderPath );
        }
        catch (System.Exception e) {
            errorOnMainPath.Invoke("Error saving images - check network and shared folder");
            Debug.LogError("Errr " + mainFolderPath  + " - "+ e);
        }
        try
        {
            a(secondaryFolderPath);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Errr " + secondaryFolderPath + " - " + e);
        }
    }

    private void OnEnable()
    {
        StartCoroutine(CoroutineDeleteOldFolders());
    }

    private IEnumerator CoroutineDeleteOldFolders()
    {
        while (true)
        {
           
            try
            {
                DeleteOldFolders();
                
            }
            catch (System.Exception) { }
            yield return new WaitForSecondsRealtime(60 * 60 * 24 * daysToSaveImageFolder);
        }
    }

    private int SimpleParseToInt(string name) {

        var lastIndexOfDir = name.LastIndexOf("/");
        if (lastIndexOfDir == -1) lastIndexOfDir = name.LastIndexOf("\\");

        var suffix = name.Substring(lastIndexOfDir+1);//System.IO.Path.GetDirectoryName(name);
        int num = 0;
        int.TryParse(suffix, out num);
        return num;
    }

    private void DeleteOldFolders()
    {
        TryCatch((folder) =>
        {
            var folders = Directory.GetDirectories(folder);
            for (int i = 0; i < folders.Length; i++)
            {
                var fInfo = new DirectoryInfo(folders[i]);
                if (System.DateTime.Now - fInfo.CreationTime > System.TimeSpan.FromDays(daysToSaveImageFolder))
                {
                    Debug.Log("Delete folder " + folders[i]);
                    Directory.Delete(folders[i], true);
                }
            }
        }
        );
        
    }

    private void FindNameOfNewFolder()
    {

        TryCatch((folder) =>
        {
            var dirs = Directory.GetDirectories(folder);
            IndexOfFolder = 0;
            if (dirs.Length > 0)
                IndexOfFolder = dirs.Select(a => SimpleParseToInt(a)).Max() + 1;


            //if (Directory.Exists(mainFolderPath + "/" + IndexOfFolder.ToString()) == false)
            //    Directory.CreateDirectory(mainFolderPath + "/" + IndexOfFolder.ToString());

            try
            {

                IndexOfFolderChanged?.Invoke();
                onDirectoryIdChanged.Invoke(IndexOfFolder.ToString());
            }
            catch (System.Exception e) { Debug.LogError(e); }
        });
    }

    public void AddImage(Texture2D texture)
    {
        if(NumberOfPic < LIMIT_OF_PICTURES)
        {
            if (teamPicture.Count < NumberOfPic + 1)
            {
                teamPicture.Add(texture);
            }
            else
            {
                teamPicture[NumberOfPic] = texture;
            }
        }       
    }

    public void AddNumberOfPic()
    {
        NumberOfPic++;
    }

    public void Finish()
    {
        SaveImages();
        //IndexOfFolder++;
        NumberOfPic = 0;
        endOfFolders = false;
        FindNameOfNewFolder();
    }

    private void SaveImages()
    {
        if(teamPicture.Count > 0)
        {

            TryCatch((folder) =>
            {
                if (Directory.Exists(folder + "/" + IndexOfFolder.ToString()) == false)
                    Directory.CreateDirectory(folder + "/" + IndexOfFolder.ToString());

                for (int i = 0; i < teamPicture.Count; i++)
                {
                    byte[] bytes = teamPicture[i].EncodeToPNG();
                    File.WriteAllBytes(folder + IndexOfFolder.ToString() + "/" + (i + 1).ToString() + ".bin", bytes);
                }
            });
        }        
    }
}
