﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screen3Manager : ScreenManager
{
    [SerializeField]
    private Text expId;
    [SerializeField]
    private Text picNumber;
    [SerializeField]
    private RawImage picture;
    [SerializeField]
    private Button addPicButton;

    private void Start()
    {
        expId.text = filesLocationManager.IndexOfFolder.ToString();
    }

    private void OnEnable()
    {
        picNumber.text = (filesLocationManager.NumberOfPic + 1).ToString();
        if(filesLocationManager.teamPicture.Count > 0)
        {
            picture.texture = filesLocationManager.teamPicture[filesLocationManager.NumberOfPic];
            float ratio = (float)(picture.texture.height)/ picture.texture.width;
            picture.rectTransform.sizeDelta = new Vector2(picture.rectTransform.sizeDelta.x, picture.rectTransform.sizeDelta.x * ratio);
            //picture.rectTransform.localScale = new Vector3(picture.rectTransform.localScale.x, picture.rectTransform.localScale.x * ratio, picture.rectTransform.localScale.z);
        }

        if(filesLocationManager.NumberOfPic >= 10)
        {
            addPicButton.interactable = false;
        }
    }    
}
