﻿using UnityEngine;
using UnityEngine.UI;

public class Screen1Manager : ScreenManager
{
    [SerializeField]
    private Text expIdText;
    private int expId;

    private void Start()
    {
        filesLocationManager.IndexOfFolderChanged += SetExpId;
    }

    private void SetExpId()
    {
        expId = filesLocationManager.IndexOfFolder;
        expIdText.text = expId.ToString();
    }
}
