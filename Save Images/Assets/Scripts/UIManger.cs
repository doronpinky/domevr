﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManger : MonoBehaviour
{
    [SerializeField]
    private GameObject screen1;
    [SerializeField]
    private GameObject screen2;
    [SerializeField]
    private GameObject screen3;

    public float widthDivideByHeight = 2f/3f;
    private void Start()
    {
        Debug.Log(Screen.orientation);

        if (Screen.width < Screen.height)
        {
            Screen.SetResolution((int)(Screen.width), (int)((1f/widthDivideByHeight) * Screen.width), true);
            Debug.Log("set height " + ((1f / widthDivideByHeight) * Screen.width) + " ratio " + (1f / widthDivideByHeight)  +  " w: " + Screen.width);
        } else {
            Screen.SetResolution((int)(widthDivideByHeight*Screen.height), Screen.height, true);
            Debug.Log("set width " + ((widthDivideByHeight) * Screen.height) + " ratio " + ( widthDivideByHeight) + " h: " + Screen.height);
        }
    }
    public void SwithScreen(int screen)
    {
        switch (screen)
        {
            case 1:
                screen1.SetActive(true);
                screen2.SetActive(false);
                screen3.SetActive(false);
                break;
            case 2:
                screen1.SetActive(false);
                screen2.SetActive(true);
                screen3.SetActive(false);
                break;
            case 3:
                screen1.SetActive(false);
                screen2.SetActive(false);
                screen3.SetActive(true);
                break;
            default:
                Debug.LogError("The Parameter is Not Valid!");
                break;
        }
    }
}
