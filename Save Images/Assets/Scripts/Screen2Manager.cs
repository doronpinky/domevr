﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screen2Manager : ScreenManager
{
    [SerializeField]
    private TextureFromCamera textureFromCamera;
    [SerializeField]
    private Text expId;
    [SerializeField]
    private Text picNumber;
    [SerializeField]
    private GameObject rawImagePic;
    [SerializeField]
    private UIManger uIManger;


    private void Start()
    {
        expId.text = filesLocationManager.IndexOfFolder.ToString();

    }

    private void OnEnable()
    {
        picNumber.text = (filesLocationManager.NumberOfPic + 1).ToString();
       // textureFromCamera.enabled = true;
        rawImagePic.SetActive(false);
    }

    public void OpenScreen3()
    {
        //textureFromCamera.enabled = false;
        StartCoroutine(DelayBeforOpenNextScreen());
    }

    //public void DisplayRawImagePic()
    //{
    //    rawImagePic.SetActive(true);
    //}

    private IEnumerator DelayBeforOpenNextScreen()
    {
        yield return new WaitForSeconds(Time.deltaTime*2 + 0.1f);
        uIManger.SwithScreen(3);
    }
}
