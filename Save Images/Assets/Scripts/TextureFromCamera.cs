﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class TextureFromCamera : MonoBehaviour
{
    //[SerializeField]
    //private GameObject imageFrame;
    [SerializeField]
    private GameObject rawImagePhoto;
    [SerializeField]
    //private float ratio = 0.25f;



    public Rect areaOfCaptureInPixels;

    public RawImage sourceOfCamera;
    public UnityAction<Texture2D> TextureSaved;

    private Texture2D texture;
    private WebCamTexture mCamera;

    private void Awake()
    {
        TextureSaved += (txtr) => rawImagePhoto.GetComponent<RawImage>().texture = txtr;
        InitCamera();
    }
    //private void InitCamera2() { 
    //    mCamera = new WebCamTexture();
    //    sourceOfCamera.texture = mCamera;
    //    mCamera.Play();

        
    //}

    private void OnEnable()
    {
        mCamera.Play();
    }

    private void OnDisable()
    {
        mCamera.Stop();
    }

    public void TakePicture()
    {
        StartCoroutine(CaptureScreen());
    }

    IEnumerator CaptureScreen()
    {
        var width = areaOfCaptureInPixels.width * mCamera.width;
        var height = areaOfCaptureInPixels.height * mCamera.height;
        texture = new Texture2D((int)width, (int)height);
        yield return new WaitForEndOfFrame();

        ReadToTexture(mCamera, (int)(areaOfCaptureInPixels.xMin * mCamera.width),
            (int)(areaOfCaptureInPixels.xMax * mCamera.width),
            (int)(areaOfCaptureInPixels.yMin * mCamera.height),
            (int)(areaOfCaptureInPixels.yMax * mCamera.height),
            texture);
        TextureSaved?.Invoke(texture);
    }

    //IEnumerator CaptureScreenOld() { 
    //    RectTransform frameTransform = imageFrame.GetComponent<RectTransform>();
    //    Rect framing = frameTransform.rect;
    //    Vector2 pivot = frameTransform.pivot;
    //    Vector2 origin = frameTransform.anchorMin;
    //    origin.x *= Screen.width;
    //    origin.y *= Screen.height;
    //    float xOffset = pivot.x * framing.width;
    //    origin.x += xOffset;
    //    float yOffset = pivot.y * framing.height;
    //    origin.y += yOffset;
    //    framing.x += origin.x;
    //    framing.y += origin.y;
    //    texture = new Texture2D((int)framing.width, (int)framing.height);

    //    yield return new WaitForEndOfFrame();
    //    texture.ReadPixels(framing, 0, 0);
    //    texture.Apply();
    //    //Vector3 photoScale = new Vector3(framing.width * ratio, framing.height * ratio, 1);
    //    rawImagePhoto.GetComponent<RawImage>().texture = texture;
    //    TextureSaved?.Invoke(texture);
    //}




    void InitCamera()
    {
        mCamera = new WebCamTexture();
        sourceOfCamera.texture = mCamera;
        mCamera.Play();
        var rotAngle = mCamera.videoRotationAngle;
        var rect = ((RectTransform)rawImagePhoto.transform);
        rect.sizeDelta = new Vector2(mCamera.width, mCamera.height);

        rect.localEulerAngles = new Vector3(rect.localEulerAngles.x, rect.localEulerAngles.y, -rotAngle);

        float ratio = (float)(mCamera.height) / mCamera.width;
        sourceOfCamera.rectTransform.sizeDelta = new Vector2(sourceOfCamera.rectTransform.sizeDelta.x, sourceOfCamera.rectTransform.sizeDelta.x * ratio);
        //var widthScale = ((float)Screen.width)/webcamTexture.width;
        //var heightScale = ((float)Screen.height)/webcamTexture.height;
        //var minScale = Mathf.Min(widthScale, heightScale);
        //var scaleDiff = minScale / rect.lossyScale.x;
        //Debug.Log(minScale + " -> " + scaleDiff + " ( " + rect.lossyScale.x + " ) ");
        // if(minScale > 0 ) rect.localScale = rect.localScale * minScale;


        Debug.LogFormat("{0}, {1} {2}", mCamera.width, mCamera.height, mCamera.deviceName);
    }

    private static Color Transparent = new Color(0, 0, 0, 0);
    public static void ReadToTexture( WebCamTexture webcam, int minX, int maxX, int minY, int maxY,  /*Out*/ Texture2D txture)
    {
        //Color32[] data = new Color32[webcam.width * webcam.height];

      //  webcam.GetPixels32(data);
        txture.Resize(maxX - minX, maxY - minY);
        //int maxX = (int)center.x + squareRadius;
        //int minX = (int)center.x - squareRadius;
        //int maxY = (int)center.y + squareRadius;
        //int minY = (int)center.y - squareRadius;
        int x = 0, y = 0;
        int u = 0, v = 0;
        try
        {
            for (x = minX; x < maxX; ++x,++u)
            {
                for (y = minY; y < maxY; ++y,++v)
                {
                    if (x < 0 || y < 0 || x >= webcam.width || y >= webcam.height)
                    {
                        txture.SetPixel(u, v, Color.black);
                        //clrs[counter] = Transparent;
                        continue;
                    }
                    else
                    {
                        txture.SetPixel(u, v, webcam.GetPixel(x, y));
                    }
                }
            }
            txture.Apply();

        }

        catch (System.Exception)
        {
            Debug.LogFormat("Counter {0}:{7} P({1},{2}) X({3},{4}) Y({5},{6})", u, x, y, minX, maxX, minY, maxY, v);
            throw;
        }

    }
}