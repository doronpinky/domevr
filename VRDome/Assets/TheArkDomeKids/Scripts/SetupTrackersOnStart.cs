﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DomeVR
{
    public class SetupTrackersOnStart : Dweiss.Singleton<SetupTrackersOnStart>
    {
        public float waitBeforeActive = 8;
        private List<SingleJoystickControl> trackers = new List<SingleJoystickControl>();
        private List<SingleJoystickControl> trackersByOrderOfWorking = new List<SingleJoystickControl>();

        public GameObject setupFollowObject;

        IEnumerator Start()
        {
            yield return new WaitForSeconds(waitBeforeActive);
            Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", "Ready to setup joysticks");
            var joysticks = GameObject.FindObjectsOfType<SingleJoystickControl>();
            trackers = joysticks.Where(a => a.joystickNum == 2).ToList();
            trackers.Sort((a, b) => a.name.CompareTo(b.name));
        }

        private void Update()
        {
            for (int i = 0; i < trackers.Count; i++)
            {
                var isIdle = trackers[i].GetComponent<DisableScriptsWhenIdle>();
                if (isIdle.IsIdle == false)
                {
                    trackersByOrderOfWorking.Add(trackers[i]);
                    var newObj = Instantiate(setupFollowObject);
                    var tt = newObj.GetComponent<Common.TrackTransform>();
                    tt.other = isIdle.transform;
                    trackers.RemoveAt(i);
                    Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", string.Format("Tracker {0} Active - {1}", trackersByOrderOfWorking.Count, isIdle.name));
                    --i;

                    //Set color
                    var color = GarbageCan.GetColor(((trackersByOrderOfWorking.Count-1)%5)+1);
                    newObj.GetComponentsInChildren<MeshRenderer>().Foreach(a => a.material.color = color);
                }
            }
            for (int i = 0; i < toRegister.Count; i++)
            {
                if( toRegister[i].id < trackersByOrderOfWorking.Count)
                {
                    var tracker = trackersByOrderOfWorking[toRegister[i].id];
                    toRegister[i].f(tracker);
                    toRegister.RemoveAt(i);
                    --i;
                }
            }
        }

        [System.Serializable]
        public class OnRegisterTracker
        {
            public int id;
            public System.Action<SingleJoystickControl> f;
        }
        private List<OnRegisterTracker> toRegister = new List<OnRegisterTracker>();
        public void RegisterTrackerById(int id, System.Action<SingleJoystickControl> onJoystick)
        {
            toRegister.Add(new OnRegisterTracker() { id = id, f = onJoystick });
        }
    }
}