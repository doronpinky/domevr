﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR { 
public class SimpleGraph : MonoBehaviour
    {
        public GameObject blockPrefab;
        public Color blockColor;
        [SerializeField]private int numOfBlocks = 20;

        private List<Transform> blocks = new List<Transform>();
        private Transform pivot;
        void Awake()
        {
            pivot = transform;
        }

        private int activeBlocks;
        public int NumOfBlocks { get { return activeBlocks; } }

        private float activePercent;
        public void AddActiviteInPercentage(float addedPercentage)
        {
            //Debug.LogFormat("{0} + {1} = {2} -> blocks {3}", activePercent, addedPercentage, (activePercent + addedPercentage), (int)((activePercent + addedPercentage) * blocks.Count));
            activePercent += addedPercentage;
            
            for (; activeBlocks < Mathf.Round(activePercent* blocks.Count); activeBlocks++)
            {
                var allRndrs = blocks[activeBlocks].GetComponentsInChildren<Renderer>(true);
                foreach (var rndr in allRndrs)
                {
                    rndr.enabled = !rndr.enabled;
                    rndr.material.color = blockColor;
                }
                //Debug.LogFormat("Activate Block " + activeBlocks);
            }
        }

        //[ContextMenu("Activate Block")]
        public void ActivateBlocks(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var allRndrs = blocks[activeBlocks].GetComponentsInChildren<Renderer>(true);
                foreach (var rndr in allRndrs)
                {
                    rndr.enabled = !rndr.enabled;
                    rndr.material.color = blockColor;
                }
                activeBlocks++;
            }
            
        }
        public float spaceBetweenBlock = .03f;
        private void AddOneBlock() { 
            var newBlock = Instantiate(blockPrefab);
            newBlock.transform.parent = pivot;

            var rndr = newBlock.GetComponentInChildren<Renderer>();
            newBlock.transform.position = pivot.position +  pivot.rotation*(new Vector3((0.5f + blocks.Count) * rndr.bounds.size.x+ blocks.Count * spaceBetweenBlock, 0 , 0));
            newBlock.transform.localRotation = Quaternion.identity;
            //newBlock.transform.LookAt(Camera.main.transform);
            blocks.Add(newBlock.transform);
        }
        
        [ContextMenu("Reset blocks")]
        public void ResetBlocks()
        {
            foreach(var t in blocks)
            {
                Destroy(t.gameObject);
            }
            blocks.Clear();
            activeBlocks = 0;
            activePercent = 0;

            SetupBlocks(numOfBlocks);
        }

        private void Start()
        {
            SetupBlocks(numOfBlocks);
        }

        private void SetupBlocks(int count)
        {
            for (int i = 0; i < count; i++)
            {
                AddOneBlock();
            }
        }


    }
}