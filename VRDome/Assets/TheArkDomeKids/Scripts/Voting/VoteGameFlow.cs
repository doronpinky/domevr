﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace DomeVR {
    public class VoteGameFlow : MonoBehaviour
    {

        public string[] animationIdToAllowContinue = new string[] { "011A", "012A", "012B", "012C", "012D" };
        public ScriptableSceneSettings[] resetContinueButtonOnScenes;
        public string fadeOutAvatarEventName = "FadeOutAvatar";
        public LastClipConfig[] secondsFromStartToRunMedal;

        [System.Serializable]
        public class LastClipConfig
        {
            public string clipId;
            public float timeFromStartToFadeOtherStories;
            public float timeFromStartToRaiseMedal;
            public float timeFromStartToMoveAvatar;
            public float timeFromStartToFadeAvatar = 50;


        }

        public Dweiss.Lerp lerpAnimator;

        private const string OnStartAnimationEventName = "AiAvatarAnimationStart";
        private const string OnEndAnimationEventName = "AiAvatarAnimationEnd";
        private const string MedalEventName = "RaiseMedal";
        public const string FadeStoriesEventName = "FadeOutStories";
        //private const string AiAvatarAnimationDoneEventName = "AiAnimationFinished";
        private const string LevelChanged = "LevelChanged";

        // Start is called before the first frame update
        void Start()
        {
            OnLevelChanged(GameFlow.S.CurrentMovieScene.name);
            //GameFlow.S.MaxSecondsBeforeEndCanContinue = float.NegativeInfinity;
        }

        private void OnClipStart(string animationId)
        {
            var logic = secondsFromStartToRunMedal.FirstOrDefault(a => a.clipId == animationId);
            Debug.Log("Start clip identify " + animationId);
            if (logic != null)
            {
                this.ExactWaitForEvent(logic.timeFromStartToRaiseMedal, () => Dweiss.Msg.MsgSystem.Raise(MedalEventName));
                this.ExactWaitForEvent(logic.timeFromStartToMoveAvatar, () => lerpAnimator.enabled = true);
                this.ExactWaitForEvent(logic.timeFromStartToFadeOtherStories, () => Dweiss.Msg.MsgSystem.Raise(FadeStoriesEventName));
                this.ExactWaitForEvent(logic.timeFromStartToFadeAvatar, () => Dweiss.Msg.MsgSystem.Raise(fadeOutAvatarEventName));
                

            }
        }
        

        private void OnEndAnimationEvent(string animationId)
        {
            Debug.Log("End clip identify " + animationId);
            if(animationIdToAllowContinue.Contains(animationId))
            {
                GameFlow.S.MaxSecondsBeforeEndCanContinue = float.PositiveInfinity;//Alow continue
            }
        }
        private void OnLevelChanged(string lvlId)
        {
            //Debug.Log("________________________________OnLevelChanged " + lvlId);
            var resetIndex = resetContinueButtonOnScenes.IndexOf(a => a.name == lvlId);
            Debug.Log("OnLevelChanged " + lvlId + " MaxSecondsBeforeEndCanContinue=Posinfinity " + (resetIndex  >= 0? "true" : "false"));
            if (resetIndex >= 0)
                GameFlow.S.MaxSecondsBeforeEndCanContinue = float.NegativeInfinity;
        }
        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register(OnEndAnimationEventName, OnEndAnimationEvent);
            Dweiss.Msg.MsgSystem.MsgStr.Register(LevelChanged, OnLevelChanged);
            Dweiss.Msg.MsgSystem.MsgStr.Register(OnStartAnimationEventName, OnClipStart);
            
        }

        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnEndAnimationEventName, OnEndAnimationEvent);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(LevelChanged, OnLevelChanged);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnStartAnimationEventName, OnClipStart);
            }
        }
    }

}