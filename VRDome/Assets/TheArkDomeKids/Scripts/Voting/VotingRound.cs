﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class VotingRound : MonoBehaviour
    {
        public Dweiss.EventEmpty onVotingActive;

        public string[] skipIds = new string[] { "011A" };

        public SimpleGraph[] graphObjects;
        public FadeAnimation[] imagesSelectionToShow;


        private const string eventNameOnVoteDone = "VoteAction";
        private const string eventNameOfWonId = "VoteWon";

        private bool enableVoting = false;

        private int numOfPlayers = 10;
        private int voteCount = 0;
        private List<int> winnerIndex = new List<int>();
        private bool fadeOutStories;

        private void Start()
        {

            numOfPlayers = (int)Dweiss.Msg.MsgCacheString.S.Get("NumOfChildren");


            FadeInImages();
        }
        private void UpdateNumOfChildren(int num)
        {
            numOfPlayers = num;
        }
       
        public void FadeInImages()
        {
            for (int i = 0; i < imagesSelectionToShow.Length; i++)
            {

                imagesSelectionToShow[i].Init();
                imagesSelectionToShow[i].StartAnimation();
            }
        }

        public void FadeUnselectedImagesOut()
        {
            if (fadeOutStories) return;
            fadeOutStories = true;
            for (int i = 0; i < imagesSelectionToShow.Length; i++)
            {
                if (winnerIndex.Contains(i) == false)
                {
                    imagesSelectionToShow[i].Init();
                    imagesSelectionToShow[i].StartReverseAnimation();
                }
            }
        }

        public void FadeCompletely()
        {
            for (int i = 0; i < imagesSelectionToShow.Length; i++)
            {
                if (winnerIndex.Contains(i))
                {
                    imagesSelectionToShow[i].Init();
                    imagesSelectionToShow[i].StartReverseAnimation();
                }
            }
        }

        private void VoteFinished()
        {

            var numOfBlocks = graphObjects.Max(a => a.NumOfBlocks);
            for (int i = 0; i < graphObjects.Length; i++)
            {
                if (graphObjects[i].NumOfBlocks == numOfBlocks)
                {
                    winnerIndex.Add(i);
                }
            }
            int winnerId = winnerIndex.Count == 1? winnerIndex[0] : 4;

            Dweiss.Msg.MsgSystem.MsgStr.Raise(eventNameOfWonId, winnerId);
        
        }
        public float dealyEnableVoting = 2;
        private void DelayEnableVoting(string clipId)
        {
            var canVoteNow = skipIds.Contains(clipId) == false;
            Debug.Log("DelayEnableVoting on id " + clipId + " ? " + canVoteNow);
            this.WaitForSeconds(dealyEnableVoting, () => EnableVote(clipId));
        }

        public void EnableVote(string clipId)
        {
            var canVoteNow = skipIds.Contains(clipId) == false;
            Debug.Log("Enable vote on id " + clipId + " ? " + canVoteNow);
            if (canVoteNow)
            {
                enableVoting = true;
                if (voteCount == numOfPlayers)
                {
                   // FadeUnselectedImagesOut();
                   // OnDisable();
                }
            }
        }

        public void SelectMovie(int index)
        {

            if(enableVoting == false)
            {
                Debug.LogWarning("Voting disable");
                return;
            }

            if(voteCount++ >= numOfPlayers)
            {
                Debug.Log("Voting is done - vote doesn't count");
                return;
            }
            enableVoting = false;
            graphObjects[index].AddActiviteInPercentage(1f/numOfPlayers);

            onVotingActive.Invoke();
            if (voteCount == numOfPlayers) VoteFinished();
            else Dweiss.Msg.MsgSystem.Raise(eventNameOnVoteDone);
        }

        
    
        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("NumOfChildren", UpdateNumOfChildren);
            //Dweiss.Msg.MsgSystem.MsgStr.Register("AiAvatarAnimationEnd", EnableVote);
            Dweiss.Msg.MsgSystem.MsgStr.Register("AiAvatarAnimationStart", DelayEnableVoting);
        }

        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("NumOfChildren", UpdateNumOfChildren);
                //Dweiss.Msg.MsgSystem.MsgStr.Unregister("AiAvatarAnimationEnd", EnableVote);
                Dweiss.Msg.MsgSystem.MsgStr.Register("AiAvatarAnimationStart", DelayEnableVoting);
            }
        }

    }
}