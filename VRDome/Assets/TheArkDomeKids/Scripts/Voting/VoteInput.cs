﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class VoteInput : MonoBehaviour
    {
        public bool debug = false;

        public Dweiss.EventInt onSelected;
        public KeyCode shift = KeyCode.LeftControl;
        public KeyCode[] keyboardKeys = new KeyCode[] { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4 };


        public KeyCode[] yellow, green, red, blue;


        private KeyCode[] keyCodes;

        private void Reset()
        {
            yellow = new KeyCode[] { KeyCode.JoystickButton3, KeyCode.JoystickButton7 };
            red = new KeyCode[] { KeyCode.JoystickButton2, KeyCode.JoystickButton10 };
            green = new KeyCode[] { KeyCode.JoystickButton4, KeyCode.JoystickButton11 };
            blue = new KeyCode[] { KeyCode.JoystickButton0, KeyCode.JoystickButton9 };
        }
        private void Start()
        {
            Debug.Log(Input.GetJoystickNames().ToCommaString());
            var newKeyCodes = new List<KeyCode>();
            newKeyCodes.AddRange(yellow);
            newKeyCodes.AddRange(green);
            newKeyCodes.AddRange(red);
            newKeyCodes.AddRange(blue);

            keyCodes = newKeyCodes.ToArray();
        }

        private KeyCode[] _joyKeyCodes;
        private KeyCode[] JoyKeyCodes
        {
            get
            {
                if (_joyKeyCodes == null) _joyKeyCodes = System.Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>().Where(a => a.ToString().Contains("Joy")).ToArray();
                return _joyKeyCodes;
            }
        }
        private void TestAllButtons()
        {
            for (int i = 0; i < JoyKeyCodes.Length; i++)
            {
                var k = JoyKeyCodes[i];
                if (Input.GetKeyDown(k))
                {
                    Debug.Log("Key down " + k + " " + i);
                }
            }
        }

        public bool checkAllButons;
        private void Update()
        {


            if (checkAllButons) TestAllButtons();
            for (int i = 0; i < keyCodes.Length; i++)
            {
                var k = keyCodes[i];
                if (Input.GetKeyDown(k))
                {
                    Debug.Log("#" + (i / 2) + " Key down " + k);
                    onSelected.Invoke(i / 2);
                }
            }
            if (Input.GetKey(shift))
            {
                for (int i = 0; i < keyboardKeys.Length; i++)
                {
                    var k = keyboardKeys[i];
                    if (Input.GetKeyDown(k))
                    {
                        Debug.Log("Key down " + k);
                        onSelected.Invoke(i);
                    }
                }
            }

        }

    }
}