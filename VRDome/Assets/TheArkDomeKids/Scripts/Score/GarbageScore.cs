﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class GarbageScore : MonoBehaviour
    {

        //public const string ThrowEventName = "ThrowGarbageTotal";

//        public const string CollectTotalEventName = "CollectGarbageTotal";

        private const string ThrowEventFormat = GarbageSpawner.ThrowEventName;
        private const string CollectEventFormat = GarbageCan.CollectEventName;


        [System.Serializable]
        public class GarbageScoreAmount
        {
            public string id;
            public int throwAmount;
            public int collectAmount;
        }

        private List<GarbageScoreAmount> counter = new List<GarbageScoreAmount>();

        void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register(GarbageSpawner.ThrowEventName, Throw);
            Dweiss.Msg.MsgSystem.MsgStr.Register(GarbageCan.CollectEventName, Collect);
        }

        void OnDisble()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Register(GarbageSpawner.ThrowEventName, Throw);
                Dweiss.Msg.MsgSystem.MsgStr.Register(GarbageCan.CollectEventName, Collect);
            }
        }

        public GarbageScoreAmount Get(string id)
        {
            for (int i = 0; i < counter.Count; i++)
            {
                if (counter[i].id == id) return counter[i];
            }
            counter.Add(new GarbageScoreAmount() { id = id });
            return counter[counter.Count - 1];
        }

        private void Throw(string garbageId) {
            Get(garbageId).throwAmount++;

        }
        private void Collect(string garbageId) {
            Get(garbageId).collectAmount++;
            Debug.Log("Score updated " + garbageId + " " + Get(garbageId).collectAmount);
        }
        
        //public void RaiseScore()
        //{
        //    Dweiss.Msg.MsgSystem.Raise(CollectTotalEventName, )
        //}

    }
}