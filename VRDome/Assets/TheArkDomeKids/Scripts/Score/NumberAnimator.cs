﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Dweiss
{
    public class NumberAnimator : MonoBehaviour
    {
        public TextMeshPro txt;
        public int targetValue;
        public int rndValueMin = 3, rndValueMax = 30;
        public float timeToRandom = 1.5f;
        public float timeBetweenNewAnimation = .1f;

        public Dweiss.EventEmpty onShow;

        public int seed;
        void OnEnable()
        {
            StartCoroutine(AnimateNumbers());
        }

        IEnumerator AnimateNumbers()
        {
            System.Random rnd = new System.Random(seed);
            var tStart = Time.time;
            while(Time.time-tStart < timeToRandom)
            {
                int v = (int)rnd.Range((float)rndValueMin, (float)rndValueMax);
                txt.text = v.ToString();
                yield return new WaitForSeconds(timeBetweenNewAnimation);
            }

            txt.text = targetValue.ToString();

            onShow.Invoke();
        }
    }
}