﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class MedalsSystem : MonoBehaviour
    {
        [System.Serializable]
        public class LevelWithMedalTime
        {
            public ScriptableSceneSettings lvl;
            public float timeToShowYear;
            public float timeToShowPadestral;
            public float timeToAddMedal;
            public float timeToHideAll;
        }
        public LevelWithMedalTime[] allConfig;

        public GameObject medalPadestal;
        //  public GameObject[] medalsGray;

        public GameObject[] medalsColor;


        public FadeAnimation faderMedals, fadeYear;
        private int medalId;
        private void Awake()
        {
            //medalPadestal.SetActive(false);
            // foreach (var m in medalsColor) m.SetActive(false);

            faderMedals.onReachAlphaStart.AddListener(FadeOutEnded);
        }

        public void ShowNextMedal()
        {
            ShowNextMedal(-1);
        }
        //OnMedal show - animate size + animate movement
        public void ShowNextMedal(float showTime)
        {
            Debug.Log(name + " ShowNextMedal ");
            medalsColor[medalId].SetActive(true);
            ShowPadestal(showTime);

            medalId++;


        }


        public void ShowPadestal(float showTime)
        {
            Debug.Log("Show padestal for " + showTime);
            if (faderMedals.IsFade == FadeAnimation.FadeState.StartAlpha)
            {
                medalPadestal.SetActive(true);
                faderMedals.Init();
                faderMedals.StartAnimation();
            }
            if (showTime >= 0)
            {
                //StopAllCoroutines();
                this.ExactWaitForEvent(showTime, HideAll);
            }
        }



        private void FadeOutEnded()
        {
            // medalPadestal.SetActive(false);
        }

        public void HidePadestal()
        {
            if (faderMedals.IsFade == FadeAnimation.FadeState.EndAlpha)
            {
                faderMedals.Init();
                faderMedals.StartReverseAnimation();
            }
        }

        private void OnLevelChanged(string lvlName)
        {
            if (eventsOnLevel.Count > 0)
            {
                HideAll();
            }
            eventsOnLevel.Clear();

            for (int i = 0; i < allConfig.Length; i++)
            {
                if (allConfig[i].lvl.name == lvlName)
                {
                    Debug.LogFormat("Ready events {0}, {1}, {2}",
                        allConfig[i].timeToShowYear, allConfig[i].timeToShowPadestral, allConfig[i].timeToAddMedal);
                    RunAllEvents(allConfig[i]);
                }
            }
        }

        public bool runSkippedCoroutine = false;

        public void HideAll()
        {
            HidePadestal();
            HideYear();
        }

        public void HideYear()
        {
            if (fadeYear.IsFade == FadeAnimation.FadeState.EndAlpha)
            {
                fadeYear.StartReverseAnimation();
            }
        }
        public void ShowYear()
        {
            fadeYear.StartAnimation();
        }
        private void RunAllEvents(LevelWithMedalTime levelWithMedalTime)
        {
            TryRunSingleEvent(levelWithMedalTime.timeToShowYear, ShowYear, levelWithMedalTime.lvl.name);
            TryRunSingleEvent(levelWithMedalTime.timeToShowPadestral, () => ShowPadestal(-1), levelWithMedalTime.lvl.name);
            TryRunSingleEvent(levelWithMedalTime.timeToAddMedal, () => ShowNextMedal(-1), levelWithMedalTime.lvl.name);
            TryRunSingleEvent(levelWithMedalTime.timeToHideAll,
                () =>
                {
                    if (levelWithMedalTime.timeToShowYear >= 0) HideYear();
                    if (levelWithMedalTime.timeToShowPadestral >= 0 ||
                    levelWithMedalTime.timeToAddMedal >= 0) HidePadestal();

                }, levelWithMedalTime.lvl.name);
        }

        private List<Coroutine> eventsOnLevel = new List<Coroutine>();
        private void TryRunSingleEvent(float wait, Action f, string sceneName)
        {
            if (wait >= 0)
            {
                if (wait == 0)
                {
                    f();
                }
                else
                {
                    eventsOnLevel.Add(this.SimpleWaitWhile(
                        () =>
                        {
                            return (GameFlow.S.VideoTime < wait) &&
                                    (GameFlow.S.CurrentMovieScene.name == sceneName);
                        },
                        () =>
                        {
                            if (GameFlow.S.CurrentMovieScene.name == sceneName || runSkippedCoroutine)
                                f();
                        }));
                }
            }
        }
        private void RotateAndShowMedal()
        {
            Debug.Log("RotateAndShowMedal");
            transform.rotation = Quaternion.Euler(0, 90, 0) * transform.rotation;

            ShowMedalAfterPadestral();
        }

        public void ShowMedalAfterPadestral()
        {
            ShowPadestal(-1);
            this.WaitForSeconds(0.2f, () => ShowNextMedal(-1));

        }
        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("LevelChanged", OnLevelChanged);
            Dweiss.Msg.MsgSystem.MsgStr.Register("RaiseMedal", RotateAndShowMedal);
            OnLevelChanged(DomeVR.GameFlow.S.CurrentMovieScene.name);
            //Dweiss.Msg.MsgSystem.MsgStr.Raise("LevelChanged", CurrentMovieScene.name);
        }

        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("LevelChanged", OnLevelChanged);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("RaiseMedal", RotateAndShowMedal);
            }
            //Dweiss.Msg.MsgSystem.MsgStr.Raise("LevelChanged", CurrentMovieScene.name);
        }

    }
}