﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class RotationRelativeToVideo : MonoBehaviour
{
    private const string VideoParentTag = "DomeVR";
    private const string GuiParentTag = "GuiPivotCalibrated";
    public RelativeTo relativeTo = RelativeTo.Movie;

    

    public enum RelativeTo
    {
        Movie,
        GUI
    }
    void Start()
    {
        SetPos();
    }

    public void SetPos() {
        Transform myRlative = null;
        switch (relativeTo)
        {
            case RelativeTo.Movie:
                var videoParent = GameObject.FindGameObjectWithTag(VideoParentTag);
                myRlative = videoParent.GetComponentInChildren<VideoPlayer>().transform;
                break;
            case RelativeTo.GUI:
                myRlative = GameObject.FindGameObjectWithTag(GuiParentTag).transform;
                break;
            default:
                throw new System.NotSupportedException("RotationRelativeToVideo doesn't support " + relativeTo);


        }
        transform.rotation = myRlative.rotation * transform.localRotation;
    }

    public void SetRelativeToGuiWithYRotation(float yRot)
    {
        var myRlative = GameObject.FindGameObjectWithTag(GuiParentTag).transform;
        transform.rotation =  myRlative.rotation * Quaternion.Euler(0, yRot, 0);
    }

}
