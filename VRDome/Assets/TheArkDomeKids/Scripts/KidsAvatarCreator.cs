﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DomeVR
{
    public class KidsAvatarCreator : MonoBehaviour
    {
        public GameObject kidPrefab;
        public GameObject aiPrefab;
        public float walkingAvatarDelayCreation = -1;
        
        public RenderTexture[] rendrTexturesForCamera;

        private string imageTagInPrefab = "Image";
        private string rndrTextureTagInPrefab = "RenderTextureAvatar";

        private string imagesMainFolder = "C:\\RoofTopImages";
        private string imagesSubPath = "1";
        private int kidsCount;
        IEnumerator Start()
        {

            imagesSubPath = (string) Dweiss.Msg.MsgCacheString.S.Get("ImagesSubPath");
            imagesMainFolder = (string)Dweiss.Msg.MsgCacheString.S.Get("ImagesMainPath");
            kidsCount = (int)Dweiss.Msg.MsgCacheString.S.Get("NumOfChildren");

            var path = Path.Combine(imagesMainFolder + "\\" + imagesSubPath + "\\");//.Replace("\\\\","\\").Replace("//", "/");
            string[] files = new string[] { "Missing directory", "path"};
            if (Directory.Exists(path)) files = Directory.GetFiles(path);

            var faceAvatars = new List<GameObject>();

            faceAvatars.Add(CreateAvatar(aiPrefab, rendrTexturesForCamera[0]));
            faceAvatars[0].transform.rotation = Quaternion.Euler(0, 0, 0);
            faceAvatars[0].SetActive(true);

            var startT = Time.time;
            if (walkingAvatarDelayCreation > 0)
                yield return new WaitWhile(() => Time.time - startT < walkingAvatarDelayCreation);

            for (int i = 0; i < kidsCount; i++)
            {
                string imgFile="";
                try
                {
                    imgFile = files[i];//.Replace(".bin", ".jpg");
                    //System.IO.File.Move(files[i], imgFile);
                    Debug.LogFormat("Loading file {0} name {1} -> {2} from {3}", i, files[i], imgFile, path);

                    var texture = LoadTexture(imgFile);

                    faceAvatars.Add(CreateFaceAvatar(texture, rendrTexturesForCamera[i+1]));
                   
                   
                }
                catch(System.Exception e)
                {
                    Debug.LogError("Problem with image " + i + "# " + imgFile + " " + e);
                }
            }

            for (int i = 1; i < faceAvatars.Count; i++)
            {
                var yAngle = i * (360f / faceAvatars.Count);
                faceAvatars[i].transform.rotation = Quaternion.Euler(0, yAngle, 0);
                faceAvatars[i].SetActive(true);
            }
        }

        private GameObject CreateAvatar(GameObject prefab, RenderTexture rndrTxtr)
        {
            var newKidPrefab = Instantiate(prefab);
            newKidPrefab.GetComponentInChildren<Camera>().targetTexture = rndrTxtr;
            newKidPrefab.transform.parent = transform;

            var imgTrans = newKidPrefab.transform.FindTagInChildren(rndrTextureTagInPrefab);
            imgTrans.GetComponent<Renderer>().material.mainTexture = rndrTxtr;
            return newKidPrefab;
        }

        private GameObject CreateFaceAvatar(Texture texture, RenderTexture rndrTxtr)
        {
            var newKidPrefab = CreateAvatar(kidPrefab, rndrTxtr);
            //var newKidPrefab = Instantiate(prefab);
            //newKidPrefab.GetComponentInChildren<Camera>().targetTexture = rndrTxtr;
            //newKidPrefab.transform.parent = transform;

            //var imgTrans = newKidPrefab.transform.FindTagInChildren(rndrTextureTagInPrefab);
            //imgTrans.GetComponent<Renderer>().material.mainTexture = rndrTxtr;


            var imgTrans = newKidPrefab.transform.FindTagInChildren(imageTagInPrefab);
            imgTrans.GetComponent<Renderer>().material.mainTexture = texture;
            return newKidPrefab;
        }


        private Texture LoadTexture(string filePath)
        {
            Texture2D tex = null;
            byte[] fileData;

            if (File.Exists(filePath))
            {
                fileData = File.ReadAllBytes(filePath);
                tex = new Texture2D(2, 2);
                tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            }
            return tex;
        }
    }
}