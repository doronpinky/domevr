﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ChangeFpsTarget : MonoBehaviour
    {
        public int targetFps = 100;
        public Dweiss.EventInt fpsTargetChanged;
        public Dweiss.EventString fpsTargetChangedTxt;
        void Start()
        {
            fpsTargetChanged.AddListener((v) => fpsTargetChangedTxt.Invoke(v.ToString()));
            UpdateFps(targetFps);
        }

        private void UpdateFps(int newTarget)
        {
            Application.targetFrameRate = newTarget;
            fpsTargetChanged.Invoke(Application.targetFrameRate);
        }

        public void AddTargetFps(int amount) { targetFps += amount; UpdateFps(targetFps); }
        
    }
}