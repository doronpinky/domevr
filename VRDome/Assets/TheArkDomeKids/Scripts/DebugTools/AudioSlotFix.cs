﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

namespace DomeVR
{   [RequireComponent(typeof(AudioSource))]
    public class AudioSlotFix : MonoBehaviour
    {
        private AudioSource aSource;
        private MovieTransition movieTrans;

        private void Awake()
        {
            aSource = GetComponent<AudioSource>();
            movieTrans = GameObject.FindObjectOfType<MovieTransition>();
            ;
        }
        public void SetVideoAudioPlay(bool play)
        {
            movieTrans.ActivePlayer.GetComponent<AudioSource>().mute = !play;
        }
        public void SetSlotAudioPlay(bool play)
        {
            if (play) aSource.Play();
            else aSource.Stop();
        }

        public void SwitchSoundVideo(bool videoOn)
        {
            SetVideoAudioPlay(videoOn);
            SetSlotAudioPlay(!videoOn);
        }

        private void OnDisable()
        {
            if(movieTrans && movieTrans.ActivePlayer)
                movieTrans.ActivePlayer.GetComponent<AudioSource>().mute = false;

            aSource.Stop();
        }
    }
}