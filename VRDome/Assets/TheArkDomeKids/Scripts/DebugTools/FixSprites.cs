﻿using System;
using System.Linq;
using System.IO;
using UnityEngine;

namespace ConsoleApp1
{
    public class FixSprites : MonoBehaviour
    {
        public string source = @"D:\Downloads\Trash_Png_Alpha\";
        public string target = @"C:\Workspace\MiniPaidProjects\TheArkDomeVR\VRDome\Assets\TheArkDomeKids\Textures\Trash\";
        public int maxCopy = 3;
        public int printEvery = 20;

        public string prefix = "Sh_011_A_masterLayer.0";//"000.png";//S011_A000.png
        public string suffixToFind = "000.png";



        [ContextMenu("Special Rename")]
        private void RenameAllInFolder()
        {
            // foreach (var lib in Directory.GetDirectories(source))
            //  {
            Debug.LogFormat("Checking lib " + source);
            int counter = 0;
            var myFiles = Directory.GetFiles(source);
            foreach (var f in myFiles)
            {
                var pngIndex = f.IndexOf(".png");
                if (pngIndex == -1)
                {

                }
                else
                {
                    var suffix = f.Substring(f.Length - suffixToFind.Length);
                    //var 

                    //var problematicIndex = f.Length - ("_0000.png".Length);
                    var newName = source + "/" + prefix + suffix;// f.Substring(0, problematicIndex) + '.' + f.Substring(problematicIndex + 1);

                    if (printEvery == 0 || counter % printEvery == 0 || counter == 0 || counter + 1 == maxCopy || counter + 1 == myFiles.Length)
                        Debug.LogFormat("{0}. {1} => {2}", counter, f, newName);

                    File.Move(f, newName);
                }
                if (counter++ >= maxCopy) break;
            }
            Debug.LogFormat("Done with lib " + source);
            //break;

            // }
        }

        //[ContextMenu("RunAll")]
        private void RunFolderSwap()
        {
            FixNaming();
            CompareFolders();
            CopyInnerFiles();
        }


        [ContextMenu("Fixed naming")]
        private void FixNaming()
        {
            foreach (var lib in Directory.GetDirectories(source))
            {
                Debug.LogFormat("Checking lib " + lib);
                foreach (var f in Directory.GetFiles(lib))
                {
                    var pngIndex = f.IndexOf(".png");
                    if (pngIndex == -1)
                    {
                        if (f.EndsWith("Thumbs.db") == false)
                        {
                            Debug.LogFormat("\nProblem with file in folder " + lib + " " + f);
                            break;
                        }
                    }
                    else
                    {
                        var problematicIndex = f.Length - ("_0000.png".Length);
                        var newName = f.Substring(0, problematicIndex) + '.' + f.Substring(problematicIndex + 1);


                        File.Move(f, newName);
                    }
                }
                Debug.LogFormat("Done with lib " + lib);

            }


        }

        [ContextMenu("Compare folders")]
        private void CompareFolders()
        {
            var sourceDirs = Directory.GetDirectories(source).ToArray();
            var targetDirs = Directory.GetDirectories(target).ToArray();
            System.Array.Sort(sourceDirs);
            System.Array.Sort(targetDirs);
            int counter = 0;
            for (int i = 0; i < sourceDirs.Length; i++)
            {
                if (i >= targetDirs.Length) break;

                var dirSource = new DirectoryInfo(sourceDirs[i]);
                var dirTarget = new DirectoryInfo(targetDirs[i]);
                if (dirSource.Name != dirTarget.Name)
                {
                    Debug.LogErrorFormat("Source  {0} != {1}",// : FullPath {2} != {3}
                        new DirectoryInfo(sourceDirs[i]).Name
                        , new DirectoryInfo(targetDirs[i]).Name
                        , sourceDirs[i]
                        , targetDirs[i]);
                }
                else
                {
                    var sourceLib = sourceDirs[i];
                    var targetLib = targetDirs[i];

                    var sourceFiles = Directory.GetFiles(sourceLib).Where(a => a.EndsWith(".png")).ToArray();
                    var targetFiles = Directory.GetFiles(targetLib).Where(a => a.EndsWith(".png")).ToArray();

                    System.Array.Sort(sourceFiles);
                    System.Array.Sort(targetFiles);

                    if (sourceFiles.Length != targetFiles.Length)
                    {
                        Debug.LogErrorFormat("Source length {0} != {1} >> {2} : {3}",// : FullPath {2} != {3}
                            new DirectoryInfo(sourceDirs[i]).Name
                            , new DirectoryInfo(targetDirs[i]).Name
                            , sourceFiles.Length
                            , targetFiles.Length);
                    }
                    //else
                    {
                        FileInfo fSourceInfo = null;
                        FileInfo fTargetInfo = null;
                        for (int j = 0; j < sourceFiles.Length; j++)
                        {
                            if (j >= targetFiles.Length)
                            {
                                Debug.LogFormat(counter + ". Finished file comparison at {0} after {1} =? {2}",  j, fSourceInfo, fTargetInfo);
                                break;
                            }

                            fSourceInfo = new FileInfo(sourceFiles[j]);
                            fTargetInfo = new FileInfo(targetFiles[j]);
                            if (fSourceInfo.Name != fTargetInfo.Name)
                            {
                                Debug.LogErrorFormat("Source  {0} != {1} \t\t>>>> {2}",// : FullPath {2} != {3}
                                   fSourceInfo.Name
                                   , fTargetInfo.Name
                                   , sourceFiles[j]
                                   , targetFiles[j]);
                            }
                            else
                            {
                                if (counter == maxCopy)
                                {
                                    Debug.Log("Stop comparing after " + maxCopy);
                                    return;
                                }

                               // var newName = fTargetInfo.Directory + "\\" + fSourceInfo.Name;
                                if (printEvery == 0 || counter % printEvery == 0 || j == 0 || counter + 1 == maxCopy || j + 1 == sourceFiles.Length 
                                    || j +1 == targetFiles.Length)
                                    Debug.LogFormat(counter + ". ({2}) check {0} vs {1}", fSourceInfo.Name, fTargetInfo.Name, j);
                               // File.Copy(sourceFiles[j], newName, true);

                                counter++;
                            }
                        }
                    }
                    Debug.LogFormat("Done with lib " + sourceDirs[i] + " vs " + targetDirs[i]);
                }

            }
            Debug.LogFormat("\nSource {0} : {1}\n{2} : {3}\n", sourceDirs.Length, targetDirs.Length, source, target);
            if (sourceDirs.Length != targetDirs.Length)
            {
                Debug.LogFormat("More dis in targetDir");
            }
        }

        [ContextMenu("Copy png files ")]
        private void CopyInnerFiles()
        {

            var sourceDirs = Directory.GetDirectories(source).ToArray();
            var targetDirs = Directory.GetDirectories(target).ToArray();
            System.Array.Sort(sourceDirs);
            System.Array.Sort(targetDirs);

            int counter = 0;

            for (int i = 0; i < sourceDirs.Length; i++)
            {
                if (i >= targetDirs.Length) break;
                if (new DirectoryInfo(sourceDirs[i]).Name != new DirectoryInfo(targetDirs[i]).Name)
                {
                    Debug.LogFormat("Source  {0} != {1}",// : FullPath {2} != {3}
                       new DirectoryInfo(sourceDirs[i]).Name
                       , new DirectoryInfo(targetDirs[i]).Name
                       , sourceDirs[i]
                       , targetDirs[i]);
                }
                else
                {
                    var sourceLib = sourceDirs[i];
                    var targetLib = targetDirs[i];

                    var sourceFiles = Directory.GetFiles(sourceLib).Where(a => a.EndsWith(".png")).ToArray();
                    var targetFiles = Directory.GetFiles(targetLib).Where(a => a.EndsWith(".png")).ToArray();

                    if (sourceFiles.Length != targetFiles.Length)
                    {
                        Debug.LogFormat("Source length {0} != {1} >> {2} : {3}",// : FullPath {2} != {3}
                            new DirectoryInfo(sourceDirs[i]).Name
                            , new DirectoryInfo(targetDirs[i]).Name
                            , sourceFiles.Length
                            , targetFiles.Length);
                    }
                    else
                    {
                        for (int j = 0; j < sourceFiles.Length; j++)
                        {
                            var fSourceInfo = new FileInfo(sourceFiles[j]);
                            var fTargetInfo = new FileInfo(targetFiles[j]);
                            if (fSourceInfo.Name != fTargetInfo.Name)
                            {
                                Debug.LogFormat("Source  {0} != {1} \t\t>>>> {2}",// : FullPath {2} != {3}
                                   fSourceInfo.Name
                                   , fTargetInfo.Name
                                   , sourceFiles[j]
                                   , targetFiles[j]);
                            }
                            else
                            {
                                if (counter == maxCopy) return;

                                var newName = fTargetInfo.Directory + "\\" + fSourceInfo.Name;
                                if (printEvery == 0 || counter % printEvery == 0 || j == 0 || counter + 1 == maxCopy || j + 1 == sourceFiles.Length)
                                    Debug.LogFormat(counter + " [{2}] Copy {0} to {1}", sourceFiles[j], newName, j);
                                File.Copy(sourceFiles[j], newName, true);

                                counter++;
                            }
                        }
                    }
                    Debug.LogFormat("Done copying with lib " + sourceDirs[i]);
                }

            }

        }


    }
}
