﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncAnimatorWithAudio : MonoBehaviour
{
    public bool debug;
    public AudioSource aSource;
    public Animator anim;

    public float minDiffToFixSound = 0.07f, maxDiffForSoundFix = 5;


    [Header("for debug only")]
    public float animationTime;
    public float soundTime;


    private void Reset()
    {
        anim = GetComponentInChildren<Animator>();
        aSource = GetComponentInChildren<AudioSource>();

    }
    private void Update()
    {
        FixAudioDiff();
    }
    private void FixAudioDiff()
    {
        var clipInfo = anim.GetCurrentAnimatorStateInfo(0);
        animationTime = clipInfo.length * clipInfo.normalizedTime;
        soundTime = aSource.time;

        if (aSource.clip != null && clipInfo.normalizedTime < .9f && Mathf.Abs(aSource.time - animationTime) > minDiffToFixSound)
            if (Mathf.Abs(aSource.time - animationTime) <= maxDiffForSoundFix)
            {

                if (debug) Debug.Log("Fixing diff");
                aSource.time = animationTime + Time.deltaTime / 2f;
            }
    }
}
