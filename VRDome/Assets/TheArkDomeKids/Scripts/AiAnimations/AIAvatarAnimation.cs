﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class AIAvatarAnimation : MonoBehaviour
    {
        public bool debug;



        public float waitBetweenAudioFixCheck = 0f;


        [Header("Sequence animations")]
        public AnimationGroup[] animationGroup;
        public string[] triggerOfLastScene;

        [System.Serializable]
        public class AnimationSequence
        {
            public string trigger;
            public AudioClip audio;
        }

        [System.Serializable]
        public class AnimationGroup
        {

            public AnimationSequence[] seq;
        }

        private int animationGroupIndex;
        private AnimationSequence[] Animation
        {
            get { return animationGroup[animationGroupIndex].seq; }
        }
        // public string loopAnimation;

        [Header("Special case")]
        public bool firstClipWaitingForOperator;
        public string operatorContinueEventStr = "OperatorContinue";



        public float minDiffToFixSound = 0.07f, maxDiffForSoundFix = 3;


        private AudioSource aSource;
        private Animator anim;
        private int askPlayerAnimationsIndex = 0;
        //private int soundPlayIndex = -1;
       // [Header("Add events context menu")]
        public AnimationClip[] animClipsToAddEvents;

       
        private const string OnEndAnimationEventName = "AiAvatarAnimationEnd";
        private const string AiAvatarAnimationDoneEventName = "AiAnimationFinished";
        private const string OnStartAnimationEventName = "AiAvatarAnimationStart";

        private bool wasSceneStarted = false, sceneStarted = false;

        [Header("___Debug readonly___")]
        public float clipTime, audioTime;
        //[ContextMenu("Setup events in animations")]
        private void SetupEvents()
        {
            //Add events
            AnimationEvent evt = new AnimationEvent();
            evt.time = 0.001f;
            evt.functionName = "OnAnimationStart";

            for (int i = 0; i < animClipsToAddEvents.Length; i++)
            {
                var c = animClipsToAddEvents[i];
                evt.stringParameter = c.name;
                c.AddEvent(evt);

                AnimationEvent evt2 = new AnimationEvent();
                evt2.time = c.length - Time.deltaTime*2-0.1f ;
                evt2.functionName = "OnAnimationEnd";
                evt2.stringParameter = c.name;
                c.AddEvent(evt2);
                Debug.Log("Added event end at " + c.name + " " + evt2.time + "/"+ c.length);

                //                var normTime = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            }
            // animClipsToAddEvents = new AnimationClip[0];
        }

        private void Awake()
        {
            anim = GetComponent<Animator>();
            aSource = GetComponentInChildren<AudioSource>();
            SetupEvents();



        }
        private void Start()
        {
            // numOfPlayerAnimations = (int) Dweiss.Msg.MsgCacheString.S.Get("NumOfChildren") ;
            //if (debug) Debug.Log("Start " + askPlayerAnimationsIndex);
            NextSpriteAnimation();
        }

        private bool SceneStarted
        {
            get { return sceneStarted; }
            set {
                sceneStarted = value;
                 }
        }


        private IEnumerator CoroutineFixAudioDiff()
        {
            yield return 0;
            //int maxCheckTime = int;
            //for (int i = 0; i < maxCheckTime / waitBetweenCheck && SceneStarted == false; i++)
            while(true)
            {
                FixAudioDiff();
                yield return new WaitForSeconds(waitBetweenAudioFixCheck);
            }
        }
        private void FixAudioDiff()
        {
            var clipInfo = anim.GetCurrentAnimatorStateInfo(0);
            clipTime = clipInfo.length * clipInfo.normalizedTime;

            if (aSource.clip != null && clipInfo.normalizedTime < .9f && Mathf.Abs(aSource.time - clipTime) > minDiffToFixSound)
                if (Mathf.Abs(aSource.time - clipTime) <= maxDiffForSoundFix) aSource.time = clipTime + Time.deltaTime/2f;
        }
        private void Update()
        {
            UpdateState();
        }

        private void UpdateState()
        {
            var clipInfo = anim.GetCurrentAnimatorStateInfo(0);
            clipTime = clipInfo.length * clipInfo.normalizedTime;
            audioTime = aSource.time;
           // Debug.LogFormat("{2} - {0} : {1}", clipTime, aSource.time, Time.time);
        }

        public void OnAnimationEnd(string nameOfClip)
        {
            if (debug) Debug.Log("___OnAnimationEnd " + Animation[askPlayerAnimationsIndex - 1].trigger +   " >> " + nameOfClip);
            StopAllCoroutines();

            Dweiss.Msg.MsgSystem.MsgStr.Raise(OnEndAnimationEventName, Animation[askPlayerAnimationsIndex - 1].trigger);
            aSource.Stop();
            aSource.clip = null;
            if (triggerOfLastScene.Contains(Animation[askPlayerAnimationsIndex-1].trigger))
            {
                Debug.Log("Finish sequence");
                Dweiss.Msg.MsgSystem.Raise(AiAvatarAnimationDoneEventName);
            }
        }

        public void OnAnimationStart(string nameOfClip)//To be called from Event configured in animations
        {
            if (debug) Debug.Log("___OnAnimationStart " + Animation[askPlayerAnimationsIndex - 1].trigger + " (" + askPlayerAnimationsIndex + ") started? " + SceneStarted + " >> " + nameOfClip);

            if (SceneStarted == false)
            {
                aSource.Stop();
                aSource.clip = Animation[askPlayerAnimationsIndex - 1].audio;
                aSource.Stop();
                aSource.time = 0;
                aSource.Play();
                FixAudioDiff();
                SceneStarted = true;
                StartCoroutine(CoroutineFixAudioDiff());
                
                Dweiss.Msg.MsgSystem.MsgStr.Raise(OnStartAnimationEventName, Animation[askPlayerAnimationsIndex - 1].trigger);
            }
        }

        [ContextMenu("Continue by physical button")]
        public void NextAskPlayerAnimationLoop()
        {
            if (SceneStarted)
            {
                if (debug) Debug.Log("NextAskPlayerAnimationLoop " + askPlayerAnimationsIndex);
                if (askPlayerAnimationsIndex <= 1 && animationGroupIndex == 0)
                {
                    Debug.Log("Cant continue by physical button?");
                }
                else 
                    NextSpriteAnimation();
            }
        }

        private void NextSpriteAnimation()
        {
            anim.SetTrigger(Animation[askPlayerAnimationsIndex++].trigger);
            SceneStarted = false;
        }

        [ContextMenu("Continue by operator button")]
        private void OperatorPressContinue()
        {
            if (debug) Debug.Log("OperatorPressContinue " + askPlayerAnimationsIndex);

            if (SceneStarted)
            {
                //Only on first animation

                if (askPlayerAnimationsIndex == 1 && animationGroupIndex == 0)
                    NextSpriteAnimation();

                else
                    Debug.Log("Cant continue by operator?");
                    
            }
        }

        public void SpecialPlayAnimationOptionSeq12(int id)
        {
            Debug.Log("SpecialPlayAnimationOptionSeq12 " + id);
            animationGroupIndex++;
            askPlayerAnimationsIndex =  id;
            NextSpriteAnimation();

            //anim.SetTrigger(animationSequence[askPlayerAnimationsIndex++]);
            //SceneStarted = false;
        }

        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register(operatorContinueEventStr, OperatorPressContinue);
        }
        private void OnDisable()
        {
            if(Dweiss.Msg.MsgSystem.S)
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(operatorContinueEventStr, OperatorPressContinue);
        }
    }
}