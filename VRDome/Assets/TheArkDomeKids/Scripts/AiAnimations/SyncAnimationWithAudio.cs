﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class SyncAnimationWithAudio : MonoBehaviour
    {
        public bool debug;
        public AudioSource aSource;
        public Animator anim;
        public Dweiss.SpriteAnimator spriteAnim;

        public float minDiffToFixSound = 0.07f, maxDiffForSoundFix = 5;


        [Header("for debug only")]
        public float animationTime;
        public float soundTime;


        private void Reset()
        {
            anim = GetComponentInChildren<Animator>();
            aSource = GetComponentInChildren<AudioSource>();
            spriteAnim = GetComponentInChildren<Dweiss.SpriteAnimator>();

        }
        private void Update()
        {
            FixAudioDiff();
        }
        private void FixAudioDiff()
        {
            if (spriteAnim != null) FixAccordingToDweissAnimtor();
            else FixAccordingToUnityAnimator();
        }
        private void FixAccordingToDweissAnimtor()
        {
            var clipTime = spriteAnim.AnimationTime;

            animationTime = clipTime;
            soundTime = aSource.time;

            if (aSource.clip != null && Mathf.Abs(aSource.time - clipTime) > minDiffToFixSound)
                if (Mathf.Abs(aSource.time - clipTime) <= maxDiffForSoundFix)
                {
                    if (debug) Debug.Log("Fixing diff");
                    aSource.time = clipTime + Time.deltaTime / 2f;
                }
        }
        private void FixAccordingToUnityAnimator() { 
            var clipInfo = anim.GetCurrentAnimatorStateInfo(0);
            animationTime = clipInfo.length * clipInfo.normalizedTime;
            soundTime = aSource.time;

            if (aSource.clip != null && clipInfo.normalizedTime < .9f && Mathf.Abs(aSource.time - animationTime) > minDiffToFixSound)
                if (Mathf.Abs(aSource.time - animationTime) <= maxDiffForSoundFix)
                {

                    if (debug) Debug.Log("Fixing diff");
                    aSource.time = animationTime + Time.deltaTime / 2f;
                }
        }
    }
}