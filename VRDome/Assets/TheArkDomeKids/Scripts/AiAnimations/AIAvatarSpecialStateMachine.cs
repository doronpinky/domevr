﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class AIAvatarSpecialStateMachine : MonoBehaviour
    {
        public bool debug;
        public string[] triggerOfLastScene;

        public int indexOfEndSequence = 10;


        private const string AiAvatarAnimationDoneEventName = "AiAnimationFinished";
        private const string AiAvatarLastAnimationEventName = "AiLastAnimation";
        private const string OnVoteWonEventName = "VoteWon";
        private const string OnVoteActionEventName = "VoteAction";
        private const string operatorContinueEventName = "OperatorContinue";


        private const string OnEndAnimationEventName = "AiAvatarAnimationEnd";
        private const string OnStartAnimationEventName = "AiAvatarAnimationStart";

        private int animationIndex = 0;//First sequence alread showed
        private bool won;


        private Dweiss.SpriteAnimator animator;
        private Dweiss.SpriteAnimatorMachine animatorMachine;

        //[Header("Special case")]
        //public bool firstClipWaitingForOperator;

        private void Awake()
        {
            animatorMachine = GetComponent<Dweiss.SpriteAnimatorMachine>();
            animator = GetComponent<Dweiss.SpriteAnimator>();
        }



        private void RunSpriteAnimation(int add)
        {
            animationIndex += add;
            animatorMachine.SetTrigger(animationIndex);//Sequnce 0 runs on start
        }

        [ContextMenu("Continue by operator button")]
        private void OperatorPressContinue()
        {
            if (debug) Debug.Log("OperatorPressContinue " + animationIndex);

            if (animationIndex == 0)
                RunSpriteAnimation(1);

            else
                Debug.Log("Cant continue by operator?");

        }

        [ContextMenu("Continue by physical button")]
        public void VoteAction()
        {
            if (debug) Debug.Log("NextAskPlayerAnimationLoop " + animationIndex);
            if (animationIndex == 0)
            {
                Debug.Log("Cant continue by physical button?");
            }
            else
                RunSpriteAnimation(1);
        }

        public void VoteWon(int id)
        {
            if(debug)Debug.Log("SpecialPlayAnimationOptionSeq12 " + id + " at " + animationIndex + " => " + (indexOfEndSequence+id));

            animationIndex = indexOfEndSequence;
            RunSpriteAnimation(id);

        }

        private void OnAnimationStart(string id) {
            if (triggerOfLastScene.Contains(id))
            {
                if (debug) Debug.Log("Last sequence");
                animator.stopAtTheEndOfAnimation = true;
                Dweiss.Msg.MsgSystem.Raise(AiAvatarLastAnimationEventName);
            }
        }
        private void OnAnimationEnd(string id) {
            if (triggerOfLastScene.Contains(id))
            {
                if (debug) Debug.Log("Finish sequence");
                Dweiss.Msg.MsgSystem.Raise(AiAvatarAnimationDoneEventName);
            }
        }

        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register(operatorContinueEventName, OperatorPressContinue);
            Dweiss.Msg.MsgSystem.MsgStr.Register(OnVoteActionEventName, VoteAction);
            Dweiss.Msg.MsgSystem.MsgStr.Register(OnVoteWonEventName, VoteWon);

            Dweiss.Msg.MsgSystem.MsgStr.Register(OnStartAnimationEventName, OnAnimationStart);
            Dweiss.Msg.MsgSystem.MsgStr.Register(OnEndAnimationEventName, OnAnimationEnd);

            //            animatorMachine.reg
        }
        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(operatorContinueEventName, OperatorPressContinue);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnVoteActionEventName, VoteAction);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnVoteWonEventName, VoteWon);

                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnStartAnimationEventName, OnAnimationStart);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(OnEndAnimationEventName, OnAnimationEnd);
            }
        }
    }
}