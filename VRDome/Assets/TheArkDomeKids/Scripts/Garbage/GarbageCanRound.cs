﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class GarbageCanRound : MonoBehaviour
    {


        public GarbageCan prefabCanForCollecting;
        public string[] canTags;

        public bool debugMouse;
        //private List<SingleJoystickControl> trackers;
        // Start is called before the first frame update
        void Awake()
        {
            //TODO: Setup controllers and remove old controllers.
            var joysticks = GameObject.FindObjectsOfType<SingleJoystickControl>();
            // trackers = joysticks.Where(a => a.joystickNum == 2).ToList();
            var id1 = joysticks.Where(a => a.joystickNum == 1).ToArray();

            // System.Array.Sort(trackers, (a, b) => { return a.name.CompareTo(b.name); });//Sort for consistency
            var goTemp = new GameObject("Missing tracker target"); goTemp.transform.position = new Vector3(1000, -1000, 1000);
            goTemp.transform.parent = transform;
            for (int i = 0; i < canTags.Length; i++)
            {
                var can = SetupCan(goTemp.transform, canTags[i]);
                var index = i;
                //var infoStart = string.Format("Can created = {0}-{1}  ", index, can.GetMyColor());
                //Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", infoStart);
                SetupTrackersOnStart.S.RegisterTrackerById(index, (tracker) =>
                {
                    var info = string.Format("Tracker = {0}-{1}  ", index, GarbageCan.GetColor(can.tagToInteractWith));
                    Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", info);
                    can.targetToFollow = tracker.transform;
                });
                //go.targetToFollow = transform;
            }

            if (id1.Length > 0 && debugMouse)
            {
                for (int i = 0; i < canTags.Length; i++)
                {
                    SetupCan(id1[0].transform, canTags[i]);
                }
            }


        }

        //private void Update()
        //{
        //    if (listOfCans.Count == 0) return;
        //    for (int i = 0; i < trackers.Count; i++)
        //    {
        //        var isIdle = trackers[i].GetComponent<DisableScriptsWhenIdle>();
        //        if(isIdle.IsIdle == false) {
        //            var can = listOfCans.First();
        //            listOfCans.RemoveAt(0);
        //            can.targetToFollow = trackers[i].transform;
        //            trackers.RemoveAt(i);
        //            --i;
        //        }
        //    }
        //}


        private GarbageCan SetupCan(Transform target, string tag)
        {
            var go = Instantiate(prefabCanForCollecting.gameObject);
            var gc = go.GetComponent<GarbageCan>();
            gc.targetToFollow = target;
            gc.transform.parent = transform;
            gc.tagToInteractWith = tag;

            return gc;
        }




    }
}