﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class GarbageShowScore : MonoBehaviour
    {

        public GarbageScore scoreKeeper;
        public ScoreGroup[] scores;
        

        [System.Serializable]
        public class ScoreGroup
        {
            public string scoreId;
            public Dweiss.NumberAnimator scoreShow;
        }

        private void Start()
        {
            if (scoreKeeper == null) scoreKeeper = GameObject.FindObjectOfType<GarbageScore>();
        }

        private void OnEnable()
        {
            for (int i = 0; i < scores.Length; i++)
            {
                scores[i].scoreShow.gameObject.SetActive(false);
            }
        }

        private bool wasFinished = false;
        public void OnFinishedAnimation()
        {
            if (wasFinished == false)
            {
                wasFinished = true;
                for (int i = 0; i < scores.Length; i++)
                {
                    scores[i].scoreShow.gameObject.SetActive(true);
                    scores[i].scoreShow.targetValue = scoreKeeper.Get(scores[i].scoreId).collectAmount;
                }
            } else
            {
                for (int i = 0; i < scores.Length; i++)
                {
                    scores[i].scoreShow.gameObject.SetActive(false);
                }
            }
        }

    }
}