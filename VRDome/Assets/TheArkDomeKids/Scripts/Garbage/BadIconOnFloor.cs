﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class BadIconOnFloor : MonoBehaviour
    {
        public GameObject prefabOnHit;
        public float ttl;
        public float distanceOfPrefab;
        private void OnTriggerEnter(Collider other)
        {
            var isGarbageItem = other.gameObject.layer == LayerMask.NameToLayer("GarbageItem");
            Debug.LogFormat("Floor hit {0} => {1} ", other.name, isGarbageItem?"Garbage":"NotGarbage");
            if (isGarbageItem)
            {
                var dir = other.transform.position;
                var floorVec = Vector3.ProjectOnPlane(dir, Vector3.up);

                GameObject newBad = Instantiate(prefabOnHit);
                newBad.transform.parent = transform;
                Destroy(newBad, ttl);
                newBad.transform.position = floorVec.normalized * distanceOfPrefab;

                Destroy(other.gameObject);
            }
            
        }


    }
}