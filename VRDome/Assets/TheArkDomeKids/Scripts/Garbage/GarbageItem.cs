﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    [RequireComponent(typeof(Rigidbody))]
    public class GarbageItem : MonoBehaviour
    {
        public Vector3 target;
       // public Texture myTexture;
        public float maxSpeed = 1;
        public float gravityFactor = 5;

        
        private Rigidbody rb;
        private void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void Start()
        {
           // var rndr = GetComponentInChildren<Renderer>();
          //  rndr.material.mainTexture = myTexture;

            var cf = gameObject.AddComponent<ConstantForce>();
            var parent = transform.parent;
            var targetAim = parent ? parent.rotation * target : target;

            cf.force = (targetAim - transform.position).normalized * gravityFactor;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            var parent = transform.parent;
            var targetAim = parent ? parent.rotation * target : target;
            Gizmos.DrawLine(transform.position, targetAim);

            Gizmos.color = Color.green*.7f;
            var cldr = GetComponentInChildren<Collider>();
            Gizmos.DrawCube(transform.position, cldr.bounds.size);
        }

        private void FixedUpdate()
        {
            rb.velocity = Mathf.Min(maxSpeed,rb.velocity.magnitude) * rb.velocity.normalized;
        }


        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Raise("RegisterItem");
        }

        private void OnDisable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Raise("UnregisterItem");
        }
    }

}