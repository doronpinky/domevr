﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class GarbageCan : MonoBehaviour
    {
        public const string CollectEventName = "CollectGarbage";


        public Transform targetToFollow;
        public string tagToInteractWith;
        public Dweiss.EventVector3 onSuccess;
        public GameObject successPrefab;

        public Renderer visual;
        private float amountOfYearsToReduceOnSuccess = .5f;
        private const string SuccessEventName = "GarbageCollectedSuccess";
        //private const string SuccessEventName = "YearsToReduce";

        private Transform t;
        private Transform camT;
        void Awake()
        {
            t = transform;
            camT = Camera.main.transform;
        }
        private float minHightFromFloor = 0f;


        private string keyIndexForScoreIncreamentCheat = "0";
        private void Start()
        {
            if(string.IsNullOrEmpty(tagToInteractWith) == false)
            {
                keyIndexForScoreIncreamentCheat = tagToInteractWith.Substring(tagToInteractWith.Length - 1);
            }
        }
        void FixedUpdate()
        {
            t.position = new Vector3(targetToFollow.position.x, 
                Mathf.Max(minHightFromFloor, targetToFollow.position.y), targetToFollow.position.z);
            t.LookAt(camT);
        }

        private void Update()
        {
            if(visual != null)
                visual.transform.position = (2.7f +2.7f - Mathf.Min(2.7f,t.position.magnitude))* t.position.normalized;

            if(Input.GetKey(KeyCode.Tab) && Input.GetKeyDown(keyIndexForScoreIncreamentCheat))
            {
                AddScore();
            }

        }

        private Coroutine stopShow;
        public float seondsToShow = 7;
        public void SwitchShowRenderer()
        {

            visual.gameObject.SetActive(true);
            visual.material.color = GetColor(this.tagToInteractWith);
            //visual.gameObject.SetActive(!visual.gameObject.activeSelf);
            if(stopShow != null) StopCoroutine(stopShow);
            this.WaitForSeconds(seondsToShow, () => { visual.gameObject.SetActive(false); });
        }

       
        public static Color GetColor(string tagToInteractWith)
        {
            int index = -1;
            try
            {
                var lastChar = tagToInteractWith[tagToInteractWith.Length - 1];
                index = int.Parse("" + lastChar);
                return GetColor(index);
            }catch(System.Exception e)
            {
                throw new System.ArgumentOutOfRangeException("Error color on " + tagToInteractWith + " - " + index + e);
            }
        }
        //Setup order:
        //red metal.
        //brown bio
        //green glass
        //yellow paper
        // blue plastic
        public static Color GetColor(int index) { 
            switch (index)
            {
                case 1: return Color.red;
                case 2: return new Color(139f/255f, 69f / 255f, 19f / 255f);
                case 3: return Color.green;
                case 4: return Color.yellow;
                case 5: return Color.cyan;
                default:throw new System.ArgumentOutOfRangeException("Invalid input for color of garbage can index " + index);
            }
        }


        //private void OnDrawGizmos()
        //{
        //    Gizmos.color = Color.green;
        //    var cldr = GetComponentInChildren<Collider>();
        //    Gizmos.DrawWireCube(transform.position, cldr.transform.localScale);
        //}
        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == tagToInteractWith)
            {
                Destroy(other.gameObject);

                onSuccess.Invoke(other.transform.position);
                AddScore();


            }
        }

        public void AddScore()
        {
            ScoreSystem.S.DecreaseYearCount(amountOfYearsToReduceOnSuccess);
            Dweiss.Msg.MsgSystem.Raise(SuccessEventName);
            Dweiss.Msg.MsgSystem.MsgStr.Raise(CollectEventName, tagToInteractWith);
        }
    }
}