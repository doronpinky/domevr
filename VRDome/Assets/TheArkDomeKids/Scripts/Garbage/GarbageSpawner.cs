﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Animations;

namespace DomeVR
{
    public class GarbageSpawner : MonoBehaviour
    {

        public const string ThrowEventName = "ThrowGarbage";

        public float sizeOfGarbage = .5f;

        public GameObject videoPrefab;
        public AudioSource aSource;
        public GarbageItem garbageItemPrefab;
        public float fallRadius = 2.7f;
        //public float chanceOfRaiseSoundEffect = 1f;
        public float waitInsideGroupThrow = 0f;

        public FallSequence[] sequence;
        public GarbageGroup[] garbagesTypes;
        public AudioEffects[] audioEffects;
        public Videos[] vids;
        private Videos[] readyVids;

        private Vector3 NotActiveVideoPosition = new Vector3(0, 1111, 0);

        public bool _stopThrow = false;
        public bool StopThrow { get { return _stopThrow; } set { _stopThrow = value; } }

        public Dweiss.EventEmpty onAllItemsClear;
        private int itemCount;


        
        [System.Serializable]
        public class AudioEffects
        {
            public AudioClip[] clips;
        }


        [System.Serializable]
        public class GarbageGroup
        {
            public GameObject[] visualPrefab;
            public string tag;
        }




        [System.Serializable]
        public class Videos
        {
            public string videoName;
            public float timeToThrow = 1f;
            internal VideoEvents plyr;
            //public VideoEvents Player { }
            // public float movieLength = 5f;
        }

        [System.Serializable]
        public class FallSequence
        {
            public float waitForActivate = 3;
            public int fallCountMax = 1;
            public int repeatCount = 3;

            public FallSequence()
            {
                waitForActivate = 5;
                fallCountMax = 1;
                repeatCount = 3;
            }
        }

        private VideoEvents veActive;
        private bool videoPlaying;
        private Vector3[] positionSequence;

        private void Awake()
        {
            onAllItemsClear.AddListener(() => { Debug.Log("Items cleared from screen"); });
        }
        IEnumerator Start()
        {
            var vidGroup = new GameObject("VideoPlayers").transform;
            vidGroup.SetParent(transform);

            int numOfAngles = 5;
            positionSequence = new Vector3[numOfAngles];
            for (int i = 0; i < positionSequence.Length; i++)
            {
                positionSequence[i] = Quaternion.Euler(0, i * (360f/ numOfAngles), 0) * Vector3.forward * fallRadius;
            }

            StartCoroutine(RunFallSequence());

            vids.Shuffle();
            for (int i = 0; i < vids.Length; i++)
            {

                var newVid = Instantiate(videoPrefab);
                newVid.transform.parent = vidGroup;
                newVid.transform.SetPositionAndRotation(NotActiveVideoPosition, vidGroup.rotation);

                var vLoader = newVid.GetComponent<VideoLoadFromAssets>();
                vLoader.videoName = vids[i].videoName;

                vids[i] = new Videos() { plyr = newVid.GetComponentInChildren<VideoEvents>() , timeToThrow = vids[i].timeToThrow, videoName = vids[i].videoName};

                vids[i].plyr.onStopVidEvent += OnVideoStop;
                readyVids = new Videos[i + 1];
                for (int j = 0; j < i + 1; j++)
                {
                    readyVids[j] = vids[j];
                }
                yield return new WaitForSeconds(3);
            }



        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow * .5f;
            Gizmos.DrawWireSphere(Vector3.zero, fallRadius);
        }
        private IEnumerator RunFallSequence()
        {

            FallSequence seq;
            for (int i = 0; i < sequence.Length; ++i)
            {
                seq = sequence[i];
                for (int r = 0; r < seq.repeatCount; r++)
                {
                    //yield return new WaitForSeconds(seq.waitForActivate);//Wait before activate movie

                    //var nextMovie = readyVids[Random.Range(0, readyVids.Length)];
                    //veActive = nextMovie.plyr;
                    //videoPlaying = true;
                    //PlayMovie(nextMovie.plyr);
                    //yield return new WaitForSeconds(nextMovie.timeToThrow);//Wait before activate movie

                    //yield return CreateGarbage(seq);
                    //yield return new WaitWhile(() => { return videoPlaying; });
                    yield return ThrowEvent(seq);
                }

            }

            seq = sequence[sequence.Length - 1];
            while (true)
            {
                yield return ThrowEvent(seq);
            }
        }

        private IEnumerator ThrowEvent(FallSequence seq)
        {
            yield return new WaitForSeconds(seq.waitForActivate);//Wait before activate movie


            yield return new WaitWhile(() => StopThrow);

            var nextMovie = readyVids[Random.Range(0, readyVids.Length)];
            veActive = nextMovie.plyr;
            videoPlaying = true;
            PlayMovie(nextMovie.plyr);
            yield return new WaitForSeconds(nextMovie.timeToThrow);//Wait before activate movie

            yield return CreateGarbage(seq);
            yield return new WaitWhile(() => { return videoPlaying; });
            //yield return new WaitForSeconds(nextMovie.movieLength - nextMovie.timeToThrow);
        }

        private void OnVideoStop(VideoEvents vp)
        {
            if (vp == veActive)
                videoPlaying = false;
        }
        private void PlayMovie(VideoEvents toPlay)
        {
            for (int i = 0; i < readyVids.Length; i++)
            {
                if (readyVids[i].plyr == toPlay)
                {
                    readyVids[i].plyr.transform.position = Vector3.zero;
                    readyVids[i].plyr.PlayWithEvent();

                }
                else
                {
                    readyVids[i].plyr.transform.position = NotActiveVideoPosition;
                    readyVids[i].plyr.Stop();
                }
            }
        }

        private void TryPlaySound(int fallCount)
        {
            if (fallCount == 1 || Random.value < .5f)
            //if (Random.value < chanceOfRaiseSoundEffect)
            {
                if (fallCount > audioEffects.Length)
                {
                    fallCount = audioEffects.Length;
                }

                var audioClips = audioEffects[fallCount - 1].clips;
                aSource.clip = audioClips[0];
                audioClips.CycleItems();
                aSource.Play();

            }
        }
        public Dweiss.EventEmpty onThrow;
        private IEnumerator CreateGarbage(FallSequence seq)
        {
            onThrow.Invoke();
            var fallCount = seq.fallCountMax;// <= 1 ? 1 : Random.Range(seq.fallCountMax/2, seq.fallCountMax);
            //positionSequence.Shuffle();
            garbagesTypes.Shuffle();
             TryPlaySound(fallCount);

            int seqStartPos = Random.Range(0, positionSequence.Length);
            for (int j = 0; j < fallCount; j++)
            {
                var group = garbagesTypes[j % garbagesTypes.Length];
                var garbage = GetNewGarbage(group);// Instantiate(garbagePrefabs[j].gameObject, transform);

                Vector3 fallTarget = Quaternion.Euler(0, Random.Range(-7,7),0)* positionSequence[(j+ seqStartPos) % positionSequence.Length];
                garbage.transform.position = transform.position;// + fallTarget;

                var gi = garbage.GetComponent<GarbageItem>();
                gi.target = fallTarget;
                gi.maxSpeed = gi.maxSpeed * (1f+j*0.05f);
                gi.gravityFactor = gi.gravityFactor * (1f + j * 0.05f);

                Dweiss.Msg.MsgSystem.MsgStr.Raise(ThrowEventName, group.tag);

                yield return new WaitForSeconds(waitInsideGroupThrow);
            }

        }
        private GameObject GetNewGarbage(GarbageGroup group)
        {
            var garbage = Instantiate(garbageItemPrefab.gameObject, transform);
            //var nextGarbagePref = group.visualPrefab[Random.Range(0, group.visualPrefab.Length)];
            var nextGarbagePref = group.visualPrefab[0]; group.visualPrefab.CycleItems();

            var visualGarbage = Instantiate(nextGarbagePref, garbage.transform);
            visualGarbage.transform.localScale = Vector3.one * .3f;
            garbage.transform.localScale = Vector3.one * sizeOfGarbage;
            garbage.tag = group.tag;
            return garbage;
        }

        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("RegisterItem", ItemCreated);
            Dweiss.Msg.MsgSystem.MsgStr.Register("UnregisterItem", ItemDestroyed);
        }

        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Register("RegisterItem", ItemCreated);
                Dweiss.Msg.MsgSystem.MsgStr.Register("UnregisterItem", ItemDestroyed);
            }

        }
        private void ItemCreated()
        {
            ++itemCount;
        }

        private void ItemDestroyed()
        {
            --itemCount;
            if(itemCount == 0)
            {
                onAllItemsClear.Invoke();
                Dweiss.Msg.MsgSystem.MsgStr.Raise("AllItemsClear");
            }
        }

      
    }
}