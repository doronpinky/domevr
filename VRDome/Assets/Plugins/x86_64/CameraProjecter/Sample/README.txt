Thank you for supporting Camera Projecter!

There are two example scenes here. 
- Grid for basic projection concept
- Sky for walkable FPC using Camera projecter.

Note that to run the demo properly you need to import Unity's standard assets: 

- Assets->Import Package->Characters
- Assets->Import Package->Environment

The Final Standard Assets setup should follows (... means all its files & folder):

+ Standard Assets/
|--+ Characters/
|  |--+ FirstPersonCharacter/
|  |  |-- ...
|--+ CrossPlatormInput/
|  |-- ...
|--+ Environment/
|  |--+ SpeedTree/
|  |--+ TerrainAssets/
|  |  |-- ...
|--+ Utility/
|  |-- ...