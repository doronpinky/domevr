using UnityEngine;
using ProjecterLib;
using UnityEngine.UI;
using System;


public class SampleProjecterDriver : MonoBehaviour {

    CameraProjecter main;
    Camera cam;

    public float fovSpeed = 10;
    public Text projectionTxt;
    public Text fovTxt;

    int lowLevel = 1;
    int highLevel = 6;

    void UpdateFOV (float increment)
    {
        main.fieldOfView = Mathf.Clamp(main.fieldOfView + increment, 1, 359);
        if (!main.VR)
            cam.fieldOfView = main.fieldOfView * 0.5f;
        fovTxt.text = (main.enabled ? main.fieldOfView : cam.fieldOfView).ToString("0°");
    }

    void OnEnable ()
    {
        var names = QualitySettings.names;
        lowLevel = Mathf.Max(Array.IndexOf(names, "Fast"), 0);
        highLevel = Array.IndexOf(names, "Beautiful");
        if (highLevel < 0)
            highLevel = names.Length - 1;
    }

    void UpdateProjection ()
    {
        if (main.enabled)
            projectionTxt.text = main.projection.ToString() + " Projection" + 
                (QualitySettings.GetQualityLevel() == lowLevel ? " (Fast)" : "");
        else
            projectionTxt.text = "Perspective Projection (CP is disabled)";
    }

    void Update ()
    {
       if (Input.anyKey) {
            if (Input.GetKey(KeyCode.PageDown)) 
                UpdateFOV(-fovSpeed * Time.deltaTime);
            else if (Input.GetKey(KeyCode.PageUp))
                UpdateFOV(fovSpeed * Time.deltaTime);
       }

       if (Input.anyKeyDown) {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                main.enabled = false;
            else if (Input.GetKeyDown(KeyCode.Alpha9))
                QualitySettings.SetQualityLevel(QualitySettings.GetQualityLevel() == lowLevel ? highLevel : lowLevel);
            else if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                cam.stereoTargetEye = cam.stereoTargetEye == StereoTargetEyeMask.None ? StereoTargetEyeMask.Both : StereoTargetEyeMask.None;
            }
            else if (Input.GetKeyDown(KeyCode.R))
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            else
            {
                if (Input.GetKeyDown(KeyCode.Alpha2))
                    main.projection = ProjecterMode.Stereographic;
                else if (Input.GetKeyDown(KeyCode.Alpha3))
                    main.projection = ProjecterMode.Equidistant;
                else if (Input.GetKeyDown(KeyCode.Alpha4))
                    main.projection = ProjecterMode.Equisolid;
                else if (Input.GetKeyDown(KeyCode.Alpha5))
                    main.projection = ProjecterMode.Orthographic;
                else if (Input.GetKeyDown(KeyCode.Alpha6))
                    main.projection = ProjecterMode.Geographic;
                else if (Input.GetKeyDown(KeyCode.Alpha7))
                    main.projection = ProjecterMode.Miller;
                else if (Input.GetKeyDown(KeyCode.Alpha8))
                    main.projection = ProjecterMode.Cassini;
                else
                    return;
                main.enabled = true;
            }
            UpdateProjection();
            UpdateFOV(0);
       }
    } 
    
    void Start ()
    {
        cam = GetComponent<Camera>();
        main = GetComponent<CameraProjecter>();      
        UpdateFOV(0);
        UpdateProjection();  
    }
}