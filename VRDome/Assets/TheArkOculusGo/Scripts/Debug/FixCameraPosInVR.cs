﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixCameraPosInVR : MonoBehaviour
{
    private Transform t, child;
    void Start()
    {
        t = transform;
        child = t.GetChild(0);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        t.localPosition = -child.localPosition;
    }
}
