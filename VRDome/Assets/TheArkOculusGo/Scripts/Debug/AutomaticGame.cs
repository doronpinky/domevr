﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using TMPro;
using DomeVR;
using System.Linq;

namespace Rooftop.OculusGo
{
    public class AutomaticGame : MonoBehaviour
    {

        public TimeForEventInScene[] scenecsEvent;

        [System.Serializable]
        public class TimeForEventInScene
        {
            public int sceneNum;
            public float waitTime;
            public Dweiss.EventEmpty action;
        }


        public void RunAutoBonusRound()
        {
            StartCoroutine(BonusRoundActive());
        }

        public float waitAfterAnswerPopup = 7;
        private IEnumerator BonusRoundActive()
        {
            yield return WaitUntil(() => GameObject.FindObjectOfType<BonusRoundNewGui>() == null);
            var qState = GameObject.FindObjectOfType<QuestionState>();

            for (int i=0;  qState != null; ++i)
            {
                var rollateRound = GameObject.FindObjectOfType<BonusRoundNewGui>();
                yield return WaitUntil(() => rollateRound.IsRunning == false);
                yield return new WaitForSeconds(1);

                Debug.Log("Auto click in rollate");
                RaiseClick();

                var showAnswers = GameObject.FindObjectOfType<ShowAnswersSeqEvent>();
                yield return WaitUntil(() => showAnswers.IsRunning == false);
                yield return new WaitForSeconds(waitAfterAnswerPopup);

                Debug.Log("Auto answer");
                GuiClickAnswer( i == 0);
                //yield return new WaitForSeconds(1);
            }
        }

        private IEnumerator WaitUntil(System.Func<bool> predicat, float interval = 1)
        {
            while (predicat())
            {
                yield return new WaitForSeconds(interval);
            }
        }

        private void RaiseClick() {
            Dweiss.Msg.MsgSystem.MsgStr.Raise("User0Click");
        }

        private void GuiClickAnswer(bool correct)
        {
            var qState = GameObject.FindObjectOfType<QuestionState>();
            var qTrivia = GameObject.FindObjectOfType<QuestionTriviaUI>();
            TextMeshPro[] allText;
            if (qTrivia) allText = qTrivia.GetComponentsInChildren<TextMeshPro>();
            else
            {
                var qNewTrivia = GameObject.FindObjectOfType<GuiControlHelper>();
                allText = qNewTrivia.GetComponentsInChildren<TextMeshPro>();
            }

            var correctAnswerTextBox = allText.First(a => a.text == qState.CurrentQ.CorrectAnswer);
            var firstAnswerIncorect = qState.CurrentQ.Answers.First(a => a != qState.CurrentQ.CorrectAnswer);
            var incorrectAnswer = allText.First(a => a.text == firstAnswerIncorect);
            GameObject.FindObjectOfType<ShowAnswersSeqEvent>().AnswerSelected(correct? correctAnswerTextBox : incorrectAnswer);
        }

        void Start()
        {
            Debug.Log("Running auto game flow!!");


            GameFlow.S.onSceneChanged.AddListener(SceneChanged);

            SceneChanged(null);
        }

        private void OnDestroy()
        {
            DomeVR.GameFlow.S.onSceneChanged.RemoveListener(SceneChanged);
        }

        private void SceneChanged(string sceneName)
        {
            var sceneIndex = DomeVR.GameFlow.S.CurrentSceneIndex;
            StopAllCoroutines();
            int count = 0;
            for (int i = 0; i < scenecsEvent.Length; i++)
            {
                var se = scenecsEvent[i];
                if (se.sceneNum == sceneIndex)
                {
                    this.WaitForSeconds(se.waitTime, () =>
                    {
                        Debug.LogFormat("<color=blue>AutoGameCheat  #{0} sceneNumber {1}</color>", i, se.sceneNum);
                        se.action.Invoke();
                    });
                    count++;
                }
            }
            Debug.LogFormat("<size=15><color=yellow>Automatic game scene {0} cheats {1}</color> </size>", sceneIndex, count);
        }

    }
}