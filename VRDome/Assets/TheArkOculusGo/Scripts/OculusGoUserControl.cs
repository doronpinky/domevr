﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OculusGo
{
    public class OculusGoUserControl : MonoBehaviour
    {

        public Dweiss.EventEmpty onClick;


        private Transform cam;
        private Transform t;

        void Start()
        {
            cam = Camera.main.transform;
            t = transform;
        }

        void Update()
        {
            CheckTouch();
        }



        private void UpdateDir()
        {
            t.position = cam.position;
            t.rotation = cam.rotation;
        }


        private int prevTouchCount;
        private void CheckTouch() { 
            if(prevTouchCount == 0 && Input.touchCount > 0)
            {
                onClick.Invoke();
            }

            prevTouchCount = Input.touchCount;
        }
    }
}