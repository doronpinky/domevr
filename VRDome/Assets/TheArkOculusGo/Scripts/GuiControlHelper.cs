﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Dweiss;
using System.Linq;

    
namespace Rooftop.OculusGo
{
    public class GuiControlHelper : MonoBehaviour
    {
        public bool debug;

        [SerializeField]private GuiControlHelper chainedGuiCntrl;


        [Header("Blue")]
        public GameObject[] answers;
        public GameObject question, blueScore, blueTimer;
        public GameObject blueLines;

        private FadeAnimation[] blueElements;

        [Header("Yellow")]
        public GameObject videoBox;
        public GameObject yellowScore, yellowTimer;
        public GameObject yellowLines;
        private FadeAnimation[] yellowElements;



        [Header("Others")]
        public Clock[] clock;
        public AudioSource narration;


        [Header("Score")]

        private int currentScore = 0;

        public float waitBetweenFadeItems = 1f;


        public void InitAnswers()
        {
            ShowAnswers(null);

            //Multi call in show answers
            //if(chainedGuiCntrl != null) chainedGuiCntrl.InitAnswers();
        }
        public void ShowAnswers(string[] answrs)
        {
            for (int i = 0; i < answers.Length; i++)
            {
                answers[i].GetComponentInChildren<TextMeshPro>(true).text = answrs == null? "" : answrs[i];
                if(answrs != null) answers[i].GetComponentInChildren<DomeVR.ClickAbleButton>(true).ResetClicks();
            }

            if (chainedGuiCntrl != null) chainedGuiCntrl.ShowAnswers(answrs);
        }
        public void ShowQuestion(string q)
        {
            question.GetComponentInChildren<TextMeshPro>(true).text = q;

            if (chainedGuiCntrl != null) chainedGuiCntrl.ShowQuestion(q);
        }

        public void ShowVideo(string name)
        {
            var movieWithSuffix = PreloadStreamingAsset.S.AddSuffixToMovie(name);
            movieWithSuffix = PreloadStreamingAsset.S.GetFullPath(movieWithSuffix);
            videoBox.SetActive(true);
            videoBox.GetComponentInChildren<VideoEvents>(true).PlayWithEvent(movieWithSuffix);

            if (chainedGuiCntrl != null) chainedGuiCntrl.ShowVideo(name);
        }

        public void PlayNarration(AudioClip c)
        {
            if (narration)
            {
                narration.clip = c;
                narration.Play();
            }
            if (chainedGuiCntrl != null) chainedGuiCntrl.PlayNarration(c);
        }

        public void StopTimers()
        {
            try
            {
                for (int i = 0; i < clock.Length; i++)
                {
                    clock[i].Stop();
                }

                if (chainedGuiCntrl != null) chainedGuiCntrl.StopTimers();
            }catch(System.Exception e)
            {
                Debug.LogError(name + " failed to stop timers " + e);
            }
        }
        public void SetTimer(int seconds)
        {
            for (int i = 0; i < clock.Length; i++)
            {
                clock[i].Reset(seconds);
            }

            if (chainedGuiCntrl != null) chainedGuiCntrl.SetTimer(seconds);
        }

        public void SetScore(int score)
        {
            currentScore = score;// currentYears;
            UpdateScore();

            if (chainedGuiCntrl != null) chainedGuiCntrl.SetScore(score);
        }

        private void UpdateScore() { 
            blueScore.GetComponentInChildren<TextMeshPro>(true).text = currentScore.ToString();
            yellowScore.GetComponentInChildren<TextMeshPro>(true).text = currentScore.ToString();
        }

        private const int DefaultAnswerFrameIndex = 0, CorrectAnswerFrameIndex = 1, WrongAnswerFrameIndex = 2, NoAnswer = -1;
        public void ShowCorrectAnswer(string answerToLook)
        {
            ShowAnswer(answerToLook, CorrectAnswerFrameIndex); 

            //Called in ShowAnswers
           // if (chainedGuiCntrl != null) chainedGuiCntrl.ShowCorrectAnswer(answerToLook);
        }
        public void ShowWrongAnswer(string answerToLook)
        {
            ShowAnswer(answerToLook, WrongAnswerFrameIndex);

            //Called in ShowAnswers
            // if (chainedGuiCntrl != null) chainedGuiCntrl.ShowWrongAnswer(answerToLook);
        }
        public void HideAllAnswersFrames()
        {
            ChangeAllAnswersFrames(NoAnswer);

            if (chainedGuiCntrl != null) chainedGuiCntrl.HideAllAnswersFrames();


        }
        public void ShowRegularAnswerFrames()
        {
            ChangeAllAnswersFrames(DefaultAnswerFrameIndex);

            if (chainedGuiCntrl != null) chainedGuiCntrl.ShowRegularAnswerFrames();
        }

        private void ChangeAllAnswersFrames(int id) { 
            for (int i = 0; i < answers.Length; i++)
            {
                answers[i].GetComponent<ShowOnlyOne>().ShowOnly(id);
                answers[i].GetComponentInChildren<TextMeshPro>(true).SetActive(id != NoAnswer);
            }
        }

        public void ShowAnswer(string answerToFind, int id)
        {
            for (int i = 0; i < answers.Length; i++)
            {
                if (answers[i].GetComponentInChildren<TextMeshPro>(true).text == answerToFind)
                {
                    answers[i].GetComponentInChildren<TextMeshPro>(true).gameObject.SetActive(true);
                    answers[i].GetComponent<ShowOnlyOne>().ShowOnly(id);
                    break;
                }
            }

            if (chainedGuiCntrl != null) chainedGuiCntrl.ShowAnswer(answerToFind, id);
        }
        private void Awake()
        {
            var fadeAnim = new List<FadeAnimation>();
            Add(fadeAnim, answers);
            Add(fadeAnim, question, blueScore, blueTimer, blueLines);
            blueElements = fadeAnim.ToArray();


            fadeAnim = new List<FadeAnimation>();
            Add(fadeAnim, videoBox, yellowScore, yellowTimer, yellowLines);
            yellowElements = fadeAnim.ToArray();
        }

        private void Add<T>(List<T> faders, params GameObject[] toAdd)
        {
            for (int i = 0; i < toAdd.Length; i++)
            {
                faders.Add(toAdd[i].GetComponent<T>());
            }
        }

        private void Animate(FadeAnimation[] elements, bool fadeIn, bool skipElementsAlreadyAtState = false)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                //if (toAvoid.IndexOf(elements[i].gameObject) != -1) continue;
                if (skipElementsAlreadyAtState) {
                    if (fadeIn == false && elements[i].IsFade == FadeAnimation.FadeState.StartAlpha) continue;
                    if (fadeIn && elements[i].IsFade == FadeAnimation.FadeState.EndAlpha) continue;
                }


                if(fadeIn)
                    elements[i].StartAnimation();
                else
                    elements[i].StartReverseAnimation();
            }
        }


        public void FadeInYellowFrame()
        {
            FadeInYellow(videoBox);

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeInYellowFrame();
        }
        public void FadeInRestOfYellow()
        {
            FadeInYellow();
            //call inside fadeinyellow
            //if (chainedGuiCntrl != null) chainedGuiCntrl.FadeInRestOfYellow();
        }


        public void HideAll()
        {
            for (int i = 0; i < yellowElements.Length; i++)
            {
                yellowElements[i].SetActive(false);
            }
            for (int i = 0; i < blueElements.Length; i++)
            {
                blueElements[i].SetActive(false);
            }
            if (narration) narration.SetActive(false);
            for (int i = 0; i < clock.Length; i++)
            {
                clock[i].SetActive(false);
            }

            if (chainedGuiCntrl != null) chainedGuiCntrl.HideAll();
        }

        public void HideAllGuiButLines()
        {
            Animate(yellowElements, false, yellowLines);
            Animate(blueElements, false, blueLines);
            
            if(narration) narration.SetActive(false);
            for (int i = 0; i < clock.Length; i++)
            {
                clock[i].Stop();
            }

            if (chainedGuiCntrl != null) chainedGuiCntrl.HideAllGuiButLines();
        }

        private void FadeInYellow(params GameObject[] toAvoid)
        {
            for(int i=0; i < yellowElements.Length; ++i)
            {
                if (toAvoid.Contains(yellowElements[i].gameObject)) continue;

                yellowElements[i].StartAnimation();
            }

            //if (chainedGuiCntrl != null) chainedGuiCntrl.FadeInYellow(toAvoid);
        }

        [ContextMenu("FadeInYellow")]
        public void FadeInYellow()
        {
            if (debug) Debug.Log("FadeInYellow");
            Animate(yellowElements, true);

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeInYellow();
        }

        

        [ContextMenu("FadeOutBlueInYellow")]
        public void FadeOutBlueInYellowFrame()
        {
            if (debug) Debug.Log("FadeOutBlueInYellow");
            StopAllCoroutines();
            Animate(blueElements, false);
            this.WaitForSeconds(waitBetweenFadeItems, () =>
            {
                FadeInYellow(videoBox);
            });

            if (chainedGuiCntrl != null)
            {
                chainedGuiCntrl.FadeOutBlueInYellowFrame();
            }
        }

        [ContextMenu("FadeOutYellowInBlue")]
        public void FadeOutYellowInBlue()
        {
            if (debug) Debug.Log("FadeOutYellowInBlue");
            StopAllCoroutines();
            Animate(yellowElements, false);
            this.WaitForSeconds(waitBetweenFadeItems, () =>
            {
                ChangeAllAnswersFrames(DefaultAnswerFrameIndex);
                Animate(blueElements, true);
                //UpdateTimers();
                //UpdateScore();
            });

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeOutYellowInBlue();
        }

        [ContextMenu("FadeInBlue")]
        public void FadeInBlue()
        {
            if (debug) Debug.Log("FadeInBlue");
            Animate(blueElements, true);

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeInBlue();
        }

        [ContextMenu("FadeOutBlue")]
        public void FadeOutBlue()
        {
            if (debug) Debug.Log("FadeOutBlue");
            Animate(blueElements, false, false);

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeOutBlue();
        }

        [ContextMenu("FadeOutYellow")]
        public void FadeOutYellow()
        {
            if (debug) Debug.Log("FadeOutYellow");
            Animate(yellowElements, false, false);

            if (chainedGuiCntrl != null) chainedGuiCntrl.FadeOutYellow();
        }
    }
}