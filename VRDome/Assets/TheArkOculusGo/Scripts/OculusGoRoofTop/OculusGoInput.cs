﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RoofTop.OculusGo
{
    public class OculusGoInput : MonoBehaviour
    {
        public float lengthOfResetClick = 7f;
        public float lengthOfSpecialClick = 3;
        public Dweiss.EventFloat onTriggerUp;
        public Dweiss.EventEmpty onTriggerDown;
        public Dweiss.EventEmpty onSpecialTimeClick, onResetClick;


        private float timeOfDown;

        private void Start()
        {
            onSpecialTimeClick.AddListener(GameObject.FindObjectOfType<DomeVR.GameFlow>().Continue);
            onResetClick.AddListener(GameObject.FindObjectOfType<DomeVR.GameFlow>().Restart);


#if OCULUS_GO 
            var overCntrls = GetComponentsInChildren<OVRControllerHelper>();
            var allJoysticks = GameObject.FindObjectsOfType<DomeVR.SingleJoystickControl>();
            foreach (var joy in allJoysticks)
            {
                if (joy.name.ToLower().Contains("oculus1"))
                {
                    onTriggerDown.AddListener(() => joy.IsPressed = true);
                    onTriggerUp.AddListener((f) => joy.IsPressed = false);
                    joy.GetComponent<Common.TrackTransform>().other = overCntrls[0].transform;
                }
                else if (joy.name.ToLower().Contains("oculus2"))
                {
                    onTriggerDown.AddListener(() => joy.IsPressed = true);
                    onTriggerUp.AddListener((f) => joy.IsPressed = false);
                    joy.GetComponent<Common.TrackTransform>().other = overCntrls[1].transform;
                }
                else
                {
                    joy.gameObject.SetActive(false);
                }
            }

#else
            Debug.LogError("Missing OCULUS_GO flag to run ");
#endif


            onTriggerDown.AddListener(() => { Dweiss.Msg.MsgSystem.MsgStr.Raise("User0Click"); });
        }

        private void Update()
        {
            CheckClick();
        }
        private void CheckClick()
        {
#if OCULUS_GO
            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
            {
                timeOfDown = Time.time;
                onTriggerDown.Invoke();
            }
            else if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
            {
                var timePass = (Time.time - timeOfDown);
                if ( timePass - Time.deltaTime < lengthOfSpecialClick && timePass >= lengthOfSpecialClick)
                {
                    onSpecialTimeClick.Invoke();
                }
                if (timePass - Time.deltaTime < lengthOfResetClick && timePass >= lengthOfResetClick)
                {
                    onResetClick.Invoke();
                }
                
            }
            else if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
            {
                onTriggerUp.Invoke(Time.time - timeOfDown);
            }
#else

#endif
        }


    }
}