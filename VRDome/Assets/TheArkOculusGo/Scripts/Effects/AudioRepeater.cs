﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRepeater : MonoBehaviour
{
    public float waitBetweenPlay = 1;
    private AudioSource aSource;
    private void Awake()
    {
        aSource = GetComponentInChildren<AudioSource>();
    }
    public void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(RepeatCoroutine());
    }

    IEnumerator RepeatCoroutine()
    {
        while (true)
        {
            aSource.Stop();
            aSource.Play();
            yield return new WaitForSeconds(waitBetweenPlay);
        }
    }


}
