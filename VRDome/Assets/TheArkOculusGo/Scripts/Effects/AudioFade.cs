﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Dweiss
{
    public class AudioFade : MonoBehaviour
    {
        public float defaultLength = .5f;
        public float defaultVolume = 1;

        public bool dontFadeIfNotNeeded;

        private AudioSource aSource;
        public ActionOnEnable actionOnEnable;
        public enum ActionOnEnable
        {
            None,
            FadeIn,
            FadeOut
        }

        private void OnEnable()
        {
            if (actionOnEnable == ActionOnEnable.FadeIn) FadeIn();
            if (actionOnEnable == ActionOnEnable.FadeOut) FadeOut();
        }
        void Awake()
        {
            aSource = GetComponent<AudioSource>();
            //defaultVolume = aSource.volume;
        }

        [ContextMenu("FadeOut")]
        public void FadeOut() {
            if (dontFadeIfNotNeeded && (aSource.isPlaying == false || aSource.volume == 0)) return;

            FadeOut(defaultLength);
        }
        public void FadeOut(float length)
        {
            StopAllCoroutines();
            StartCoroutine(FadeVolume(defaultVolume, 0, length, null, () => aSource.Pause()));
        }



        [ContextMenu("FadeIn")]
        public void FadeIn()
        {
            if (dontFadeIfNotNeeded && aSource.isPlaying && aSource.volume > 0) return;
            FadeIn(defaultLength);
        }
        public void FadeIn(float length)
        {
            StopAllCoroutines();
            StartCoroutine(FadeVolume(0, defaultVolume, length, () => aSource.Play()));
        }

        private void SetVolume(float v)
        {
            aSource.volume = v;
        }
        IEnumerator FadeVolume(float vStart, float vEnd, float length, System.Action onStart = null, System.Action onEnd = null)
        {
            if (onStart != null) onStart.Invoke();
            SetVolume(vStart);
            

            float startTime = Time.time;
            float endTime = Time.time + length;
            float percent = 0;

            while (endTime > Time.time)
            {
                percent = (Time.time - startTime) / length;
                float alpha = Mathf.Lerp(vStart, vEnd, percent);

                SetVolume(alpha);

                yield return 0;
            }

            SetVolume(vEnd);

            if (onEnd != null) onEnd.Invoke();
        }
    }
}