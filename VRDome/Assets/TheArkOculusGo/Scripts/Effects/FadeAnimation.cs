﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FadeAnimation : MonoBehaviour
{
    public bool repeat;
    public float alphaStart = 0f, alphaEnd = 1f;
    public float startWaitTime = 0, activeTime = 1;

    public bool setRndrAndGoOnStart;
    public bool changeGameObjectOnAnimation = false;
    public bool changeRndrsOnAnimation = false;

    public string[] colorPropertyName = new string[] { "_Color", "_FaceColor" };
    private Renderer[][] rndrs;
    UnityEngine.UI.Image[] uiImgs = new UnityEngine.UI.Image[0];

    public float ActiveTime { get { return activeTime; } set { activeTime = value; } }

    private bool _inEndAnimation;
    public bool InEndAnimation { get { return _inEndAnimation; }private set { _inEndAnimation = value; } }

    [Header("Start anim events")]
    public Dweiss.EventEmpty onStartAlphaStart;
    public Dweiss.EventEmpty onReachAlphaEnd;

    [Header("Start reverse anim events")]
    public Dweiss.EventEmpty onStartAlphaEnd;
    public Dweiss.EventEmpty onReachAlphaStart;

    public FadeState IsFade { get; private set; }
    public enum FadeState
    {
        StartAlpha,
        Transition,
        EndAlpha
    }

    void Awake()
    {
        Init();
        onReachAlphaStart.AddListener(() => { IsFade = FadeState.StartAlpha; });
        onStartAlphaEnd.AddListener(() => { IsFade = FadeState.EndAlpha; });
        onStartAlphaStart.AddListener(() => { IsFade = FadeState.StartAlpha; });
        onReachAlphaEnd.AddListener(() => { IsFade = FadeState.EndAlpha; });

    }
    [ContextMenu("Init")]
    public void Init() {
        rndrs = new Renderer[colorPropertyName.Length][];
        for (int i = 0; i < colorPropertyName.Length; i++)
        {
            rndrs[i] = GetComponentsInChildren<Renderer>().Where(a => a.enabled && a.materials.Length > 0 && a.material.HasProperty(colorPropertyName[i])).ToArray();
        }
        ResetAlpha();
        
        uiImgs = GetComponentsInChildren<UnityEngine.UI.Image>().ToArray();

        if (setRndrAndGoOnStart)
        {
            if (changeRndrsOnAnimation) ChangeRndrs(false);
            if (changeGameObjectOnAnimation) gameObject.SetActive(false);
        }


    }

    [ContextMenu("RandomColor")]
    private void RandomColor()
    {
        SetColor(Random.Range(0f,1f));
        
    }

    private void ChangeRndrs(bool rndrEnabeld)
    {
        for (int i = 0; i < rndrs.Length; i++)
        {
            foreach (var rndr in rndrs[i])
            {
                rndr.enabled = rndrEnabeld;
            }
        }
        for (int i = 0; i < uiImgs.Length; i++)
        {
            uiImgs[i].enabled = rndrEnabeld;
        }
    }

    //private void OnEnable()
    //{
    //    StartAnimation();
    //}
    //private void OnDisable()
    //{
    //    ResetAlpha();
    //}

    [ContextMenu("Reset alpha")]
    public void ResetAlpha()
    {
        SetColor(alphaStart);
        IsFade = FadeState.StartAlpha;
        //onReachAlphaStart.Invoke();

        //TODO might cause logic issues on events
    }
    private void SetColor(float alpha) {
        for (int i = 0; i < rndrs.Length; i++)
        {
            foreach (var rndr in rndrs[i])
            {
                for (int j = 0; j < rndr.materials.Length; j++)
                {
                    Color clr = rndr.materials[j].GetColor(colorPropertyName[i]);
                    clr.a = alpha;
                    rndr.materials[j].SetColor(colorPropertyName[i], clr);
                }
            }
        }

        for (int i = 0; i < uiImgs.Length; i++)
        {
            Color clr = uiImgs[i].color;
            clr.a = alpha;
            uiImgs[i].color = clr;
        }

    }

    [ContextMenu("Start animation")]
    public void StartAnimation()
    {
        if (InEndAnimation) return;

        InEndAnimation = true;
        if (changeGameObjectOnAnimation) gameObject.SetActive(true);
        if (changeRndrsOnAnimation) ChangeRndrs(true);
        StopAllCoroutines();
        Debug.Log(name + " changeGameObjectOnAnimation " + changeGameObjectOnAnimation);
        //onStartAlphaStart, onStartAlphaEnd


        
        StartCoroutine(RunAnimation(alphaStart, alphaEnd, ()=> { onStartAlphaStart.Invoke(); }, () => { onReachAlphaEnd.Invoke(); }));
    }
    [ContextMenu("Start reverse animation")]
    public void StartReverseAnimation()
    {
        if (InEndAnimation == false) return;

        InEndAnimation = false;

        if (changeGameObjectOnAnimation) gameObject.SetActive(true);
        if (changeRndrsOnAnimation) ChangeRndrs(true);
        StopAllCoroutines();

        StartCoroutine(RunAnimation(alphaEnd, alphaStart
            , () => { onStartAlphaEnd.Invoke(); }
            , ()=> {
            if (changeRndrsOnAnimation) ChangeRndrs(false);
            if (changeGameObjectOnAnimation) gameObject.SetActive(false);
                onReachAlphaStart.Invoke();

            }));
    }

    IEnumerator RunAnimation(float aStart, float aEnd, System.Action onStart, System.Action onEnd)
    {
        if(onStart != null) onStart.Invoke();
        SetColor(aStart);

        if (startWaitTime >= 0)
        yield return new WaitForSeconds(startWaitTime);

        float startTime = Time.time;
        float endTime = Time.time + activeTime;
        float percent = 0;

        IsFade = FadeState.Transition;
        while (endTime > Time.time)
        {
            percent = (Time.time-startTime)/ activeTime;
            float alpha = Mathf.Lerp(aStart, aEnd, percent);

            SetColor(alpha);

            yield return 0;
        }

        SetColor(aEnd);

        if (onEnd != null) onEnd.Invoke();
        if (repeat) StartCoroutine(RunAnimation(aEnd, aStart, onEnd, onStart));
    }

}
