﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;

public class TriggerAnimations : MonoBehaviour
{
    public string[] triggers;

    private Animator[] anim;
    private void Awake()
    {
        anim = GetComponentsInChildren<Animator>();
        if(anim == null)
            anim = GetComponentsInParent<Animator>();
    }

    [ContextMenu("RunTrigger0")]
    private void RunTrigger0() {
        RaiseTriggerByIndex(0);
    }

    [ContextMenu("RunTrigger1")]
    private void RunTrigger1()
    {
        RaiseTriggerByIndex(1);
    }


    //public Material[] sdgArr;

    //[ContextMenu("Dup")]
    //private void Dup()
    //{
    //    var pos = new Vector3(0, 1, 0);
    //    //var euler = new Vector3(0, 0, 0);
    //    var eulerMul = new Vector3(0, 1, 0);
    //    //transform.localPosition = 0;
    //    for (int i = 0; i < 17; ++i)
    //    {
    //        var go = Instantiate(gameObject);
    //        go.transform.localPosition = pos;
    //        go.transform.eulerAngles = - i * (360f / 17f) * eulerMul;
    //        go.name = "SDG #" + i + " - " + (-i * (360f / 17f)).ToSingleDotStr();

    //        var rndr = go.transform.FindChild((a) => a.name == "SDGRender").GetComponent<Renderer>();
    //        rndr.material = sdgArr[i];
    //    }
    //}


    public void RaiseTrigger(string trigger)
    {
        for (int i = 0; i < anim.Length; i++)
        {
            anim[i].SetTrigger(trigger);
        }
    }

    public void RaiseTriggerByIndex(int triggerIndex)
    {
        for (int i = 0; i < triggers.Length; i++)
        {
            for (int j = 0; j < anim.Length; j++)
            {
                anim[j].ResetTrigger(triggers[i]);
            }
        }
        for (int i = 0; i < anim.Length; i++)
        {
            anim[i].SetTrigger(triggers[triggerIndex]);
        }
    }
}
