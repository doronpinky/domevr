﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneAPI : MonoBehaviour
{

    public void LoadScene(int nextSceneIndex)
    {
        Debug.Log("Loading next scene " + nextSceneIndex);
        SceneManager.LoadScene(nextSceneIndex);
    }
    public void LoadScene(string sceneName)
    {
        Debug.Log("Loading next scene " + sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void NextScene()
    {
        var nextSceneIndex = (SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings;
        Debug.Log("Loading next scene "+nextSceneIndex);
        SceneManager.LoadScene(nextSceneIndex);
    }

    public void RestartScene()
    {
        Debug.Log("Reloading current scene " + SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
