﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ScaleFromZero : MonoBehaviour
    {
        public float timeToScale = .2f;

        IEnumerator Start()
        {
            var t = transform;
            var endScale = t.localScale;
            t.localScale = Vector3.zero;
            var endT = Time.time + timeToScale;
            var startT = Time.time;
            while (Time.time < endT)
            {
                var percent = (Time.time - startT) / timeToScale;
                t.localScale = Vector3.Lerp(Vector3.zero, endScale, percent);
                yield return 0;
            }
            t.localScale = endScale;
        }

    }
}