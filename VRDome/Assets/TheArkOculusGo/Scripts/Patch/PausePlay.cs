﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoofTop.OculusGo
{
    public class PausePlay : MonoBehaviour
    {
        public void PauseFor(float time)
        {
            Pause();
            this.WaitForSecondsRealTime(time, () => { Continue(); });
        }
        public void CallGarbageCollector()
        {
            System.GC.Collect();
        }
        public void Pause()
        {
            DomeVR.GameFlow.S.Pause();   
        }
        public void Continue()
        {
            DomeVR.GameFlow.S.Continue();
        }
    }
}