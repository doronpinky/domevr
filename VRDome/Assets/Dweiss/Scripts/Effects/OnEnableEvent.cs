﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss { 
public class OnEnableEvent : MonoBehaviour {
        public UnityEngine.Events.UnityEvent onAwake, onEnable, onDisable;
        public Dweiss.EventBool onChangeEvent, onReverseChangeEvent;


        private void Awake()
        {
            onAwake.Invoke();
        }

        void OnEnable() {
            onEnable.Invoke();
            onChangeEvent.Invoke(true);
            onReverseChangeEvent.Invoke(false);
        }

        void OnDisable()
        {
            onDisable.Invoke();
            onChangeEvent.Invoke(false);
            onReverseChangeEvent.Invoke(true);
        }

    }
}