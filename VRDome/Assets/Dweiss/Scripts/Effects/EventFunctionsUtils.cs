﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class EventFunctionsUtils : MonoBehaviour
    {
       public void FixLocalYRotate(float degrees)
        {
            var euler = transform.localRotation.eulerAngles;
            transform.localRotation = Quaternion.Euler(euler.x, degrees, euler.z);
        }
        public void AddLocalYRotate(float degrees)
        {
            var euler = transform.localRotation.eulerAngles;
            transform.localRotation = Quaternion.Euler(euler.x, euler.y+degrees, euler.z);
        }
    }
}