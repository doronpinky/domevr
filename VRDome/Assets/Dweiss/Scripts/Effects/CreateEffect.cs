﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class CreateEffect : MonoBehaviour
    {
        public GameObject prefab;
        
        public float ttl;
        public bool destroyOld = true;
        private GameObject current;

        public void StartEffect()
        {
            if (destroyOld) StopEffect();
            current = Instantiate(prefab, transform);
            current.transform.localPosition = Vector3.zero;
            current.transform.localRotation = Quaternion.identity;
            if (ttl > 0) Destroy(current, ttl);
        }

        public void StartEffect(Collider cldr) { StartEffect(cldr.transform.position); }
        public void StartEffect(Transform t) { StartEffect(t.position); }
        public void StartEffect(GameObject go) { StartEffect(go.transform.position); }

        public void StartEffect(Vector3 absPosition)
        {
            if (destroyOld) StopEffect();
            current = Instantiate(prefab, transform);
            current.transform.position = absPosition;
            //current.transform.localRotation = Quaternion.identity;
            if (ttl > 0) Destroy(current, ttl);
        }

        public void StopEffect()
        {
            if (current)
            {
                Destroy(current);
                current = null;
            }
        }

        private void OnDisable()
        {
            StopEffect();
        }
    }
}