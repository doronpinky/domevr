﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;


namespace Dweiss
{
    public class OnEnableActivateRandomGameObject : MonoBehaviour
    {
        //public UnityEngine.Events.UnityEvent u;
        public GameObject[] randomEnable;
        
        void OnEnable()
        {
            randomEnable[Random.Range(0, randomEnable.Length)].SetActive(true);

        }

    }
}