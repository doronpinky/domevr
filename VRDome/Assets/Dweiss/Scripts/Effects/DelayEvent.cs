﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class DelayEvent : MonoBehaviour
    {
        [Tooltip("Value < 0 doesn't run on start")]
        public float runOnEnableWithDelay = -1;

        public EventEmpty afterDelay;

        private void OnEnable()
        {
            if(runOnEnableWithDelay >= 0)
            {
                Raise(runOnEnableWithDelay);
            }
        }

        public void Raise(float delay)
        {
            if(delay < 0)
            {
                afterDelay.Invoke();
            }else
            {
                this.WaitForSeconds(delay, afterDelay.Invoke);
            }
        }


        public void RaiseAndStopOthers(float delay)
        {
            StopAllCoroutines();
            if (delay < 0)
            {
                afterDelay.Invoke();
            }
            else
            {
                this.WaitForSeconds(delay, afterDelay.Invoke);
            }
        }
    }
}