﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePrefab : MonoBehaviour
{
    public GameObject[] prefabs;
    public ParentingOption parenting;

    public float minWaitBetweenCreate = 0;

    private float lastCreateTime;

    public enum ParentingOption
    {
        DontSetParent,
        SetAsChild,
        SetAsSibling
    }

    public void RandomCreate()
    {
        RandomCreate(transform.position);
    }

    private void TryInstantiate(GameObject pref, Vector3 point)
    {
        if (Time.time >= lastCreateTime + minWaitBetweenCreate)
        {
            var newObj = Instantiate(pref);
            SetupObject(newObj, point);
            lastCreateTime = Time.time;
        }
    }

    public void RandomCreate(Vector3 point)
    {
        TryInstantiate(prefabs[Random.Range(0, prefabs.Length)], point);
        //SetupObject(pref, point);
    }
    private void SetupObject(GameObject pref, Vector3 point)
    {
        pref.transform.position = point;
        switch (parenting)
        {
            case ParentingOption.DontSetParent:
                break;
            case ParentingOption.SetAsChild:
                pref.transform.parent = transform;
                break;

            case ParentingOption.SetAsSibling:
                pref.transform.parent = transform.parent;
                break;
            default: throw new System.ArgumentOutOfRangeException(this + " enum state not supported " + parenting);
        }
    }
    public void Create(int index)
    {
        Create(index, transform.position);
    }

    public void Create(int index, Vector3 point)
    {
        TryInstantiate(prefabs[index], point);
    }
}
