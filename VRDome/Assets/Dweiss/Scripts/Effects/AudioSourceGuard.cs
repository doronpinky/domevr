﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class AudioSourceGuard : MonoBehaviour
    {
        [SerializeField] private new AudioSource audio;
        [SerializeField] private AudioClip[] clips;
        [SerializeField] private Dweiss.EventEmpty onAudioFinished;

        public bool duplicateOnPlay;
        private void Reset()
        {
            audio = this.GetComponentInChildrenOrParents<AudioSource>();
        }
        
        private void OnAudioFinished()
        {
            onAudioFinished.Invoke();
        }
        public void Play()
        {
            if (duplicateOnPlay)
            {
                var go = Instantiate(gameObject, transform.parent);
                Destroy(go.GetComponent<AudioSourceGuard>());
                var newAudio = go.GetComponent<AudioSource>();
                newAudio.Play();
                Destroy(go, audio.clip.length+.1f);

                this.WaitForSeconds(audio.clip.length, OnAudioFinished);
            }
            else if(audio.isPlaying == false)
            {
                audio.Play();
                this.WaitForSeconds(audio.clip.length, OnAudioFinished);
            }
        }
        public void PlayRandomClip()
        {
            if (audio.isPlaying == false)
            {
                audio.clip = clips[Random.Range(0, clips.Length)];
                Play();
                this.WaitForSeconds(audio.clip.length, OnAudioFinished);
            }
        }


    }
}