﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class ViveInput : Dweiss.Singleton<ViveInput>
    {
        private const string JystickButton = "joystick button ";
        public const string JystickAxisButton = "joystick button ";

        public Dweiss.EventEmpty onAnyPress;
        public float minTimeBetweenClicks = 0.3f;

        public enum JoystickKey
        {
            LeftMenu = 2,//reset
            LeftTrigger = 14,//14,//exit
            LeftTouch = 8,//16,//Assist

            // LeftGrip = 11,

            RightMenu = 0,//reset
            RightTrigger = 15,//15,//exit
            RightTouch = 9,//17,//Assist

            //RightGrip = 12,
        }
        public enum TwoJoystickKey
        {
            Menu = 0,//reset
            Trigger = 15,//exit
            Touch = 17,//Assist
                       // Grip = 12 // only "button" exists
        }


        private bool PressedDown(JoystickKey key)
        {
            return Input.GetKeyDown(JystickButton + (int)key);
        }
        private bool Pressed(JoystickKey key)
        {
            var isPressed = Input.GetKey(JystickButton + (int)key); ;
            //Debug.Log("Pressed " + key + " = " + isPressed);

            return isPressed;
        }


        private bool PressedDown(TwoJoystickKey key)
        {
            var keyNum = (int)key;
            var keyNum2 = keyNum == 0 ? 2 : keyNum - 1;
            return PressedDown((JoystickKey)keyNum) || PressedDown((JoystickKey)keyNum2);
        }
        private bool Pressed(TwoJoystickKey key)
        {
            var keyNum = (int)key;
            var keyNum2 = keyNum == 0 ? 2 : keyNum - 1;
            return Pressed((JoystickKey)keyNum) || Pressed((JoystickKey)keyNum2);
        }
        private void UpdateJoystickRight()
        {
            isClickingRight = GetJoystick(10, isClickingRight);
        }
        private void UpdateJoystickLeft()
        {
            isClickingLeft = GetJoystick(9, isClickingLeft);
        }
        private bool GetJoystick(int num, bool isClicking)
        {
            var str = "HTC_VIU_UnityAxis" + num;
            if (isClicking && Mathf.Abs(Input.GetAxis(str)) >= maxValueToRelease) return true;
            else if (isClicking && Mathf.Abs(Input.GetAxis(str)) < maxValueToRelease) return false;

            if (isClicking == false && Mathf.Abs(Input.GetAxis(str)) >= minValueToHold) return true;
            else if (isClicking == false && Mathf.Abs(Input.GetAxis(str)) < minValueToHold) return false;
            return false;
        }
        public Dweiss.EventBool leftPress, rightPress;
        // private float lastClickLeft, lastClickRight;
        //public float minTimeBetweenClicks = .5f;

        private bool isClickingLeft, isClickingRight;




        private bool isClickingLeftReally, isClickingRightReally;
        private float minValueToHold = .7f, maxValueToRelease = .4f;
        private float _lastPressTimeRight, _lastPressTimeLeft;

        private void Start()
        {
            leftPress.AddListener(press => { if (press) onAnyPress.Invoke(); });
            rightPress.AddListener(press => { if (press) onAnyPress.Invoke(); });
        }

        private void Update()
        {
            //UpdateJoystickLeft();

            ////if (isClickingRightReally != isClickingRight){}//Do nothing
            //if (isClickingLeftReally == false && isClickingLeft)
            //{
            //    if (Time.time - _lastPressTimeLeft >= minTimeBetweenClicks)
            //    {
            //        isClickingLeftReally = true;
            //    }
            //}
            //else if (isClickingLeftReally && isClickingLeft == false)
            //{
            //    _lastPressTimeLeft = Time.time;
            //    isClickingLeftReally = false;
            //}
            //leftPress.Invoke(isClickingLeftReally);


            //UpdateJoystickRight();
            //if (isClickingRightReally == false && isClickingRight)
            //{
            //    if (Time.time - _lastPressTimeRight >= minTimeBetweenClicks)
            //    {
            //        isClickingRightReally = true;
            //    }
            //}
            //else if (isClickingRightReally && isClickingRight == false)
            //{
            //    _lastPressTimeRight = Time.time;
            //    isClickingRightReally = false;
            //}
            //rightPress.Invoke(isClickingRightReally);

            UpdateJoystickLeft();
            leftPress.Invoke(isClickingLeft);
            
            UpdateJoystickRight();
            rightPress.Invoke(isClickingRight);
            /************************** Old system
            * UpdateJoystickLeft();
            * leftPress.Invoke(isClickingLeft);
            * 
            * UpdateJoystickRight();
            * rightPress.Invoke(isClickingRight);
            ****************************/
        }

    }
}