﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class ShowLog : MonoBehaviour
    {

        public TextMesh txt;
        public TextMesh stackTxt;
        public UnityEngine.UI.Text log, stack;
        public TextMesh errorTxt;

        public string logFormat = "{0} >> {1}";
        public int lineCount = 5;
        private Queue<string> logLines = new Queue<string>();

        [System.Serializable]
        public class LogInfo : UnityEngine.Events.UnityEvent<string> { };
        [System.Serializable]
        public class FullLogInfo : UnityEngine.Events.UnityEvent<string> { };
        [System.Serializable]
        public class LogInfoAndStack : UnityEngine.Events.UnityEvent<string, string> { };


        public LogInfo onLogInfo;
        public FullLogInfo onFullLogInfo;
        public LogInfoAndStack onLogInfoAndStack;

        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;

        }
        void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        public int maxErrorTextLength = 100;
        void HandleLog(string logString, string stackTrace, LogType type)
        {
            onLogInfo.Invoke(type + "-" + logString);
            onFullLogInfo.Invoke(type + "-" + logString + "\n\n" + stackTrace);
            onLogInfoAndStack.Invoke(type + "-" + logString, stackTrace);

            if (stackTxt) stackTxt.text = stackTrace;
            if (stack) stack.text = stackTrace;

            AddLine(logString, type);
            if (errorTxt != null)
            {
                if (type == LogType.Error || type == LogType.Exception)
                {
                    var newErrTxt = logString + "\n" + errorTxt.text;
                    newErrTxt = newErrTxt.Substring(0, System.Math.Min(newErrTxt.Length, maxErrorTextLength));
                    errorTxt.text = newErrTxt;
                }
            }
        }

        private void AddLine(string logString, LogType type)
        {
            while (logLines.Count > lineCount + 1) logLines.Dequeue();
            logLines.Enqueue(string.Format(logFormat, type, logString));
            var strBuilder = new System.Text.StringBuilder();

            foreach (var q in logLines)
            {
                strBuilder.Append(q + "\n");

            }


            if (log)
            {
                log.text = strBuilder.ToString();
            }

            if (txt)
            {
                
                txt.text = strBuilder.ToString();
            }
        }
    }
}