﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Dweiss
{
    public class TimeScale : MonoBehaviour
    {
        public float setTimeScale;

        private Animation[] allAnim;
        private AudioSource[] aSources;
        public float skipToTimeInAnimation = -1;
        public string animationName;

        private void Awake()
        {
            aSources = GameObject.FindObjectsOfType<AudioSource>();
            allAnim = GameObject.FindObjectsOfType<Animation>();
        }
        private void Update()
        {
            if (Time.timeScale != setTimeScale)
            {
                Time.timeScale = setTimeScale;
                for (int i = 0; i < aSources.Length; i++)
                {
                    aSources[i].pitch = setTimeScale;
                }
            }

            if (skipToTimeInAnimation >= 0)
            {
                for (int i = 0; i < allAnim.Length; i++)
                {
                    allAnim[i][animationName].time = skipToTimeInAnimation;
                }
                for (int i = 0; i < aSources.Length; i++)
                {
                    aSources[i].time = skipToTimeInAnimation;
                }

                skipToTimeInAnimation = -1;
            }
        }
    }
}