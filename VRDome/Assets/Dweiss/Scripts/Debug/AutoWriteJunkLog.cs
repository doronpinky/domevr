﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoWriteJunkLog : MonoBehaviour {

    public float timeBetweenWrite = 5;

    IEnumerator Start()
    {
        while (true)
        {
            Debug.Log("JUNK");
            yield return new WaitForSecondsRealtime(timeBetweenWrite);
        }
    }
	
}
