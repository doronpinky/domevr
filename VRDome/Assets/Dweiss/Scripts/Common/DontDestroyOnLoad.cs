using UnityEngine;
using System.Linq;

namespace Dweiss{

	public class DontDestroyOnLoad : MonoBehaviour {
        public bool destroyOtherLikeMe = true;
        public Dweiss.EventEmpty onSingletonActive;
        public string id;
        private void Reset()
        {
            id = transform.FullName();//Wont work for child transform
            
        }
        private void Start()//To enable
        {
            
        }
        void Awake()
        {
            if (enabled == false) return;

            if (destroyOtherLikeMe)
            {
                var allLikeMe = FindObjectsOfType<DontDestroyOnLoad>().Where(a => a.id == id);
                if (allLikeMe.Count() > 1)
                {
                    Destroy(gameObject);
                    Debug.LogWarning("Multiple DontDestroyOnLoad objects with id " + id + ": " + gameObject.transform.FullName()) ;
                    return;
                }
            }
            DontDestroyOnLoad(gameObject);
            onSingletonActive.Invoke();
        }
    }
}
