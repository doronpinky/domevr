﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss.Msg
{

    [DefaultExecutionOrder(-900)]
    public class LoadCache : MonoBehaviour
    {
        public string cacheId;

        public RaiseAt raiseAt;
        public EventType eventType;

        public Dweiss.EventObject onGetCacheObj;
        public Dweiss.EventInt onGetCacheInt;
        public Dweiss.EventString onGetCacheStr;

        public enum RaiseAt
        {
            Awake,
            Enable,
            Start
            
        }

        public enum EventType
        {
            Object,Int,String, ToString

        }

        public void LoadCacheAndRaiseEvent()
        {
            System.Object obj = MsgCacheString.S.Get(cacheId);
            switch (eventType)
            {
                case EventType.Object: onGetCacheObj.Invoke(obj); return;
                case EventType.Int: onGetCacheInt.Invoke((int)obj); return;
                case EventType.String: onGetCacheStr.Invoke((string)obj); return;
                case EventType.ToString: onGetCacheStr.Invoke(obj.ToString()); return;

                default: throw new System.ArgumentOutOfRangeException(name + " type not supported for conversion " + eventType);
            }
        }

        private void Awake()
        {
            if (raiseAt == RaiseAt.Awake) LoadCacheAndRaiseEvent();
        }
        void Start()
        {
            if (raiseAt == RaiseAt.Start) LoadCacheAndRaiseEvent();
        }
        private void OnEnable()
        {
            if (raiseAt == RaiseAt.Enable) LoadCacheAndRaiseEvent();
        }

    }
}