﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss.Msg
{
    [DefaultExecutionOrder(-1000)]
    public class MsgCacheString : MsgCacheGeneric<string>
    {
        [System.Serializable]
        public class MsgTuple
        {
            public string key;
            public string val;
        }
        private List<MsgTuple> cacheView = new List<MsgTuple>();

#if UNITY_EDITOR

        private IEnumerator Start()
        {
            while (true)
            {
                for (int i = 0; i < 10; ++i)
                {
                    if (_cache.Count != cacheView.Count)
                    {
                        ResetCache();
                    }
                    yield return new WaitForSeconds(.5f);
                }
                ResetCache();
                yield return new WaitForSeconds(1f);
            }
        }

        private void ResetCache()
        {
            cacheView.Clear();
            foreach (var d in _cache)
            {
                cacheView.Add(new MsgTuple() { val = d.Value.ToString(), key = d.Key });
            }
        }
#endif
    }
}