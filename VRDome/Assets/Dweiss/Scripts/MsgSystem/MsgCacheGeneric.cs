﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss.Msg {
    [DefaultExecutionOrder(-1000)]
    public class MsgCacheGeneric<T> : Singleton<MsgCacheGeneric<T>>
    {
        public bool debug;

        public T[] idsObjToRegister;


        protected Dictionary<T, System.Object> _cache = new Dictionary<T, System.Object>();


        new void Awake()
        {
            base.Awake();
            RegisterObj();
            RegisterIntObj();
            RegisterStrObj();
        }


        private System.Tuple<T, System.Action<object>>[] actions;
        private void RegisterObj() {
            if (debug) Debug.Log("Registering obj count " + idsObjToRegister.Length);
            actions = new System.Tuple<T, System.Action<object>>[idsObjToRegister.Length];
            for (int i = 0; i < idsObjToRegister.Length; ++i)
            {
                var id = idsObjToRegister[i];
                actions[i] = System.Tuple.Create<T, System.Action<object>>(id, (obj) =>
                {
                    _cache[id] = obj;
                    if (debug) Debug.Log("Caching " + id + " : " + obj);
                });
                Debug.Log("Registering obj for " + id );
                MsgSystem.Get<T>().Register<System.Object>(id, actions[i].Item2);
            }
        }

        private System.Tuple<T, System.Action<int>>[] actionsInt;
        private void RegisterIntObj()
        {
            if (debug) Debug.Log("Registering int count " + idsObjToRegister.Length);
            actionsInt = new System.Tuple<T, System.Action<int>>[idsObjToRegister.Length];
            for (int i = 0; i < idsObjToRegister.Length; ++i)
            {
                var id = idsObjToRegister[i];
                actionsInt[i] = System.Tuple.Create<T, System.Action<int>>(id, (obj) =>
                {
                    _cache[id] = obj;
                    if (debug) Debug.Log("Caching " + id + " : " + obj);
                });
                if (debug) Debug.Log("Registering int for " + id);
                MsgSystem.Get<T>().Register(id, actionsInt[i].Item2);
            }
        }


        private System.Tuple<T, System.Action<string>>[] actionsStr;
        private void RegisterStrObj()
        {
            if (debug) Debug.Log("Registering str count " + idsObjToRegister.Length);
            actionsStr = new System.Tuple<T, System.Action<string>>[idsObjToRegister.Length];
            for (int i = 0; i < idsObjToRegister.Length; ++i)
            {
                var id = idsObjToRegister[i];
                actionsStr[i] = System.Tuple.Create<T, System.Action<string>>(id, (obj) =>
                {
                    _cache[id] = obj;
                    if (debug) Debug.Log("Caching " + id + " : " + obj);
                });
                if (debug) Debug.Log("Registering str for " + id);
                MsgSystem.Get<T>().Register(id, actionsStr[i].Item2);
            }
        }

        protected override void OnDestroy()
        {
            if (MsgSystem.S)
            {
                for (int i = 0; i < actions.Length; ++i)
                {
                    MsgSystem.Get<T>().Unregister<System.Object>(actions[i].Item1, actions[i].Item2);
                }

                for (int i = 0; i < actionsInt.Length; ++i)
                {
                    MsgSystem.Get<T>().Unregister(actionsInt[i].Item1, actionsInt[i].Item2);
                }

                for (int i = 0; i < actionsStr.Length; ++i)
                {
                    MsgSystem.Get<T>().Unregister(actionsStr[i].Item1, actionsStr[i].Item2);
                }
            }
        }

        

      

        public System.Object Get(T id)
        {
            object outVal;
            if(_cache.TryGetValue(id, out outVal))
            {
                return outVal;
            }
            return null;
        }

        //public void Raise(T id, object data)
        //{
        //    MsgSystem.Get<T>().Raise<object>(id, data);
        //}

        public void Register(T id, System.Action<object> action)
        {
            MsgSystem.Get<T>().Register<object>(id, action);
            var val = Get(id);
            if(val != null) action(val);
        }
        public void Unregister(T id, System.Action<object> action)
        {
            if(MsgSystem.S)
                MsgSystem.Get<T>().Unregister<object>(id, action);
        }

        public void Register(T id, System.Action<int> action)
        {
            MsgSystem.Get<T>().Register(id, action);
            var val = Get(id);
            if (val != null) action((int)val);
        }
        public void Unregister(T id, System.Action<int> action)
        {
            if (MsgSystem.S)
                MsgSystem.Get<T>().Unregister(id, action);
        }

        public void Register(T id, System.Action<string> action)
        {
            MsgSystem.Get<T>().Register(id, action);
            var val = Get(id);
            if (val != null) action((string)val);
        }
        public void Unregister(T id, System.Action<string> action)
        {
            if (MsgSystem.S)
                MsgSystem.Get<T>().Unregister(id, action);
        }
    }
}