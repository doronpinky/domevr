﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    //[RequireComponent(typeof(Renderer))]
    public class SpriteAnimator : MonoBehaviour
    {



        [System.Serializable]
        public class SpriteProvider
        {
            [SerializeField]
            private string id;
            [SerializeField] private string firstName;
            [SerializeField] private int count;

            [SerializeField]
            private Sprite[] _sprites;
            public SpriteProvider(string id, Sprite[] sprites)
            {
                this.id = id;
                System.Array.Sort(sprites, (a, b) => a.name.CompareTo(b.name));
                _sprites = sprites;
                count = sprites.Length;
                firstName = count > 0 ? sprites[0].name : "???";
            }

            public string ID
            {
                get { return id; }
            }
            public Sprite GetSpriteAt(int index)
            {
                return _sprites[index];
            }

            public int Count { get { return _sprites.Length; } }
        }

        public bool debug;

        public Sprite[] sprites;

        public Dweiss.EventString onStartSequence, onEndSequence;

        private SpriteProvider sprite1, sprite2;
        private bool isSprite1 = true;

        public int fps = 30;

        public bool stopAtTheEndOfAnimation;

        private int spriteNum;

        private float lastFrameTime;
        private float concecutiveMissTime;

        private float frameLength;


        private SpriteProvider Spriter { get { return isSprite1 ? sprite1 : sprite2; } }
        private float startAnimationTime;

        public float AnimationStartTime { get { return startAnimationTime; } }
        public float AnimationTime { get { return Time.time - startAnimationTime; } }
        public float AnimationLength { get { return ((float)Spriter.Count) / fps; } }

        private void OnValidate()
        {
            System.Array.Sort(sprites, (a, b) => a.name.CompareTo(b.name));
            if (sprites.Length > 0)
            {
                var rndr = GetComponent<Renderer>();
                rndr.sharedMaterial.mainTexture = sprites[0].texture;
            }
        }

        [ContextMenu("Sort sprites")]
        private void SortSprites()
        {
            System.Array.Sort(sprites, (a, b) => a.name.CompareTo(b.name));
        }

        public void SetNextSequence(SpriteProvider newSeq)
        {
            if (isSprite1) sprite2 = newSeq;
            else sprite1 = newSeq;
        }
        public void InitSequences(SpriteProvider newSeq)
        {
            sprite2 = newSeq;
            sprite1 = newSeq;
        }

        private Renderer rndr;
        private SpriteRenderer rndrSprite;
        private void SetImage(Sprite newImg)
        {
            if (rndrSprite != null) rndrSprite.sprite = newImg;
            else rndr.material.mainTexture = newImg.texture;
        }
        private void Awake()
        {
            rndr = GetComponentInChildren<Renderer>();
            rndrSprite = GetComponentInChildren<SpriteRenderer>();
            frameLength = 1f / fps;

            

            startAnimationTime = Time.time;
            onStartSequence.AddListener((id) => { if (debug) Debug.LogFormat("{2}.{0} - {1}", Spriter.GetSpriteAt(0), Time.time - startAnimationTime, id); });
            //onEndSequence.AddListener((id) => { if (debug) Debug.LogFormat("{2}.{0} - {1}", Spriter.GetSpriteAt(0), Time.time - startAnimationTime, id); });

            if (sprite1 == null) sprite1 = new SpriteProvider("", sprites==null? new Sprite[0] : sprites);
            if (sprite2 == null) sprite2 = new SpriteProvider("", sprites == null ? new Sprite[0] : sprites);
        }


        private void Start()
        {
            onStartSequence.Invoke(Spriter.ID);
        }
        void Update()
        {
#if UNITY_EDITOR
            frameLength = 1f / fps;
#endif

            concecutiveMissTime += Time.deltaTime;

            int skipCount = (int)(concecutiveMissTime / frameLength);

            concecutiveMissTime -= skipCount * frameLength;

            spriteNum = (spriteNum + skipCount);
            if (spriteNum >= Spriter.Count) // start new Animation
            {
                
                try
                {
                    onEndSequence.Invoke(Spriter.ID);
                }
                catch (System.Exception e) { Debug.LogError(name + " onEndSequence: " + Spriter.ID + " " + e); }

                if (stopAtTheEndOfAnimation)
                {
                    spriteNum = Spriter.Count;
                    SetImage(Spriter.GetSpriteAt(spriteNum - 1));
                    concecutiveMissTime = 0;
                    enabled = false;
                    return;
                }

                spriteNum = spriteNum % Spriter.Count;
                isSprite1 = !isSprite1;
                spriteNum = spriteNum % Spriter.Count;//Assumes not skip all animation


                startAnimationTime = Time.time - (spriteNum * Time.deltaTime) - concecutiveMissTime;

                //startAnimationTime = Time.time - (spriteNum * Time.deltaTime);// - concecutiveMissTime;
                //concecutiveMissTime = 0;//reset


                try
                {
                    onStartSequence.Invoke(Spriter.ID);
                }
                catch (System.Exception e) { Debug.LogError(name + " onStartSequence: " + Spriter.ID + " " + e); }
            }


            var sprite = Spriter.GetSpriteAt(spriteNum);

            SetImage(sprite);

        }
    }
}