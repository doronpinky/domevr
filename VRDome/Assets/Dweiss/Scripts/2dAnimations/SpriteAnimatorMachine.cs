﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Dweiss
{
    [RequireComponent(typeof(AudioSource))]
    public class SpriteAnimatorMachine : MonoBehaviour
    {
        public bool debug;


        public Sprite[] allSprites;

        [Header("Clips setup")]
        public SpriteAnimator.SpriteProvider[] animations;
        public NameToId[] allSeq;


        [Header("Specail casses")]
        public string idOfFirstSeq;
        public string loopId;

        [Header("AudioFix")]
        public float minDiffToFixSound = 0.07f;
        public float maxDiffForSoundFix = 7;



        private const string OnEndAnimationEventName = "AiAvatarAnimationEnd";
        private const string OnStartAnimationEventName = "AiAvatarAnimationStart";

        private SpriteAnimator animator;

        [System.Serializable]
        public class NameToId
        {
            public string trigger;
            public int id;
            public AudioClip clip;
        }


        #region UnityDebugOnly
        [Space()]
        [Header("___Unity debug only___")]
        public int numOfSprites;
        
        public float animationTime, audioTime;

        private void OnValidate()
        {
            numOfSprites = allSprites.Length;
        }


        [ContextMenu("Setup 'Animations' list")]
        private void SetupAnimations()
        {
            var dic = new Dictionary<string, List<Sprite>>();
            var hash = new HashSet<Sprite>(allSprites);
            allSprites = hash.ToArray();
            System.Array.Sort(allSprites, (a, b) => a.name.CompareTo(b.name));

            foreach (var s in allSprites)
            {
                var prefix = GetSpriteId(s); //s.name.Substring(0, s.name.Length - "0000".Length);
                if (prefix.EndsWith("011B") || prefix.EndsWith("011C"))//combine animations
                {
                    prefix = "011BC";
                }
                List<Sprite> val;

                if (dic.TryGetValue(prefix, out val) == false)
                {
                    val = new List<Sprite>();
                    dic[prefix] = val;
                }

                val.Add(s);
            }

            animations = new SpriteAnimator.SpriteProvider[dic.Count];
            int counter = 0;
            foreach (var s in dic)
            {
                var spriteArr = s.Value.ToArray();
                animations[counter++] = new SpriteAnimator.SpriteProvider(s.Key, spriteArr);
            }

            System.Array.Sort(animations, (a, b) => a.ID.CompareTo(b.ID));
        }

        [ContextMenu("Setup 'AllSeq' list - name to id")]
        private void SetupNameToId()
        {
            allSeq = new NameToId[animations.Length];
            for (int i = 0; i < animations.Length; i++)
            {
                allSeq[i] = new NameToId() { id = i, trigger = animations[i].ID };
            }
        }
        private string GetSpriteId(Sprite[] sprite) { return GetSpriteId(sprite[0]); }
        private string GetSpriteId(Sprite sprite)
        {
            var id = sprite.name.ToLower();
           // id = id.Replace("_masterlayer","").Replace("013","");

            var isUnknown = id.IndexOf("sprite") >= 0;
            if (isUnknown) { id = "Loop"; }
            else
            {
                id = id.Substring(id.IndexOf('0'));
                id = id.Substring(0, id.IndexOf('_') + 2);
                id = id.Replace("_", "").Replace("sh", "").Replace("s", "").ToUpper().Replace("masterlayer", "");
            }
            return id;
        }


        [Space()]
        [Header("Testing")]
        [SerializeField] private int nextAnim;
        [ContextMenu("Run next Anim")]
        private void RunNextAnimation()
        {
            SetTrigger(allSeq[nextAnim].trigger);
        }

        private void Update()
        {
            if (nextAnim >= 0)
            {
                RunNextAnimation();
                nextAnim = -1;
            }
        }

        #endregion
        private void Awake()
        {
            animator = GetComponent<SpriteAnimator>();
            aSource = GetComponent<AudioSource>();

            var firstAnim = allSeq.First(a => a.trigger == idOfFirstSeq);
            var loopAnim = allSeq.First(a => a.trigger == loopId);

            animator.InitSequences(animations[firstAnim.id]);
            animator.SetNextSequence(animations[loopAnim.id]);

        }
        private void OnEnable()
        {
            animator.onEndSequence.AddListener(OnEndSequence);
            animator.onStartSequence.AddListener(OnStartSequence);
        }

        private void OnDisable()
        {
            animator.onEndSequence.RemoveListener(OnEndSequence);
            animator.onStartSequence.RemoveListener(OnStartSequence);
        }

        private void OnStartSequence(string newSeq)
        {
            SetTrigger(loopId);
            // animator.SetNextSequence(animations[loopId.id]);

            if (debug) if (newSeq != loopId) Debug.Log("___OnAnimationStart " + newSeq);

            var seq = allSeq.First(a => a.trigger == newSeq);
            aSource.Stop();
            if (seq.clip != null)
            {
                aSource.clip = seq.clip;
                aSource.time = 0;
                aSource.Play();
            }
            else
            {
                aSource.clip = null;
            }
            FixAudioDiff();
            StartCoroutine(CoroutineFixAudioDiff());

            if (newSeq != loopId)
                Dweiss.Msg.MsgSystem.MsgStr.Raise(OnStartAnimationEventName, newSeq);
        }



        private void OnEndSequence(string oldSequence)
        {
            if (debug) if (oldSequence != loopId) Debug.Log("___OnAnimationEnd " + oldSequence);
            StopAllCoroutines();

            aSource.Stop();
            aSource.clip = null;

            if (oldSequence != loopId)
                Dweiss.Msg.MsgSystem.MsgStr.Raise(OnEndAnimationEventName, oldSequence);


        }

        public void SetTrigger(string trigger)
        {
            if (debug) Debug.Log("Trigger " + trigger);
            var nextAnim = allSeq.First(a => a.trigger == trigger);
            animator.SetNextSequence(animations[nextAnim.id]);
        }

        public void SetTrigger(int index)
        {
            animator.SetNextSequence(animations[allSeq[index].id]);
        }

        private AudioSource aSource;


        private IEnumerator CoroutineFixAudioDiff()
        {
            while (true)
            {
                FixAudioDiff();
                yield return 0;
            }
        }

        

        private void FixAudioDiff()
        {
            var clipTime = animator.AnimationTime;

            animationTime = clipTime;
            audioTime = aSource.time;

            if (aSource.clip != null && Mathf.Abs(aSource.time - clipTime) > minDiffToFixSound)
                if (Mathf.Abs(aSource.time - clipTime) <= maxDiffForSoundFix)
                {
                    if (debug) Debug.Log("Fixing diff");
                    aSource.time = clipTime + Time.deltaTime / 2f;
                }
        }
    }
}