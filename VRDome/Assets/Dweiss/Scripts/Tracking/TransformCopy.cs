﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class TransformCopy : MonoBehaviour
    {
        public Transform target;

        public bool pos = true, rot = true;

        private Transform t;
        private void Awake()
        {
            t = transform;
        }

        private void Update()
        {
            if(pos) t.position = target.position;
            if(rot) t.rotation = target.rotation;
        }
    }
}