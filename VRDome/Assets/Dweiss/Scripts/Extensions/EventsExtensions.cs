﻿using UnityEngine.Events;
using UnityEngine;

namespace Dweiss 
{
    [System.Serializable] public class EventEmpty : UnityEvent { }

    [System.Serializable] public class EventString : UnityEvent<string> { }
    [System.Serializable] public class EventByteArr : UnityEvent<byte[]> { }
    [System.Serializable] public class EventBool : UnityEvent<bool> { }
    [System.Serializable] public class EventFloat : UnityEvent<float> { }
    [System.Serializable] public class EventInt : UnityEvent<int> { }
    [System.Serializable] public class EventVector3 : UnityEvent<Vector3> { }

    [System.Serializable] public class EventObject : UnityEvent<System.Object> { }
    [System.Serializable] public class EventUnityObject : UnityEvent<UnityEngine.Object> { }

    [System.Serializable] public class EventAudioClip : UnityEvent<AudioClip> { }

    [System.Serializable] public class EventScriptableObject : UnityEvent<ScriptableObject> { }
    [System.Serializable] public class EventMonoBehaviour : UnityEvent<MonoBehaviour> { }
    [System.Serializable] public class EventGameObject : UnityEvent<GameObject> { }
    [System.Serializable] public class EventCollider : UnityEvent<Collider> { }
    [System.Serializable] public class EventMonoSenderObject : UnityEvent<MonoBehaviour, System.Object> { }
    [System.Serializable] public class EventComponent : UnityEvent<Component> { }
}