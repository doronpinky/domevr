﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//namespace System
//{
    public static class LinqExtension
    {

        public static List<E> ToList<E,T>(this IEnumerable<T> l) where T:E
        {
            var ret = new List<E>();
            foreach (var elm in l) ret.Add(elm);
            return ret;
            
        }
        public static void For<T>(this T[] qGroup, System.Action<T> func)
        {
            for (int i = 0; i < qGroup.Length; i++)
            {
                func(qGroup[i]);
            }
        }
        public static void For<T>(this T[] qGroup, System.Action<T, int> func)
        {
            for (int i = 0; i < qGroup.Length; i++)
            {
                func(qGroup[i], i);
            }
        }
        public static E[] Get<T, E>(this T[] qGroup, System.Func<T, E> func)
        {
            var ret = new E[qGroup.Length];
            for (int i = 0; i < qGroup.Length; i++)
            {
                ret[i] = func(qGroup[i]);
            }
            return ret;
        }
        public static E GetFirst<T, E>(this T[] qGroup, System.Func<T, E> func) where E : class
        {
            for (int i = 0; i < qGroup.Length; i++)
            {
                var ret = func(qGroup[i]);
                if (ret != null) return ret;
            }
            return null;
        }

        public static int GetIndex<T>(this T[] qGroup, System.Func<T, bool> func)
        {
            for (int i = 0; i < qGroup.Length; i++)
            {
                if (func(qGroup[i])) return i;
            }
            return -1;
        }

        public static int IndexOf<T>(this T[] that, System.Func<T, bool> search)
        {
            for(int i=0; i < that.Length; ++i)
            {
                if (search(that[i])) return i;
            }
            return -1;
        }

        public static int IndexOf<T>(this IList<T> that, System.Func<T, bool> search)
        {
            for (int i = 0; i < that.Count; ++i)
            {
                if (search(that[i])) return i;
            }
            return -1;
        }

        public static int IndexOf<T>(this T[] that, T search)
        {
            for (int i = 0; i < that.Length; ++i)
            {
                if (that[i].Equals(search)) return i;
            }
            return -1;
        }


       
    }
//}