﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioWhenReachOnTop : MonoBehaviour
{
    public AudioSource aSource;
    public float delayPlayOnMove = .4f;
    public bool playOnTop, playOnMove;

    private Transform t;
    private float lastY;
    private float lastLastY;
    private Rigidbody rb;

    private Vector3 lastSpeed;
    private void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        t = transform;
        lastLastY = 1; lastY = 2;//remove startsound
        //select = selected;
        // selected = true;
    }

    //private static bool selected = false;

    //private bool select;
    void FixedUpdate()
    {
    }

    private void RunLogic() { 
        //Debug.Log(t.position + "   " + rb.velocity);
        if (lastLastY >= lastY && t.position.y > lastY)
        {
            if (playOnMove && rb.velocity.magnitude > .2f) PlayDelay();
        }

        if (lastSpeed.y >= 0 && rb.velocity.y < 0)
        {
            if (playOnTop)
            {
                Debug.Log("Play top ");
                aSource.Play();
            }
        }

        lastSpeed = rb.velocity;

        lastLastY = lastY;
        lastY = t.position.y;

    }
    public void PlayDelay()
    {
        StartCoroutine(CoroutinePlayDelay());
    }
    IEnumerator CoroutinePlayDelay()
    {
        yield return new WaitForSeconds(delayPlayOnMove);
        //Debug.Log("Play delay on move`");
        aSource.Play();
    }
}
