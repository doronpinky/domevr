﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ParticalesReduceOverTime : MonoBehaviour
    {
        public float timeToReduce = 1;
        private ParticleSystem attachedParticales;

        private void Awake()
        {
            attachedParticales = GetComponent<ParticleSystem>();
        }

        IEnumerator Start()
        {
            var emissionRate = attachedParticales.emissionRate;
            var maxParticles = attachedParticales.maxParticles;

            for (float startTime = Time.time; startTime + timeToReduce > Time.time; )
            {
                var percent = 1- ((Time.time- startTime) / timeToReduce);
                attachedParticales.emissionRate = timeToReduce * percent * emissionRate;
                attachedParticales.maxParticles = (int)(timeToReduce * percent * maxParticles);
                yield return 0;
            }
            attachedParticales.emissionRate = 0;
            attachedParticales.maxParticles = 0;
        }
    }
}