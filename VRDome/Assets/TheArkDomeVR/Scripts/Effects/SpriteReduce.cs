﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Dweiss
{
    public class SpriteReduce : MonoBehaviour
    {
        public bool deathOnEnd = true;
        public float timeToReduce = .3f;
        private const string colorName = "_Color";
        public bool reduceOnStart = false;
        private Renderer rnd;
        void Start()
        {
            rnd = GetComponent<Renderer>();
            if (reduceOnStart)
            {
                StartReduce(-1);
            }
        }

        public void StartReduce(float startDelay)
        {
            StopAllCoroutines();
            StartCoroutine(CoroutineReduce(startDelay));
        }

        IEnumerator CoroutineReduce(float startDelay)
        {
            if (startDelay >= 0) yield return new WaitForSeconds(startDelay);


            var timeStart = Time.time;
            var matColor = rnd.material.GetColor(colorName);
            matColor.a = 1;//Make ppl notice it
            while (Time.time < timeStart + timeToReduce)
            {
                var percent = (Time.time-timeStart)/ timeToReduce;
                var color = new Color(matColor.r, matColor.g, matColor.b, Mathf.Sqrt(1 - percent)* matColor.a);
                
                rnd.material.SetColor(colorName, color);
                yield return 0;
            }
            rnd.material.SetColor(colorName, new Color(1, 1, 1, 0));

            if (deathOnEnd) Destroy(gameObject);
        }
    }
}