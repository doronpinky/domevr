﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class VideoLoadFromAssets : MonoBehaviour
    {
        public bool playOnReady = false;
        public string folderSource;
        public string videoName;


        private VideoEvents videoSource;

        private void Start()
        {
            videoSource = GetComponent<VideoEvents>();

            var fullVideoName = (string.IsNullOrEmpty(folderSource) ? "" : folderSource + "/") + videoName;
            Dweiss.PreloadStreamingAsset.S.Load(fullVideoName, videoSource.VideoPlayer, (v) =>
            {
                Debug.Log("Prep movie finished: " + fullVideoName);
                if (playOnReady)
                {
                    videoSource.PlayWithEvent();
                }
            });
            
        }


    }
}