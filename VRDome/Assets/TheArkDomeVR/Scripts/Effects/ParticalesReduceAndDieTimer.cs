﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ParticalesReduceAndDieTimer : MonoBehaviour
    {
        public float lengthOfTimeOfDeathEffects = 2;

        public bool useReductionOne = true;
        public ParticalesReduce[] reducers;

        public bool dieOnStart = false;

        void Reset() { reducers = GetComponentsInChildren<ParticalesReduce>(); }

        public float destroyOnEndDelay = 0;

        private void Start()
        {
            if (dieOnStart) StartDeathSequence();
        }

        public void StartDeathSequence()
        {
            StartCoroutine(CoroutineDeathSequence(lengthOfTimeOfDeathEffects));
        }

        IEnumerator CoroutineDeathSequence(float timeToRemove)
        {
            float startTime = Time.time;

            for (int i = 0; i < reducers.Length; i++)
            {
                reducers[i].valueToIncreasePerSec = 0;
            }
            //Debug.Log("StartReduce " + Time.time);
            while (Time.time - startTime < timeToRemove)
            {
                yield return 0;
//                Debug.Log("Reducing time " + Time.time);
                for (int i = 0; i < reducers.Length; i++)
                {
                    if(useReductionOne)
                        reducers[i].SetPercent(1- ((Time.time - startTime) / timeToRemove));    
                    else
                        reducers[i].SetPercent2(1 - ((Time.time - startTime) / timeToRemove));
                }
            }
            for (int i = 0; i < reducers.Length; i++)
            {
                if (useReductionOne)
                    reducers[i].SetPercent(0);
                else
                    reducers[i].SetPercent2(0);
            }
            // Debug.Log("StopReduce" + Time.time);

            yield return 0;

            yield return new WaitForSeconds(destroyOnEndDelay) ;
            //if (destroyOnEnd)
            {
                for (int i = 0; i < reducers.Length; i++)
                {
                    Destroy(reducers[i].gameObject);
                }

                Destroy(gameObject);
            }
        }
    }
}