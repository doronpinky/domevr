﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class RegisterAudio : MonoBehaviour
    {

        private static bool shuttingDown = false;

        private void OnApplicationQuit()
        {
            shuttingDown = true;
        }

        private AudioSource vid;
        private void OnEnable()
        {
            vid = GetComponentInChildren<AudioSource>();
            if (vid != null)
                GameFlow.S.RegisterAudio(vid);
            else
                Debug.LogError(transform.FullName() + " missing audio");
        }

        private void OnDisable()
        {
            if (vid != null )
                if (GameFlow.S) GameFlow.S.UnregisterAudio(vid);
            else
                if (shuttingDown == false)
                    Debug.LogWarning(transform.FullName() + " missing audio");
        }
    }
}