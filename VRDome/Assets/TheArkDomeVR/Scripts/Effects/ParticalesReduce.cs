﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ParticalesReduce : MonoBehaviour
    {
        public bool debug;
        public float fillUpSpeedFactor = 1/10;
        public float maxTimeToRemove = 2f;
        private float accumulateTimeOfReducing;

        public ParticleSystem attachedParticales;

        private float startEmissionRate;
        private int maxParticales;

        public float valueToIncreasePerSec = 0.5f;

        public float refillWaitAfterEmptyTime;


        void Reset() { attachedParticales = GetComponentInChildren<ParticleSystem>(); }

        public Dweiss.EventBool onFillStatus;
        // public bool destroyOnEmpty = true;
        public Dweiss.EventMonoBehaviour onParticalesEnded;

        

        void Awake()
        {
            //attachedParticales = GetComponentInChildren<ParticleSystem>();
            startEmissionRate = attachedParticales.emissionRate;
            maxParticales = attachedParticales.maxParticles;
        }

        private float startLifeTime, startSize;
        private void Start()
        {
            startLifeTime = attachedParticales.startLifetime;
            startSize = attachedParticales.startSize;
        }

        public float LeftValueToReduce { get { return (maxTimeToRemove-accumulateTimeOfReducing); } }

        private float _emptyTime;

        private bool gotFull = true;
        private void Update()
        {
            if (gotFull == false && Time.time > _emptyTime + refillWaitAfterEmptyTime)
            {
                if (debug) Debug.Log(name + " fast refil");
                accumulateTimeOfReducing = 0;// Mathf.Max(accumulateTimeOfReducing - valueToIncreasePerSec * Time.deltaTime, 0);
                if (accumulateTimeOfReducing == 0)
                {
                    SetPercent(1- accumulateTimeOfReducing);
                    if (debug) Debug.Log(name + " <color=green>onFill 1</color>");
                    onFillStatus.Invoke(true);
                    gotFull = true;
                }
            } else
            {
                accumulateTimeOfReducing = Mathf.Max(accumulateTimeOfReducing - valueToIncreasePerSec* fillUpSpeedFactor * Time.deltaTime, 0);
                UpdateAccumulateTime();


            }
            
            //if(maxTimeToRemove/accumulateTimeOfReducing)
        }
        private void UpdateAccumulateTime()
        {
            var percent = accumulateTimeOfReducing / maxTimeToRemove;
            SetPercent(1 - percent);
        }
        public void Reduce()
        {
            Reduce(Time.deltaTime);
        }
        public void SetPercent(float percent)
        {
            attachedParticales.emissionRate = percent * startEmissionRate;
            attachedParticales.maxParticles = (int)(percent * maxParticales);
        }

        public void Reduce(float reduceTimeValue)
        {

            if (enabled)
            {
                if (debug) Debug.Log(name + " reducing ");
                accumulateTimeOfReducing += reduceTimeValue;
                accumulateTimeOfReducing = Mathf.Min(accumulateTimeOfReducing, maxTimeToRemove);
                var percent = accumulateTimeOfReducing / maxTimeToRemove;

                UpdateAccumulateTime();

                if (percent == 1)
                {
                    onFillStatus.Invoke(false);
                    if (debug) Debug.Log(name + " <color=green>onFill 0</color>");
                    gotFull = false;
                    _emptyTime = Time.time;
                    SetPercent(0);
                    // enabled = false;
                    // onParticalesEnded.Invoke(this);
                    //  Dweiss.Msg.MsgSystem.MsgStr.Raise("ParticleReduce", this);
                    //if(destroyOnEmpty)Destroy(gameObject);
                }
            }
        }


        public void SetPercent2(float percent)
        {
            attachedParticales.startLifetime = percent * startLifeTime;
            attachedParticales.emissionRate = percent * startEmissionRate;
            attachedParticales.maxParticles = (int)(Mathf.Max(Mathf.Sqrt(percent), -1f) * maxParticales);

            //attachedParticales.main.startSize = new ParticleSystem.MinMaxCurve( percent* startSize);

        }


    }
}