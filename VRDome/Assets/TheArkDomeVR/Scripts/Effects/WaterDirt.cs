﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;

namespace DomeVR
{
    public class WaterDirt : MonoBehaviour
    {
        public Sprite[] sprites;
        public int amountOfDirt;
        public GameObject prefabForSprite;
        public Collider areaWithNoDirt;
        public Collider areaWithDirt;

        public float timeToRemove = 3;
        public float destroyDirtAfter = -1;
        public float timeToGetDirty = 3f;

        public int seed;

        public ParticalesReduce reducer;
        private System.Random posRandom;

        void Start()
        {
            posRandom = new System.Random(seed);
            StartCoroutine(CoroutineCreateDirt());
            if (destroyDirtAfter >= 0) this.Invoke("RemoveAllDirt", destroyDirtAfter);
        }


        public void Reduce()
        {

            var maxSpritesToRemove = transform.childCount == 0 ? 0 : Mathf.Sqrt(transform.childCount);
            var maxParticalesToRemove = reducer.LeftValueToReduce == 0 ? 0 : Mathf.Sqrt(reducer.LeftValueToReduce);

          //  ReduceTextures(transform.childCount / 2);
            ReduceTextures( 3*(int)maxSpritesToRemove);
            reducer.Reduce( maxParticalesToRemove);

            //Debug.Log("Reduce " + (2 * (int)maxSpritesToRemove) + ", " + 2 * maxParticalesToRemove);
        }

        private void ReduceTextures(int amount)
        {
            for (int i = 0; i < transform.childCount && i < amount; ++i)
            {
                var child = transform.GetChild(i);
                child.GetComponent<SpriteReduce>().StartReduce(-1);
                child.GetComponent<SimpleRotate>().enabled = false;
            }
        }
        private Vector3 GetRandomPos()
        {
            Vector3 p = Vector3.zero;
            for(int i = 0; i < 3; ++i)
            {
                p = areaWithDirt.bounds.RandomPointInBounds(posRandom);
                if (areaWithNoDirt.bounds.Contains(p) == false) return p;
            }
            return areaWithDirt.bounds.ClosestPoint(p);
        }

        private IEnumerator CoroutineCreateDirt()
        {
            var waitTime = timeToGetDirty / amountOfDirt;
            for (int i=0; i < amountOfDirt; ++i)
            {
                yield return new WaitForSeconds(waitTime);
                //if (i % 30 == 0) 
                CreateSingleDirt();
            }
        }
        private void CreateSingleDirt()
        {
            var pos = GetRandomPos();
            var prefab = Instantiate(prefabForSprite, transform);
            prefab.transform.position = pos;
            prefab.transform.eulerAngles = new Vector3((float)posRandom.NextDouble()*360,
                (float)posRandom.NextDouble() * 360,
                (float)posRandom.NextDouble() * 360);

            prefab.GetComponent<Renderer>().material.mainTexture = sprites[(int)posRandom.Range(0f, (float)sprites.Length)].texture;
            var rotater = prefab.GetComponent<SimpleRotate>();

            prefab.transform.LookAt(Camera.main.transform);
            //rotater.axe = Random.rotation.eulerAngles;
            rotater.degreePerSec = posRandom.Range(30f, 100) * (posRandom.NextDouble() > 0.5? 1 : -1);
        }


        public void RemoveAllDirt()
        {
            StartCoroutine(CoroutineRemoveAllDirt(timeToRemove));
        }

        IEnumerator CoroutineRemoveAllDirt(float timeToRemove)
        {
            float startTime = Time.time;
            int removed = 0, total = transform.childCount;
            var percentToRemove = 0f;
            while (percentToRemove < 1)
            {
                yield return 0;
                percentToRemove = (Time.time - startTime) / timeToRemove;
                float amountToRemove = percentToRemove * total - removed;
                var lastIndex = total - removed;
                for (int i = lastIndex-(int)amountToRemove; i < transform.childCount && i < lastIndex; ++i)
                {
                    transform.GetChild(i).GetComponent<SpriteReduce>().StartReduce(-1);
                    //Destroy(transform.GetChild(i).gameObject);
                    removed++;
                }
                reducer.Reduce(1);
            }

            yield return 0;
            for (int i = transform.childCount-1; i >= 0; --i)
            {
                Destroy(transform.GetChild(i).gameObject);
            }

            Destroy(reducer.gameObject);
        }

    }
}