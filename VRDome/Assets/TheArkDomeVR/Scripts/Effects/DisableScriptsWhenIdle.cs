﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class DisableScriptsWhenIdle : MonoBehaviour
    {
        public float minIdleTime = 5f;
        public List<MonoBehaviour> toDisable;

        public bool startDisabled;

        private Vector3 _lastValue;
        private Quaternion _lastRot;
        private float _lastChangeTime;
        public float deltaMoveSqr = 0.01f, deltaAngleSqr = 1;
        //private Renderer[] rndrs;
        private Transform t;
        public bool useLocalPos;

        private bool active = true;
        public bool IsIdle { get { return active == false; } }
        private List<OnOfWrapper> wrappers;
        [System.Serializable]
        public abstract class OnOfWrapper
        {
            public abstract GameObject go { get; }
            public abstract bool enabled { get; set; }
        }

        [System.Serializable]
        public class MonoWrapper : OnOfWrapper
        {
            public override GameObject go { get { return mono.gameObject; } }
            public MonoBehaviour mono;
            public override bool enabled { get { return mono.enabled; } set { mono.enabled = value; } }
        }
        [System.Serializable]
        public class RndrWrapper : OnOfWrapper
        {
            public override GameObject go { get { return rndr.gameObject; } }
            public Renderer rndr;
            public override bool enabled { get { return rndr.enabled; } set { rndr.enabled = value; } }
        }
        private void OnEnable()
        {
            wrappers = toDisable.Select(a => new MonoWrapper() { mono = a }).ToList<OnOfWrapper, MonoWrapper>();

            wrappers.AddRange(GetComponentsInChildren<Renderer>().Select(a => new RndrWrapper() { rndr = a }).ToList<OnOfWrapper, RndrWrapper>());

            t = transform;
            StartCoroutine(CoroutineTestIdle());

            if (startDisabled)
            {
                if (active)
                {
                    ChangeState();
                    _lastChangeTime = float.NegativeInfinity;

                    _lastValue = useLocalPos ? t.localPosition : t.position;
                    _lastRot = t.localRotation;
                }
            }
        }

        [ContextMenu("Reset Active")]
        private void ChangeState()
        {
            active = !active;
            for (int i = 0; i < wrappers.Count; i++)
            {
                wrappers[i].enabled = active;
            }
            //Debug.Log(name + " disable/enable is " + active);
        }
        IEnumerator CoroutineTestIdle()
        {
            _lastValue = useLocalPos ? t.localPosition : t.position;
            _lastRot = t.localRotation;
            CheckState(0);
            yield return new WaitForSeconds(Random.Range(0, minIdleTime/10));
            CheckState(0);
            yield return new WaitForSeconds(Random.Range(0, minIdleTime / 10));
            CheckState(0);

            while (true)
            {
                yield return new WaitForSeconds(minIdleTime / 10);
                CheckState(minIdleTime);
            }
        }

        private void CheckState(float idleTime)
        {
            var pos = useLocalPos ? t.localPosition : t.position;
            if (Vector3.SqrMagnitude(_lastValue - pos) >= deltaMoveSqr ||
                Quaternion.Angle(_lastRot, t.localRotation) >= deltaAngleSqr)
            {
                _lastChangeTime = Time.time;
            }
            _lastRot = t.localRotation;
            _lastValue = pos;

            if (active && (Time.time - _lastChangeTime) > idleTime)
            {
                ChangeState();
            }
            else if (active == false && (Time.time - _lastChangeTime) < idleTime)
            {
                ChangeState();
            }
        }

    }
}