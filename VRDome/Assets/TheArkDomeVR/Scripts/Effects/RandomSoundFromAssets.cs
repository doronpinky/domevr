﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class RandomSoundFromAssets : MonoBehaviour
    {

        public bool playOnAwake = false;
        public string folderSource;
        public string[] clipNames;


        private AudioSource aSource;
        private AudioClip[] clips;

        private void Awake()
        {
            aSource = GetComponent<AudioSource>();

            clips = new AudioClip[clipNames.Length];
            for (int i = 0; i < clipNames.Length; i++)
            {
                int index = i;
                Dweiss.PreloadStreamingAsset.S.Load(folderSource + clipNames[i], (AudioClip c) => {
                    clips[index] = c;
                    if (playOnAwake)
                    {
                        PlayRandomClip();
                        playOnAwake = false;
                    }
                });
            }

            
        }

        public void PlayRandomClip()
        {
            aSource.clip = clips[Random.Range(0, clips.Length)];
            aSource.Stop();
            aSource.Play();
        }

    }
}