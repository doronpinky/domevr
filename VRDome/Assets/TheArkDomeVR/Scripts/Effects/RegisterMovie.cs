﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace DomeVR
{
    public class RegisterMovie : MonoBehaviour
    {
        private static bool shuttingDown = false;

        private void OnApplicationQuit()
        {
            shuttingDown = true;
        }

        private VideoPlayer vid;
        private void OnEnable()
        {
            vid = GetComponentInChildren<VideoPlayer>(true);
            if (vid != null)
                GameFlow.S.RegisterVideo(vid);
            else
                Debug.LogError(transform.FullName() + " missing vid");
        }

        private void OnDisable()
        {
            if (vid != null)
            {
                if (GameFlow.S) GameFlow.S.UnregisterVideo(vid);
            }
            else
                if(shuttingDown == false)
                    Debug.LogError(transform.FullName() + " missing vid");
        }
    }
}