﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShowOnlyOne : MonoBehaviour
{
    public GameObject[] allRndrs;

    private void Reset()
    {
        allRndrs = GetComponentsInChildren<Renderer>(true).Where(a => a.enabled).Select(a => a.gameObject).ToArray();
    }

    public void ShowOnly(int index)
    {
        for (int i = 0; i < allRndrs.Length; i++)
        {
            allRndrs[i].SetActive(i == index);
        }
    }
}
