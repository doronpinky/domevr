﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Dweiss
{
    public class EventOnAngle : MonoBehaviour
    {
        public bool debug;

        public float angleShift;
        public float intervalAngleToCheck = 360;
        public Dweiss.EventEmpty onAngleReached;

        private float prevAngle;
        private Transform t;
        void Awake()
        {
            t = transform;
        }
        private void OnEnable()
        {
            prevAngle = t.eulerAngles.y;
        }

        void Update()
        {
            var prevAngleInterval = (prevAngle + angleShift) / intervalAngleToCheck;
            var prevGroup = (int)prevAngleInterval;

            var angleInterval = (t.eulerAngles.y + angleShift)/ intervalAngleToCheck;
            var curGroup = (int)angleInterval;
            if (debug)
            {
                Debug.LogFormat("{0} -> {1} VS {2} -> {3}", prevAngleInterval, prevGroup, angleInterval, curGroup);
            }
            if(prevGroup != curGroup)
            {
                onAngleReached.Invoke();
            }
            prevAngle = t.eulerAngles.y;
        }

        private bool ReachedAngle(float prevAngle, float curAngle)
        {
            return ((int)prevAngle) != ((int)curAngle);
        }
    }
}