﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ScoreSystem : Dweiss.Singleton<ScoreSystem>
    {
        private bool debugReduceMoreYearsThanPossible;

        [SerializeField] private int yearStart = 2070, minYearScore = 2025;
        [Header("debug")]
        [SerializeField] private bool overrideAllSuccessCount = false;

        private System.DateTime date;
        private Dictionary<string, int> answersPerLevel = new Dictionary<string, int>();
        private float yearsToReduce;
        private string _currentLvl;


        public const string YearsToReduceEventName = "YearsToReduce";

        private void Start()
        {
            ResetScore();
        }
        private void ResetScore()
        {
            answersPerLevel.Clear();
            yearsToReduce = 0;
            Dweiss.Msg.MsgSystem.Raise("YearsToReduce");
        }

        private const string BestYearScore = "BestYear";
        private void TryUpdateBestYear(int myYear)
        {
            var bestYear = PlayerPrefs.GetInt(BestYearScore, yearStart);
            if (bestYear > myYear)
            {
                PlayerPrefs.SetString(BestYearScore+".Date", date.ToLongDateString());
                PlayerPrefs.SetString(BestYearScore+".Time", date.ToLongTimeString());

                PlayerPrefs.SetInt(BestYearScore, myYear);
            }
        }

        private void TryUpdateBestLevel(string lvl, int currentScore)
        {
            var bestLvlScore = PlayerPrefs.GetInt(lvl, 0);
            if (bestLvlScore < currentScore)
            {
                PlayerPrefs.SetString(lvl + ".Date", date.ToLongDateString());
                PlayerPrefs.SetString(lvl + ".Time", date.ToLongTimeString());

                PlayerPrefs.SetInt(lvl, currentScore);
            }
        }

        private void UpdateBestScore()
        {
            TryUpdateBestYear(CurrentYears);

            foreach (var sl in answersPerLevel)
            {
                TryUpdateBestLevel(sl.Key, sl.Value);
            }
        }

        private void SaveThisUserScore()
        {
            var id = PlayerPrefs.GetInt("id", 0);
            PlayerPrefs.SetInt("id", id + 1);

            PlayerPrefs.SetString(id + ".Date", date.ToLongDateString());
            PlayerPrefs.SetString(id + ".Time", date.ToLongTimeString());

            PlayerPrefs.SetInt(id + ".MyYear", CurrentYears);
            foreach (var sl in answersPerLevel)
            {

                PlayerPrefs.SetInt(id + "." + sl.Key, sl.Value);
            }


        }

        #region Public methods


        
        public void OverrideAllSuccessCount()
        {
            overrideAllSuccessCount = !overrideAllSuccessCount;
        }


        public int MinYear { get { return minYearScore; } }
        public int YearStart { get { return yearStart; } }


        public float CurrentYearsFloat { get { return System.Math.Max(MinYear,(yearStart - yearsToReduce)); } }

        public int CurrentYears { get { return System.Math.Max(MinYear,(int)(YearStart - Mathf.CeilToInt(yearsToReduce))); } }
        public int SceneCorrectAnswerCount { get { return answersPerLevel[_currentLvl]; } }
        public int GetSceneCorrectAnswerCount(string lvlName) {
            if (overrideAllSuccessCount) return 9999;

            if (answersPerLevel.ContainsKey(lvlName) == false) return 0;
            return answersPerLevel[lvlName];
        }

        public void IncreaseCorrectAnswerCount()
        {
            answersPerLevel[_currentLvl]++;
        }
        public bool ReduceMoreYearsThanPossible
        {
            get {
                var calculatedYear = (int)(YearStart - Mathf.CeilToInt(yearsToReduce));
                return MinYear > calculatedYear;
            }
        }

        public void DecreaseYearCount(float count) {
            yearsToReduce += count;

            Dweiss.Msg.MsgSystem.Raise(YearsToReduceEventName);
            Debug.Log("Update score by " + count + " to " + yearsToReduce);

            debugReduceMoreYearsThanPossible = ReduceMoreYearsThanPossible;


        }
        public int BestYear
        {
            get
            {
                return PlayerPrefs.GetInt("BestYear", yearStart);
            }
        }
        public int BestLevelAnswerCount
        {
            get
            {
                return PlayerPrefs.GetInt(_currentLvl, 0);
            }
        }


        private void ResetSession()
        {
            overrideAllSuccessCount = false;
            SaveThisUserScore();
            UpdateBestScore();
            ResetScore();
        }

        private void LevelChanged(string newLevelName)
        {
            _currentLvl = newLevelName;
            if (answersPerLevel.ContainsKey(newLevelName) == false)
            {
                answersPerLevel[newLevelName] = 0;
            }
        }


        #endregion

        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("ResetSession", ResetSession);
            Dweiss.Msg.MsgSystem.MsgStr.Register("LevelChanged", LevelChanged);
        }
        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("ResetSession", ResetSession);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("LevelChanged", LevelChanged);
            }
        }
    }
}

//namespace DomeVR2
//{
//    public class ScoreSystem : Dweiss.Singleton<ScoreSystem>
//    {
//        [SerializeField] private int yearStart = 2070, minYearScore = 2025;
//        [SerializeField] private int pointsPerCorrectAnswer;

//        public int MinYear { get { return minYearScore; } }

//        private System.DateTime date;

//        private Dictionary<string, int> answersPerLevel = new Dictionary<string, int>();
//        private float currentYears;

//        private int GetCurrentRightAnswers()
//        {
//            return GetRightAnswers(GameFlow.S.CurrentMovieScene.name);
//        }

//        private int GetRightAnswers(string sceneName)
//        {
//            if (answersPerLevel.ContainsKey(sceneName) == false)
//            {
//                answersPerLevel[sceneName] = 0;
//            }
//            return answersPerLevel[sceneName];
//        }
//        private void SetCurrentRightAnswers(int correctAnswers)
//        {
//            if (answersPerLevel.ContainsKey(GameFlow.S.CurrentMovieScene.name) == false)
//            {
//                answersPerLevel[GameFlow.S.CurrentMovieScene.name] = 0;
//            }
//            answersPerLevel[GameFlow.S.CurrentMovieScene.name] = correctAnswers;
//        }

//        private void Start()
//        {
//            ResetScore();
//        }
//        private void ResetScore()
//        {
//            answersPerLevel.Clear();
//            currentYears = yearStart;
//        }

//        public int YearStart { get { return yearStart; } }
//        public int PointsPerCorrectAnswer { get { return pointsPerCorrectAnswer; } }

//        private void TryUpdateBestYear(int myYear)
//        {
//            var bestYear = PlayerPrefs.GetInt("BestYear", yearStart);
//            if (bestYear > myYear)
//            {
//                PlayerPrefs.SetString("BestYear.Date", date.ToLongDateString());
//                PlayerPrefs.SetString("BestYear.Time", date.ToLongTimeString());

//                PlayerPrefs.SetInt("BestYear", myYear);
//            }
//        }


//        private void TryUpdateBestLevel(string lvl, int currentScore)
//        {
//            var bestLvlScore = PlayerPrefs.GetInt(lvl, 0);
//            if (bestLvlScore < currentScore)
//            {
//                PlayerPrefs.SetString(lvl + ".Date", date.ToLongDateString());
//                PlayerPrefs.SetString(lvl + ".Time", date.ToLongTimeString());

//                PlayerPrefs.SetInt(lvl, currentScore);
//            }
//        }

//        private void UpdateBestScore()
//        {
//            TryUpdateBestYear((int)currentYears);

//            foreach (var sl in answersPerLevel)
//            {
//                TryUpdateBestLevel(sl.Key, sl.Value);
//            }
//        }

//        private void SaveThisUserScore()
//        {
//            var id = PlayerPrefs.GetInt("id", 0);
//            PlayerPrefs.SetInt("id", id + 1);

//            PlayerPrefs.SetString(id + ".Date", date.ToLongDateString());
//            PlayerPrefs.SetString(id + ".Time", date.ToLongTimeString());

//            PlayerPrefs.SetInt(id + ".MyYear", (int)currentYears);
//            foreach (var sl in answersPerLevel)
//            {

//                PlayerPrefs.SetInt(id + "." + sl.Key, sl.Value);
//            }


//        }

//        #region Public methods

//        public int CurrentYears { get { return (int)currentYears; } }
//        public int CurrentPoints { get { return GetCurrentRightAnswers() * pointsPerCorrectAnswer; } }

//        //public int GetScore(string sceneId) {  return GetCurrentRightAnswers() / pointsPerCorrectAnswer; }
//        public int GetCorrectAnswersCount(string sceneId) { return GetRightAnswers(sceneId); }



//        public int BestYear
//        {
//            get
//            {
//                return PlayerPrefs.GetInt("BestYear", yearStart);
//            }
//        }
//        public int BestLevelPoints
//        {
//            get
//            {
//                return PlayerPrefs.GetInt(GameFlow.S.CurrentMovieScene.name, 0) * pointsPerCorrectAnswer;
//            }
//        }

//        public void AddCorrectAnswer()
//        {
//            currentYears -= GameFlow.S.CurrentMovieScene.triviaInfo.YearPerQuestion;
//            currentYears = System.Math.Max(minYearScore, currentYears);
//            SetCurrentRightAnswers(GetCurrentRightAnswers() + 1);
//        }

//        public void ResetSession()
//        {
//            SaveThisUserScore();
//            UpdateBestScore();
//            ResetScore();
//        }


//        private void OnEnable()
//        {
//            Dweiss.Msg.MsgSystem.MsgStr.Register("ResetSession", ResetSession);
//        }
//        private void OnDisable()
//        {
//            if (Dweiss.Msg.MsgSystem.S)
//                Dweiss.Msg.MsgSystem.MsgStr.Unregister("ResetSession", ResetSession);
//        }

//        #endregion


//    }
//}