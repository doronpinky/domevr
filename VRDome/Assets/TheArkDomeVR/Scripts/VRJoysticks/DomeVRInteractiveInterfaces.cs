﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR {
    public interface IController
    {
        string name { get; }
        int Id { get; }
        //bool IsLocked { get; }
    }

    public interface IInteractiveItem
    {
        string name { get; }
        bool IsInteractable(IController cntrlr);

        void OnClickStart(IController cntrlr, Vector3 pointInteract);
        void OnClickContinues(IController cntrlr, Vector3 pointInteract);
        void OnClickEnd(IController cntrlr, Vector3 pointInteract);

        void OnHoverEnter(IController cntrlr, Vector3 pointInteract);
        void OnHoverContinues(IController cntrlr, Vector3 pointInteract);
        void OnHoverExit(IController cntrlr, Vector3 pointInteract);
    }
}
