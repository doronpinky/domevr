﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class SingleJoystickControl : MonoBehaviour, IController
    {
        public bool debug;

        public string domePivotTag = "DomeVR";
        public int joystickNum;
        public Transform aimingDot;

        public float dotDistance = 2.7f;

        public LayerMask raycastToInteractiveMask;
        public LayerMask raycastToCalculationMask;
        public LayerMask raycastForTargetMask;

        public int Id
        {
            get { return joystickNum; }
        }

        private Transform domePivot;

        private IInteractiveItem _activeItem;
        private IInteractiveItem _aimingAt;

        public Dweiss.EventEmpty onPress;
        private bool isPressed;
        public bool IsPressed { get { return isPressed; } set {
                if (enabled == false) return;

                var wasClicked = isPressed;
                isPressed = value;
                if (wasClicked != isPressed)
                {
                    UpdateLogicState(aimingDot.position, true, wasClicked == false && isPressed);
                    if (isPressed && Id == 0)
                    {
                        Dweiss.Msg.MsgSystem.Raise("User0Click");
                        onPress.Invoke();
                    }
                }
            } }

        public enum HoldingState
        {
            Nothing, Hover, HoldingAndPressed, PressingEmpty
        }

        private HoldingState State { get
            {
               
                //if (_activeItem != null && _hoverItem == null)
                //{
                //    return HoldingState.HoldingOutOfHovering;
                //}
                if (_activeItem != null && IsPressed) // isPressed
                {
                    return HoldingState.HoldingAndPressed;
                }
                else if (_activeItem == null && IsPressed)
                {
                    return HoldingState.PressingEmpty;
                }
                else if (_aimingAt != null )
                {
                    return HoldingState.Hover;
                }
                else// (_activeItem == null && _hoverItem == null)
                {
                    return HoldingState.Nothing;
                }
            } }

      
        private void ReleaseItem(Vector3 currentPoint)
        {
            _activeItem.OnClickEnd(this, currentPoint);
            _activeItem = null;
        }
        private void HoldHoverItem(Vector3 currentPoint)
        {
            _activeItem = _aimingAt;
            _activeItem.OnClickStart(this, currentPoint);
            //Debug.Log("Click " + _activeItem.name);
        }

        private bool ItemCantBeHeld { get { return IsPressed == false || _aimingAt == null; } }
        private bool ItemInViewShouldBeHeld { get { return _aimingAt != null && IsPressed; } }

        private Vector3 lastP;
        private void UpdateLogicState(Vector3 currentPoint, bool clickOnly, bool justClicked)
        {
            if (debug) Debug.LogFormat(
                "{0} - item:{1} aiming:{2} isClicking:{3} clickOnly:{4} itemCan?:{5}",
                name , _activeItem, _aimingAt, justClicked, clickOnly, ItemCantBeHeld );

            if (_activeItem != null && _aimingAt != _activeItem)//Item beeing held
            {
                ReleaseItem(currentPoint);
            }

            if (_activeItem != null && ItemCantBeHeld) // isPressed
            {
                ReleaseItem(currentPoint);
                return;
            }
            else if (justClicked && _activeItem == null && ItemInViewShouldBeHeld)
            {
                HoldHoverItem(currentPoint);
                return;
            }

            //else if(_activeItem != null && _aimingAt != _activeItem)//Item came into view while holding
            //{
            //    ReleaseItem(currentPoint);
            //    return;
            //}
            else if (clickOnly == false && _activeItem != null && _aimingAt == _activeItem && IsPressed)//Item beeing held
            {
                _activeItem.OnClickContinues(this, currentPoint);
            }
            else if (clickOnly == false && _aimingAt != null && IsPressed == false && _activeItem == null)//Item beeing hoverd
            {
                _aimingAt.OnHoverContinues(this, currentPoint);
            }

        }

        void Awake()
        {
            domePivot = GameObject.FindGameObjectWithTag(domePivotTag).transform;
        }
       // public bool multiDomeScale = false;
        void Update()
        {
            var ray = new Ray(transform.position, transform.forward);
            //if (multiDomeScale)
            //{
                var rayForDome = GetRayForDome(ray);
                RaycastToTarget(rayForDome);
                RayForTargetImage(rayForDome);
            //}
            //else
            //{
            //    RaycastToTarget(ray);
            //    RayForTargetImage(ray);
            //}
        }


        private Ray GetRayForDome(Ray ray)
        {
            RaycastHit hit;
            //Debug.DrawRay(ray.origin + ray.direction, ray.direction * 5, Color.white);
            if (Physics.Raycast(ray, out hit, float.PositiveInfinity, raycastToCalculationMask))
            {
                Debug.DrawLine(ray.origin, hit.point, Color.red);

                if (debugHit) Debug.Log("Hit " + hit.collider.transform.FullName());
                return new Ray(domePivot.position, hit.point - domePivot.position);
            }
            if (debugHit) Debug.LogWarning("No GetRayForDome");
            return ray;
        }

        
        private void RayForTargetImage(Ray ray)
        {
            RaycastHit hit;
           // Debug.DrawRay(ray.origin, ray.direction * 5, Color.white);
            if (Physics.Raycast(ray, out hit, float.PositiveInfinity, raycastForTargetMask))
            {
                if(debugHit) Debug.Log("Hit " + hit.collider.transform.FullName());
                aimingDot.position = hit.point;
                

                Debug.DrawLine(ray.origin + ray.direction * 1.5f, hit.point, Color.blue);
                //return new Ray(domePivot.position, hit.point - domePivot.position);
            }
            else
            {
                aimingDot.position = ray.origin + ray.direction * dotDistance;
                if (debugHit) Debug.LogWarning("No RayForTargetImage");
            }
            //return new Ray(domePivot.position, Vector3.zero);
        }

        // Update is called once per frame
        void RaycastToTarget(Ray ray)
        {
            var oldTarget = _aimingAt;

            RaycastHit hit;
            //Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);
            if (Physics.Raycast(ray, out hit, float.PositiveInfinity, raycastToInteractiveMask)){
                var item = hit.collider.GetComponent<IInteractiveItem>();
                aimingDot.position = hit.point;

                Debug.DrawLine(ray.origin + ray.direction*.75f, hit.point, Color.green);

                //Debug.DrawRay(hit.point, ray.origin * 10, Color.yellow);
                if (debugHit) Debug.Log("Hit " + hit.collider.transform.FullName());
                if (item != null && item.IsInteractable(this))
                {
                    _aimingAt = item;
                    if (_aimingAt != oldTarget)
                    {
                        //if (_aimingAt != null && _aimingAt != oldTarget)
                        // {
                        if (debugHit) state = RaycastState.FoundItem;
                        //Debug.Log("Found interactable item " + item);
                        _aimingAt.OnHoverEnter(this, hit.point);
                        // }
                    }
                    //else nothing needs to happen
                }
                else // no item in target
                {
                    if (debugHit) state = RaycastState.NullItem;
                    //Debug.Log("Item not interactable " + hit.collider);
                    _aimingAt = null;
                }
                if (debugHit) itemName = hit.collider.name;
                //if (oldTarget != null && _aimingAt != oldTarget)
                //{
                //    Debug.Log("Switch item exit hover " + hit.collider);
                //    oldTarget.OnHoverExit(this, hit.point);
                //}
                // Debug.Log(Id + " Found " + hit.transform.name);
            }
            else // no item in target
            {

                if (debugHit) state = RaycastState.NoRaycast;

                if (debugHit) itemName = "None";
                if (debugHit) Debug.LogWarning(Id + " No target to view");
                _aimingAt = null;
            }

            if (oldTarget != null && _aimingAt != oldTarget) // Update old aiming if needed
            {
                //Debug.Log("Miss everything. no hover " + hit.collider);
                oldTarget.OnHoverExit(this, hit.point);
            }

            UpdateLogicState(hit.point, false, false); //Click and continue actions


        }

        [Header("Debug only")]
        public bool debugHit;

        public RaycastState state;
        public string itemName;
        public enum RaycastState
        {
            FoundItem, NullItem, NoRaycast
        }
    }
    
}