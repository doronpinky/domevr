﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovieScene : MonoBehaviour
{
    public Transform[] movies;

    public KeyCode changeMovieKey = KeyCode.N;
    private int currentIndex = 0;

    void SelectMovieToSee(int index)
    {
        currentIndex = index % movies.Length;
        for (int i = 0; i < movies.Length; i++)
        {
            movies[i].localPosition = currentIndex == i ? Vector3.zero : Vector3.one * 100;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SelectMovieToSee(currentIndex);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(changeMovieKey))
        {
            SelectMovieToSee(currentIndex + 1);
        }
    }
}
