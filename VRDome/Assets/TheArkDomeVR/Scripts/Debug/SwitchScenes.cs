﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SwitchScenes : Dweiss.Singleton<SwitchScenes>
{
    public KeyCode changeSceneKey = KeyCode.Escape;

    void Update()
    {
        // var allScenes = SceneManager.sceneCount;
        //var curretnIndex = SceneManager.GetActiveScene().buildIndex;

        if (Input.GetKeyDown(changeSceneKey))
        {
            NextScene();
        }


    }

    public void NextScene()
    {

        var sceneCounts = SceneManager.sceneCountInBuildSettings;
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % sceneCounts);
    }
}
