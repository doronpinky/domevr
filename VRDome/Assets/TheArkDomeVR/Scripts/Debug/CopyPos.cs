﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyPos : MonoBehaviour
{
    public Transform other;

    public Vector3 factor = Vector3.one;

    public bool hisLocalPos = false;
    // Update is called once per frame
    void Update()
    {
        if (hisLocalPos)
        {
            transform.position = other.localPosition.PointMul(factor) + transform.position.PointMul(Vector3.one - factor);
        }    else
        {
            transform.position = other.position.PointMul(factor) + transform.position.PointMul (Vector3.one-factor);
        }   
    }
}
