﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class DomeRenderAccordingToScreen : MonoBehaviour
    {

        private CameraProjecter camProj;
        void Awake()
        {
            camProj = GetComponent<CameraProjecter>();
        }


        private void Update()
        {
            if (Input.GetKey(KeyCode.Tab))
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    var old = camProj.options.antiAliasing;
                    var aaExp = Mathf.Log((int)camProj.options.antiAliasing, 2)+1;
                    var aaExpMod = aaExp %4;
                    var aa = Mathf.Pow(2, aaExpMod);
                    Debug.LogFormat("AA changed {0} -> {1} -> {2} -> {3}", old, aaExp, aaExpMod, aa);
                    Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", "AA = " + camProj.options.antiAliasing);
                    camProj.options.antiAliasing = (ProjecterLib.AntiAliasValue)aa;
                }
                if (Input.GetKeyDown(KeyCode.F))
                {
                    var old = camProj.options.filtering;
                    var nextFiltering = ((int)camProj.options.filtering+1) % 3;
                    camProj.options.filtering = (FilterMode)nextFiltering;
                    Dweiss.Msg.MsgSystem.MsgStr.Raise("Info", "FilterM = " + camProj.options.filtering);
                }
            }
        }

        private IEnumerator AutoChangeRenderSettings()
        {
            for (int i = 0; i < 10; i++)
            {
                FixResolution();
                yield return new WaitForSecondsRealtime(10);

            }
        }
        private void FixResolution() { 
            if (Screen.width > 1920)
                camProj.SetResolution(4096);
            else if(Screen.width > 1900)
                camProj.SetResolution(2048);
            else 
                camProj.SetResolution(512);

        }

    }
}