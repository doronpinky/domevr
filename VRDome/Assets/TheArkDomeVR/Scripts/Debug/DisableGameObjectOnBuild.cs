﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class DisableGameObjectOnBuild : MonoBehaviour
    {
        public GameObject go;
        public KeyCode enable = KeyCode.M, secondKey = KeyCode.LeftShift;
        // Start is called before the first frame update
        void Awake()
        {
#if UNITY_EDITOR == false
        go.SetActive(false);
#endif
        }


        private void Update()
        {
            if (enable == KeyCode.None) return;

            if(Input.GetKey(secondKey) && Input.GetKeyDown(enable))
            {
                go.SetActive(!go.activeSelf);
            }
        }



    }
}