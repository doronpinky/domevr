﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DomeVR;
using System.Linq;

public class TimeMangagerDebug : MonoBehaviour
{
    private const float MaxTimeScale = 3;


    [Header("___Information___")]

    public float appTime;
    public float timeInScene;
    public float videoTime;

    [Space()]
    [Range(0, 1)]
    public float percentInScene;
    public float sceneLength = 1;


    [Header("___Config___")]
    [Space()]
    [Space()]
    public float timeScale = 1;
    public float targetSceneTime = -1;
    public float setVideoTime = -1;
    public float skipToSceneTime = -1;
    //[Range(0, 3)]
    private float targetSceneTimeSpeed = 2;

    private float switchSceneTime;
    private float lastScale;

    private List<VideoPlayer> plyrs;
    private List<AudioSource> audios;
    private void OnValidate()
    {
        if (timeScale > MaxTimeScale)
        {
            timeScale = MaxTimeScale;
        }
    }

    private void Awake()
    {
        plyrs = GameObject.FindObjectsOfType<VideoPlayer>().ToList();
        audios = GameObject.FindObjectsOfType<AudioSource>().ToList();
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(3);
        SceneSwitch();
        yield return CoroutineUpdate();
    }
    private VideoPlayer GetActivePlayer()
    {

        for (int i = 0; i < plyrs.Count; i++)
        {
            if (plyrs[i].isPlaying)
            {
                return plyrs[i];
            }
        }
        return null;
    }

    private void UpdateVideo()
    {
        var plyr = GetActivePlayer();
        //videoTime = -1f;
        // foreach (var plyr in plyrs) {
        if (plyr != null)
        {
            if (plyr.isPlaying)
            {
                //videoTime = (float)plyr.time;

                if (setVideoTime >= 0)
                {
                    SetVideoToTime(setVideoTime);
                    setVideoTime = -1;
                }
            }
            //      }
        }

    }

    private void SetVideoToTime(float vidTime)
    {

        // var plyr = GetActivePlayer();

        for (int i = 0; i < plyrs.Count; i++)
        {
            if (plyrs[i].isPlaying)
            {
                if (vidTime > 0)
                    plyrs[i].time = vidTime;
                //return plyrs[i];
            }
        }

        //if (plyr != null)
        //{
        //    if (plyr.isPlaying)
        //    {
        //        if(vidTime > 0)
        //            plyr.time =  vidTime;
        //    }
        //}
    }



    IEnumerator CoroutineUpdate()
    {
        while (true)
        {

            yield return 0;
            try
            {
                LogicCycle();
            }
            catch (System.Exception) { }
        }
    
    }

    private void LogicCycle()
    {
        appTime = Time.time;
        timeInScene = GameFlow.S.SceneTime;// Time.time - switchSceneTime;
        videoTime = GameFlow.S.VideoTime;

        if (skipToSceneTime > 0)
        {
            if (timeInScene < skipToSceneTime - 0.5f * Time.deltaTime)
            {
                timeScale = 20;

            }
            else
            {
                var deltaT = 0.02f;// Time.deltaTime;
                timeScale = 0;
                skipToSceneTime = -1;
                SetVideoToTime(timeInScene + deltaT);
                this.WaitForSecondsRealTime(1, () =>
                {
                    SetVideoToTime(timeInScene + deltaT);
                });
                this.WaitForSecondsRealTime(3, () =>
                {
                    timeScale = 1;
                    //SetVideoToTime(timeInScene );
                });
            }

        }
        else
        {
            UpdateVideo();


            if (targetSceneTime > 0)
            {
                if (timeInScene < targetSceneTime)
                {
                    targetSceneTimeSpeed = Mathf.Min(targetSceneTimeSpeed, MaxTimeScale);
                    if (targetSceneTime - timeInScene < 5)
                    {
                        timeScale = 0f;
                        targetSceneTime = 0;
                    }
                    else if (targetSceneTime - timeInScene < 7)
                    {
                        timeScale = 1;
                    }
                    else if (targetSceneTime - timeInScene < 15)
                    {
                        timeScale = Mathf.Max(1, 1.5f);
                    }
                    else
                    {
                        timeScale = targetSceneTimeSpeed;
                    }
                }
            }
        }

        percentInScene = Mathf.Min(1, timeInScene / sceneLength);

        if (lastScale != timeScale)
        {
            timeScale = Mathf.Clamp(timeScale, 0, 20);
            //Time.timeScale = timeScale;
            lastScale = timeScale;
            try
            {
                GameFlow.S.SetSpeed(timeScale);
            }
            catch (System.Exception e)
            {
                Debug.Log("err using GameFlow to set speed " + timeScale);
                this.timeScale = Mathf.Clamp(timeScale, 0, 15);
                Time.timeScale = this.timeScale;

                for (int i = plyrs.Count - 1; i >= 0; i--)
                {
                    if (plyrs[i] == null) plyrs.RemoveAt(i);
                    else plyrs[i].playbackSpeed = timeScale;
                }
                for (int i = audios.Count - 1; i >= 0; i--)
                {
                    if (audios[i] == null) audios.RemoveAt(i);
                    else audios[i].pitch = timeScale;
                }
            }

        }
    }
    //timeInScene = CurrentScene


    private ScriptableSceneSettings CurrentScene { get { return GameFlow.S.CurrentMovieScene; } }
    private void SceneSwitch()
    {
        if (GameFlow.S)
            sceneLength = CurrentScene.SceneLength;
        switchSceneTime = Time.time;
    }

    private void OnEnable()
    {
        Dweiss.Msg.MsgSystem.MsgStr.Register("OnSceneFinished", SceneSwitch);
    }
    private void OnDisable()
    {
        if (Dweiss.Msg.MsgSystem.S)
        {
            Dweiss.Msg.MsgSystem.MsgStr.Unregister("OnSceneFinished", SceneSwitch);
        }
    }
}
