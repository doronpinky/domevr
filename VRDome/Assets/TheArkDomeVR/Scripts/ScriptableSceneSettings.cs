﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR {
    [CreateAssetMenu(fileName = "SceneConfig", menuName = "SceneConfig")]
    public class ScriptableSceneSettings : ScriptableObject
    {
        [SerializeField] private string movieName;
        [SerializeField] private float sceneLength = 0;//0 = looping and waiting for operator
        [SerializeField] private bool loop = false;
        [SerializeField] private bool waitForOperator = false;

        [SerializeField] private int minimumCorrectAnswersForRoundSuccess = 1;

        //[SerializeField] private int numOfQuestions = 0;//0 = Not interactive

        [SerializeField]
        private ScriptableSceneSettings[] nextMovies;

        [SerializeField]
        public ScriptableSceneSettings[] NextMovies { get { return nextMovies; } set { nextMovies = value; } }

        public GameObject prefabToCreate;
        public float delayInPrefabCreation = -1;
        public float SceneLength { get { return sceneLength; } }
        public bool IsLoop { get { return loop; } }
        public bool IsWaitForOperator { get { return waitForOperator || sceneLength <= 0; } }
        
        public string MovieName { get { return movieName; } }
        public int MinimumCorrectAnswersForRoundSuccess { get { return minimumCorrectAnswersForRoundSuccess; } }
        public bool useScoreToDetermindBranch = false;

        public TriviaInfo triviaInfo;


        [System.Serializable]
        public class TriviaInfo
        {
            //public float waitForGuiToAppear = 5;
            //public float guiStopAt = -1;
            [SerializeField] private string questionId;
            [SerializeField] private int maxYearsToReduce;
            [SerializeField] private int totalQuestionsInRound;

            public string QuestionId { get { return questionId; } }
            public int MaxYearsToReduce { get { return maxYearsToReduce; } }
            public float YearPerQuestion { get { return totalQuestionsInRound == 0? 0f : (float)maxYearsToReduce / (float)totalQuestionsInRound; } }
            public int TotalQuestionsInRound { get { return totalQuestionsInRound; } }

            public bool HasQuestion { get { return totalQuestionTime > 0; } }
            public bool HasTimeLimitPerQuestion { get { return secondsPerQuestion > 0; } }
            public bool AnswerAsManyQuestionPerRound { get { return secondsPerQuestion <= 0; } }
          //  public bool TryUseTwoQuestion { get { return optionalTwoQuestion; } }

            public float TotalQTime { get { return totalQuestionTime; } }

            //No longer relevant
            public float SecondsPerQuestion { get { return secondsPerQuestion; } }//No longer relevant

            public int TotalQuestionsInPredfinedTimedQuestion { get { return (int)(secondsPerQuestion <= 0 ? float.MaxValue : (TotalQTime / secondsPerQuestion)); } }

            [SerializeField] private float totalQuestionTime; // 0 means noQuestion

            //No longer relevant
            private float secondsPerQuestion = 0;//0 means question changes whenever an answer is given //No longer relevant

            //No longer relevant
           // private AnswerOption[] questionSequence;//No longer relevant
           // public AnswerOption[] QuestionSequence { get { return questionSequence; } }
            //public bool HasSpecificQuestionSequence { get { return questionSequence.Length > 0; } }

            //[System.Serializable]
            //public class AnswerOption
            //{
            //    public int[] questionSequence;
            //}

        }

        public override string ToString()
        {
            return string.Format("{0}({1}) IsLoop {2} WaitForOpe {3} NextMovies {4}",name, sceneLength, loop?"1":"0", waitForOperator? "1" : "0", nextMovies.ToCommaString(a => a.name));
        }

    }
}