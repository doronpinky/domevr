﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR {
    public class Mouse : MonoBehaviour
    {
        public SingleJoystickControl joystick;
        public bool rotateMouseOnSphere = true;
        
        private void Update()
        {
            if (rotateMouseOnSphere)
            {
                UpdateMouseForRotation();
                MoveMousePos();
            }
            else UpdateMouse();
        }

        void UpdateMouse()
        {
            var mousePos = Input.mousePosition;
            var mousePInWorld = Camera.main.ScreenPointToRay(mousePos);
            transform.position = mousePInWorld.origin + mousePInWorld.direction* 1;
            transform.forward = mousePInWorld.direction;
        //    Debug.DrawLine(Camera.main.transform.position, transform.position, Color.magenta);
            joystick.IsPressed = Input.GetMouseButton(0);
        }

        void UpdateMouseForRotation()
        {
            var mousePos = Input.mousePosition;
            var view = Camera.main.ScreenToViewportPoint(mousePos);
            var angleUp = view.y * 180;
            var angleSide = view.x * 360 - 180;

            var sideRot = Quaternion.AngleAxis(angleSide, Vector3.up);

            transform.rotation = sideRot;
            var upRot = Quaternion.AngleAxis(-angleUp, transform.right);
            transform.rotation = upRot * sideRot;
            //transform.forward = mousePInWorld.direction;
            Debug.DrawLine(transform.position, transform.forward*10, Color.magenta);
            joystick.IsPressed = Input.GetMouseButton(0);
        }

        private float angle;
        private float dist = 2.6f;
        private float height;
        public float distPerSec = 1;
        void MoveMousePos()
        {
            height += ((Input.GetKey(KeyCode.Home) ? -1 : 0) +
                (Input.GetKey(KeyCode.End) ? 1 : 0)) * Time.deltaTime;
            dist += Input.GetAxis("Vertical") * Time.deltaTime * distPerSec;
            angle += Input.GetAxis("Horizontal");
            var newPos = Quaternion.Euler(0, angle, 0) * Vector3.forward * dist + new Vector3(0, height,0);

            transform.position = newPos;

        }
    }
}