﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dweiss;

public class Clock : MonoBehaviour
{

    public AudioSource audioS;
    private int slowSound = 30;
    public int makeSoundFromTime = 30, makeQuickSoundFromTime = 15, doubleTimeSound = 5;
    [System.Serializable]
    public class ColorSequence
    {
        public Color color;
        public float percent;
    }
    public List<ColorSequence> colorSequence;

    public enum ClockShape
    {
        Seconds,
        MinutesAndSeconds,
        HoursAndMinutesAndSeconds,
        TotalSeconds
    }

    public int countTime = 360;
    [SerializeField] private ClockShape shape;

    [SerializeField] private UnityEngine.UI.Text _uiText;
    [SerializeField] private TMPro.TextMeshPro _txtMesh;

    [SerializeField] private bool startClock;

    public UnityEngine.Events.UnityEvent onReset, onTimeEnd;

    //private int secondsLeft;
    private float startTime;
    public float SecondsLeft { get { return (startTime + countTime)-Time.time ; }  }
    public float SecondsPass { get { return Time.time - startTime; } }

    public void Stop()
    {
        startTime = float.NegativeInfinity;
        StopAllCoroutines();

        var str = ToClock((int)0);
        if (_uiText != null) _uiText.text = str;
        if (_txtMesh != null) _txtMesh.text = str;
        SetColor(SecondsLeft);
    }

    public void Reset(int countSec)
    {
        countTime = countSec;
        startTime = Time.time;
        StopAllCoroutines();
        StartCoroutines();
        onReset.Invoke();
    }

    private void Start()
    {
        colorSequence.Sort((a, b) => -a.percent.CompareTo(b.percent));
        if (startClock) StartCoroutines();
    }

    private void StartCoroutines()
    {
        SetColor(countTime);
        StartCoroutine(CountTime());
        StartCoroutine(TickSound());
    }



    private void SetColor(float secLeft)
    {
        var currentPercent = (secLeft / (float)countTime) * 100f;
        for (int i = 0; i < colorSequence.Count; ++i)
        {
            if (currentPercent > colorSequence[i].percent)
            {
                if (i > 0)
                {
                    var i2 = i - 1;
                    var pSmall = colorSequence[i].percent;
                    var pLarge = colorSequence[i2].percent;
                    var t = (currentPercent - pSmall) / (pLarge - pSmall);
                    //Debug.LogFormat("T: {0}, i {1},{2}\n ps {3} pl {4}", t, i, i2, pSmall, pLarge);
                    if(_txtMesh)_txtMesh.color = Color.Lerp(colorSequence[i].color, colorSequence[i2].color, t);
                }
                else if (_txtMesh) _txtMesh.color = colorSequence[i].color;
                break;
            }
        }

    }

    private float GetTimeToNextTick(float wait)
    {
        return wait-(SecondsPass % wait);
    }

    private IEnumerator TickSound()
    {
        while (SecondsLeft > 0)
        {
            if (doubleTimeSound > SecondsLeft)
            {
                yield return new WaitForSeconds(GetTimeToNextTick(.25f));
                if (audioS) audioS.Play();
            }
            else if (makeQuickSoundFromTime > SecondsLeft)
            {
                yield return new WaitForSeconds(GetTimeToNextTick(.5f));
                if (audioS) audioS.Play();
            }
            else if (makeSoundFromTime > SecondsLeft)
            {
                yield return new WaitForSeconds(GetTimeToNextTick(1));
                if (audioS) audioS.Play();
            }
            else if (slowSound > SecondsLeft)
            {
                if (audioS) audioS.Play();
                yield return new WaitForSeconds(GetTimeToNextTick(2));
            }
            else
            {
                yield return 0;
            }

        }
    }
    private IEnumerator CountTime()
    {
        string str = "";
        while (SecondsLeft > 0)
        {
            //secondsLeft = countTime - i;
            str = ToClock((int)SecondsLeft);
            if (_uiText != null) _uiText.text = str;
            if (_txtMesh != null) _txtMesh.text = str;
            SetColor(SecondsLeft);

            yield return new WaitForSeconds(GetTimeToNextTick(1));
        }
        str = ToClock(0);
        if (_uiText != null) _uiText.text = str;
        if (_txtMesh != null) _txtMesh.text = str;

        onTimeEnd.Invoke();
    }
    
    private string ToClock(int timeLeft)
    {
        switch (shape)
        {
            case ClockShape.Seconds:
                return string.Format("{0}", (timeLeft % 60).ToString("00"));

            case ClockShape.MinutesAndSeconds:
                return string.Format("{0}:{1}", (timeLeft / 60 % 60).ToString("00"), (timeLeft % 60).ToString("00"));

            case ClockShape.HoursAndMinutesAndSeconds:
                return string.Format("{0}:{1}:{2}", (timeLeft / 60 / 60).ToString("00"), (timeLeft / 60 % 60).ToString("00"), (timeLeft % 60).ToString("00"));
            case ClockShape.TotalSeconds:
                return string.Format("{0}", timeLeft.ToString("000"));
            default:
                throw new System.ArgumentOutOfRangeException("clock shapre not supported " + shape);
        }
    }
}
