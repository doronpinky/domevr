﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ButtonAnimation : MonoBehaviour
    {
        private Animator anim;

        private void Awake()
        {
            anim = GetComponent<Animator>();
        }

        public void OnHoverStart() { SetState("ScaleUp"); }
        public void OnClickStart() { SetState("Click"); }
        public void OnHoverEnd() { SetState("ScaleDown"); }


        private void OnEnable()
        {
            SetState("ScaleDown");
        }

        private void SetState(string value)
        {
            anim.ResetTrigger("ScaleUp");
            anim.ResetTrigger("Click");
            anim.ResetTrigger("ScaleDown");
            anim.SetTrigger(value);
        }

    }
}