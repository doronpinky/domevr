﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Linq;

namespace DomeVR
{
    public class ClickAbleButton : MonoBehaviour, IInteractiveItem
    {
        public bool debug;
        public int[] idsInteractable;

        //public bool clickReverse = false;

        public int maxClicks = 1;

        public float waitTimeAfterEnable = 0f;
        private float enableTime;
        public void StopClicks()
        {
            clickCount = maxClicks;
        }
        public string domePivotTag = "GuiDome";
        private string maskName = "GuiSetupDome";

        public Dweiss.EventEmpty onClickEnter, onClickContinue, onClickExit;
        public Dweiss.EventEmpty onHoverEnter, onHoverContinue, onHoverExit;

        public Dweiss.EventEmpty isPressed, isUnpressed;
       
        private int clickCount;

        private bool TimeToClick { get { return Time.time >= enableTime + waitTimeAfterEnable; } }

        private bool CanClickMore
        {
            get { return maxClicks < 0 || clickCount < maxClicks; }
        }
        public bool IsInteractable(IController cntrlr) {
            return IsClickable(cntrlr);
        }

        public void ResetClicks()
        {
            clickCount = 0; enableTime = Time.time;
        }

        public bool IsClickable(IController cntrlr)
        {
            if (CanClickMore == false || TimeToClick == false) return false;
            for (int i = 0; i < idsInteractable.Length; i++)
            {
                if (idsInteractable[i] == cntrlr.Id) return true;
            }
            return false;
        }

        public void OnClickContinues(IController cntrlr, Vector3 pointInteract)
        {
            if (IsClickable(cntrlr) == false) return;

            onClickContinue.Invoke();
        }

        public void OnClickEnd(IController cntrlr, Vector3 pointInteract)
        {
            if (IsClickable(cntrlr) == false) return;

            onClickExit.Invoke();
        }

        public void OnClickStart(IController cntrlr, Vector3 pointInteract)
        {
            if (IsClickable(cntrlr) == false) return;
            ++clickCount;

            Debug.Log(transform.FullName() + " OnClickStart");
            onClickEnter.Invoke();
            if (clickCount == maxClicks) isPressed.Invoke();
            else if (clickCount == 0)  isUnpressed.Invoke();
        }

        public void OnHoverContinues(IController cntrlr, Vector3 pointInteract)
        {
            if (debug) Debug.Log(name + " OnHoverContinues");
            //if (IsInteractable(cntrlr) == false) return;
            onHoverContinue.Invoke();
        }

        public void OnHoverEnter(IController cntrlr, Vector3 pointInteract)
        {
            if (debug) Debug.Log(name + " OnHoverEnter");
            //if (IsInteractable(cntrlr) == false) return;
            onHoverEnter.Invoke();
        }

        public void OnHoverExit(IController cntrlr, Vector3 pointInteract)
        {
            if (debug) Debug.Log(name + " OnHoverExit");
            //if (IsInteractable(cntrlr) == false) return;
            onHoverExit.Invoke();
        }

        [ContextMenu("Look at dome")]
        private void SetLook()
        {
            var dome = GameObject.FindGameObjectWithTag(domePivotTag).transform;
            var domeCldr = dome.GetComponentInChildren<SphereCollider>();
            var domeSize = domeCldr.transform.lossyScale;

            var dir = this.transform.position - dome.position;
            transform.position = dome.transform.position + dir.normalized * domeSize.x / 2 * .8f;



            transform.rotation = Quaternion.LookRotation(-dir, Vector3.up);
           
            //transform.LookAt(dome, );
        }


        private void OnDrawGizmosSelected()
        {
            var domePivot = GameObject.FindGameObjectWithTag(domePivotTag);
            if (domePivot == null) return;
           // Debug.Log(domePivotTag);
            var dome = domePivot.transform;

            var dirToDome = dome.position - this.transform.position;
            var dirNoRotation = Vector3.ProjectOnPlane(dirToDome, Vector3.up);

            var angleToRotate = Vector3.SignedAngle(dirNoRotation, dirToDome, transform.right);

            //Gizmos.color = Color.red;
            //Gizmos.DrawRay(transform.position, dirToDome);

            //Gizmos.color = Color.blue;
            //Gizmos.DrawRay(transform.position, dirNoRotation);

            Gizmos.color = Color.white;
            Gizmos.DrawRay(transform.position, transform.forward * 7);
            Gizmos.DrawRay(transform.position, transform.up * 2);

            var cldr = GetComponentInChildren<Collider>();
            var wasClickable = cldr.enabled;
            cldr.enabled = true;
            var bounds = cldr.bounds;
            var boundsPoints = bounds.GetCenterPoints();
            cldr.enabled = wasClickable;

            var layerMask = 1 << LayerMask.NameToLayer("GuiSetupDome");
            var p1 = GetPointOnSphere(new Ray(boundsPoints[2] - transform.forward * 100, transform.forward), layerMask);
            var p2 = GetPointOnSphere(new Ray(boundsPoints[3] - transform.forward * 100, transform.forward), layerMask);
            var p3 = GetPointOnSphere(new Ray(boundsPoints[3] - new Vector3(0, 0.1f, 0) - transform.forward * 100, transform.forward), layerMask);
            var vOnShpere = (p1 - p2).normalized;
            var vBefore = (p2 - p3).normalized;

            var v = p2 - dome.position;
            var angle = Vector3.SignedAngle(vOnShpere, vBefore, transform.right);

            //Debug.DrawRay(transform.position, vOnShpere * 5, Color.yellow);
            //Debug.DrawRay(transform.position, vBefore * 5, Color.cyan);
            //Debug.Log("vOnShpere " + vOnShpere + " vBefore " + vBefore + " ang " + angle);

            var rotationCalc = Quaternion.AngleAxis(-2 * angle, transform.right);
            var newForwad = rotationCalc * transform.rotation * Vector3.forward;
            var newUp = rotationCalc * transform.rotation * Vector3.up;

       //     Debug.DrawRay(transform.position, newForwad * 7, Color.magenta);
        //    Debug.DrawRay(transform.position, newUp * 2, Color.magenta);
        }

        [ContextMenu("Rotate With dome")]
        private void SetRotation() { 
            SetLook();
            var dome = GameObject.FindGameObjectWithTag(domePivotTag).transform;

            var cldr = GetComponentInChildren<Collider>();
            var wasClickable = cldr.enabled;
            cldr.enabled = true;
            var boundsPoints = cldr.bounds.GetCenterPoints();
            cldr.enabled = wasClickable;

            var layerMask = 1 << LayerMask.NameToLayer(maskName);
            var p1 = GetPointOnSphere(new Ray(boundsPoints[2] - transform.forward * 100, transform.forward), layerMask);
            var p2 = GetPointOnSphere(new Ray(boundsPoints[3] - transform.forward * 100, transform.forward), layerMask);
            var p3 = GetPointOnSphere(new Ray(boundsPoints[3] - new Vector3(0, 0.1f, 0) - transform.forward * 100, transform.forward), layerMask);
            var vOnShpere = (p1 - p2).normalized;
            var vBefore = (p2 - p3).normalized;

            var v = p2 - dome.position;
            var angle = Vector3.SignedAngle(vOnShpere, vBefore, transform.right);

            Debug.LogFormat("LYR {0} {1} {2} {3}", layerMask, p1, p2, p3);

            var rotationCalc2 = Quaternion.AngleAxis(-2*angle, transform.right);
            var newForwad = rotationCalc2 * transform.rotation * Vector3.forward;
            var newUp = rotationCalc2 * transform.rotation * Vector3.up;
            transform.rotation = Quaternion.LookRotation(newForwad, newUp);
            
        }


        private Vector3 GetPointOnSphere(Ray ray, LayerMask layerMask)
        {
            RaycastHit hit;
            //Debug.DrawRay(ray.origin, ray.direction*100, Color.blue, 5);
            if(Physics.Raycast(ray, out hit, float.MaxValue, layerMask)){
                //Debug.DrawLine(ray.origin, hit.point, Color.red, 5);
                return hit.point;
            }
            return Vector3.positiveInfinity;
        }


        private void OnEnable()
        {
            enableTime = Time.time;
        }
    }
}