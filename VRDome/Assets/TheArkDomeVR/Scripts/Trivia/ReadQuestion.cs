﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace DomeVR
{
    public class ReadQuestion : MonoBehaviour
    {

        public string fileName = "q.txt";

        public string folder = "Questions";
        public string defaultClipSuffix = ".wav";
        public string defaultVideoSuffix = ".mp4";

        public bool IsTrueFalseQuestion { get { return isTrueFalseQ; } }
        public string[] Header { get { return header; } }
        public QuestionInfo[] QuestionList { get { return questionList; } }

        private string[] header;
        private QuestionInfo[] questionList;
        private bool isTrueFalseQ;


        [System.Serializable]
        public class QuestionInfo
        {
            public int questionNum;
            public bool IsAnswerCorrect(string answer) { return correctAnswer.ToLower() == answer.ToLower(); }
            public string CorrectAnswer { get { return correctAnswer; } }
            public int CorrectAnswerNum
            {
                get
                {
                    for (int i = 0; i < answers.Length; i++)
                    {
                        if (answers[i] == correctAnswer) return i;
                    }
                    return -1;
                }
            }
            public string Question { get { return question; } }
            public string[] Answers { get { return answers; } }

            public AudioClip Clip { get { return null; } }//TODO fix

            public string QuestionVoiceOverClipName { get { return questionVoiceOverClipName; } }
            public string AfterInfoAudioName { get { return afterInfoAudioName; } }
            public string AfterInfoText { get { return afterInfoText; } }
            public string PreMovieName { get { return preMovieName; } }


            public int GroupId { get { return groupId; } }
            public AnswerSoundEffectsEnum AnswerSoundEffects { get { return answerSoundEffects; } }

            public string[] OtherColums { get { return otherColums; } }
            private string[] otherColums;
            private void ShuffleAnswers() { answers.Shuffle(); }

            public void SortAnswers()
            {
                System.Array.Sort(answers, (a, b) => b.CompareTo(a));
            }
            private string questionVoiceOverClipName;
            private string afterInfoAudioName;
            private string afterInfoText;
            private string preMovieName;

            private int groupId = -1;

            private AnswerSoundEffectsEnum answerSoundEffects;
            public enum AnswerSoundEffectsEnum
            {
                Any,
                NotFunnyOnTrue
            }

            private string question;
            private string[] answers;
            private string correctAnswer;
            public QuestionInfo(string question, string[] answers, string questionVoiceOverClipName, string preMovieName, string afterInfoAudioName
                , string afterInfoText,
                string groupId,
                string afterAnswerSoundEffects,

                int questionNum, string[] otherColums)
            {

                answerSoundEffects = string.IsNullOrEmpty(afterAnswerSoundEffects) || afterAnswerSoundEffects.ToLower() == "any" ? AnswerSoundEffectsEnum.Any : AnswerSoundEffectsEnum.NotFunnyOnTrue;

                this.otherColums = otherColums;
                this.questionNum = questionNum;
                this.questionVoiceOverClipName = questionVoiceOverClipName;
                this.afterInfoAudioName = afterInfoAudioName;
                this.afterInfoText = afterInfoText;
                this.preMovieName = preMovieName;
                correctAnswer = answers[0];
                this.question = question;
                this.answers = answers;
                try
                {
                    if (string.IsNullOrEmpty(groupId) == false) this.groupId = int.Parse(groupId);//Will throw exception on a problem
                }
                catch (System.Exception)
                {
                    Debug.LogError(" Parse error of " + groupId + " : " + ToString());
                    throw;
                }
                answers.Shuffle();

                // Debug.LogFormat("Q#{3}. Clips {0} {1} {2}",questionVoiceOverClipName, afterInfoAudioName, preMovieName, questionNum);
            }

            public override string ToString()
            {
                var str = new System.Text.StringBuilder();
                str.Append(question + " >>> " + correctAnswer);
                for (int i = 0; i < answers.Length; i++)
                {
                    str.Append("\n");
                    str.Append(answers[i]);
                }
                return str.ToString();
            }

        }
        void Start()
        {
            ReadFile();
        }
        public void Init()
        {
            ReadFile();

        }

        void ReadFile()
        {

            var filePath = "";// Application.streamingAssetsPath;
            if (string.IsNullOrEmpty(folder) == false) filePath = System.IO.Path.Combine(filePath, folder);
            filePath += "/" + fileName;
            if (fileName.IndexOf(".") == -1) filePath += ".txt";

            // Debug.Log("read file " + filePath);
            PreloadStreamingAsset.S.Load(filePath, FillData);
            Debug.Log("Finish reading file " + fileName);
        }


        private bool IsTrueOrFalse(string str)
        {
            return str.ToLower() == "false" || str.ToLower() == "true";
        }

        // splits a CSV row 
        static public string[] SplitCsvLine(string line)
        {
            return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
            @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
            System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                    select m.Groups[1].Value).ToArray();
        }


        private bool CmprEither(string str, params string[] either)
        {
            for (int i = 0; i < either.Length; i++)
            {
                if (str.ToLower() == either[i].ToLower()) return true;
            }
            return false;
        }

        private const int QuestionDescIndex = 0,
            AnswerCorrectIndex = 1, AnswerWrong1Index = 2, AnswerWrong2Index = 3, AnswerWrong3Index = 4,
            QuestionVoiceOverNameIndex = 6, PreMovieNameIndex = 7, PostInfoTextIndex = 8, PostInfoAudioIndex = 9, GroupId = 10, AfterAnswerSoundEffects = 11
            ;

        private const char Seperator = '\t';

        private string GetFileWithSuffix(string fileName, string defaultSuffix)
        {
            if (string.IsNullOrEmpty(fileName) == false && fileName.Contains(".") == false)
            {
                return fileName + defaultSuffix;
            }
            return fileName;
        }

        private string TryStripParentasis(string txt)
        {
            if (stripParentasis) return StripParentasis(txt);
            return txt;
        }
        private string StripParentasis(string txt)
        {
            if (txt == null || txt.Length < 2) return txt;
            if (txt[0] == '"' && txt[txt.Length - 1] == '"')
            {
                var ret = txt.Substring(1, txt.Length - 2);
                Debug.LogFormat("Stripped parentasis {0} >>> {1}", txt, ret);
                return ret;
            }
            return txt;
        }
        public bool stripParentasis = false;
        private void FillData(string txt)
        {
            List<QuestionInfo> qBag = new List<QuestionInfo>();
            var lines = txt.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);
            header = lines[0].Split(Seperator);

            for (int i = 1 /*line[0] is the  header*/; i < lines.Length; i++)
            {
                if (lines[i].Any(a => a != Seperator && a != '\n' && a != '\r') == false) continue; // skip empty lines

                //Debug.Log(lines[i]);
                var colums = lines[i].Split(Seperator);

                //Debug.Log(string.Join("\n",colums));
                if (i == 1) isTrueFalseQ = string.IsNullOrEmpty(colums[AnswerWrong2Index]) && string.IsNullOrEmpty(colums[AnswerWrong3Index]);

                var answers = new List<string>();
                var others = new List<string>();

                string q = colums[QuestionDescIndex].Replace("\n", "");

                answers.Add(colums[AnswerCorrectIndex]);
                answers.Add(colums[AnswerWrong1Index]);
                if (isTrueFalseQ == false)
                {
                    answers.Add(colums[AnswerWrong2Index]);
                    answers.Add(colums[AnswerWrong3Index]);
                }

                var voiceOverName = colums.ElementAtOrDefault(QuestionVoiceOverNameIndex);
                var preMovieName = colums.ElementAtOrDefault(PreMovieNameIndex);
                var postText = colums.ElementAtOrDefault(PostInfoTextIndex);
                var postInfoAudio = colums.ElementAtOrDefault(PostInfoAudioIndex);

                voiceOverName = GetFileWithSuffix(voiceOverName, defaultClipSuffix);
                preMovieName = GetFileWithSuffix(preMovieName, defaultVideoSuffix);
                postInfoAudio = GetFileWithSuffix(postInfoAudio, defaultClipSuffix);

                var groupId = colums.ElementAtOrDefault(GroupId);
                var afterAnswerSoundEffects = colums.ElementAtOrDefault(AfterAnswerSoundEffects);


                qBag.Add(new QuestionInfo(TryStripParentasis(q), answers.Select(a=> TryStripParentasis(a)).ToArray(), voiceOverName, preMovieName, postInfoAudio, TryStripParentasis(postText), groupId, afterAnswerSoundEffects, i - 1, colums));

                

                //var cA = colums[0].Replace("\n", "");
            
                //Debug.Log(answers.ToCommaString());
                //qBag.Add(new QuestionInfo(q, answers.ToArray(), 
                //    questionsAudios.Length == 0 ? null : questionsAudios[i - 1], i - 1, others.ToArray()));

            }
            questionList = qBag.ToArray();
        }



        public override string ToString()
        {
            return ToString(false);
        }
        public string ToString(bool includeQuestions)
        {
            var str = new System.Text.StringBuilder();
            str.Append(name + " (" + questionList.Length + ")");
            if (includeQuestions)
            {
                for (int i = 0; i < questionList.Length; i++)
                {
                    str.Append(questionList[i].ToString());
                }
            }
            return str.ToString();
        }
    }
}