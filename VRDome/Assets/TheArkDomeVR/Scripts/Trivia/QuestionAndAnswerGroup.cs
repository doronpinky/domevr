﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

namespace DomeVR
{
    [DefaultExecutionOrder(-111)]
    public class QuestionAndAnswerGroup : MonoBehaviour
    {

        //private string moviePrefix = "";
       // private const string movieSuffix = ".mp4";

        public TextMeshPro info;

        public TextMeshPro question;
        public TextMeshPro[] answers;
        public TextMeshPro currentScore;
        public TextMeshPro bestScore;


        public bool startClear = false;
        public Clock timer;

        public VideoPlayer[] movieFrame = new VideoPlayer[0];

        public TextMeshPro endYearScore;

        private ClickAbleButton[] answersButtons;

        private ClickAbleButton clickedAnswer;
        public ClickAbleButton SelectedAnswer
        {
            get { return clickedAnswer; }
        }
        private Dictionary<Behaviour, GameObject> itemToHisParent = new Dictionary<Behaviour, GameObject>();


        public bool IsMyAnswer(GameObject go)
        {
            for (int i = 0; i < answers.Length; i++)
            {
                if (itemToHisParent[answers[i]] == go) return true;
            }
            return false;
        }

        private void OnDisable()
        {
            Debug.Log(">> " + transform.FullName() + " disabled");
        }

        private void Awake()
        {
            Debug.Log("Awake " + transform.FullName() + " " + enabled);
            if (enabled == false) return;

            /// moviePrefix = Application.dataPath + "/StreamingAssets/";

            //   // SetGroup(movieFrame.transform, false);
            //    SetGroup(endYearScore.transform, false);
            //}
            //private void Awake()
            //{
            if(info) itemToHisParent[info] = info.GetParentAt(2).gameObject;
            itemToHisParent[question] = question.GetParentAt(2).gameObject;
            foreach (var a in answers) itemToHisParent[a] = a.GetParentAt(3).gameObject;
            itemToHisParent[currentScore] = currentScore.GetParentAt(2).gameObject;

            itemToHisParent[bestScore] = bestScore.GetParentAt(2).gameObject;

            movieFrame = movieFrame.Where(a => a != null).ToArray();

            for (int i = 0; i < movieFrame.Length; i++)
            {
                itemToHisParent[movieFrame[i]] = movieFrame[i].transform.GetParentAt(2).gameObject;
            }
            itemToHisParent[endYearScore] = endYearScore.GetParentAt(2).gameObject;
            itemToHisParent[timer] = timer.gameObject;


            //foreach (Transform t in transform)
            //{
            //    t.gameObject.SetActive(true);
            //}
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }
            foreach (var kv in itemToHisParent)
            {
                if(kv.Value == gameObject)
                {
                    Debug.LogError("Problem with " + kv.Key.transform.FullName() + " go " + gameObject);
                }
                if(movieFrame.Contains(kv.Key) == false) kv.Value.SetActive(true);
            }

            itemToHisParent[endYearScore].SetActive(false);

            //Debug.Log("Dic " + itemToHisParent.ToCommaString("\n", true));
        }
        private void Start()
        {
            answersButtons = new ClickAbleButton[answers.Length];
            for (int i = 0; i < answers.Length; i++)
            {
                var button = answers[i].GetComponentInParent<ClickAbleButton>();
                answersButtons[i] = button;
                //Debug.Log("Buttons from answer "+ i + ": " + answers[i].transform.FullName() + " -> " + button);
                answersButtons[i].onClickEnter.AddListener(() => AnswerClicked(button));
            }
            if (startClear)
            {
                ClearGui();
            }


        }

        

        //public void ShowQuestionAnswersScoreAndTimer(string q, string[] ans, int score, int sec)
        //{
        //    foreach (Transform t in transform)
        //    {
        //        t.gameObject.SetActive(false);
        //    }

        //    itemToHisParent[bestScore].SetActive(true);
        //    SetQuestion(q, ans);
        //    SetScore(score);
        //    ResetTimer(sec);
        //}
        public void ShowInfoCorrectAnswersAndScore(string q, string correctAns, int score)
        {
            Debug.Log("ShowInfoCorrectAnswersAndScore");
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }

            itemToHisParent[bestScore].SetActive(true);

            HideAllNoneAnswers(correctAns);
            ShowJustQuestion(q);
            SetScore(score);
        }


        public void ResetTimer(float time)
        {
            Debug.Log("ResetTimer");
            itemToHisParent[timer].SetActive(true);
            timer.Reset((int)time);
        }
        public void SetAnswers(string[] newAnswers)
        {
            Debug.Log("SetAnswers");
            for (int i = 0; i < newAnswers.Length; i++)
            {
                answersButtons[i].ResetClicks();
                itemToHisParent[answers[i]].SetActive(true);

                answersButtons[i].StopClicks();
                answers[i].text = newAnswers[i];
            }
        }
        public void ResetAnswers()
        {
            Debug.Log("ResetAnswers");
            for (int i = 0; i < answers.Length; i++)
            {
                answersButtons[i].StopClicks();
                answers[i].text = "";
            }
        }


        public void SetInfo(string txt)
        {

            Debug.Log("SetInfo");

            info.text = txt;


        }

        public void HideQuestion()
        {
            Debug.Log("HideQuestion");
            itemToHisParent[question].SetActive(false);
        }
        public void HideAllNoneAnswers(string answer)
        {
            Debug.Log("HideAllNoneAnswers");
            for (int i = 0; i < answers.Length; i++)
            {
                itemToHisParent[answers[i]].SetActive(answers[i].text == answer);
                if(answers[i].text == answer)
                {
                    answersButtons[i].StopClicks();
                }
            }
        }

        private void AnswerClicked(ClickAbleButton button)
        {
            Debug.Log("AnswerClicked");
            clickedAnswer = button;
            for (int i = 0; i < answersButtons.Length; i++)
            {
                if (clickedAnswer != answersButtons[i])
                    answersButtons[i].ResetClicks();
            }
        }

        public void ShowJustQuestion(string q)
        {

            Debug.Log("ShowJustQuestion");
            itemToHisParent[question].SetActive(true);

            question.text = q;
        }


        public void SetQuestion(string q, string[] newAnswers)
        {
            Debug.Log("SetQuestion");
            itemToHisParent[question].SetActive(true);

            if (q != null) question.text = q;
            for (int i = 0; i < newAnswers.Length; i++)
            {
                itemToHisParent[answers[i]].SetActive(true);
                
                answersButtons[i].ResetClicks();
                answers[i].text = newAnswers[i];
            }
            for (int i = 0; i < answersButtons.Length; i++)
            {
                answersButtons[i].ResetClicks();
            }
            clickedAnswer = null;
        }

        public void SetScore(int score)
        {
            Debug.Log("SetScore");
            itemToHisParent[currentScore].SetActive(true);
            currentScore.text = score.ToString();
        }


        public void SetBestScore(int score)
        {
            Debug.Log("SetBestScore");
            itemToHisParent[bestScore].SetActive(true);
            bestScore.text = "Best " + score.ToString();
        }


        public void ClearGui()
        {
            Debug.Log("ClearGui");
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }
            foreach (var kv in itemToHisParent)
            {
                kv.Value.SetActive(false);
            }

        }
        //private void SetGroup(Transform thirdChild, bool isActive = false)
        //{
        //    //question.GetParentAt(2).gameObject.SetActive(false);
        //    thirdChild.transform.parent.parent.gameObject.SetActive(isActive);
        //}
        public void SetYearScore(int score)
        {

            Debug.Log("Set year");
            //SetGroup(question.transform);
            //foreach (var a in answers) SetGroup(a.transform);
            //SetGroup(currentScore.transform);
            //SetGroup(bestScore.transform);

            //timer.gameObject.SetActive(false);
            //SetGroup(movieFrame.transform);

            //SetGroup(endYearScore.transform, true);

            ClearGui();
            itemToHisParent[endYearScore].SetActive(true);

            //public TextMeshPro endYearScore;
            endYearScore.text = score.ToString();
        }

        //public void PrepareMovie(string fileName)
        //{
        //    movieFrame.url = GetFullMovieName(fileName);
        //    movieFrame.Prepare();
        //}

        private System.Action videoPrepCallback;
        public void PlayMovie(VideoClip clip, System.Action callback)
        {
            videoPrepCallback = callback;

            for (int i = 0; i < movieFrame.Length; i++)
            {
                itemToHisParent[movieFrame[i]].SetActive(true);
                movieFrame[i].clip = clip;
                movieFrame[i].Prepare();
                movieFrame[i].Play();
            }
            
            movieFrame[0].prepareCompleted += WaitForPrepearation;
            

            
            //movieFrame.GetComponent<Renderer>().enabled = true;
        }

        public void PlayNow(VideoClip clip, System.Action callback)
        {
            //7videoPrepCallback = callback;

            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }

            itemToHisParent[currentScore].SetActive(true);
            itemToHisParent[bestScore].SetActive(true);
            // movieFrame.transform.parent.GetComponent<Renderer>().enabled = false;
            movieFrame.Foreach(a => itemToHisParent[a].SetActive(true));
            if (movieFrame[0].isPrepared)
            {
                movieFrame.Foreach(a => a.Play());
                if (callback != null)
                    StartCoroutine(FindEnd(callback));
            } else
            {
                PlayMovie(clip, callback);
            }


           // Debug.Log("WaitForPrepearation");
            this.WaitForSeconds(.3f, () =>
            {
                movieFrame.Foreach(a => a.GetComponent<Renderer>().enabled = true);
                //movieFrame.transform.parent.GetComponent<Renderer>().enabled = true;
            });

        }

        public void PrepareMovie(VideoClip clip)
        {
            movieFrame.Foreach(a =>
            {
                itemToHisParent[a].SetActive(true);
                //movieFrame.transform.parent.GetComponent<Renderer>(). enabled = false;
                a.clip = clip;
                a.Prepare();
                a.GetComponent<Renderer>().enabled = false;
            }
            );

        }
        private void WaitForPrepearation(VideoPlayer source)
        {
            movieFrame[0].prepareCompleted -= WaitForPrepearation;

            //Debug.Log("WaitForPrepearation");
            this.WaitForSeconds(.3f, () =>
            {
                movieFrame.Foreach(a => a.GetComponent<Renderer>().enabled = true);
                //movieFrame.transform.parent.GetComponent<Renderer>().enabled = true;
            });

            if (videoPrepCallback != null)
            {
                StartCoroutine(FindEnd(videoPrepCallback));
                videoPrepCallback = null;
            }
        }
        private IEnumerator FindEnd(System.Action callback)
        {
            
            while ( movieFrame[0].isPlaying)
            {
                yield return 0;
            }
            Debug.Log("End movie " + transform.FullName());
            callback();
        }


        //private string GetFullMovieName(string name)
        //{
        //    if (name[0] == '/' || name[0] == '\\') name = name.Substring(1);
        //    if (name.IndexOf(".") < 0)
        //    {
        //        name += movieSuffix;
        //    }
        //    if (name.StartsWith(moviePrefix) == false)
        //    {
        //        name = moviePrefix + name;
        //    }
        //    return name;//.Replace("/", "\\"); ;
        //}
    }
}