﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR {
public class ReadTextFromFile : MonoBehaviour
    {

        public Dweiss.EventString onReadText;

        public string fileName;
        public string folder;

        public void Start()
        {
            ReadFile();

        }

        void ReadFile()
        {

            var filePath = "";// Application.streamingAssetsPath;
            if (string.IsNullOrEmpty(folder) == false) filePath = System.IO.Path.Combine(filePath, folder);
            filePath += "/" + fileName;
            if (fileName.IndexOf(".") == -1) filePath += ".txt";

            // Debug.Log("read file " + filePath);
            Dweiss.PreloadStreamingAsset.S.Load(filePath, OnLoadText);
            Debug.Log("Finish reading file " + fileName);
        }

        private void OnLoadText(string str)
        {
            onReadText.Invoke(str);
        }

    }
}