﻿//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//using Dweiss.Msg;
//using System.Linq;
//using Dweiss;

//namespace DomeVR
//{
//    //namespace MyTriviaHelper
//    //{
//    //    public static class MyTriviaHelper
//    //    {
//    //        public static void For<T>(this T[] qGroup, System.Action<T> func)
//    //        {
//    //            for (int i = 0; i < qGroup.Length; i++)
//    //            {
//    //                func(qGroup[i]);
//    //            }
//    //        }
//    //        public static void For<T>(this T[] qGroup, System.Action<T, int> func)
//    //        {
//    //            for (int i = 0; i < qGroup.Length; i++)
//    //            {
//    //                func(qGroup[i], i);
//    //            }
//    //        }
//    //        public static E[] Get<T, E>(this T[] qGroup, System.Func<T, E> func)
//    //        {
//    //            var ret = new E[qGroup.Length];
//    //            for (int i = 0; i < qGroup.Length; i++)
//    //            {
//    //                ret[i] = func(qGroup[i]);
//    //            }
//    //            return ret;
//    //        }
//    //        public static E GetFirst<T, E>(this T[] qGroup, System.Func<T, E> func) where E : class
//    //        {
//    //            for (int i = 0; i < qGroup.Length; i++)
//    //            {
//    //                var ret = func(qGroup[i]);
//    //                if (ret != null) return ret;
//    //            }
//    //            return null;
//    //        }

//    //        public static int GetIndex<T>(this T[] qGroup, System.Func<T, bool> func)
//    //        {
//    //            for (int i = 0; i < qGroup.Length; i++)
//    //            {
//    //                if (func(qGroup[i])) return i;
//    //            }
//    //            return -1;
//    //        }
//    //    }

//    //}

//    public class TriviaLogic : MonoBehaviour
//    {
//        public bool moveNextQuestionAutomatically = true;
//        public bool setQuestionOnStart = true;
//        public float scoreMultiplier = 1;


//        public QuestionBag qBag;
//        public AudioSource aSource;
//        public QuestionAndAnswerGroup[] qAndAGroup;
//        public bool stopTimerOnAnswer = false;
//        public float delayAfterInfo = 3;
//        public float delayFromQuestionAnswer = .2f;

//        public Dweiss.EventEmpty onCorrectAnswer, onWrongAnswer, onStartQuestion,
//            onRoundFinished, onAnswerTimeEnd;

//        [Header("Id of question")]
//        public Dweiss.EventInt onQAnswered;

//        public Dweiss.EventEmpty onInfoFinish;


//        private int answersCount = 0;

//        public bool showYearScore;

//        private ScriptableSceneSettings.TriviaInfo TriviaInfo
//        {
//            get { return GameFlow.S.CurrentMovieScene.triviaInfo; }
//        }

//        IEnumerator Start()
//        {
//            yield return 0;
//            if (TriviaInfo.AnswerAsManyQuestionPerRound)
//            {
//                Debug.Log("Register to total timer");
//                qAndAGroup[0].timer.onTimeEnd.AddListener(RoundTimeEnd);
//            }
//            else
//            {
//                Debug.Log("Register to question timer");
//                qAndAGroup[0].timer.onTimeEnd.AddListener(OnAnswerTimeEnd);//Register once
//            }
//            qBag.Init(TriviaInfo.QuestionSequence, TriviaInfo.QuestionId);//TODO allow randomize
//            if (setQuestionOnStart)
//            {
//                ResetTimers();
//                SetQuestion();
//                qAndAGroup.For(a => a.SetScore((int)(ScoreSystem.S.CurrentPoints * scoreMultiplier)));
//                qAndAGroup.For(a => a.SetBestScore((int)(ScoreSystem.S.BestLevelPoints * scoreMultiplier)));
//            }


//            if (qBag.QuestionList[0].Answers.Length != qAndAGroup[0].answers.Length)
//            {
//                Debug.LogErrorFormat("Answers list from document not like available answers box {0} : {1}",
//                    qBag.QuestionList[0].Answers.Length, qAndAGroup[0].answers.Length);
//            }

//            MsgSystem.MsgStr.Raise("TriviaRoundStart");
//            Debug.Log("TriviaRoundStart");
//        }

//        public void ResetTimers()
//        {
//            if (TriviaInfo.AnswerAsManyQuestionPerRound)
//            {
//                qAndAGroup.For(a => a.ResetTimer((int)TriviaInfo.TotalQTime));
//            }
//            else
//            {
//                qAndAGroup.For(a => a.ResetTimer((int)TriviaInfo.SecondsPerQuestion));
//            }
//        }
//        private void SetQuestion()
//        {

//            MsgSystem.MsgStr.Raise("SetQuestion");

//            //Debug.Log("SetQuestion");
//            if (qBag.HasMoreQuestions)
//            {
//                if (qBag.CurrentQ.Clip != null)
//                {
//                    aSource.clip = qBag.CurrentQ.Clip;
//                    aSource.Play();
//                }
//                SetupQAndA(qBag.CurrentQ.Question, qBag.CurrentQ.Answers);
//            }
//            else
//            {
//                RoundQuestionsEnd();
//            }
//        }

//        public void OnAnswerTimeEnd()
//        {
//            Debug.Log("OnAnswerTimeEnd");
//            qAndAGroup.For(a => a.timer.Stop());
//            var answer = qAndAGroup.FirstOrDefault(a => a.SelectedAnswer != null);
//            if (answer != null)
//            {
//                var txt = answer.GetComponent<TextMeshPro>();
//                OnAnswerSelected(txt.text);

//            }
//            else
//            {
//                OnAnswerSelected("");
//            }
//            onAnswerTimeEnd.Invoke();

//        }

//        private void RoundTimeEnd()
//        {
//            EndTrivia();
//        }

//        private void RoundQuestionsEnd()
//        {
//            EndTrivia();
//        }
//        //private void EndTrivia()
//        //{
//        //    MsgSystem.MsgStr.Raise("TriviaRoundEnd");
//        //    Debug.Log("TriviaRoundEnd");
//        //    onRoundFinished.Invoke();
//        //    AllSetupQAndA("", null);
//        //    qAndAGroup.For(a => a.SetYearScore(ScoreSystem.S.CurrentYears));
//        //}


//        ////public void OnRoundTimeEnd() { throw new System.NotImplementedException(); }
//        public void EndTrivia()
//        {
//            StopAllCoroutines();
//            qAndAGroup[0].timer.Stop();// no event

//            MsgSystem.MsgStr.Raise("TriviaRoundEnd");
//            Debug.Log("TriviaRoundEnd");
//            onRoundFinished.Invoke();
//            //SetupQAndA("", null);

//            if (showYearScore)
//            {
//                qAndAGroup.For(a => a.ClearGui());
//                qAndAGroup.For(a => a.SetYearScore(ScoreSystem.S.CurrentYears));
//            }
//            else
//            {
//                qAndAGroup.For(a => a.ClearGui());
//            }

//        }
        

//        public void MoveToNextQuestion()
//        {
//            //Debug.Log("MoveToNextQuestion");
//            if (TriviaInfo.HasTimeLimitPerQuestion && answersCount == TriviaInfo.TotalQuestionsInPredfinedTimedQuestion)
//            {
//                RoundQuestionsEnd();
//            }
//            else
//            {
//                if (moveNextQuestionAutomatically) qBag.MoveToNextQuestion();
//                SetQuestion();
//            }
//        }


//        public void OnAnswerSelected(string answerText)
//        {
//            if(stopTimerOnAnswer)
//                qAndAGroup[0].timer.Stop();// no event
//            answersCount++;
//            Debug.Log("OnAnswerSelected " + answersCount + ". " + answerText);

//            if (qBag.CurrentQ.CorrectAnswer == answerText)
//            {
//                MsgSystem.MsgStr.Raise("CorrectAnswer");
//                onCorrectAnswer.Invoke();
//                ScoreSystem.S.AddCorrectAnswer();
//                qAndAGroup.For(a => a.SetScore((int)(ScoreSystem.S.CurrentPoints * scoreMultiplier)));
//            }
//            else
//            {
//                MsgSystem.MsgStr.Raise("WrongAnswer");
//                onWrongAnswer.Invoke();
//            }
//            onQAnswered.Invoke(qBag.CurrentQuestionIndex);

//        }

//        private void SetupQAndA(string question, string[] answers)
//        {
//            // Debug.Log("SetupQAndA");
//            onStartQuestion.Invoke();

//            if (answers != null && qBag.IsTrueFalse)
//            {
//                System.Array.Sort(answers, (a, b) => b.CompareTo(a));
//            }

//            qAndAGroup.For(a => a.ResetAnswers());
//            if (answers != null)
//            {
//                qAndAGroup.For(a => a.SetQuestion(question, answers));
//            }

//            if (TriviaInfo.HasTimeLimitPerQuestion)
//                qAndAGroup.For(a => a.ResetTimer((int)TriviaInfo.SecondsPerQuestion));

//            Debug.Log("SetupQAndA " + (answersCount + 1) + ". " + question);
//        }

//        private void OnGameObjectAnswerSelected(GameObject go)
//        {
//            if(stopTimerOnAnswer)
//                qAndAGroup[0].timer.Stop();// no event
//            //Debug.Log("OnGameObjectAnswerSelected");

//            var txt = go.GetComponentInChildren<TMPro.TextMeshPro>();
//            StartCoroutine(DelayAnswerSelected(txt.text));
//        }
//        private IEnumerator DelayAnswerSelected(string txt)
//        {
//            if (delayFromQuestionAnswer > 0) yield return new WaitForSeconds(delayFromQuestionAnswer);
//            OnAnswerSelected(txt);
//        }

//        private void OnEnable()
//        {
//            Dweiss.Msg.MsgSystem.MsgStr.Register("AnswerSelected", OnGameObjectAnswerSelected);
//        }
//        private void OnDisable()
//        {
//            if (Dweiss.Msg.MsgSystem.S)
//                Dweiss.Msg.MsgSystem.MsgStr.Unregister("AnswerSelected", OnGameObjectAnswerSelected);
//        }


//        //private void SetInfo(string info)
//        //{

//        //    qAndAGroup.For(a => a.HideAllNoneAnswers(qBag.CurrentQ.CorrectAnswer));
//        //    qAndAGroup.For(a => a.SetInfo(info));
//        //    //Debug.Log("Show info with wait");
//        //    this.WaitForSeconds(delayAfterInfo, InfoFinished);
//        //}
//        private void ShowScore()
//        {
//            qAndAGroup.For(a => a.SetScore((int)(ScoreSystem.S.CurrentPoints * scoreMultiplier)));
//            qAndAGroup.For(a => a.SetBestScore((int)(ScoreSystem.S.BestLevelPoints * scoreMultiplier)));
//        }
//        private void InfoFinished()
//        {
//            //Debug.Log("InfoFinished");
//            onInfoFinish.Invoke();
//        }

//        public void PrepareMovie(UnityEngine.Video.VideoClip clip)
//        {
//            for (int i = 0; i < qAndAGroup.Length; i++)
//            {
//                qAndAGroup[i].PrepareMovie(clip);
//            }
//        }

//        public void PlayNow(UnityEngine.Video.VideoClip clip, System.Action callback)
//        {
//            for (int i = 0; i < qAndAGroup.Length; i++)
//            {
//                qAndAGroup[i].PlayNow(clip, i == 0 ? callback : null);
//            }
//        }

//        //public void PlayMovie(UnityEngine.Video.VideoClip clip, System.Action callback)
//        //{
//        //    for (int i = 0; i < qAndAGroup.Length; i++)
//        //    {
//        //        qAndAGroup[i].PlayMovie(clip, i == 0 ? callback : null);
//        //    }
//        //}

//        //public void HideQuestion()
//        //{
//        //    qAndAGroup.For(a => a.HideQuestion());
//        //}
//        //public void HideAllNoneAnswers(string answer)
//        //{
//        //    qAndAGroup.For(a => a.HideAllNoneAnswers(answer));
//        //}

//        public void ClearGui()
//        {
//            qAndAGroup.For(a => a.ClearGui());
//        }


//        public void ShowQuestionAnswersScoreAndTimer()
//        {
            
//            ClearGui();
//            SetQuestion();
//            ShowScore();
//            ResetTimers();
            
//            //onStartQuestion.Invoke();
//            //MsgSystem.MsgStr.Raise("SetQuestion");
//        }


//        public void ShowCorrectAnswerAndDelayWait()
//        {
//            qAndAGroup.For(a => a.HideAllNoneAnswers(qBag.CurrentQ.CorrectAnswer));
//            Debug.Log("ShowCorrectAnswerAndDelayWait");
//            this.WaitForSeconds(delayAfterInfo, InfoFinished);
//        }

//        public void ShowInfoCorrectAnswersAndScore()
//        {
//            qAndAGroup.For(a => a.ShowInfoCorrectAnswersAndScore(qBag.CurrentQ.Question,
//                qBag.CurrentQ.CorrectAnswer, (int)(ScoreSystem.S.CurrentPoints * scoreMultiplier)));

//            //Debug.Log("ShowInfoCorrectAnswersAndScore");
//            this.WaitForSeconds(delayAfterInfo, InfoFinished);
//        }

//    }
//}