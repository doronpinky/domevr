﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace DomeVR
{
    public class QuestionBag : MonoBehaviour
    {
        public bool endlessQuestion = false;

        private ReadQuestion qReader;

        private int currentQuestion;
        private int[] questionOrder;


        public ReadQuestion.QuestionInfo[] QuestionList { get {
                return qReader.QuestionList;
            } }
        public bool IsTrueFalse { get { return qReader.IsTrueFalseQuestion; } }

        public void MoveToNextQuestion() {
            ++currentQuestion;
            if (endlessQuestion && currentQuestion >= questionOrder.Length)
            {
                currentQuestion = 0;
            }
            
            //Debug.Log("QBag MoveToNextQuestion");
        }
        public int CurrentQuestionIndex { get { return questionOrder[currentQuestion]; } }
        public bool HasMoreQuestions { get { return endlessQuestion? true: currentQuestion < questionOrder.Length-1; } }
        public ReadQuestion.QuestionInfo GetQuestionAt(int index) { return QuestionList[questionOrder[index]]; }
        public ReadQuestion.QuestionInfo CurrentQ
        {
            get
            {
                var indexInQList = questionOrder[currentQuestion];
                return QuestionList[indexInQList];
            }
        }

        public void Init(string questionId, bool shuffleQuestionList = true)
        {
            Debug.LogFormat("Init {0}",questionId);

            qReader = GameObject.FindObjectsOfType<ReadQuestion>().First(a => a.fileName == questionId);

            Debug.LogFormat("qReader {0}", qReader);
            questionOrder = new int[QuestionList.Length];
            for (int i = 0; i < QuestionList.Length; i++)
            {
                questionOrder[i] = i;
            }
            if(shuffleQuestionList) questionOrder.Shuffle();
            Debug.LogFormat("Finished with {0}", questionId);
        }

    }
}