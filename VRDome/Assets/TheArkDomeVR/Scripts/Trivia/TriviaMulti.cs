﻿//using System.Collections;
//using System.Collections.Generic;
//using TMPro;
//using UnityEngine;
//using Dweiss.Msg;
//using System.Linq;
//using Dweiss;

//namespace DomeVR
//{
//    public class TriviaMulti : MonoBehaviour
//    {
//        [SerializeField] private QuestionBag qBag;
//        [SerializeField] private AudioSource aSource;
//        [SerializeField] private QuestionAndAnswerGroup[] qAndAGroup;
//        private ReadQuestion.QuestionInfo[] myActiveQuestion;



//        public float delayAfterInfo = 3;
//        [SerializeField] private float delayFromQuestionAnswer = .2f;

//        public Dweiss.EventEmpty onCorrectAnswer, onWrongAnswer, onStartQuestion, onRoundFinished;

//        [Header("Id of group answerd")]
//        public Dweiss.EventInt onQAnswered;
//            public Dweiss.EventInt onInfoFinish;


//        private int answersCount = 0;
//        [SerializeField]private bool moveToNextQuestionAutomatically = true;


//        private ScriptableSceneSettings.TriviaInfo TriviaInfo
//        {
//            get { return GameFlow.S.CurrentMovieScene.triviaInfo; }
//        }
//        private void Awake()
//        {
//            if(enabled)
//                myActiveQuestion = new ReadQuestion.QuestionInfo[qAndAGroup.Length];
//        }
//        private void Start()
//        {

//            qAndAGroup[0].timer.onTimeEnd.AddListener(RoundTimeEnd);
//            //qAndAGroup.For(a => a.timer.Reset((int)TriviaInfo.TotalQTime));
            
     
//            qBag.Init(TriviaInfo.QuestionSequence, TriviaInfo.QuestionId);//TODO allow randomize
//            qAndAGroup.For((a, index) => SetupNewQuestion(index));

//            ResetTimers();
//            //SetQuestion();
//            qAndAGroup.For(a => a.SetScore((int)(ScoreSystem.S.CurrentPoints)));
//            qAndAGroup.For(a => a.SetBestScore((int)(ScoreSystem.S.BestLevelPoints)));

//            //qAndAGroup.For(a => a.SetScore(0));
//            //qAndAGroup.For(a => a.SetBestScore(ScoreSystem.S.BestLevelPoints));


//            MsgSystem.MsgStr.Raise("TriviaRoundStart");
//            Debug.Log("TriviaRoundStart");
//        }

//        public void ResetTimers()
//        {
//                qAndAGroup.For(a => a.ResetTimer((int)TriviaInfo.TotalQTime));
//        }

//        private void RoundTimeEnd()
//        {
//            EndTrivia();
//        }

//        private void RoundQuestionsEnd()
//        {
//            EndTrivia();
//        }
//        private void EndTrivia()
//        {

//            MsgSystem.MsgStr.Raise("TriviaRoundEnd");
//            Debug.Log("TriviaRoundEnd");
//            StopAllCoroutines();
//            onRoundFinished.Invoke();
//            AllSetupQAndA("", null);

//            qAndAGroup.For(a => a.ClearGui());
//            qAndAGroup.For(a => a.SetYearScore(ScoreSystem.S.CurrentYears));
//        }
       

//            //Call setup after animation finished of question answered
//        public void SetupNewQuestion(int groupIndex)
//        {
//            MsgSystem.MsgStr.Raise("SetQuestion");

//            if (TriviaInfo.HasTimeLimitPerQuestion && answersCount == TriviaInfo.TotalQuestionsInPredfinedTimedQuestion)
//            {
//                RoundQuestionsEnd();
//            }
//            else
//            {
//                if (qBag.HasMoreQuestions)
//                {
//                    if (qBag.CurrentQ.Clip != null)
//                    {
//                        aSource.clip = qBag.CurrentQ.Clip;
//                        aSource.Play();
//                    }
//                    myActiveQuestion[groupIndex] = qBag.CurrentQ;
//                    SetupQAndA(qAndAGroup[groupIndex], qBag.CurrentQ.Question, qBag.CurrentQ.Answers);
//                    qBag.MoveToNextQuestion();
//                }
//                else
//                {
//                    RoundQuestionsEnd();
//                }
//            }
//        }


//        public void OnAnswerTimeEnd()
//        {
//            var txt = qAndAGroup.GetFirst(a => a.SelectedAnswer.gameObject);
//            qAndAGroup.For(a =>
//            {
//                OnAnswerSelected(a.SelectedAnswer.gameObject, a.SelectedAnswer.GetComponent<TextMeshPro>().text);
//            });
//        }

//        private void OnAnswerSelected(GameObject go, string answerText)
//        {
//            //qAndAGroup[0].timer.Stop();// no event
//            answersCount++;
//            Debug.Log("OnAnswerSelected " + answersCount + ". " + answerText);


//            var index = qAndAGroup.GetIndex(a => a.IsMyAnswer(go));
//            var q = myActiveQuestion[index];
//            if (q.CorrectAnswer == answerText)
//            {
//                MsgSystem.MsgStr.Raise("CorrectAnswer");
//                onCorrectAnswer.Invoke();
//                ScoreSystem.S.AddCorrectAnswer();
//                qAndAGroup.For(a => a.SetScore(ScoreSystem.S.CurrentPoints));
//            }
//            else
//            {
//                MsgSystem.MsgStr.Raise("WrongAnswer");
//                onWrongAnswer.Invoke();
//            }
//            onQAnswered.Invoke(index);
//            //if (moveToNextQuestionAutomatically) SetupNewQuestion(index);
//        }


//        public void ShowCorrectAnswerAndDelayWait(int groupIndex)
//        {
//            qAndAGroup[groupIndex].HideAllNoneAnswers(myActiveQuestion[groupIndex].CorrectAnswer);
//            this.WaitForSeconds(delayAfterInfo, ()=>InfoFinished(groupIndex));
//        }

//        private void InfoFinished(int groupIndex)
//        {
//            //Debug.Log("InfoFinished");
//            onInfoFinish.Invoke(groupIndex);
//        }

//        private void AllSetupQAndA(string question, string[] answers)
//        {
//            onStartQuestion.Invoke();

//            qAndAGroup.For(a => a.question.text = question);


//            if (answers != null && qBag.IsTrueFalse)
//            {
//                System.Array.Sort(answers, (a, b) => b.CompareTo(a));
//            }

//            qAndAGroup.For(a => a.ResetAnswers());


//            if (answers != null)
//            {
//                qAndAGroup.For(a => a.SetAnswers(answers));
//            }


//            if (TriviaInfo.HasTimeLimitPerQuestion)
//                qAndAGroup.For(a => a.timer.Reset((int)TriviaInfo.SecondsPerQuestion));

//            Debug.Log("SetupQAndA " + (answersCount + 1) + ". " + question);
//        }

//        private void SetupQAndA(QuestionAndAnswerGroup qG, string question, string[] answers)
//        {
//            onStartQuestion.Invoke();
//            if (answers != null && qBag.IsTrueFalse)
//            {
//                System.Array.Sort(answers, (a, b) => b.CompareTo(a));
//            }
//            qG.ResetAnswers();
//            if (answers != null)
//            {
//                qG.SetQuestion(question, answers);
//            }
            

//            Debug.Log("SetupQAndA " + (answersCount + 1) + ". " + question);
//        }

//        //Recieve event
//        public void OnGameObjectAnswerSelected(GameObject go)
//        {
//            Debug.Log(name + " OnGameObjectAnswerSelected " + go.transform.FullName());
//            var txt = go.GetComponentInChildren<TMPro.TextMeshPro>();
//            StartCoroutine(DelayAnswerSelected(go, txt.text));
//        }

//        private IEnumerator DelayAnswerSelected(GameObject go, string txt)
//        {
//            if (delayFromQuestionAnswer > 0) yield return new WaitForSeconds(delayFromQuestionAnswer);
//            OnAnswerSelected(go, txt);
//        }

//        private void OnEnable()
//        {
//            Dweiss.Msg.MsgSystem.MsgStr.Register("AnswerSelected", OnGameObjectAnswerSelected);
//        }
//        private void OnDisable()
//        {
//            if (Dweiss.Msg.MsgSystem.S)
//                Dweiss.Msg.MsgSystem.MsgStr.Unregister("AnswerSelected", OnGameObjectAnswerSelected);
//        }
//    }
//}