﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullRigidBody : MonoBehaviour
{
    public bool considerDistance;
    public float force, distanceFactor;
    public LayerMask ForceLayerMask;

    private Collider[] cldrs = new Collider[17];
    public Transform pullTarget;

    private float radius;
    private Transform t;
    private void Start()
    {
        t = transform;
        radius = transform.lossyScale.x / 2;
    }

    private void OnDrawGizmosSelected()
    {
#if UNITY_EDITOR
        radius = transform.lossyScale.x / 2;
#endif
        Gizmos.DrawWireSphere(transform.position, radius);
    }


    private void OnTriggerEnter(Collider cldr)
    {
        Pull(cldr);
        var audioS = cldr.GetComponentInChildren<AudioSource>();
        if (audioS) audioS.Play();
    }

    private void OnTriggerStay(Collider cldr)
    {
        Pull(cldr);
       
    }
    private void Pull(Collider cldr)
    {
        var rb = cldr.GetComponent<Rigidbody>();
        if (rb)
        {
            var dir = (cldr.transform.position - pullTarget.position);
            var distanceSqr = dir.sqrMagnitude;
            var distance = Mathf.Sqrt(distanceSqr);
            var distanceDiff = radius - distance;
            rb.AddForce(dir.normalized * force * (considerDistance ? distanceDiff * distanceFactor : 1));
        }
    }

//    void FixedUpdate()
//    {
//#if UNITY_EDITOR
//        radius = transform.lossyScale.x / 2;
//#endif


//        var count = Physics.OverlapSphereNonAlloc(t.position, radius, cldrs, ForceLayerMask);
//        for (int i = 0; i < count; i++)
//        {
//            Debug.Log("Hit " + cldrs[i]);
//            Pull(cldrs[i]);
//        }
//    }
}
