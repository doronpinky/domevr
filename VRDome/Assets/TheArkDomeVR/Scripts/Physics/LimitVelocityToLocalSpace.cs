﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitVelocityToLocalSpace : MonoBehaviour
{
    public Vector3 localFactor = Vector3.one;

    private Rigidbody rb;

    private Transform t;
    // Start is called before the first frame update
    void Start()
    {
        t = transform;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = t.rotation * ((Quaternion.Inverse(t.rotation) * rb.velocity).PointMul( localFactor));
    }
}
