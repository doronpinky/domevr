﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Video;
using UnityEngine.Audio;

namespace DomeVR
{
    public class GameFlow : Dweiss.Singleton<GameFlow>
    {
        public bool debug;
        public bool gcBetweenScenes;


        public void RegisterVideo(VideoPlayer vid)
        {
            videos.Add(vid);
            vid.playbackSpeed = timeScale;
            // SetSpeed(timeScale);
            this.WaitForSeconds(.05f, () => vid.playbackSpeed = timeScale);
        }
        public void UnregisterVideo(VideoPlayer vid)
        {
            videos.Remove(vid);
        }
        public void RegisterAudio(AudioSource audio)
        {
            audios.Add(audio);
            audio.pitch = timeScale;
            //SetSpeed(timeScale);
            this.WaitForSeconds(.05f, () => audio.pitch = timeScale);
        }
        public void UnregisterAudio(AudioSource vid)
        {
            audios.Remove(vid);
        }
        private List<VideoPlayer> videos = new List<VideoPlayer>();
        private List<AudioSource> audios = new List<AudioSource>();
        private AudioMixer master;

        public Dweiss.EventEmpty onContinue;

        private float timeScale = 1;

        public void SetSpeed(float tScale)
        {
            //Debug.Log("Set speed " + tScale );
            this.timeScale = Mathf.Clamp(tScale, 0, 15);
            Time.timeScale = this.timeScale;

            for (int i = videos.Count - 1; i >= 0; i--)
            {
                if (videos[i] == null) videos.RemoveAt(i);
                else videos[i].playbackSpeed = timeScale;
            }
            for (int i = audios.Count - 1; i >= 0; i--)
            {
                if (audios[i] == null) audios.RemoveAt(i);
                else audios[i].pitch = timeScale;
            }
        }


        public int startIndexInEditor = 0;
        public Transform pivotForPrefabs;
        public MovieTransition moviePlayer;
        public ScriptableSceneSettings[] movieScenes;

        public int GetSceneId(ScriptableSceneSettings scene)
        {
            if(scene == null) throw new System.NullReferenceException("Null scene doesn't exists " );

            for (int i = 0; i < movieScenes.Length; i++)
            {
                if (movieScenes[i] == scene) return i;
            }
            throw new System.ArgumentOutOfRangeException("No such scene in scene lists " + scene);
        }


        private int _currentSceneIndex;
        public int CurrentSceneIndex { get { return _currentSceneIndex; } private set { _currentSceneIndex = value; } }
        private int lastSessionIndex;
        private int _nextSessionIndex;
        private int NextSessionIndex { get { return _nextSessionIndex; } set { _nextSessionIndex = value; Debug.Log("New nextIndex " + _nextSessionIndex); } }

        public UnityEngine.UI.Text sceneTime;
        public Dweiss.EventString onSceneChanged, onMovieChanged;
        public ScriptableSceneSettings LastMovieScene
        {
            get { return movieScenes[lastSessionIndex]; }
        }

        public ScriptableSceneSettings CurrentMovieScene
        {
            get { return movieScenes[CurrentSceneIndex]; }
        }
        public float SceneTime { get { return (Time.time - movieStartTime); } }
        //private ScriptableSceneSettings NextMoviescene
        //{
        //    get { return movieScenes[nextSessionIndex]; }
        //}

        private ScriptableSceneSettings[] OptionalNextMoviescene
        {
            get
            {
                ScriptableSceneSettings[] ret;
                if (CurrentMovieScene.NextMovies == null || CurrentMovieScene.NextMovies.Length == 0)
                {
                    Debug.Log("No next movies " + CurrentMovieScene.name);
                    ret = new ScriptableSceneSettings[] {
                        movieScenes[(CurrentSceneIndex + 1) % movieScenes.Length],
                        movieScenes[(CurrentSceneIndex + 1) % movieScenes.Length] };
                }
                else
                {
                    
                    if (CurrentMovieScene.NextMovies.Length == 1)
                    {
                        ret = new ScriptableSceneSettings[]
                        { CurrentMovieScene.NextMovies[0], CurrentMovieScene.NextMovies[0] };
                    }
                    else ret = CurrentMovieScene.NextMovies.ToArray();


                }
                if (debug) Debug.Log(CurrentMovieScene.name +" Next movies " + CurrentMovieScene.NextMovies.Select(a => a.name).ToCommaString());
                return ret;
            }
        }

        protected override void Awake()
        {
            base.Awake();
#if UNITY_EDITOR
            CurrentSceneIndex = startIndexInEditor;
            NextSessionIndex = startIndexInEditor;
            lastSessionIndex = CurrentSceneIndex;
#endif

            System.GC.Collect();

        }

        

        //private IEnumerator PreLoadMoviesToMemory_NotUsed()
        //{
        //    const float waitBetweenWarmVideos = 3.3f;
        //    const float warmVideoForLengthOf = 2f;

        //    if (float.IsInfinity(warmVideoForLengthOf) == false && float.IsInfinity(waitBetweenWarmVideos) == false)
        //    {
        //        var moviesWarm = new HashSet<string>();
        //        for (int i = 0; i < movieScenes.Length; i++)
        //        {
        //            if (moviesWarm.Contains(movieScenes[i].MovieName)) continue;
        //            yield return new WaitForSeconds(waitBetweenWarmVideos);
        //            var go = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //            go.name = ">>> PrewarmMovie " + i;
        //            go.transform.position = Camera.main.transform.position - Camera.main.transform.forward * 10;
        //            Destroy(go, warmVideoForLengthOf);
        //            var vp = go.AddComponent<VideoPlayer>();
        //            var sm = go.AddComponent<Dweiss.ShowMovie>();
        //            vp.source = VideoSource.Url;
        //            vp.renderMode = VideoRenderMode.MaterialOverride;
        //            vp.audioOutputMode = VideoAudioOutputMode.None;
        //            sm.videoPlayer = vp;
        //            sm.autoPlay = true;
        //            sm.PrepereMovie(movieScenes[i].MovieName, false);
        //            sm.Play();
        //            moviesWarm.Add(movieScenes[i].MovieName);
        //        }
        //    }
        //}



        private void Start()
        {

            moviePlayer.onMoviePlayFirstFrame += OnMovieStart;

            if (debug) Debug.Log("Play current scene " + CurrentMovieScene);
            //lastSessionIndex = CurrentSceneIndex;
            var mov = CurrentMovieScene;
            //NextSessionIndex = IndexOf(CurrentMovieScene.NextMovies[0]);
            moviePlayer.PrepareMovie(
                (m) => { moviePlayer.PlayAt(mov.MovieName, -1); },
                mov.MovieName, mov.IsLoop);

            Dweiss.Msg.MsgSystem.MsgStr.Raise("LevelChanged", CurrentMovieScene.name);

        }

        public float VideoTime
        {
            get { return moviePlayer.VideoTime; }
        }
        
        private void Update()
        {
            if(sceneTime) sceneTime.text = SceneTime.ToString("###,##0.0");
        }

        private Coroutine knowWhichScene;
        private void PrepareNextScenes()
        {
            if (debug) Debug.Log("PrepareNextScenes of " + CurrentMovieScene);

            var optional = OptionalNextMoviescene;
            // Debug.LogFormat("Optional movies " + optional.ToCommaString());
            var moviesPrep = new HashSet<string>();
            var prep = optional.Where(a => {
                //var ret = moviesPrep.Contains(a.MovieName) == false;
                //moviesPrep.Add(a.MovieName);
                //return ret;
                return true;
                }).Select(a => new MovieTransition.MoviePrepConfig(a.MovieName, a.IsLoop)).ToArray();
            moviePlayer.PrepareMovie(null, prep);

            // Debug.Log("PrepareNextScenes " + prep.Length);
            //Prepare next scene
            //moviePlayer.PrepareMovie(null, NextMoviescene.MovieName, NextMoviescene.IsLoop);
            if (CurrentMovieScene.IsWaitForOperator == false)
            {
                if(knowWhichScene != null) StopCoroutine(knowWhichScene);
                knowWhichScene = StartCoroutine(WaitToKnowWhichScene());
            }
            //moviePlayer.PlayAt(optional[0].name, Time.time + CurrentMovieScene.SceneLength);
        }

        private IEnumerator WaitToKnowWhichScene()
        {

            var endOfThisSceneTime = (movieStartTime + CurrentMovieScene.SceneLength);
            var waitTIme = endOfThisSceneTime - Time.time - 1;
            Debug.Log(CurrentMovieScene + " WaitToKnowWhichScene start for " + endOfThisSceneTime + " total of " + waitTIme);

            yield return new WaitForSeconds(waitTIme);//wait 1 second before the enx
            GoToNextSceneAccordingToUserAction(endOfThisSceneTime);
        }

        //public bool 

        public void GoToNextSceneAccordingToUserAction(float timeOfPlay = -1)
        {

            if (debug) Debug.Log("GoToNextSceneAccordingToUserAction of " + CurrentMovieScene);

            var optional = OptionalNextMoviescene;


            this.WaitForSeconds(timeOfPlay, () => {
                if (gcBetweenScenes) {
                    Debug.Log("Call garbage collector ");
                    System.GC.Collect();
                }
            });

            if (optional.Length == 1 || 
                (optional.Length == 2 && optional[0] == optional[1]) ||
                (CurrentMovieScene.useScoreToDetermindBranch == false && CurrentMovieScene.MinimumCorrectAnswersForRoundSuccess <= 0))
            {

                NextSessionIndex = IndexOf(optional[0]);
                Debug.Log(CurrentMovieScene.name + " Next scene is " + optional[0].name);
                moviePlayer.PlayAt(optional[0].MovieName, timeOfPlay);
                
            }
            else
            {
                if (CurrentMovieScene.useScoreToDetermindBranch)
                {
                    Debug.Log(CurrentMovieScene.name + " useScoreToDetermindBranch " + ScoreSystem.S.SceneCorrectAnswerCount);
                    var nextMovie = optional[ScoreSystem.S.SceneCorrectAnswerCount];
                    NextSessionIndex = IndexOf(nextMovie);
                    Debug.Log(CurrentMovieScene.name + " Next scene is " + nextMovie.name);
                    moviePlayer.PlayAt(nextMovie.MovieName, timeOfPlay);
                }
                else
                {
                    if (ScoreSystem.S.SceneCorrectAnswerCount >= CurrentMovieScene.MinimumCorrectAnswersForRoundSuccess)
                    {
                        Debug.Log(CurrentMovieScene.name + " success " + ScoreSystem.S.SceneCorrectAnswerCount + "/" + CurrentMovieScene.MinimumCorrectAnswersForRoundSuccess);
                        NextSessionIndex = IndexOf(optional[1]);
                        Debug.Log(CurrentMovieScene.name + " Next scene is " + optional[1].name);
                        moviePlayer.PlayAt(optional[1].MovieName, timeOfPlay);
                    }
                    else
                    {
                        Debug.Log(CurrentMovieScene.name + " failed " + ScoreSystem.S.SceneCorrectAnswerCount + "/" + CurrentMovieScene.MinimumCorrectAnswersForRoundSuccess);
                        NextSessionIndex = IndexOf(optional[0]);
                        Debug.Log(CurrentMovieScene.name + " Next scene is " + optional[0].name);
                        moviePlayer.PlayAt(optional[0].MovieName, timeOfPlay);
                    }
                }
            }
        }

        private int IndexOf(ScriptableSceneSettings nextScene)
        {
            for (int i = 0; i < movieScenes.Length; i++)
            {
                if (nextScene == movieScenes[i]) return i;
            }
            Debug.LogError("Missing next scene from gameflow list " + nextScene);
            return -1;
        }

        private Coroutine createGui;
        private void SetupCurrentMovieScene()
        {
            if (debug) Debug.Log("SetupCurrentMovieScene of " + CurrentMovieScene);

            if (createGui != null)
            {
                Debug.Log("Stop creation of prefab");
                StopCoroutine(createGui);
            }

            if (CurrentMovieScene.prefabToCreate != null)
            {
                if (CurrentMovieScene.delayInPrefabCreation >= 0)
                {
                    Debug.Log("Delay creation of prefab " + CurrentMovieScene.delayInPrefabCreation);
                    var go = new GameObject("GameFlowHelper");
                    go.transform.parent = transform;
                    createGui = this.WaitForSeconds(CurrentMovieScene.delayInPrefabCreation, CreateMoviePrefab);
                }
                else
                    CreateMoviePrefab();
            }
        }

        private void CreateMoviePrefab()
        {
            var newScene = Instantiate(CurrentMovieScene.prefabToCreate);
            newScene.transform.parent = pivotForPrefabs;

            newScene.transform.localRotation = Quaternion.identity;
            newScene.transform.localPosition = Vector3.zero;

            var sceneFlow = newScene.GetComponentInChildren<SingleSceneFlow>();
            if (sceneFlow) sceneFlow.InitSceneSettings(CurrentMovieScene);
            createGui = null;
        }


        private void SessionFinished()
        {

            if (debug) Debug.Log("SessionFinished");
            Dweiss.Msg.MsgSystem.MsgStr.Raise("ResetSession");
        }
        private float movieStartTime;

        private void OnMovieStart(Dweiss.ShowMovie movieShow)
        {
            if (debug) Debug.Log("OnMovieStart of " + movieShow.videoPlayer.url + " VS " + CurrentMovieScene);

#if UNITY_EDITOR
            this.WaitForSeconds(0, () => SetSpeed(timeScale));
            this.WaitForSeconds(0.1f, () => SetSpeed(timeScale));
#endif

            movieStartTime = Time.time;
            Debug.Log("OnMovieStart " + CurrentMovieScene + " ( " + movieStartTime + " ) ");
            //Cleanup
            foreach (Transform c in pivotForPrefabs) { Destroy(c.gameObject); }

            //NextScene has came
            lastSessionIndex = CurrentSceneIndex;
            CurrentSceneIndex = NextSessionIndex;
            NextSessionIndex = IndexOf(OptionalNextMoviescene[0]);

            try
            {
                Dweiss.Msg.MsgSystem.MsgStr.Raise("OnSceneFinished");
                if (CurrentSceneIndex == 0) SessionFinished();

            }
            catch (System.Exception e)
            {
                Debug.LogError("Error in events of video ends " + e);
            }
            //index = (index + 1) % movieScenes.Length;
            MaxSecondsBeforeEndCanContinue = CurrentMovieScene.IsLoop? float.PositiveInfinity : MaxDefaultSecondsBeforeEndForContinue;
            onSceneChanged.Invoke("" + CurrentSceneIndex + "." + CurrentMovieScene.name);
            onMovieChanged.Invoke(CurrentMovieScene.MovieName + " (" + CurrentMovieScene.SceneLength + ")");
            Dweiss.Msg.MsgSystem.MsgStr.Raise("LevelChanged", CurrentMovieScene.name);
            
            //Create events
            SetupCurrentMovieScene();

            PrepareNextScenes();


        }

        private float _lastButtonPressTime = 0;
        private bool CanPressButton() { return Time.time - _lastButtonPressTime >= 1; }

        private const float MaxDefaultSecondsBeforeEndForContinue = 5;
        private float _maxSecondsBeforeEndCanContinue;
        public float MaxSecondsBeforeEndCanContinue { get { return _maxSecondsBeforeEndCanContinue; } set { _maxSecondsBeforeEndCanContinue = value; } }

        private bool CloseToMovieFinish() { return CurrentMovieScene.SceneLength - (Time.time - movieStartTime) < MaxSecondsBeforeEndCanContinue; }
        private bool EnoughWaitAfterMovieStarts() { return (Time.time - movieStartTime) > 0.8f; }

        private void ContinueToNextMoviescene()
        {
            Dweiss.Msg.MsgSystem.Raise("OperatorContinue");

            Debug.Log("Try ContinueToNextMoviescene of " + CurrentMovieScene);
            if (CanPressButton() == false || EnoughWaitAfterMovieStarts() == false) return;
            var optionalNext = OptionalNextMoviescene;
            if (CurrentMovieScene.IsWaitForOperator && (CurrentMovieScene.IsLoop||CloseToMovieFinish()))
            {

                _lastButtonPressTime = Time.time;
                Debug.Log("ContinueToNextMoviescene " + optionalNext[0]);
                StopAllCoroutines();
                if (optionalNext[0] == movieScenes[0]) { Restart(); }
                else
                {
                    //NextSessionIndex = IndexOf(optionalNext[0]);
                    moviePlayer.PlayAt(optionalNext[0].MovieName, -1f);
                }
            }
        }

        public void TryForceContinueToNextMoviescene()
        {
            Debug.Log("TryForceContinueToNextMoviescene of " + CurrentMovieScene);
            if (CanPressButton() == false || EnoughWaitAfterMovieStarts() == false) return;
            _lastButtonPressTime = Time.time;

            OverrideForceContinueNextScene();
        }

        public void OverrideForceContinueNextScene()
        {
            Debug.Log("Skip - ForceContinueToNextMoviescene");
            StopAllCoroutines();
            var optionalNext = OptionalNextMoviescene;
             var nextIndex = IndexOf(optionalNext[0]);
            if (nextIndex == 0)
                Restart();
            else 
                moviePlayer.PlayAt(optionalNext[0].MovieName, -1f);
        }
        //
        public void PlaySceneAgain()
        {
            Debug.Log("TryForceContinueToNextMoviescene of " + CurrentMovieScene);
            if (CanPressButton() == false || EnoughWaitAfterMovieStarts() == false) return;
            _lastButtonPressTime = Time.time;

            Debug.Log("Skip - ForceContinueToNextMoviescene");
            StopAllCoroutines();

            NextSessionIndex = IndexOf(CurrentMovieScene);
            moviePlayer.PlayAt(CurrentMovieScene.MovieName, -1f);
        }

        private void ContinueFromPause()
        {
            if (debug) Debug.Log("ContinueFromPause of " + CurrentMovieScene);
            moviePlayer.Continue();
            SetSpeed(1);
        }


        public void Continue()
        {
            if (debug) Debug.Log("Continue of " + CurrentMovieScene);
            if (timeScale != 1) ContinueFromPause();
            else
                ContinueToNextMoviescene();

            onContinue.Invoke();
        }

        public void Pause()
        {
            Debug.Log("Play/Pause of " + CurrentMovieScene);
            if (timeScale != 1) ContinueFromPause();
            else
            {
                SetSpeed(0);
                moviePlayer.Pause();
            }
        }

        public void Restart()
        {
            Debug.Log("Restart of " + CurrentMovieScene);

            if (CanPressButton() == false || EnoughWaitAfterMovieStarts() == false) return;

            _lastButtonPressTime = Time.time;

            StopAllCoroutines();

            
            //index = 0;
            //PlayCurrentScene();

            var index = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
            UnityEngine.SceneManagement.SceneManager.LoadScene(index);

            Dweiss.Msg.MsgSystem.MsgStr.Raise("ResetGame");
        }

        public void Exit()
        {
            Application.Quit();
        }

        
        //public void SkipToNextLevel()
        //{
        //    StopAllCoroutines();
        //    moviePlayer.PlayAt(OptionalNextMoviescene[OptionalNextMoviescene.Length - 1].MovieName, -1f);
        //}
    }
}