﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DomeVR
{
    public class CalibrateVivePosition : MonoBehaviour
    {
        [SerializeField]private VivePosition vivePos;


        public TMPro.TMP_InputField xPos, yPos, zPos, yRot, guiRot;
        public float waitForWrite = .3f;

        public Transform rightJostickToUseToCalibrate;

        IEnumerator Start()
        {
            yield return new WaitForSecondsRealtime(1);

            xPos.onValueChanged.AddListener(XPosChanged);
            yPos.onValueChanged.AddListener(YPosChanged);
            zPos.onValueChanged.AddListener(ZPosChanged);

            yRot.onValueChanged.AddListener(YRotChanged);//todo un listen?

            guiRot.onValueChanged.AddListener(GuiRotOnlyPosChanged);
            UpdateTextUI();
        }
        private void UpdateTextUI() {
            Transform t = vivePos.transform;
            xPos.text = t.position.x.ToMiliStr();
            yPos.text = t.position.y.ToMiliStr();
            zPos.text = t.position.z.ToMiliStr();

            yRot.text = Camera.main.transform.localEulerAngles.z.ToMiliStr();

            guiRot.text = vivePos.guiRot.localEulerAngles.y.ToMiliStr();
        }

        private void DelayUpdateValue()
        {
            StopAllCoroutines();
            StartCoroutine(DelayWriteCoroutine());
        }
        private IEnumerator DelayWriteCoroutine()
        {
            yield return new WaitForSecondsRealtime(waitForWrite);
            vivePos.SetFromTransform();
            
        }

        private void GuiRotOnlyPosChanged(string txt)
        {
            var value = string.IsNullOrEmpty(txt) ? 0 : float.Parse(txt);
            var r = vivePos.guiRot.localEulerAngles;
            vivePos.guiRot.localEulerAngles = new Vector3(r.x, value, r.z);

            DelayUpdateValue();
        }

        private void XPosChanged(string txt)
        {
            var value = string.IsNullOrEmpty(txt) ? 0 : float.Parse(txt);
            var p = vivePos.transform.position;
            vivePos.transform.position = new Vector3(value, p.y, p.z);
            DelayUpdateValue();
        }

        private void YPosChanged(string txt)
        {
            var value = string.IsNullOrEmpty(txt) ? 0 : float.Parse(txt);
            var p = vivePos.transform.position;
            vivePos.transform.position = new Vector3( p.x, value, p.z);

            DelayUpdateValue();
        }


        private void ZPosChanged(string txt)
        {
            var value = string.IsNullOrEmpty(txt) ? 0 : float.Parse(txt);
            var p = vivePos.transform.position;
            vivePos.transform.position = new Vector3(p.x, p.y, value);

            DelayUpdateValue();
        }
        private void YRotChanged(string txt)
        {
            var value = string.IsNullOrEmpty(txt) ? 0 : float.Parse(txt);
            var r = Camera.main.transform.localEulerAngles;
            Camera.main.transform.localEulerAngles = new Vector3(r.x, r.y, value);

            DelayUpdateValue();
        }


        [System.Serializable]
        public class Scaler
        {
            public float scaleValue;
            public KeyCode scaleKey;
        }
        public Scaler[] scaleKeys;
        public KeyCode signKey = KeyCode.Minus;

        public KeyCode xPosKey, yPosKey, zPosKey, yRotKey, guiRotKey = KeyCode.J;
        public KeyCode resetValue = KeyCode.Alpha0, resetAll = KeyCode.Alpha9;

        float GetDeltaValue()
        {
            foreach(var k in scaleKeys)
            {
                if (Input.GetKeyDown(k.scaleKey))
                {
                    return (Input.GetKey(signKey)? -1 : 1) * k.scaleValue;
                }
            }
            return 0;
        }

        public KeyCode calibrateXZWithJoystick, calibrateYWithJoystick, calibrateYRotWithJoystick, calibrateYParentJoyWithJoystick;
        void Update()
        {
            if (Input.GetKey(xPosKey))
            {
                var value = GetDeltaValue();
                var p = vivePos.transform.position;
                vivePos.transform.position = new Vector3(Input.GetKeyDown(resetValue)? 0 : (p.x + value), p.y, p.z);
                UpdateTextUI();
                DelayUpdateValue();
            } else if (Input.GetKey(yPosKey))
            {
                var value = GetDeltaValue();
                var p = vivePos.transform.position;
                vivePos.transform.position = new Vector3(p.x, Input.GetKeyDown(resetValue) ? 0 : (p.y + value), p.z);
                UpdateTextUI();
                DelayUpdateValue();
            } else if (Input.GetKey(zPosKey))
            {
                var value = GetDeltaValue();
                var p = vivePos.transform.position;
                vivePos.transform.position = new Vector3(p.x, p.y, Input.GetKeyDown(resetValue) ? 0 : (p.z + value));
                UpdateTextUI();
                DelayUpdateValue();
            }
            else if (Input.GetKey(guiRotKey))
            {
                var value = GetDeltaValue();
                var p = vivePos.guiRot.localEulerAngles;
                vivePos.guiRot.localEulerAngles = new Vector3(p.x, Input.GetKeyDown(resetValue) ? 0 : (p.y + value), p.z);
                UpdateTextUI();
                DelayUpdateValue();
            }
            else if (Input.GetKey(yRotKey))
            {
                var value = GetDeltaValue();
                var p = Camera.main.transform.localEulerAngles;
                Camera.main.transform.localEulerAngles = new Vector3(p.x, p.y, Input.GetKeyDown(resetValue) ? 0 : (p.z + value));
                UpdateTextUI();
                DelayUpdateValue();
            }
            if (Input.GetKey(resetAll))
            {
                var p = vivePos.guiRot.localEulerAngles;
                vivePos.guiRot.localEulerAngles = new Vector3(p.x,0, p.z);
                vivePos.transform.position = new Vector3(0,0,0);
                var r = vivePos.transform.localEulerAngles;
                Camera.main.transform.localEulerAngles = new Vector3(r.x, r.y, 0);
                UpdateTextUI();
                DelayUpdateValue();
            }
                CalibrateWithJoystick();
        }

        private void CalibrateWithJoystick()
        {
            if (Input.GetKeyDown(calibrateXZWithJoystick))
            {
                var value = GetDeltaValue();
                var p = vivePos.transform.localPosition;
                vivePos.transform.position = new Vector3(-rightJostickToUseToCalibrate.position.x, 
                    p.y, 
                    -rightJostickToUseToCalibrate.position.z);
                UpdateTextUI();
                DelayUpdateValue();
            }
            if (Input.GetKeyDown(calibrateYWithJoystick))
            {
                var value = GetDeltaValue();
                var p = vivePos.transform.localPosition;
                vivePos.transform.position = new Vector3(p.x, -rightJostickToUseToCalibrate.position.y, p.z);
                UpdateTextUI();
                DelayUpdateValue();
            }
            if (Input.GetKeyDown(calibrateYParentJoyWithJoystick))
            {
                var value = GetDeltaValue();
                var p = vivePos.guiRot.localEulerAngles;
                vivePos.guiRot.localEulerAngles = new Vector3(p.x, -rightJostickToUseToCalibrate.position.y, p.z);
                UpdateTextUI();
                DelayUpdateValue();
            }
            if (Input.GetKeyDown(calibrateYRotWithJoystick))
            {
                var value = GetDeltaValue();
                var p = Camera.main.transform.localEulerAngles;
                Camera.main.transform.localEulerAngles = new Vector3(p.x, p.y, -rightJostickToUseToCalibrate.localEulerAngles.y);
                UpdateTextUI();
                DelayUpdateValue();
            }
        }
    }
}