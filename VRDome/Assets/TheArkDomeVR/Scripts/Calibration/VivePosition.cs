﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    

    public class VivePosition : MonoBehaviour
    {
        [SerializeField] private string jsonFolder = "/ViveCalibration/", jsonFileName = "ViveTransform.json";
        private Dweiss.JsonSettingsFile jsonFile = new Dweiss.JsonSettingsFile();

        [System.Serializable]
        public class Position
        {
            public float x, z;
            public float y;
            public float rot;

            public float guiRot;

            public override string ToString()
            {
                return string.Format("xyz ({0},{1},{2}), {3} {4} ",x,y,z, rot, guiRot);
            }
            public Position()
            {

            }

            public Position(Transform t, Transform guiRotTrans, Transform rotRig)
            {
                x = t.position.x;
                y = t.position.y;
                z = t.position.z;

                guiRot = guiRotTrans.localEulerAngles.y;

                rot = rotRig.localEulerAngles.z;
            }

            public void SetToTransform(Transform t, Transform guiRotTrans, Transform rotRig)
            {
                t.position = new Vector3(x, y, z);

                guiRotTrans.localEulerAngles = new Vector3(guiRotTrans.localEulerAngles.x, guiRot, guiRotTrans.localEulerAngles.z);
                //joysticksOnly.position = new Vector3(joysticksOnly.position.x, yJoysticksOnly, joysticksOnly.position.z);
                rotRig.localEulerAngles = new Vector3(rotRig.localEulerAngles.x, rotRig.localEulerAngles.y, rot);
            }
        }


        [SerializeField]private Position pos;
        public Position Pos { get { return pos; } }
        private string FolderName { get { return System.IO.Path.Combine(Application.dataPath, jsonFolder); } }


        public bool loadPos = true;
        public Transform guiRot;
        private void Load()
        {
            //Debug.Log("Read );
            
            pos = new Position(transform, guiRot, Camera.main.transform);
            Debug.Log("Loading " + pos + " from " + FolderName);
            if (loadPos)
            {
                try
                {
                    jsonFile.Read<Position>(jsonFileName, FolderName, out pos);
                }
                catch (System.Exception e) { Debug.LogError("Error " + e); }
                //JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString("Cal", JsonUtility.ToJson(pos)), pos);
            }

            
        }
        [ContextMenu("Save position")]
        private void Save()
        {
            Debug.Log("Saving "  + pos);
            //PlayerPrefs.SetString("Cal", JsonUtility.ToJson(pos));
            jsonFile.Write(jsonFileName, FolderName, pos);
        }


        void Start()
        {
            Load();
            pos.SetToTransform(transform, guiRot, Camera.main.transform);

            //UpdateTransform(pos);
        }

        //private void UpdateTransform(Position newPos)
        //{
        //    transform.position = new Vector3(newPos.x, newPos.y, newPos.z);
        //    Camera.main.transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, newPos.rot);
        //}

        public void Set(Position newPos)
        {
            pos = newPos;
            pos.SetToTransform(transform, guiRot, Camera.main.transform);
            //UpdateTransform(newPos);
            Save();
        }

        public void SetFromTransform()
        {
            Set(new Position(transform, guiRot, Camera.main.transform));
        }
    }
}