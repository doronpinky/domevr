﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ScreenResolution4k : MonoBehaviour
    {
        public int width = 3840, height = 2160;
        void Awake()
        {
            Screen.SetResolution(width, height, true);
        }
    }
}