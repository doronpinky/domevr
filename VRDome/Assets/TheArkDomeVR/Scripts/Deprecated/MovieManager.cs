﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Video;
//using System;


//namespace Dweiss
//{
//    public class MovieManager : MonoBehaviour
//    {
//        public string startMovieName;

//        public float movieLoadTime = .5f;

//        private int movieIndex;

//        private string playingMovie;
//        private Dweiss.ShowMovie[] movieDisplay;

//        private DateTime playTime;

//        private IEnumerator playDelayCoroutine, loopDealyCourotine;
//        public float swapDelay = 0.6f;

//        [SerializeField] private bool manualLoop = false;

//        [System.Serializable]
//        public class EventWithDateTIme : UnityEngine.Events.UnityEvent<DateTime> { }
//        public EventWithDateTIme onMovieAlmostEnded;
//        public Dweiss.EventEmpty onPlay;



//        private bool isLooping;

//        private void Awake()
//        {
//            movieDisplay = GameObject.FindObjectsOfType<Dweiss.ShowMovie>();
//        }
//        private void Start()
//        {
//            for (int i = 0; i < movieDisplay.Length; ++i)
//            {
//                movieDisplay[i].onPreperationComplete.AddListener(PrepareCompleted);
//                movieDisplay[i].onLoopReached.AddListener(LoopEnd);
//                movieDisplay[i].onMovieStartPlay.AddListener(MovieStart);
//            }
//            PlayMovie(startMovieName, DateTime.Now, true);
//        }


//        private void MovieStart(Dweiss.ShowMovie plyr)
//        {
//            onPlay.Invoke();
//        }

//        public void PlayMovie(string movieName, DateTime playAt, bool loop)
//        {
//            // return;
//            Debug.LogFormat("Start playing {0} at {1} in loop? {2}", movieName, playAt, loop);
//            isLooping = loop;
//            playTime = playAt;
//            playingMovie = movieName;
//            movieDisplay[movieIndex].PrepereMovie(playingMovie, isLooping);
//            movieIndex = ++movieIndex % movieDisplay.Length;
//        }
//        public void PrepareCompleted(Dweiss.ShowMovie mov)
//        {
//            StopAllCoroutines();

//            Debug.Log("Vid prepered. " + mov.videoPlayer.url);
//            mov.Stop();//Movie automatically plays. Stops until we want to continue

//            mov.transform.localScale = Vector3.one;
//            foreach (var m in movieDisplay) if (m != mov) m.transform.localScale = Vector3.one * 1.1f;
//            //mov.videoPlayer.renderMode = VideoRenderMode.CameraNearPlane;
//            //foreach(var m in movieDisplay) if(m != mov) m.videoPlayer.renderMode = VideoRenderMode.CameraFarPlane;

//            //if (playDelayCoroutine != null) StopCoroutine(playDelayCoroutine);
//            playDelayCoroutine = DelayPlay(mov, playTime);
//            StartCoroutine(playDelayCoroutine);
//        }

//        private IEnumerator DelayPlay(Dweiss.ShowMovie mov, DateTime playAt)
//        {

//            while (DateTime.Now < playAt - TimeSpan.FromSeconds(Time.deltaTime))
//            {
//                yield return 0;
//            }
//            mov.Play();
//            if (loopDealyCourotine != null) StopCoroutine(loopDealyCourotine);
//            yield return new WaitForSeconds(swapDelay);


//            mov.transform.localScale = Vector3.one * 1.1f;

//            //if (playDelayCoroutine != null) StopCoroutine(playDelayCoroutine);
//            foreach (var vp in movieDisplay)
//            {
//                if (vp != mov)
//                {
//                    vp.videoPlayer.Stop();
//                }
//                else
//                {
//                }
//            }

//            // if (isLooping && manualLoop) { 
//            if (loopDealyCourotine != null) StopCoroutine(loopDealyCourotine);
//            var timeOfPlay = DateTime.Now + TimeSpan.FromMilliseconds(mov.videoDuration) - TimeSpan.FromSeconds(movieLoadTime);
//            loopDealyCourotine = PlayLoop(mov.videoDuration - movieLoadTime - Time.deltaTime,
//                timeOfPlay, isLooping && manualLoop ? playingMovie : null);
//            StartCoroutine(loopDealyCourotine);
//            Debug.LogFormat("Playing {0} without looping. Revert to loop in {1}", playingMovie, timeOfPlay);
//            //  }

//        }



//        private IEnumerator PlayLoop(float delay, DateTime playTime, string movieName)
//        {
//            // Debug.Log("Looper delay " + delay);
//            yield return new WaitForSeconds(delay);
//            onMovieAlmostEnded.Invoke(playTime);
//            if (string.IsNullOrEmpty(movieName) == false) PlayMovie(movieName, playTime, true);
//        }

//        public void LoopEnd(Dweiss.ShowMovie mov)
//        {
//            // Debug.Log("Looper LoopEnd " + mov.videoPlayer.url);
//        }

//        //public List<string> randomMovies;
//        //public KeyCode key;
//        //void Update()
//        //{
//        //    if (Input.GetKeyDown(key))
//        //    {
//        //        var movie = randomMovies[UnityEngine.Random.Range(0, randomMovies.Count)];
//        //        Debug.Log("User play " + movie);
//        //        PlayMovie(movie, DateTime.Now, true);// + TimeSpan.FromSeconds(4));
//        //    }
//        //}
//    }
//}