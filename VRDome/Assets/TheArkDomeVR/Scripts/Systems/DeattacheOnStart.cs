﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeattacheOnStart : MonoBehaviour
{
    public string tagToAttach;
    public float delayInDeatatch = -1f;
    IEnumerator Start()
    {
        if (delayInDeatatch > 0) yield return new WaitForSeconds(delayInDeatatch);
        transform.parent = null;
        if (string.IsNullOrEmpty(tagToAttach))
        {
            Destroy(this);
        }
    }

    private void Update()
    {
        if (string.IsNullOrEmpty(tagToAttach) == false)
        {
            var tag = GameObject.FindGameObjectWithTag(tagToAttach);
            if (tag != null)
            {
                transform.parent = tag.transform;
                Destroy(this);
            }
        }
    }
}
