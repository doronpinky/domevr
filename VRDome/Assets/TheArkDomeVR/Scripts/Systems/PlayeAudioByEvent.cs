﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class PlayeAudioByEvent : MonoBehaviour
    {
        public AudioSource source;
        public AudioClip[] audioClips;

        public float skipTime = 0f;
        //public string cacheMsgId;

        private IEnumerator Start()
        {
            yield return 0;

            var index = GameFlowSessionState.S.GetBestUserInClimateRound();
            if (index < 0 || index >= audioClips.Length - 1)
            {
                Debug.LogError(transform.FullName() + " Missing audio clip for index " + index);
                source.clip = audioClips[Random.Range(0, audioClips.Length)];
            }
            else
            {
                Debug.Log("Got plyr " + index + " audio " + audioClips[index]);
                source.clip = audioClips[index];
            }
            yield return 0;

            source.time = skipTime;
            source.Play();
        }

    }
}