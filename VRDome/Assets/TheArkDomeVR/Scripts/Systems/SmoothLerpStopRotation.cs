﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class SmoothLerpStopRotation : MonoBehaviour
    {
        public float m_minAngleMove = 10;
        public float m_targetRot = 180;
        public float m_angleToMove = 40;
        private Transform t;

        private const float EpsilonAngle = 0.1f;

        public Dweiss.EventEmpty onReachAngle;

        void Start()
        {
            t = transform;
        }


        private float AngleDiff()
        {
            var currentAngle = t.localEulerAngles.y;
            var diff = Mathf.DeltaAngle(currentAngle, m_targetRot);
            if (diff < 0) diff += 360;
            //Debug.Log("Diff " + diff);
            return diff;
        }

        [ContextMenu("Stop at rotation")]
        public void StopAtRotation()
        {
            StopAllCoroutines();
            StartCoroutine(RotateTowards());
        }

        private IEnumerator RotateTowards()
        {

            var angleDiff = AngleDiff();
            //if (angleDiff > 190)
            //{
            //   // Debug.Log("DIFF1 " + angleDiff);
            //    yield return MoveAngle(m_angleToMove, m_angleToMove, angleDiff, 185);
            //    angleDiff = AngleDiff();
            //}
            if (angleDiff > 179)
            {
                //Debug.Log("DIFF2 " + angleDiff);
                yield return MoveAngle(m_angleToMove, m_angleToMove, angleDiff, 179);
                angleDiff = AngleDiff();
            }
            //if (angleDiff > 90)
            //{
            //    Debug.Log("DIFF3 " + angleDiff);
            //    yield return MoveAngle(m_angleToMove, m_angleToMove, angleDiff, 90);
            //    angleDiff = AngleDiff();
            //}
            //Debug.Log("DIFF-- " + angleDiff);
            yield return MoveAngle(m_angleToMove, m_minAngleMove, angleDiff, 0);

            onReachAngle.Invoke();

        }

        private IEnumerator MoveAngle(float startSpeed, float endSpeed, float startAngleDiff, float targetDiffAngle)
        {
            var diffAngle = AngleDiff();
            float angleSpeed;
            while (diffAngle - targetDiffAngle > EpsilonAngle)
            {
                var lerp = (startAngleDiff - diffAngle) / startAngleDiff;
                angleSpeed = Mathf.Lerp(startSpeed, endSpeed, lerp);
                var angleSpeedPerSec = Mathf.Min(angleSpeed * Time.deltaTime, (diffAngle - targetDiffAngle));
                t.Rotate(Vector3.up, angleSpeedPerSec);
                yield return 0;
                var prevAngle = diffAngle;
                diffAngle = AngleDiff();
                if (prevAngle < diffAngle)
                {
                    Debug.LogWarningFormat("angle diff bigger {0} -> {1}", prevAngle, diffAngle);
                    break;
                }
            }

            t.Rotate(Vector3.up, diffAngle - targetDiffAngle);
        }


    }
}