﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class SDGRollate : MonoBehaviour
    {
        public float waitTimeForRollate = 4;

        public float anglePerSlot = 20;

        private SimpleRotate rotater;
        public float deltaAngle = 3;
      //  private float angleMoved;

        IEnumerator StartRotateToSlot(int targetIndex, System.Action<int> onSelectedSdg)
        {
            yield return new WaitForSeconds(waitTimeForRollate);
            var targetAngle = targetIndex * anglePerSlot;
            //while (targetAngle > 180) targetAngle = 360 - targetAngle;
            Debug.Log("Target angle " + targetAngle);
            //var anglePerDeltaTime = deltaAngle;
          //  var lastAngle = rotater.transform.localEulerAngles.y;
            while (IsClose(targetAngle) == false)
            {
              //  angleMoved = Mathf.DeltaAngle(lastAngle, rotater.transform.localEulerAngles.y);
                yield return 0;
                //deltaAngle = 5 * angleMoved;
                //lastAngle = rotater.transform.localEulerAngles.y;
            }
            //Debug.Log();
            rotater.enabled = false;
            yield return new WaitForSeconds(.3f);

            onSelectedSdg.Invoke(targetIndex);
        }



        private bool IsClose(float angle)
        {
            if (Mathf.Abs(Mathf.DeltaAngle(angle, rotater.transform.localEulerAngles.y)) < deltaAngle)
            {
                return true;
            }
            return false;
        }

        public void StartRotate(int index, System.Action<int> onSelectedSdg)
        {
            rotater.gameObject.SetActive(true);
            rotater.enabled = true;
            StartCoroutine(StartRotateToSlot(index, onSelectedSdg));
        }
        //private Transform t;


        void Start()
        {
            //t = transform;
            rotater = GetComponentInChildren<SimpleRotate>(true);
        }

    }
}