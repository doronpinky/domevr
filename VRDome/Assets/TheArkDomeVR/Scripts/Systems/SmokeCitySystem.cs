﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class SmokeCitySystem : MonoBehaviour
    {
        public GameObject smokePrefab;
        public string smokeGroupTag, interactiveSmokeTag;
        public float timeOfInteractiveSmoke = 12;
        public float smokeDieStart = 2 * 120 + 5;

        private GameObject smokeGroup;

        public bool dieOnStart = false;

        private void Start()
        {
            smokeGroup = GameObject.FindGameObjectWithTag(smokeGroupTag);
            if(smokeGroup == null)
            {
                var smoke = Instantiate(smokePrefab, null);
                smoke.transform.parent = transform;
                smoke.transform.localPosition = Vector3.zero;
                smoke.transform.localEulerAngles = Vector3.zero;
                smoke.SetActive(true);
            }
            smokeGroup = GameObject.FindGameObjectWithTag(smokeGroupTag);
            smokeGroup.transform.parent = transform;
            this.WaitForSeconds(timeOfInteractiveSmoke, () =>
            {
                var interactiveSmoke = GameObject.FindGameObjectWithTag(interactiveSmokeTag);
                foreach (Transform c in interactiveSmoke.transform) c.gameObject.SetActive(true);
            });


            this.WaitForSeconds(smokeDieStart, () =>
            {
                KillSmoke();
            });
            if (dieOnStart)
            {
                KillSmoke();
            }
            this.WaitForSeconds(0, () => smokeGroup.transform.parent = transform);
        }

        private void KillSmoke()
        {
            var killers = smokeGroup.GetComponentsInChildren<ParticalesReduceAndDieTimer>();
            foreach (var k in killers) k.StartDeathSequence();
        }
    }
}