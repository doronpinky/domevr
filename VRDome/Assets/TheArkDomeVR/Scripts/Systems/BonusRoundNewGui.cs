﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

namespace DomeVR
{
    public class BonusRoundNewGui : BaseQuestionTriviaSeq
    {
        [SerializeField]private SDGRollateNewGui rollate;
        

        [SerializeField] private AudioSource spinningSound;
        [SerializeField] private AudioSource selectedSound;

        public Dweiss.EventEmpty onUserClick;


        public float autoClickAfter = -1;

        //        [SerializeField] private float delayToSeeSelected = 1;
        private bool userClicked = false;
        private Coroutine autoClick;
        public int stgSkipAfterStop = 1;
        private QuestionStateInDemand myQS;
        protected new void Start()
        {
            base.Start();

            myQS = (QuestionStateInDemand)qState;
            if (rollate) rollate.onTargetReached.AddListener(RollateStopped);
            onForceStop.AddListener(() => {
                rollate.gameObject.SetActive(false);
                spinningSound.Stop();
                selectedSound.Stop();
            });
        }

        protected override void RunIt()
        {
            spinningSound.Stop();
            selectedSound.Stop();

            userClicked = false;
            rollate.gameObject.SetActive(true);

            Debug.Log("Group id " + qState.CurrentQ.GroupId );
            if (qState.CurrentQ.GroupId < 0)
                TimeEnd();
            else
            {
                length = float.PositiveInfinity;
                StartRollate();
            }

            if(autoClickAfter >= 0)
                autoClick = this.WaitForSeconds(autoClickAfter, UserClicked);
        }

        private void StartRollate()
        {
            //Activate rotate music
            spinningSound.Play();
            rollate.StartRotate();
        }


        private void RollateStopped()
        {
            //Stop music
            spinningSound.Stop();
            selectedSound.Play();
            TimeEnd();
        }
        

        [ContextMenu("User clicked")]
        public void UserClicked() {
            if (autoClick != null)
            {
                StopCoroutine(autoClick);
                autoClick = null;
            }
            if (IsRunning && userClicked == false) {
                userClicked = true;

                myQS.SetClosestQuestionByGroupId(rollate.GetAngleIndex() + 1 + 1 + stgSkipAfterStop);//1 for zero base to 1 base count, +2 for delay stop

                rollate.StopRotate(qState.CurrentQ.GroupId - 1);

                Debug.Log("Stop on group " + qState.CurrentQ.GroupId+ " Question start #" + qState.QuestionCount + ". " +
                    qState.CurrentQ.Question +
                    "(" + qState.CurrentQ.questionNum + ")");

                onUserClick.Invoke();
            }
        }

        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("User0Click", UserClicked);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if(Dweiss.Msg.MsgSystem.S)
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("User0Click", UserClicked);

            //spinningSound.Stop();
            //selectedSound.Stop();
        }
    }
}