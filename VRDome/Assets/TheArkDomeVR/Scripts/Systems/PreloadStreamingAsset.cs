﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;

namespace Dweiss
{
    public class PreloadStreamingAsset : Dweiss.Singleton<PreloadStreamingAsset>
    {
        private const string AndroidInternalFolderPrefix = "file://";
        public bool preloadInEditor = true;

        [Header("__On Finish__")]
        public string eventToRaise;
        public Dweiss.EventEmpty preloadComplete;

        [Header("__Prewarm__")]
        [SerializeField] private string[] preloadClips;
        [SerializeField] private string[] prewarmVideoClips;


        [Header("__Files inside the build__")]
        public PreCompiledSounds[] preCompiled;
        
        [Header("__Path settings__")]
        public string optionalPath = "RoofTop";
        ///storage/emulated/0/Android/data/<packagename>/files
        ///%userprofile%\AppData\Local\Packages\<productname>\LocalState.
        public string defaultMovieSuffix = ".mp4";
        public string defaultAudioClipSuffix = ".wav";

        public enum LoadResource
        {
            StreamingAssets,
            AppStorage,
            OptionalPath
        }


        public string folderPrefix;


        [Serializable]
        public class PreCompiledSounds
        {
            public string id;
            public AudioClip aClip;
        }

        [System.Serializable]
        public class VideoToMem
        {
            public GameObject go;
            public VideoPlayer vp;
            public VideoEvents ve;
            public Action<VideoPlayer> onVidePlyr;
        }

        private List<VideoToMem> loadingVids = new List<VideoToMem>();

        private Dictionary<string, AudioClip> clipNameToAudioClip = new Dictionary<string, AudioClip>();

        private void OnValidate()
        {
            foreach(var pre in preCompiled)
            {
                if(string.IsNullOrEmpty(pre.id) == false && pre.id.EndsWith("/")
                    && pre.aClip != null)
                {
                    pre.id += pre.aClip.name;
                }
            }
        }

        [ContextMenu("AddPrecompiledToPreload")]
        private void AddPrecompiledToPreload()
        {
            var newPreLoad = new List<string>(preloadClips);
            newPreLoad.AddRange(preCompiled.Select(a => a.id));

            preloadClips = newPreLoad.ToArray();
        }

        private void Start()
        {
            if (string.IsNullOrWhiteSpace(eventToRaise) == false) preloadComplete.AddListener(() => {  { Dweiss.Msg.MsgSystem.Raise(eventToRaise); } });

            if (preloadInEditor == false) return;

            clipNameToAudioClip = preCompiled.ToDictionary(a => a.id.Replace("\\", "/").ToLower(), b => b.aClip);
            if (preloadClips.Length == 0 && prewarmVideoClips.Length == 0) preloadComplete.Invoke();

            for (int i = 0; i < preloadClips.Length; i++)
            {
                int index = i;
                Load(preloadClips[i], (AudioClip clip) => { Debug.Log("clip prewarm " + preloadClips[index]); clipNameToAudioClip[preloadClips[index]] = clip; UpdatePreloadingComplete(); });
            }
            for (int i = 0; i < prewarmVideoClips.Length; i++)
            {
                Load(prewarmVideoClips[i], true, (VideoPlayer vp) => { Debug.Log("Video prewarm " + vp.url);  Destroy(vp.gameObject); UpdatePreloadingComplete(); });
            }
        }


        private int loadingCount = 0;
        private void UpdatePreloadingComplete()//TODO what if movie load failed?
        {
            loadingCount++;
            if (prewarmVideoClips.Length + preloadClips.Length == loadingCount)
            {
                Debug.Log("Preload complete");
                preloadComplete.Invoke();
            }
        }

        private string GetStartPath(LoadResource loadOption)
        {
            switch (loadOption)
            {
                case LoadResource.StreamingAssets: return Application.streamingAssetsPath;
                case LoadResource.AppStorage:
#if UNITY_ANDROID && UNITY_EDITOR == false
                    var presistant = Application.persistentDataPath;
                    if (presistant[0] == '/')
                        presistant = presistant.Substring(1, presistant.Length - 1);
                    return  presistant;
#else
                    return Application.persistentDataPath;
#endif
                case LoadResource.OptionalPath:
#if UNITY_ANDROID && UNITY_EDITOR == false
                    return Application.persistentDataPath.Substring(0, Application.persistentDataPath.IndexOf("/Android")) + "/"+ optionalPath;
                    
#else
                    return "c:/" + optionalPath;
#endif
                default: throw new System.NotSupportedException("No such load path defined " + loadOption);
            }
        }


        private string GetActivePath(string fileName, LoadResource type)
        {
            if (string.IsNullOrEmpty(folderPrefix))
                return GetStartPath(type) + "/" + fileName;
            else
                return GetStartPath(type) + "/" + folderPrefix + "/" + fileName;
        }


        private LoadResource[] _resourcePath;
        private LoadResource[] ResourcePath
        {
            get
            {
                if (_resourcePath == null)
                {
                    _resourcePath = System.Enum.GetValues(typeof(LoadResource)).Cast<LoadResource>().ToArray();
                }
                return _resourcePath;
            }
        }
        #region Utils

        private string AddSuffix(String fileName, string suffix)
        {
            var suffixIndex = fileName.IndexOf(".");
            if (suffixIndex == -1)
            {
                return fileName + suffix;
            }
            else
            {
                var currSuffix= fileName.Substring(suffixIndex);
                if (currSuffix == suffix)
                {
                    Debug.Log("suffix== " + fileName + " (" + suffix +")" );
                    return fileName;
                }
                var newName = fileName.Substring(0, suffixIndex) + suffix; ;
                Debug.Log("suffix!= " + fileName + " (" + suffix + ") =>" + newName);
                return newName;
            }
        }
        public string AddSuffixToMovie(String movName)
        {
            return AddSuffix(movName, defaultMovieSuffix);
        }
        public string AddSuffixToAudioClip(String audioName)
        {
            return AddSuffix(audioName, defaultAudioClipSuffix);
        }
        public string GetFullPath(string fileName)
        {
            for (int i = 0; i < ResourcePath.Length; i++)
            {
                var path = GetActivePath(fileName, ResourcePath[i]);
                //Debug.Log("try path " + path);
                if (System.IO.File.Exists(path))
                {
                    //LogRepeat("(:) Found path " + path);
                    return path;
                }
            }
            //LogErrorRepeat(";; Missing path for " + fileName + " : " + GetActivePath(fileName, LoadResource.OptionalPath));
            return null;
        }
        #endregion
        private bool FileExists(string fileName)
        {
            return GetFullPath(fileName) != null;
        }
        private void LogErrorRepeat(string str)
        {
            Debug.LogError(Time.time + " --> " + str);
        }
        //private IEnumerator LogErrorRepeatCoroutine(string str)//coroutine errors for android debug
        //{
        //    var time = Time.time;
        //    Debug.LogError(time + " --> " + str);
        //    //for (int i = 1; i < 10; i++)
        //    {
        //        yield return new WaitForSecondsRealtime(10);
        //        Debug.LogError(time + " --> " + str);
        //    }
        //}
        private void LogRepeat(string str)
        {
            var time = Time.time;
            Debug.Log(time + " --> " + str);
            //StartCoroutine(LogRepeatCoroutine(str));
        }
        //private IEnumerator LogRepeatCoroutine(string str)
        //{
        //    var time = Time.time;
        //    Debug.Log(time + " --> " + str);
        //    //for (int i = 1; i < 10; i++)
        //    {
        //        yield return new WaitForSecondsRealtime(10);
        //        Debug.Log(time + " --> " + str);
        //        //yield return new WaitForSecondsRealtime(i);
        //    }
        //}


        public void Load(string fileName, Action<string> onLoadingCompleted)
        {
            Debug.Log("file name " + fileName);
            using (FileStream file = new FileStream(GetFullPath(fileName), FileMode.Open, FileAccess.Read))
            {
                using (StreamReader sr = new StreamReader(file))
                {

                    string str = null;
                    str = sr.ReadToEnd();
                    onLoadingCompleted(str);
                    sr.Close();
                    file.Close();
                }
            }
            //StartCoroutine(LoadClipCoroutine(GetFullPath(fileName), onLoadingCompleted));
        }
        IEnumerator LoadClipCoroutine(string file, Action<string> onLoadingCompleted)
        {
            var request = new WWW(file);
            yield return request;

            if (onLoadingCompleted != null)
                onLoadingCompleted(request.text);

            request.Dispose();
        }


        private string RemoveSuffix(string fileName)
        {
            var index = fileName.IndexOf('.');
            return (index > -1) ? fileName.Substring(0, index) : fileName;
        }
        private string RemoveLibraryPrefix(string fileName)
        {
            if(fileName.StartsWith("/") || fileName.StartsWith("\\"))
            {
                return fileName.Substring(1);
            }
            return fileName;
        }

        public void Load(string fileName, Action<AudioClip> onLoadingCompleted)
        {
            AudioClip clip;
            Debug.Log("Search " + fileName + " in clips count " + clipNameToAudioClip.Count );
            if (clipNameToAudioClip.TryGetValue(RemoveLibraryPrefix(RemoveSuffix(fileName)).Replace("\\","/").ToLower(), out clip))
            {
                Debug.Log("AudioClip load from memory " + fileName);
                onLoadingCompleted(clip);
            }
            else
            {
                Debug.Log("Not found in cache " + fileName + " cache : " + clipNameToAudioClip.ToCommaString());

                var fullPathDefault = GetFullPath(AddSuffixToAudioClip(fileName));
                Debug.LogFormat("Try Load AudioClip name {0} -> {1} ", fileName, fullPathDefault);
                if (fullPathDefault != null) StartCoroutine(LoadClipCoroutine(fullPathDefault, onLoadingCompleted));
                else
                {
                    var fullPathMp3 = GetFullPath(AddSuffix(fileName, ".mp3"));
                    Debug.LogFormat("Try Load AudioClip name {0} -> {1} ", fileName, fullPathMp3);
                    if (fullPathMp3 != null) StartCoroutine(LoadClipCoroutine(fullPathMp3, onLoadingCompleted));
                    else Debug.LogErrorFormat("Missing AudioClip name {0} ", fileName);

                }
            }
        }

        IEnumerator LoadClipCoroutine(string file, Action<AudioClip> onLoadingCompleted)
        {
            var request = new WWW(file);
            
            yield return request;
            if(string.IsNullOrEmpty(request.error) == false) Debug.LogError("Read audio failed for <" + file + "> url: " + request.url + " error:" + request.error);
            if (onLoadingCompleted != null)
                onLoadingCompleted(request.GetAudioClip(true, true));

            request.Dispose();
        }

     

        public void Load(string shortFileName, bool muteOnStart, Action<VideoPlayer> onLoadingCompleted)
        {
            var fileNameWithSuffix = AddSuffixToMovie(shortFileName);
            var fileName = GetFullPath(fileNameWithSuffix);

            if (fileName == null) Debug.LogErrorFormat("Missing video name {0} in path {1} ", shortFileName, fileNameWithSuffix);

            var videoToMem = new VideoToMem();
            videoToMem.go = new GameObject("VideLoader_" + fileName);
            videoToMem.vp = videoToMem.go.AddComponent<VideoPlayer>();
            videoToMem.ve = videoToMem.go.AddComponent<VideoEvents>();

            videoToMem.onVidePlyr = onLoadingCompleted;

            videoToMem.vp.source = VideoSource.Url;
            videoToMem.vp.url = fileName;

            videoToMem.ve.pauseOnPlay = true;


            UnityEngine.Events.UnityAction<VideoPlayer> func = null;
            func = new UnityEngine.Events.UnityAction<VideoPlayer>((vid) =>
            {
                
                loadingVids.Remove(videoToMem);
                videoToMem.onVidePlyr.Invoke(videoToMem.vp);

                videoToMem.ve.onPlayStarted.RemoveListener(func);
            });
            videoToMem.ve.onPlayStarted.AddListener(func);

            // Debug.Log("videoToMem.ve.onPreperationComplete " + videoToMem.ve.onPreperationComplete);
            //videoToMem.ve.onPlayStarted.AddListener((vid) => {
            //    videoToMem.onVidePlyr.Invoke(videoToMem.vp);
            //    loadingVids.Remove(videoToMem);

            //    videoToMem.ve.onPlayStarted = null;
            //});
            videoToMem.ve.PlayWithEvent();

            if (muteOnStart)
            {
                videoToMem.vp.SetDirectAudioMute(0, true);
            }

            loadingVids.Add(videoToMem);
        }

        public void Load(string shortFileName, VideoPlayer vp, Action<VideoPlayer> onLoadingCompleted)
        {
            var fileNameWithSuffix = AddSuffixToMovie(shortFileName);
            var fileName = GetFullPath(fileNameWithSuffix);

            if (fileName == null) Debug.LogErrorFormat("Missing video name {0} in path {1} ", shortFileName, fileNameWithSuffix);


            var videoToMem = new VideoToMem() { go = vp.gameObject, vp = vp };
            videoToMem.ve = videoToMem.go.GetComponent<VideoEvents>();
            if(videoToMem.ve  == null) videoToMem.ve = videoToMem.go.AddComponent<VideoEvents>();

            videoToMem.onVidePlyr = onLoadingCompleted;

            videoToMem.vp.source = VideoSource.Url;
            videoToMem.vp.url = fileName;

            videoToMem.ve.pauseOnPlay = true;
            if(videoToMem.vp.audioOutputMode == VideoAudioOutputMode.AudioSource)
            {
                videoToMem.vp.GetComponent<AudioSource>().mute = true;
            }

            UnityEngine.Events.UnityAction<VideoPlayer> func = null;
            func = new UnityEngine.Events.UnityAction<VideoPlayer>((vid) =>
            {
                loadingVids.Remove(videoToMem);
                videoToMem.onVidePlyr.Invoke(videoToMem.vp);

                videoToMem.ve.onPlayStarted.RemoveListener(func);

                if (videoToMem.vp.audioOutputMode == VideoAudioOutputMode.AudioSource)
                {
                    videoToMem.vp.GetComponent<AudioSource>().mute = false;
                }
            });
            videoToMem.ve.onPlayStarted.AddListener(func);
            videoToMem.ve.PlayWithEvent();

            loadingVids.Add(videoToMem);
        }

        

    }
}