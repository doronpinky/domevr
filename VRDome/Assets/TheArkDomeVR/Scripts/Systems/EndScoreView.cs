﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace DomeVR
{
    public class EndScoreView : MonoBehaviour
    {
        public float randomNumberWaitTimes = 10, waitBetweenRandomNumber = .2f;


        public TextMeshPro[] currentYear;
        public TextMeshPro[] bestYear;
        public AudioSource playOnEnd;
            
        private int[] randomYears = new int[] { 1985, 1988, 2017, 1977, 1978,2014,2015,
            2076, 2222, };

        IEnumerator Start()
        {
            SetText(bestYear, ScoreSystem.S.BestYear, "Best ");
            for (int i = 0; i < randomNumberWaitTimes; ++i)
            {
                var randomYear = 1111;
                if(Random.value < .5)
                {
                    randomYear = randomYears[Random.Range(0, randomYears.Length)];
                }else
                {
                    randomYear = (int)(Random.Range(2025, 2076));
                }
                SetText(currentYear, randomYear);
                yield return new WaitForSeconds(waitBetweenRandomNumber);
            }
            SetText(currentYear, ScoreSystem.S.CurrentYears);
            playOnEnd.Play();
        }

        private static void SetText(TextMeshPro[] texts, int value, string prefix = "")
        {
            foreach (var tx in texts) tx.text = prefix + value.ToString();
        }
        
    }
}