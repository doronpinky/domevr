﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieOnEvent : MonoBehaviour
{
    public string[] eventsNames;



    private void Death()
    {
        Destroy(gameObject);
    }

    private void OnEnable()
    {
        foreach(var ev in eventsNames)
            Dweiss.Msg.MsgSystem.MsgStr.Register(ev, Death);



    }
    private void OnDisable()
    {
        if (Dweiss.Msg.MsgSystem.S)
        {
            foreach (var ev in eventsNames)
                Dweiss.Msg.MsgSystem.MsgStr.Unregister(ev, Death);
        }
    }
}
