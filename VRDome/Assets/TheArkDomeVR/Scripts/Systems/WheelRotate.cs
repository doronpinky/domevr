﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dweiss
{
    public class WheelRotate : MonoBehaviour
    {
        private const float MinAnglePerSec = 0;

        public float maxAnglePerSec;
        public float accelAnglePerSec;
        public float deaccelAnglePerSec;

        public float decFactorOnIncrease;
        private float anglePerSec;


        //public void SetAngleAccelPerSec(float accelAnglePerSec)
        //{
        //    this.accelAnglePerSec = accelAnglePerSec;
        //}
        public void SetMaxAngleAccelPerSec(float maxAnglePerSec)
        {
            this.maxAnglePerSec = maxAnglePerSec;
        }


        public Vector3 axe;

        private Transform t;

        public enum ForceState
        {
            Increase,
            Decrease,
            Steady
        }

        private ForceState forceState = ForceState.Steady;

        void Start()
        {
            t = transform;
        }

        [ContextMenu("Start rotate")]
        public void StartRotate()
        {
            forceState = ForceState.Increase;
            // Debug.Log(t.eulerAngles);

        }
        [ContextMenu("Stop rotate")]
        public void StopRotate()
        {
            forceState = ForceState.Decrease;
            //   Debug.Log(t.eulerAngles);
        }



        void Update()
        {

            if (forceState == ForceState.Increase)
            {
                anglePerSec += accelAnglePerSec * Time.deltaTime;

                if (anglePerSec >= maxAnglePerSec)
                {
                    anglePerSec = maxAnglePerSec;
                    forceState = ForceState.Steady;
                    //     Debug.Log("onMax " + t.eulerAngles);
                }
                else
                {
                    anglePerSec = (1 - decFactorOnIncrease) * anglePerSec;
                }
            }
            else if (forceState == ForceState.Decrease)
            {
                anglePerSec -= deaccelAnglePerSec * Time.deltaTime;
                if (anglePerSec <= MinAnglePerSec)
                {
                    anglePerSec = MinAnglePerSec;
                    forceState = ForceState.Steady;
                    //   Debug.Log("onMin " + t.eulerAngles);
                }
            }

            t.Rotate(axe, anglePerSec * Time.deltaTime);
        }
    }
}