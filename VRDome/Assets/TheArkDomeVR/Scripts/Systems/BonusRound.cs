﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Video;
//using System.Linq;

//namespace DomeVR
//{
//    public class BonusRound : MonoBehaviour
//    {
//        public SDGRollate rollate;
//        public QuestionBag questions;
//        public QidToInfo[] info;
//        public TriviaLogic trivia;

//        public AudioSource spinningSound;
//        public AudioSource questionNarration;

//        public AudioSource selectedSound;
//        public float delayForSelected;

//        //public bool autoContinue;

//        //public float waitToStart = .1f;
        
//        [System.Serializable]
//        public class QidToInfo
//        {
//            public int qIndex;
//            public VideoClip videoClip;
//            public int sdgIndex;
//            public AudioClip questionNarration;
//            //public string moreInfo;
//        }
//        public AudioSource backgroundMusic;

//        private QidToInfo currentQid;

//        private bool isAutoSpin = false;

//        private bool isShowingQuestion = false;
//        private void StartSpin()
//        {
//            if (isShowingQuestion) return;
//            //isAutoSpin = true;

//            spinningSound.Play();
//            Debug.Log("StartSpin ");
//            isShowingQuestion = true;
//                var qId = questions.CurrentQ.questionNum;
//            Debug.LogFormat("Looking for {0}.{1} in\n{2}", qId, questions.CurrentQ.Question, info.ToCommaString());
//            currentQid = info.First(a => a.qIndex == qId);

//            rollate.StartRotate(currentQid.sdgIndex, OnSpinEndShowQuestion);
//            trivia.PrepareMovie(currentQid.videoClip);
//        }


//       private IEnumerator MakeSelectedSound()
//        {
//            yield return new WaitForSeconds(delayForSelected);
//            selectedSound.Play();
//        }

//        public void OnSpinEndShowQuestion(int index)
//        {
//            StartCoroutine(MakeSelectedSound());
//            Debug.Log("Spin end " + index);
//            spinningSound.Stop();
//            trivia.ShowQuestionAnswersScoreAndTimer();

//            if (currentQid.questionNarration)
//            {
//                questionNarration.clip = currentQid.questionNarration;
//                questionNarration.Play();
//            }
//            //trivia.ResetTimers();
//            //trivia.ShowScore();
//            //trivia.MoveToNextQuestion();
//        }
        

//        public void OnAnswerQuestionShowInfo() {
//            questionNarration.Stop();
//            trivia.ShowInfoCorrectAnswersAndScore();

//            //trivia.SetInfo(currentQid.moreInfo);
//            //trivia.HideQuestion();
//            //throw new System.NotImplementedException();
//        }

//        public void OnShowInfoFinishShowMovie() {
//            backgroundMusic.Pause();
//            //trivia.SetInfo("");
//            //trivia.HideAllNoneAnswers("");
//            trivia.PlayNow(currentQid.videoClip, OnShowMovieEndClearGuiAndWaitForSpin);

//            //throw new System.NotImplementedException();
//        }
//        public void OnShowMovieEndClearGuiAndWaitForSpin()
//        {
            

//            backgroundMusic.Play();
//           // Debug.Log("OnShowMovieEndClearGuiAndWaitForSpin");
//            trivia.ClearGui();
//            questions.MoveToNextQuestion();
//            if (questions.HasMoreQuestions == false)
//            {
//                EndRound();
//            }
//            else
//            {
//                isShowingQuestion = false;
//            }
//            if (isAutoSpin) StartSpin();
//        }

//        public float dieAfter = .8f;
//        public float endTimeLength = 3;
//        private IEnumerator RoundEndEffects()
//        {
           
//            var startTime = Time.time;
//           // rollate.StartRotate(0, (sdg) => RoundEndEffects());
//            trivia.ClearGui();

//            var endScale = new Vector3(0, 0, 0);
//            var startScale = transform.localScale;
//            var startPos = transform.position;
            
//            while (Time.time - startTime < endTimeLength)
//            {
//                yield return 0;
//                var percent = (Time.time - startTime) / endTimeLength;
//                if(backgroundMusic) backgroundMusic.volume = (1 - percent)/2;
//                transform.position = Vector3.Lerp(startPos, new Vector3(0,1000,0), percent);
//                transform.localScale = Vector3.Lerp(startScale, endScale, percent);

//            }
//            GameFlow.S.GoToNextSceneAccordingToUserAction(-1);
//        }

//        private void EndRound()
//        {

            
//            StartCoroutine(RoundEndEffects());
//            this.WaitForSeconds(dieAfter, () => GameFlow.S.GoToNextSceneAccordingToUserAction(-1));

//            var sounds = GetComponentsInChildren<AudioSource>();
//            foreach (var s in sounds) if(s != backgroundMusic) Destroy(s);

//            //Debug.LogError("Bonus EndRound");

//        }
//        //Talking.
//        // Start rollate with trigger
//        // Show question
//        // Show answer with info
//        // Show movie
//        // Empty GUI
//        // Repeat

//        private IEnumerator Start()
//        {
 
//            trivia.onInfoFinish.AddListener(OnShowInfoFinishShowMovie);
//            trivia.onQAnswered.AddListener((i) => OnAnswerQuestionShowInfo());
//            trivia.onAnswerTimeEnd.AddListener(OnAnswerQuestionShowInfo);
//            backgroundMusic.Play();
//            yield return 0;
//            //if (autoContinue)
//            //{
//            //    yield return 0;

//            //    StartSpin();
//            //}

//        }

//        private void OnEnable()
//        {
//            Dweiss.Msg.MsgSystem.MsgStr.Register("User0Click", StartSpin);
//        }
//        private void OnDisable()
//        {
//            if(Dweiss.Msg.MsgSystem.S)
//                Dweiss.Msg.MsgSystem.MsgStr.Unregister("User0Click", StartSpin);
//        }
//    }
//}