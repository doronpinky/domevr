﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class SDGRollateNewGui : MonoBehaviour
    {
        //public int skipMinSDGToStop = 3;//Create read question interface?
        [SerializeField] private Dweiss.WheelRotate rotater;
        [SerializeField] private Dweiss.SmoothLerpStopRotation stopper;

        [SerializeField] private float anglePerSlot = 20;

        public Dweiss.EventEmpty onTargetReached;


        private void Start()
        {
            stopper.onReachAngle.AddListener(onTargetReached.Invoke);
        }

        public void StartRotate()
        {
            stopper.StopAllCoroutines();
            rotater.enabled = true;
            rotater.StartRotate();
        }

        public int GetAngleIndex()
        {
            return (int)(transform.localEulerAngles.y / anglePerSlot);
        }
        public void StopRotate(int index)
        {
            var targetAngle = index * anglePerSlot;
            rotater.enabled = false;
            stopper.m_targetRot = targetAngle;

            stopper.StopAtRotation();
        }




    }
}