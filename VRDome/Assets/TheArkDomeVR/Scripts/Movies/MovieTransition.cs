﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using System;
using UnityEngine.Video;

namespace DomeVR
{
    public class MovieTransition : MonoBehaviour
    {
        public bool debug;
        [SerializeField] private ShowMovie[] plyrs;
        [SerializeField] private float swapDelay = -1f;

        private int activePlyrIndex = -1;
        public Action onMovieDelayStart, onMovieBeforeDelayStart;
        public Action<ShowMovie> onMoviePlayFirstFrame;
        private void Awake()
        {
            for (int i = 0; i < plyrs.Length; i++)
            {
                plyrs[i].onVideoPlayFirstFrame += (m) =>
                {
                    if (onMoviePlayFirstFrame != null)
                        onMoviePlayFirstFrame.Invoke(m);
                } ;
            }

            SwapForwardPlayer(-1);
            SwapForwardPlayer(0);
        }

        public float VideoTime
        {
            get { return activePlyrIndex < 0 || plyrs[activePlyrIndex].videoPlayer.isPlaying == false? -1f : (float) plyrs[activePlyrIndex].videoPlayer.time; }
        }
        public VideoPlayer ActivePlayer { get { return activePlyrIndex < 0 ? null : plyrs[activePlyrIndex].videoPlayer; } }

        public void Pause()
        {
            plyrs[activePlyrIndex].videoPlayer.Pause();
        }
        public void Continue()
        {
            plyrs[activePlyrIndex].videoPlayer.Play();
        }
        private readonly Vector3 BackgroundDomePos = new Vector3(0, 5000f, 0);
        private readonly Vector3 ForgroundDomePos = new Vector3(0, 0, 0);

        // private static Vector3 BackgroundDomeScale = Vector3.one;// * 1.1f;
        // private static Vector3 ForgroundDomeScale = Vector3.one;
        //public ScriptableSceneSettings[] flowSequence;
        //private int sequenceIndex;

        private void SwapForwardPlayer(int newActiveIndex)//ShowMovie newFront, ShowMovie newBack)
        {
            for (int i = 0; i < plyrs.Length; i++)
            {
                if (i == newActiveIndex)
                {
                    //Debug.Log("ForgroundDomePos " + i);
                    plyrs[i].transform.position = ForgroundDomePos;
                }
                else
                {
                    //Debug.Log("BackgroundDomePos " + i);
                    plyrs[i].transform.position = BackgroundDomePos;
                    plyrs[i].Stop();
                }
            }
            if(debug) Debug.LogFormat("Swap to {0} - {1}", newActiveIndex, (newActiveIndex >= 0 && newActiveIndex < plyrs.Length? plyrs[newActiveIndex].name : "NULL"));
        }

       



        public void PlayUnpreperdMovieNow(string movieName, bool loop, Action<Dweiss.ShowMovie> onReady)
        {
            if (debug) Debug.Log("PlayUnpreperdMovieNow " + movieName);
            var plyrIndex = activePlyrIndex;
            //var otherPlyr = player2;
            plyrs[plyrIndex].PrepereMovie(movieName, loop,
                (m) =>
                {
                    if(debug) Debug.Log("Prepered finished " + movieName);
                    SwapForwardPlayer(plyrIndex);
                    plyrs[plyrIndex].Play();
                    if (onReady != null) onReady.Invoke(m);
                    //if (onMovieDealyStart != null) onMovieDealyStart.Invoke();
                }
                );
        }

        public struct MoviePrepConfig
        {
            public string movieName;
            public bool loop;

            public MoviePrepConfig(string movieName, bool loop)
            {
                this.movieName = movieName;
                this.loop = loop;
            }
        }
        public void PrepareMovie(Action<Dweiss.ShowMovie> onReady, string movieName, bool movieLoop)
        {
            PrepareMovie(onReady, new MoviePrepConfig(movieName, movieLoop));
        }
        public void PrepareMovie(Action<Dweiss.ShowMovie> onReady, params MoviePrepConfig[] config)
        {
            var configIndex = 0;
            for (int i = 0; configIndex < config.Length; i++)
            {
                if(config.Length > plyrs.Length)
                {
                    Debug.LogError("Error not enough players in movie transition");
                }
                if (i != activePlyrIndex)
                {
                    Debug.Log("Prepare " + i + " with " + config[configIndex].movieName);
                    plyrs[i].PrepereMovie(config[configIndex].movieName, config[configIndex].loop, onReady);
                    configIndex++;
                }
            }
        }

        public void PlayAt(string movieName, float startAt)
        {
            if (debug) Debug.Log("Play at " + movieName + " dekay of " + startAt );
            StopAllCoroutines();
            StartCoroutine(DelayPlayRealTime(movieName, startAt));
        }

        private int GetVideoIndexByName(string videoName)
        {
            for (int i = 0; i < plyrs.Length; i++)
            {
                if (plyrs[i].VideoName == videoName)
                {
                    return i;
                }
            }
            return -1;
        }
        private IEnumerator DelayPlayRealTime(string movieName, float playAt)
        {

            while (Time.time < (playAt - Time.deltaTime))
            {
                yield return 0;
            }
            var index = GetVideoIndexByName(movieName);
            if (debug) Debug.Log("Ply " + index  + " " + movieName + " T=" + Time.time + " wait To T=" + playAt + " dlta " + Time.deltaTime);
            plyrs[index].Play();

            activePlyrIndex = index;

            
            if (onMovieBeforeDelayStart != null) onMovieBeforeDelayStart.Invoke();
            if (swapDelay >= 0) yield return new WaitForSeconds(swapDelay);

            SwapForwardPlayer(index);
            if (onMovieDelayStart != null) onMovieDelayStart.Invoke();

        }
    }
}