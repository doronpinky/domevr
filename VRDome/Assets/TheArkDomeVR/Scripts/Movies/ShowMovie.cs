﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace Dweiss
{
    public class ShowMovie : MonoBehaviour
    {

        [SerializeField] private bool showDebug;
        public string videoStartUrlName;
        public bool autoPlay;

        // public Texture texture;
        public VideoPlayer videoPlayer;
        public AudioSource[] audioS;
        public string defaultSuffix = ".mp4";


        public Action<ShowMovie> oneTimePrepare, onStartPlay, onVideoPlayFirstFrame;

        public float videoDuration;

        private Action<ShowMovie> onPrepare;
        private string prefix;
        private bool stopping = true;
        private string videoName;
        private VideoEvents ve;

        public string VideoName { get { return videoName; } }


        private void OnGUI()
        {
            if (showDebug && videoPlayer.isPlaying)

                GUI.Label(new Rect(10, Screen.height- 30, Screen.width *2/ 9, 90),
                    string.Format("{1} - {0}/{2}"
                        , videoPlayer.time.ToString("0.0")
                        , videoPlayer.url.Substring(videoPlayer.url.LastIndexOf("/") + 1)
                        , videoDuration.ToString("0.0")
                        )
                    );
        }

        private void Awake()
        {
            Init();
        }
        private void Init (){
            prefix = Application.dataPath + "/StreamingAssets";

            ve = videoPlayer.GetComponent<VideoEvents>();
            if (ve == false)
            {
                ve = videoPlayer.gameObject.AddComponent<VideoEvents>();
            }
            ve.onPlayStarted.AddListener((m) =>
            {
                if (onVideoPlayFirstFrame != null)
                    onVideoPlayFirstFrame.Invoke(this);
            });
        }


#if UNITY_EDITOR
        [SerializeField] [Header("Debug editor mode only")]private float currentTime;
        [SerializeField] private float setTime;

        private void OnValidate()
        {
            if (Mathf.Abs(setTime - currentTime) > 0.05)
            {
                SkipTo(setTime);
            }
        }
        private void Update()
        {
            setTime = currentTime = (float)videoPlayer.time;
        }
#endif
        public void SkipTo(float newTime)
        {
            videoPlayer.time = newTime;
        }
        private void Start()
        {

            videoPlayer.loopPointReached += VideoPlayer_loopPointReached;
            videoPlayer.prepareCompleted += OnMoviePreperd;
            videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

            if (string.IsNullOrEmpty(videoStartUrlName) == false)
            {
                PrepereMovie(videoStartUrlName, true);
            }
        }

        private void VideoPlayer_loopPointReached(VideoPlayer source)
        {
            //onLoopReached.Invoke(this);

        }

        private void OnMoviePreperd(VideoPlayer vp)
        {
            if (stopping) return;
            // Debug.Log("Ready: " + this);
            videoDuration = videoPlayer.frameCount / videoPlayer.frameRate;
            //onPreperationComplete.Invoke(this);

            SetSound();


            if (oneTimePrepare != null)
            {
                oneTimePrepare.Invoke(this);
                oneTimePrepare = null;
            }

            if (autoPlay)
            {
                Play();
            }
        }

        //private string GetFullMovieName(string name)
        //{
        //    if (name[0] != '/' && name[0] != '\\') name = "/" + name;
        //    if (name.IndexOf(".") < 0)
        //    {
        //        name += defaultSuffix;
        //    }
        //    if (name.StartsWith(prefix) == false)
        //    {
        //        name = prefix + name;
        //    }
        //    return name;//.Replace("/", "\\"); ;
        //}
        private void SetSound()
        {
            if (audioS != null)
            {
                videoPlayer.controlledAudioTrackCount = (ushort)audioS.Length;
                for (int i = 0; i < audioS.Length; i++)
                {
                    videoPlayer.EnableAudioTrack(0, true);
                    videoPlayer.SetTargetAudioSource((ushort)i, audioS[i]);
                    //videoPlayer.SetDirectAudioVolume(0, 1);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0} - stop {1}", videoPlayer.url, stopping);
        }

        #region Public Movie API

        public void Stop()
        {
            stopping = true;
            videoPlayer.Pause();
        }

        public void PrepereMovie(string name, bool loop, Action<ShowMovie> onOneTimePrepare = null, Action<ShowMovie> onOneTimeStartPlay = null)
        {
            stopping = false;
            videoPlayer.isLooping = loop;
            videoName = name;

            Debug.Log("PreloadStreamingAsset.S " + (PreloadStreamingAsset.S == null?"NULL": PreloadStreamingAsset.S.transform.FullName()));
            var fullMovieName = PreloadStreamingAsset.S.GetFullPath(PreloadStreamingAsset.S.AddSuffixToMovie(videoName));// GetFullMovieName(name);


           // Dweiss.PreloadStreamingAsset.S.Load(videoName, videoPlayer, (vp) =>
            //{

                //Debug.Log("PrepereMovie " + fullMovieName);
                videoPlayer.url = fullMovieName;// "/Users/graham/movie.mov";
                videoPlayer.Stop();
                videoPlayer.Prepare();


                oneTimePrepare = onOneTimePrepare;
                onStartPlay = onOneTimeStartPlay;
                SetSound();
           // });
        }

        public bool Play()
        {
            ve.PlayWithEvent();
            
            //videoPlayer.Play();
            SetSound();
            if (onStartPlay != null)
            {
                onStartPlay(this);
                onStartPlay = null;
            }

            if (videoPlayer.isPrepared)
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}