﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoEvents : MonoBehaviour
{
    public bool debug;
    private VideoPlayer videoPlayer;

    public bool pauseOnPlay;

    public VideoPlayerEvent onPreperationComplete = new VideoPlayerEvent();
    public VideoPlayerEvent onStop = new VideoPlayerEvent();
    public VideoPlayerEvent onPlayStarted = new VideoPlayerEvent();

    [System.Serializable]
    public class VideoPlayerEvent :  UnityEngine.Events.UnityEvent<VideoPlayer> { }

    public System.Action<VideoEvents> onStopVidEvent;

    private Coroutine waitForStop;

    public VideoPlayer VideoPlayer { get { return videoPlayer; } }
    public bool backupWaitForStopEvent = false;

    private void Reset()
    {
        videoPlayer = GetComponentInChildren<VideoPlayer>();
    }

    public void PreprareVideo()
    {
        if (debug) Debug.Log(name + " PreprareVideo " + videoPlayer.url);
        if (waitForStop != null) StopCoroutine(waitForStop);
        videoPlayer.Prepare();
    }


    public void PlayWithEvent(string url)
    {
        if(debug)Debug.Log(name +" PlayWithEvent " + url);
        videoPlayer.url = url;
        PlayWithEvent();
    }

    [ContextMenu("Play with events")]
    public void PlayWithEvent()
    {
        if (debug) Debug.Log(name + " PlayWithEvent " + videoPlayer.name + " >> " + videoPlayer.url);
        if (waitForStop != null) StopCoroutine(waitForStop);

        //Debug.Log(name + " Play movie");
        videoPlayer.sendFrameReadyEvents = true;
        videoPlayer.Play();
        if(backupWaitForStopEvent) waitForStop = StartCoroutine(WaitForStop(5));//Delay start listen to event

    }

    [ContextMenu("Stop-Play with events")]
    public void StopPlayWithEvent()
    {
        if (debug) Debug.Log(name + " StopPlayWithEvent " + videoPlayer.name + " >> " + videoPlayer.url);

        Stop();
        PlayWithEvent();
    }
    [ContextMenu("Stop")]
    public void Stop()
    {
        if (debug) Debug.Log(name + " Stop " + videoPlayer.name + " >> " + videoPlayer.url);
        
        videoPlayer.Stop();
    }

    private void VideoPlayer_frameReady(VideoPlayer source, long frameIdx)
    {
        if (debug) Debug.Log(name + " VideoPlayer_frameReady " + videoPlayer.name + " >> " + videoPlayer.url);
        videoPlayer.sendFrameReadyEvents = false;
        if (pauseOnPlay)
        {
            videoPlayer.Pause();
            pauseOnPlay = false;

            if (onStop != null) onStop.Invoke(videoPlayer);
        }
        else
        {
            if (waitForStop != null) StopCoroutine(waitForStop);
            waitForStop = StartCoroutine(WaitForStop());
        }
        try
        {
            if(onPlayStarted != null) onPlayStarted.Invoke(source);
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error " + e);
        }

    }


    IEnumerator WaitForStop(float delayRun = -1)
    {
        if (delayRun >= 0) yield return new WaitForSeconds(delayRun);

        while (videoPlayer.isPlaying)
            yield return null;
        if (onStop != null) onStop.Invoke(videoPlayer);
    }

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        onStop.AddListener((v) => { if (onStopVidEvent != null) onStopVidEvent.Invoke(this); });
    }

    void Start()
    {
        videoPlayer.prepareCompleted += (vp) => { if (onPreperationComplete != null) onPreperationComplete.Invoke(vp); };
        videoPlayer.frameReady += VideoPlayer_frameReady;

        if (videoPlayer.playOnAwake)
            PlayWithEvent();
    }
}
