﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class GameFlowSessionState : Dweiss.Singleton<GameFlowSessionState>
    {
        public int maxPpl = 10;
        //public ScriptableSceneSettings bonusRound;
        //public ScriptableSceneSettings climateRound;

        //public string MainImagesFolderFullPath = "z:\\PhotoBooth\\";
        //public string ImagesFolderPath = "1";

        public UnityEngine.UI.InputField pplCount;

        public int numOfPlayers = 8;
        // public int minCorrectAnswersInBonusRound = 3;
        public bool setPplValueOnStart = true;
        //private int joystickAtUser;
        private float questionStartTime;

        private void Start()
        {
             if(setPplValueOnStart ) pplCount.text = numOfPlayers.ToString();
        }

        public class UserSelectionInfo
        {
            public int id;
            public bool correct;
            public float timeToSelection;

            public static int CompareTo(UserSelectionInfo that, UserSelectionInfo other)
            {
                if (that.correct && other.correct) return that.timeToSelection.CompareTo(other.timeToSelection);
                if (that.correct == false && other.correct == false) return that.timeToSelection.CompareTo(other.timeToSelection);
                if (that.correct) return -1;
                else return 1;
            }

            public override string ToString()
            {
                return string.Format("{0} {1} {2}", id, correct, timeToSelection);
            }
        }
       // public Dictionary<int, UserSelectionInfo> userSelectedCorrectly = new Dictionary<int, UserSelectionInfo>();

        public void SetNumOfPpl(string count)
        {
            SetNumOfPpl(int.Parse(count));
        }
        public void SetNumOfPpl(int count)
        {
            numOfPlayers = System.Math.Max(System.Math.Min(count, maxPpl), 1);
            pplCount.text = numOfPlayers.ToString();
        }
        public int GetBestUserInClimateRound()
        {
            throw new System.NotSupportedException("Deprecated");
            //return 1;//TODO this fun is not used
            //var users = userSelectedCorrectly.Values.ToArray();
            //if (users.Length == 0) return -1;

            //System.Array.Sort(users, UserSelectionInfo.CompareTo);
            //Debug.Log("Best user " + users.ToCommaString());
            //return users[0].id % numOfPlayers;
        }

        private void OnSceneFinished()
        {
            //if (GameFlow.S.LastMovieScene == climateRound)
            //{
            //    //Debug.Log("Best user " + users.ToCommaString());
            //    Dweiss.Msg.MsgSystem.MsgStr.Raise<object>("BestUserInClimateRound", GetBestUserInClimateRound());
            //}
        }

        //public bool SuccessInBonusRound()
        //{
        //    var ret = GetBonusRoundCorrectAnswers() >= bonusRound.MinimumCorrectAnswersForRoundSuccess;
        //    Debug.LogFormat("SuccessInBonusRound {2} >> {0} >= {1} ", GetBonusRoundCorrectAnswers(), bonusRound.MinimumCorrectAnswersForRoundSuccess, ret);
        //    return ret;
        //}

        //private int GetBonusRoundCorrectAnswers()
        //{
        //    return ScoreSystem.S.GetSceneCorrectAnswerCount(bonusRound.name);
        //}

        private void TriviaRoundStart()
        {
            NewQuestionStart();
            //if (GameFlow.S.CurrentMovieScene == climateRound)
            //{
            //    Dweiss.Msg.MsgSystem.MsgStr.Raise<object>("BestUserInClimateRound", GetBestUserInClimateRound());
            //}
        }

        private void NewQuestionStart()
        {
            questionStartTime = Time.time;
        }

        private void OnCorrectAnswer()
        {
            //if (GameFlow.S.CurrentMovieScene == climateRound)
            //{
            //    userSelectedCorrectly[joystickAtUser] = new UserSelectionInfo() { id = joystickAtUser, correct = true, timeToSelection = Time.time - questionStartTime };
            //    joystickAtUser = (joystickAtUser + 1);
            //}
            NewQuestionStart();

            //if (GameFlow.S.CurrentMovieScene == climateRound)
            //{
            //    Dweiss.Msg.MsgSystem.MsgStr.Raise<object>("BestUserInClimateRound", GetBestUserInClimateRound());
            //}
        }

        private void OnWrongAnswer()
        {
            //if (GameFlow.S.CurrentMovieScene == climateRound)
            //{
            //    userSelectedCorrectly[joystickAtUser] = new UserSelectionInfo() { correct = false, timeToSelection = Time.time - questionStartTime };
            //    joystickAtUser++;
            //}
            NewQuestionStart();

            //if (GameFlow.S.CurrentMovieScene == climateRound)
            //{
            //    Dweiss.Msg.MsgSystem.MsgStr.Raise<object>("BestUserInClimateRound", GetBestUserInClimateRound());
            //}
        }

        private void ResetSession()
        {
            //joystickAtUser = 0;
            //userSelectedCorrectly.Clear();

            Dweiss.Msg.MsgSystem.MsgStr.Raise<object>("BestUserInClimateRound", -1);
        }


        private void OnEnable()
        {
            ResetSession();


            Dweiss.Msg.MsgSystem.MsgStr.Register("ResetSession", ResetSession);
            Dweiss.Msg.MsgSystem.MsgStr.Register("WrongAnswer", OnWrongAnswer);
            Dweiss.Msg.MsgSystem.MsgStr.Register("CorrectAnswer", OnCorrectAnswer);
            Dweiss.Msg.MsgSystem.MsgStr.Register("TriviaRoundStart", TriviaRoundStart);
            Dweiss.Msg.MsgSystem.MsgStr.Register("OnSceneFinished", OnSceneFinished);



        }
        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
            {
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("ResetSession", ResetSession);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("WrongAnswer", OnWrongAnswer);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("CorrectAnswer", OnCorrectAnswer);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("TriviaRoundStart", TriviaRoundStart);
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("OnSceneFinished", OnSceneFinished);
            }
        }

    }
}