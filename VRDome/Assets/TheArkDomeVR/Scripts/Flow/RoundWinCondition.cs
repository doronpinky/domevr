﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class RoundWinCondition : MonoBehaviour
    {
        [SerializeField] private bool successOverrideInEditor;

        public ScriptableSceneSettings dependedScene;
        //public int minScoreToWin;

        public Dweiss.EventEmpty onWin, onLose, postEvent;



        private void Awake()
        {
            var score = ScoreSystem.S.GetSceneCorrectAnswerCount(dependedScene.name);
#if UNITY_EDITOR
            if (successOverrideInEditor)
            {
                score = dependedScene.MinimumCorrectAnswersForRoundSuccess;
            }
#endif
            if( score >= dependedScene.MinimumCorrectAnswersForRoundSuccess)
            {
                onWin.Invoke();
            }else
            {
                onLose.Invoke();
            }
            postEvent.Invoke();
        }
    }
}