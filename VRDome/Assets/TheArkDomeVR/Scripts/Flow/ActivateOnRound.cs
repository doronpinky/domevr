﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ActivateOnRound : MonoBehaviour
    {
        public ScriptableSceneSettings sceneToInteract;
        public float delay = -1;

        public Dweiss.EventEmpty onSceneStart;

        public DelayFunction[] moreDelays;
        //public Dweiss.EventEmpty onSceneEnd;

            [System.Serializable]
            public class DelayFunction
        {
            public float delay = -1;

            public Dweiss.EventEmpty onSceneStart;
        }


        private void Reset()
        {
            delay = -1;
        }

        
        private void OnLevelStart(string lvlName) {
            //var mySceneId = GameFlow.S.GetSceneId(sceneToInteract);

            Debug.LogFormat(name + " ___ <color=red> now id {0} vs my id {1} - {2} </color>", lvlName, sceneToInteract.name, delay);
            if (sceneToInteract.name == lvlName)
            {
                DelayAction(delay, onSceneStart);

                for (int i = 0; i < moreDelays.Length; i++)
                {
                    DelayAction(moreDelays[i].delay, moreDelays[i].onSceneStart);
                }
            }
        }


        private void DelayAction(float delay, Dweiss.EventEmpty action)
        {
            if (delay < 0)
            {
                action.Invoke();
                Debug.LogFormat(name + " ___ <color=blue>RunNow After {0}.{1} </color>", sceneToInteract.name, delay);
            }
            else
                this.SimpleWaitWhile(() => GameFlow.S.CurrentMovieScene.name == sceneToInteract.name &&
                GameFlow.S.VideoTime < delay - 0.5f * Time.deltaTime,
                    () => {if (GameFlow.S.CurrentMovieScene.name == sceneToInteract.name)
                        {
                            Debug.LogFormat(name + " ___ <color=blue>RunWait After {0}.{1} </color>", sceneToInteract.name, delay);
                        action.Invoke();
                        }
                    });
        }



        private void OnEnable()
        {
            Dweiss.Msg.MsgSystem.MsgStr.Register("LevelChanged", OnLevelStart);
            OnLevelStart(GameFlow.S.CurrentMovieScene.name);
        }
        private void OnDisable()
        {
            if(Dweiss.Msg.MsgSystem.S)
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("LevelChanged", OnLevelStart);
        }

    }
}