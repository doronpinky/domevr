﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
    public class SingleSceneFlow : MonoBehaviour
    {

        [SerializeField] private bool dontWaitInEditor = false;
        [SerializeField] private MovieEvent[] movieEvents;

        public MovieEvent[] MovieEvents { get { return movieEvents; } }

        public float waitForGuiToAppear = -1;
        public float guiStopAt = -1;

        [System.Serializable]
        public class MovieEvent
        {
            [Tooltip("Video time -1 means run now,  -2 means don't run at all")]
            public float timeOfEvent;
            public Dweiss.EventEmpty action;

            public override string ToString()
            {
                return string.Format("EVNT - {0} {1}", timeOfEvent, action.ToString());
            }
        }
        

        private ScriptableSceneSettings sceneSettings;
        public void InitSceneSettings(ScriptableSceneSettings sceneSettings)
        {
            this.sceneSettings = sceneSettings;
        }

        public GameObject guiChild;
        private void Reset()
        {
            guiChild = transform.GetChild(0).gameObject;
        }

        private float VideoTime
        {
            get { return (GameFlow.S.CurrentMovieScene.IsLoop || GameFlow.S.VideoTime < 0  ? GameFlow.S.SceneTime : GameFlow.S.VideoTime); }
        }

        IEnumerator Start()
        {
            StartCoroutine(TimedEvents(MovieEvents));
            if (float.IsInfinity(waitForGuiToAppear) == false && guiChild == null && transform.childCount > 0)
                guiChild = transform.GetChild(0).gameObject;

            if (guiChild != null)
            {

#if UNITY_EDITOR
                if (dontWaitInEditor)
                {
                    guiStopAt -= waitForGuiToAppear;
                    waitForGuiToAppear = 0;
                }
#endif

                if (waitForGuiToAppear >= 0)
                    yield return ComponentExtension.ExactWait(waitForGuiToAppear, () => {return VideoTime; });
                //yield return new WaitForSeconds(waitForGuiToAppear - VideoTime);
                if(guiChild) guiChild.SetActive(true);
                if (guiStopAt > 0)
                {
                    yield return ComponentExtension.ExactWait(guiStopAt, () => { return VideoTime; });
                    if(guiChild) guiChild.SetActive(false);
                }
            }
        }
        [ContextMenu("Sort events")]
        private void SortEvents()
        {

            System.Array.Sort(movieEvents, (a, b) => a.timeOfEvent.CompareTo(b.timeOfEvent));
        }
        private IEnumerator TimedEvents(MovieEvent[] events)
        {
            System.Array.Sort(events, (a, b) => a.timeOfEvent.CompareTo(b.timeOfEvent));

#if UNITY_EDITOR
            if (dontWaitInEditor)
            {
                var minTime = events.Select(a => a.timeOfEvent).Where(v => v > 0).Min();
                Debug.Log("Min time " + minTime);
                foreach (var m in events) m.timeOfEvent -= minTime;

               // for (int i = 0; i < events.Length; i++) if(events[i].timeOfEvent >= 0 ) events[i].timeOfEvent = i * 2;

            }
#endif

            Debug.Log("Events " + events.ToCommaString());

            var startTime = Time.time;




            for (int i = 0; i < events.Length; ++i)
            {
                if (events[i].timeOfEvent < 0)
                {
                    Debug.Log("Skip scene event " + events[i]);
                    continue;
                }

                var totalWaitTime = events[i].timeOfEvent - VideoTime;
                var waitTime = totalWaitTime;
                Debug.Log("E_" + i + ". >> wait time " + waitTime);
                while (waitTime > .5f * Time.deltaTime)
                {
                    yield return 0;
                    waitTime = events[i].timeOfEvent - (VideoTime == 0 ? Time.time - startTime : VideoTime);
                }

                Debug.Log("E_"+i+". >> waited total " + totalWaitTime + " to T_" + Time.time + " EventTimeInMovie " + events[i].timeOfEvent
                   + " vid time " + VideoTime + " wait time " + waitTime);

                try
                {
                    events[i].action.Invoke();
                }
                catch (System.Exception e)
                {
                    Debug.LogError("Evetns err " + e);
                }
            }
            Debug.Log(name + " Finished events");
        }
    }
}