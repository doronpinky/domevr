﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;


public abstract class ASequenceEvent : MonoBehaviour
{
    protected const bool debug = false;

    public bool runOnStart;

    [Tooltip("-1 means no waiting (SimpleSequenceEvent will run next event if script disabled.")]
    [SerializeField] protected float length = -1;
    [SerializeField] protected ASequenceEvent[] nextEvents;
    public void SetLength(float value)
    {
        if (IsRunning == false)
            length = value;
    }

    [Space()]
    [Header("Inspector events")]
    public EventEmpty onStart;
    public EventEmpty onTimeEnd;
    public EventEmpty onForceStop;
    public EventEmpty onAnyStop;

    public abstract bool IsRunning { get; protected set; }

    protected void Invoke(EventEmpty action)
    {
        action.Invoke();
    }

    protected virtual void Start()
    {
        //if (debug) Debug.Log("--- Start " + name );
        onStart.AddListener(() => { if (debug) Debug.Log(">> " + name + " InvokeStart " + length); });
        onTimeEnd.AddListener(() => { if (debug) Debug.Log(">> " + name + " onTimeEnd " + length); });
        onForceStop.AddListener(() => { if (debug) Debug.Log(">> " + name + " onForceStop " + length); });


        if (runOnStart) this.WaitForSeconds(0, Run);//wait 1 frame
    }


    protected void RunNext()
    {
        for (int i = 0; i < nextEvents.Length; i++)
        {
            var next = nextEvents[i];
            if (next.gameObject.activeSelf == false)
            {
                next.gameObject.SetActive(true);
            }
            if (debug) Debug.Log("--- " + name + " RunNext  >> " + next.gameObject.name);
            next.Run();
        }

    }

    protected virtual void OnDisable()
    {
        ForceStop();
    }

    public abstract void Run();
    protected abstract void AnyStop();

    protected abstract void TimeEnd();
    public abstract void ForceStop();
}

