﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;

public class SimpleSequenceEvent : ASequenceEvent
{
    
    private bool _isRunning;
    public override bool IsRunning { get { /*if (waitTime == null) Debug.LogWarning(name + " not running");*/ return _isRunning; }
        protected set { _isRunning = value; /*Debug.Log(">>> " + name + " runnig:" + value);*/ } }
    protected Coroutine waitTime;

    protected virtual void RunIt()
    {

    }

    
    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            var hasPrefix = (name.Length > 0 && name[0] == '*');
            if (_isRunning && hasPrefix == false) name = "*" + name;
            else if (_isRunning == false && hasPrefix) name = name.Substring(1);
        }
    }

    public override void Run()
    {
       
        if (IsRunning)
        {
            Debug.LogError(name + " can't start while running");
            return;
        }
        if (enabled == false || gameObject.activeInHierarchy == false)
        {
            RunNext();
            return;
        }
        //Debug.Log(">>> " + name + " Run");

        IsRunning = true;

        try
        {
            RunIt();

        }
        catch (System.Exception e)
        {
            Debug.LogError("Run it failed on " + e);
        }

        if (IsRunning)// run it stopped the play?
        {
            Invoke(onStart);
            WaitEventLength();
        }
    }
    
    protected void WaitEventLength() {
        //Debug.Log(name + "WaitEventLength " + length);
       
        if (length < 0)
            ExecuteTimeEnd();
        else if(float.IsPositiveInfinity(length) == false)
            waitTime = this.WaitForSeconds(length, TimeEnd);//Set running state
    }

    protected void StopWaiting()
    {
        IsRunning = false;
        if (waitTime != null)
        {
            StopCoroutine(waitTime);
            waitTime = null;
        }
    }
    protected override void AnyStop() 
    {
        if (IsRunning == false) return;
        StopWaiting();

        Invoke(onAnyStop); 
    }

    protected override void TimeEnd()
    {
        if (IsRunning == false)return;
           
        ExecuteTimeEnd();
    }

    protected void ExecuteTimeEnd() {
        StopWaiting();

        Invoke(onTimeEnd);
        Invoke(onAnyStop);
        RunNext();
    }

    public override void ForceStop()
    {
        try
        {
            if (IsRunning == false)
            {
                //Debug.Log(name +  " Cant force stop not running");
                return;
            }
            Debug.Log(name + " was forced stopped ");
            StopWaiting();


            Invoke(onForceStop);
            Invoke(onAnyStop);
        }
        catch (System.Exception e) { Debug.LogError(name + " " + e + " failed forced stopped "); }

    }


}
