﻿using Dweiss.Msg;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;

namespace DomeVR
{
    /// <summary>
    /// After question was answered, show only correct question animation, make sound effect, create voice over and wait x seconds. Move to first event
    /// </summary>
    public class SeeAnswerSeqEvent : BaseQuestionTriviaSeq
    {
        public enum ShowAnswer { SelectedWasCorrect, ShowSelectedWrong, ShowNotSelectedCorrect }
        public ShowAnswer showAnswer;


        public enum AnswerOption { Show, Hide, SetTransparent, MarkCorrect, MarkWrong }
        [System.Serializable]
        public class EventAnswerOption : UnityEngine.Events.UnityEvent<AnswerOption[]> { }


        [Space()]
        [Header("Trivia events")]
        public Dweiss.EventString onCorrectAnswer;
        public Dweiss.EventString onWrongAnswer;
        public EventAnswerOption onAnswerSet ;





        [Header("Score")]
        public EventInt onScoreChanged;

        [Header("Narration")]
        public EventEmpty onCorrectNarration;
        public EventEmpty onWrongNarration, onCorrectSensetiveNarration;

        private void InvokeAudioNarration()
        {
            switch (showAnswer)
            {
                case ShowAnswer.SelectedWasCorrect:
                    if (qState.CurrentQ.AnswerSoundEffects == ReadQuestion.QuestionInfo.AnswerSoundEffectsEnum.Any)
                    {
                        onCorrectNarration.Invoke();
                    }
                    else
                    {
                        onCorrectSensetiveNarration.Invoke();
                    }
                    break;
                case ShowAnswer.ShowSelectedWrong:
                    onWrongNarration.Invoke();
                    break;
                case ShowAnswer.ShowNotSelectedCorrect:
                    break;
                default:
                    Debug.LogError("SeeAnswerSeqEvent missing support for show answer " + showAnswer);
                    break;
            }
        }

        protected override void RunIt()
        {
            InvokeAudioNarration();
            InvokeAnswerState();
        }
        public bool affectYearsCount = true;
        public bool showYears = true;
        private void InvokeAnswerState() { 
            
            var answersState = new AnswerOption[qState.CurrentQ.Answers.Length];
            for (int i = 0; i < qState.CurrentQ.Answers.Length; i++)
            {
                switch (showAnswer)
                {
                    case ShowAnswer.SelectedWasCorrect: 
                        if (qState.CurrentQ.CorrectAnswer == qState.CurrentQ.Answers[i])
                        {
                            answersState[i] = (AnswerOption.MarkCorrect);

                            ScoreSystem.S.IncreaseCorrectAnswerCount();
                            if(affectYearsCount) ScoreSystem.S.DecreaseYearCount(
                                qState.TriviaInfo.YearPerQuestion);

                            onScoreChanged.Invoke(showYears ? ScoreSystem.S.CurrentYears : ScoreSystem.S.SceneCorrectAnswerCount);

                            //onScoreChanged.Invoke(ScoreSystem.S.CurrentYears);
                            onCorrectAnswer.Invoke(qState.SelectedAnswer);
                            MsgSystem.MsgStr.Raise("CorrectAnswer");
                        }
                        else
                        {
                            answersState[i] = AnswerOption.Show;
                        }
                        break;
                    case ShowAnswer.ShowSelectedWrong:
                        if (qState.CurrentQ.Answers[i] == qState.SelectedAnswer)
                        {
                            answersState[i] = (AnswerOption.MarkWrong);
                            onWrongAnswer.Invoke(qState.SelectedAnswer);
                            MsgSystem.MsgStr.Raise("WrongAnswer");
                        }
                        else
                        {
                            answersState[i] = (AnswerOption.Show);
                        }
                        break;
                    case ShowAnswer.ShowNotSelectedCorrect:
                        if (qState.CurrentQ.CorrectAnswer == qState.CurrentQ.Answers[i])
                        {
                            answersState[i] = (AnswerOption.MarkCorrect);
                            onCorrectAnswer.Invoke(qState.CurrentQ.Answers[i]);
                        }
                        else
                        {
                            answersState[i] = AnswerOption.Hide;
                        }
                        break;
                    default:
                        Debug.LogError("SeeAnswerSeqEvent missing support for show answer " + showAnswer);
                        break;
                }


            }
            onAnswerSet.Invoke(answersState);
            
        }

        //protected override void TimeEnd()
        //{
        //    for (int i = 0; i < qState.CurrentQ.Answers.Length; i++)
        //    {
        //        answerIndexIsCorrect[i].Invoke(false);
        //    }
        //    base.TimeEnd();
        //}
    }
}