﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class ShowInfoSeqEvent : BaseQuestionTriviaSeq
    {

        public Dweiss.EventString onInfo;
        public Dweiss.EventAudioClip onInfoClip;

        private const float DefaultLengthOnClip = 3;
        public float delayAfterAnswerFinished = 1;
        public string folderPrefix;

        protected override void RunIt()
        {
            var startTime = Time.time;  
            if (string.IsNullOrEmpty(qState.CurrentQ.AfterInfoAudioName) == false)
            {
                if (string.IsNullOrEmpty(qState.CurrentQ.AfterInfoText) == false)
                {
                    onInfo.Invoke(qState.CurrentQ.AfterInfoText);
                }
                length = DefaultLengthOnClip;
                var fileName = (string.IsNullOrEmpty(folderPrefix) ? "" : folderPrefix + "\\") + qState.CurrentQ.AfterInfoAudioName;
                Dweiss.PreloadStreamingAsset.S.Load(
                   fileName, 
                    (ac) => {

                        if (IsRunning)
                            onInfoClip.Invoke(ac);

                        length = ac.length;

                        if(waitTime != null) StopCoroutine(waitTime);
                        var timeLeft = length - (Time.time - startTime);
                        waitTime = this.WaitForSeconds(timeLeft + delayAfterAnswerFinished, TimeEnd);//Set running state
                    });
            }else
            {
                length = -1;
            }
        }
    }
}