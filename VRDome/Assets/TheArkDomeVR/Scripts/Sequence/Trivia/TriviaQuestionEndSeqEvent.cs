﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class TriviaQuestionEndSeqEvent : SimpleSequenceEvent
    {
        public bool moveToNextSceneOnEnd = false;
        protected override void Start()
        {
            onAnyStop.AddListener(() => { if(moveToNextSceneOnEnd) GameFlow.S.GoToNextSceneAccordingToUserAction(-1); });
        }
    }
}