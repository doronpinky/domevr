﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class ShowYearSeqEvent : BaseQuestionTriviaSeq
    {
        [SerializeField] private Dweiss.EventInt onYearScore;
        [SerializeField] private Dweiss.EventString onYearScoreStr;


        protected override void RunIt() {
            
            onYearScore.Invoke(ScoreSystem.S.CurrentYears);
            onYearScoreStr.Invoke(ScoreSystem.S.CurrentYears.ToString());

        }
    }
}