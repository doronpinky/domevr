﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using Dweiss.Msg;

namespace DomeVR
{
    public class TriviaTimerSeqEvent : BaseQuestionTriviaSeq
    {

        [Space()]
        [Header("Start events")]
        public EventInt onTimeLimitStart;
        public EventEmpty onTimerStop;

        public EventInt onCurrentScore;

        public SimpleSequenceEvent treeToStop;

        protected new void Start()
        {
            base.Start();

            onAnyStop.AddListener(() => { try { onTimerStop.Invoke(); } catch (System.Exception e) { Debug.LogError(name+" onTimerStop stop error " + e); } });

            if (treeToStop != null)
            {
                onAnyStop.AddListener(() =>
                {
                    foreach (var sse in treeToStop.GetComponentsInChildren<ASequenceEvent>(true)) try { sse.ForceStop(); } catch (System.Exception e) { Debug.LogError("Timer stop error " + e); };
                });
            }
            onAnyStop.AddListener(() => { StopAllCoroutines(); });
        }

        protected override void RunIt()
        {
            //Debug.Log("QState time limit per q " + qState.TriviaInfo.HasTimeLimitPerQuestion);
            if (qState.TriviaInfo.HasTimeLimitPerQuestion == false)
            {
                onTimeLimitStart.Invoke((int)qState.TriviaInfo.TotalQTime);
                length = ((int)qState.TriviaInfo.TotalQTime);
            }
            else
            {
                length = float.PositiveInfinity;
            }

            MsgSystem.MsgStr.Raise("TriviaRoundStart");

            //onBestScore.Invoke(ScoreSystem.S.BestLevelPoints);
            onCurrentScore.Invoke(showYears? ScoreSystem.S.CurrentYears : ScoreSystem.S.SceneCorrectAnswerCount);


        }

        public bool showYears = true;


    }

}