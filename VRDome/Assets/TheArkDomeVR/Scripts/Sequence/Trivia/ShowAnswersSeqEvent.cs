﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Dweiss;
using Dweiss.Msg;

namespace DomeVR
{
    public class ShowAnswersSeqEvent : BaseQuestionTriviaSeq
    {
        [Space()]
        [Header("Trivia options")]
        public ASequenceEvent onCorrectAnswer;
        public ASequenceEvent onWrongAnswer;
        public ASequenceEvent onNotSelected;
        

        [Space()]
        [Header("Trivia events")]
        public EventStrArr onAnswersTxt;

        [System.Serializable]
        public class EventStrArr : UnityEngine.Events.UnityEvent<string[]> { }

        public EventInt onQuestionTimeLimitStart;
        public EventEmpty onTimerStop;


        protected new void Start()
        {
            base.Start();
            //length = float.PositiveInfinity;

            if (qState.TriviaInfo.HasTimeLimitPerQuestion)
                length = (int)qState.TriviaInfo.SecondsPerQuestion;

            onAnyStop.AddListener(()=> {
                Debug.Log(name + " AnyStop " + qState.TriviaInfo.HasTimeLimitPerQuestion);
                if (qState.TriviaInfo.HasTimeLimitPerQuestion)
                    onTimerStop.Invoke();
            });
        }

        protected override void RunIt()
        {

            if (qState.TriviaInfo.HasTimeLimitPerQuestion)
            {
                onQuestionTimeLimitStart.Invoke((int)qState.TriviaInfo.SecondsPerQuestion);
                length = qState.TriviaInfo.SecondsPerQuestion;
            }  
            

            if (qState.CurrentQ.Answers != null && qState.IsTrueFalse)//TODO when answers can be null?
            {
                qState.CurrentQ.SortAnswers();
            }

            if (qState.CurrentQ.Answers != null)
            {
                onAnswersTxt.Invoke(qState.CurrentQ.Answers);
            }

            nextEvents = new ASequenceEvent[] { onNotSelected };
        }

        public void AnswerSelected(int index)
        {
            AnswerSelected(qState.CurrentQ.Answers[index]);
        }

        public void AnswerSelected(string answer)
        {

            Debug.Log("AnswerSelected");
            if (IsRunning == false)
            {
                Debug.LogError("Clicked answer out of answering state have no effect");
                return;
            }
            qState.SelectedAnswer = null;
            //Add delay to press?    
            if (answer == null)
            {
                nextEvents = new ASequenceEvent[] { onNotSelected };
            }
            else
            {
                qState.SelectedAnswer = answer;

                if (qState.IsCorrectAnswer()) nextEvents = new ASequenceEvent[] { onCorrectAnswer };
                else nextEvents = new ASequenceEvent[] { onWrongAnswer };
            }

            TimeEnd();
        }

        public void AnswerSelected(TextMeshPro answerUi)
        {
            AnswerSelected(answerUi == null ? null : answerUi.text);
        }


    }
}