﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;
using Dweiss.Msg;

namespace DomeVR
{
    public class QuestionCycleStartSeqEvent : BaseQuestionTriviaSeq
    {

        private bool overrideStopQuestion;
        public EventQuestionType eventQuestionType;

        [System.Serializable]
        public class EventQuestionType : UnityEngine.Events.UnityEvent<QuestionTriviaUI.QuestionType> { }

        protected override void Start()
        {
            base.Start();
            eventQuestionType.Invoke(qState.IsTrueFalse? QuestionTriviaUI.QuestionType.QuestionWith2Answers : QuestionTriviaUI.QuestionType.QuestionWith4Answers);
        }

        public void StopQuestions()
        {
            overrideStopQuestion = true;
        }

        protected override void RunIt()
        {
            Debug.Log("Is CYcle start running " + IsRunning);
            //Debug.LogFormat("Has {0} qCount {1} total {2}", qState.QBag.HasMoreQuestions, qState.QuestionCount, qState.TriviaInfo.TotalQuestionsInRound);
            if (overrideStopQuestion == false  && qState.HasMoreQuestions && qState.QuestionCount < qState.TriviaInfo.TotalQuestionsInRound)
            {
                qState.MoveToNextQuestion();

                Debug.Log("Question start #" + qState.QuestionCount + ". " +
                    qState.CurrentQ.Question +
                    "(" + qState.CurrentQ.questionNum + ")");
            }
            else
            {
                Debug.Log("Trivia cycle finish questions");
                ForceStop();
            }
        }


    }

}