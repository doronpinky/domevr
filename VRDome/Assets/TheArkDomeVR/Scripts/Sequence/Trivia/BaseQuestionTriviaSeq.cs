﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class BaseQuestionTriviaSeq : SimpleSequenceEvent
    {
        [Space()]
        [Header("Trivia options")]
        [SerializeField] protected AQuestionState qState;
        
    }
}