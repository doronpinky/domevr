﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Dweiss;
using Dweiss.Msg;

namespace DomeVR
{
    public class ShowQuestionSeqEvent : BaseQuestionTriviaSeq
    {
        public float delayVoiceOverOnFirstTime = 0;

        [Space()]
        [Header("Trivia events")]
        public EventString onQuestionStr;
        public EventAudioClip onVoiceOver;

        public string folderPrefix;
        private Coroutine delayVoice;
        public void StopDelayVoiceOverOperations()
        {
            if (delayVoice != null) StopCoroutine(delayVoice);
            delayVoice = null;
        }

        protected override void RunIt()
        {

            var question = qState.CurrentQ.Question;

            if (question != null)
            {
                onQuestionStr.Invoke(question);
                if (qState.CurrentQ.Clip != null)
                {
                    if (delayVoiceOverOnFirstTime <= 0)
                        onVoiceOver.Invoke(qState.CurrentQ.Clip);
                    else
                    {
                        delayVoice = this.WaitForSeconds(delayVoiceOverOnFirstTime, () => onVoiceOver.Invoke(qState.CurrentQ.Clip));
                        delayVoiceOverOnFirstTime = -1;
                    }
                }
            }

            if (string.IsNullOrEmpty(qState.CurrentQ.QuestionVoiceOverClipName) == false)
            {
                var narrationLocation = (string.IsNullOrEmpty(folderPrefix) ? "" : folderPrefix + "/") + qState.CurrentQ.QuestionVoiceOverClipName;
                PreloadStreamingAsset.S.Load(narrationLocation, (ac) => { if (IsRunning || nextEvents[0].IsRunning) onVoiceOver.Invoke(ac); }); 
            }
        }

        

    }
}