﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{
    public class ShowVideoSequenceEvent : BaseQuestionTriviaSeq
    {

        public Dweiss.EventString onVideoName;

        public string folderPrefix;

        private const float DefaultLengthOnMovie = float.PositiveInfinity;

        private bool movieWasLoaded = false;
        protected override void RunIt()
        {
            if (string.IsNullOrEmpty(qState.CurrentQ.PreMovieName) == false)
            {
                length = DefaultLengthOnMovie;

                LoadMovie();
            }
            else
            {
                length = -1;
            }
            movieWasLoaded = false;
        }


        private void LoadMovie()
        {
            if (movieWasLoaded == false)
            {
                var movieName = (string.IsNullOrEmpty(folderPrefix) ? "" : folderPrefix + "/") + qState.CurrentQ.PreMovieName;
                Debug.Log("Play movie " + movieName + " need stop event to be configured manually");
                Debug.LogWarning("Play movie " + movieName + " need stop event to be configured manually");
                onVideoName.Invoke(movieName);
                movieWasLoaded = true;
            }
        }

        public void PreloadMovie()
        {
            LoadMovie(); ;

//#if UNITY_ANDROID
//            LoadMovie();
//#else
//            CacheMovie();
//#endif
        }

        private void CacheMovie()
        {
            var movieName = (string.IsNullOrEmpty(folderPrefix) ? "" : folderPrefix + "/") + qState.CurrentQ.PreMovieName;
            Dweiss.PreloadStreamingAsset.S.Load(movieName, true, (v) =>
            {
                Destroy(v.gameObject);
                Debug.Log("Prep movie finished: " + movieName + " waiting " + v.length + " v prep " + v.isPrepared);
            });
        }

        public void OnMovieEnd()
        {
            TimeEnd();
        }

    }
}