﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class TriviaUtils : MonoBehaviour
    {
        public bool useYearsAsScore = true;

        public string yearsFloatFormat = "0";
        [SerializeField] private Dweiss.EventInt onScore;
        [SerializeField] private Dweiss.EventFloat onScoreFloat;
        [SerializeField] private Dweiss.EventString onScoreStr;

        public void MoveToNextMovie()
        {
            GameFlow.S.GoToNextSceneAccordingToUserAction(-1);
        }

        private void Start()//Show enable button
        {
            
        }
        private void Awake()
        {
            UpdateScore();
        }

        public void UpdateScore()
        {
            float score;
            score = useYearsAsScore ? ScoreSystem.S.CurrentYearsFloat : (float)ScoreSystem.S.SceneCorrectAnswerCount;
            SetScore(score);
        }

        public void AddScore(float num)
        {
            ScoreSystem.S.DecreaseYearCount(num);
        }

        private void SetScore(float score)
        {
            Debug.Log("Update score as " + (useYearsAsScore ? "Years " : "Count ") + score);
            onScore.Invoke((int)score);
            onScoreFloat.Invoke(score);
            onScoreStr.Invoke(score.ToString(yearsFloatFormat));
        }

        public void OverrideScoreOnGui(float score)
        {
            SetScore(score);
        }

        private void OnEnable()
        {
            if(Dweiss.Msg.MsgSystem.S)
                Dweiss.Msg.MsgSystem.MsgStr.Register("YearsToReduce", UpdateScore);

            UpdateScore();
        }

        private void OnDisable()
        {
            if (Dweiss.Msg.MsgSystem.S)
                Dweiss.Msg.MsgSystem.MsgStr.Unregister("YearsToReduce", UpdateScore);
        }
    }
}