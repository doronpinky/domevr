﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR
{
   [DefaultExecutionOrder(-10)]
    public class QuestionStateInDemand : AQuestionState
    {
        [SerializeField]private QuestionBag qBag;

        public override ScriptableSceneSettings.TriviaInfo TriviaInfo
        {
            get { return GameFlow.S.CurrentMovieScene.triviaInfo; }
        }

        private ReadQuestion.QuestionInfo _currentQ;
        public override ReadQuestion.QuestionInfo CurrentQ { get { return _currentQ; } }
        public override void MoveToNextQuestion() { _currentQ = myAvailableQ[0]; /*Stub for error avoidance*/}

        private List<ReadQuestion.QuestionInfo> myAvailableQ;
        public void SetClosestQuestionByGroupId(int groupIdToFind)
        {
            groupIdToFind = (groupIdToFind % (maxNum-1))+1;
            myAvailableQ.Sort((a, b) => {
                var aCount = a.GroupId;
                if (aCount < groupIdToFind) aCount += 2*maxNum;

                var bCount = b.GroupId;
                if (bCount < groupIdToFind) bCount += 2 * maxNum;

                return aCount.CompareTo(bCount);

            });

            _currentQ = myAvailableQ[0];
            myAvailableQ.RemoveAt(0);

            ++_questionCount;
        }


        private string _selectedAnswer;
        public override string SelectedAnswer
        {
            get { return _selectedAnswer; }
            set { _selectedAnswer = value; }
        }

        public override bool IsCorrectAnswer()
        {
            return CurrentQ.IsAnswerCorrect(SelectedAnswer);
        }

        public override bool IsTrueFalse { get { return qBag.IsTrueFalse; } }

        public override bool HasMoreQuestions { get { return myAvailableQ.Count > 0 && _questionCount < TriviaInfo.TotalQuestionsInRound; } }

        private int _questionCount;
        public override int QuestionCount { get { return _questionCount; }  }

        private int maxNum;
        private void Start()
        {
            qBag.Init(TriviaInfo.QuestionId);//TODO allow randomize

            myAvailableQ = qBag.QuestionList.ToList();
            myAvailableQ.Sort((a, b) => a.GroupId.CompareTo(b.GroupId));
            maxNum = myAvailableQ[myAvailableQ.Count - 1].GroupId;
        }
    }
}