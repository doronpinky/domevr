﻿using Dweiss;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;
using System.Linq;


namespace DomeVR
{
    //TODO add q&A true false. Add logic movie show. Add logic show true
    public class QuestionTriviaUI : MonoBehaviour
    {
        public bool debug;

        public QuestionTriviaUI triviaUi;

        public enum QuestionType { QuestionWith4Answers, QuestionWith2Answers}
        [SerializeField]private QuestionType qType = QuestionType.QuestionWith4Answers;

        private const float notAnswerAlpha = .2f;

        [SerializeField] private AudioSource qVoiceOver;


        [SerializeField] private UIClockElement[] timers;
      //  [SerializeField] private UIClockElement[] squareTimers;

        private UIClockElement[] Timers
        {
            get
            {
                switch (qType)
                {
                    case QuestionType.QuestionWith4Answers: return timers;
                    case QuestionType.QuestionWith2Answers: return timers;
                    //case QuestionType.QuestionBoxWith2Answers: return squareTimers;
                    default: return timers;
                }
            }
        }

        [SerializeField] private UITextElement[] questionsUi;
        [SerializeField] private UITextElement[] questionsTrueFalseUi;
       // [SerializeField] private UITextElement[] questionsSquareTrueFalseUi;
        private UITextElement[] QnswersUI { get {
                switch (qType)
                {
                    case QuestionType.QuestionWith4Answers: return questionsUi;
                    case QuestionType.QuestionWith2Answers: return questionsTrueFalseUi;
                    //case QuestionType.QuestionBoxWith2Answers: return questionsSquareTrueFalseUi;
                    default: return questionsUi;
                }
            } }

        [SerializeField] private UIAnswerElement[] answersUi;
        [SerializeField] private UIAnswerElement[] answersTrueFalseUi;
      //  [SerializeField] private UIAnswerElement[] answersSquareTrueFalseUi;

        public void ChangeQuestionType(QuestionType qType)
        {
            this.qType = qType;

            if (triviaUi)triviaUi.ChangeQuestionType(qType);
        }

        private UIAnswerElement[] AnswersUI
        {
            get
            {
                switch (qType)
                {
                    case QuestionType.QuestionWith4Answers: return answersUi;
                    case QuestionType.QuestionWith2Answers: return answersTrueFalseUi;
                  //  case QuestionType.QuestionBoxWith2Answers: return answersSquareTrueFalseUi;
                    default: return answersUi;
                }
            }
        }
        [SerializeField] private UITextElement[] scores;
        //[SerializeField] private UITextElement[] bestScores;
        //[SerializeField] private UITextElement[] years;

        [SerializeField] private UITextElement[] infoText;

        [SerializeField] private UIVideoElement[] videos;

        private void SetupUIAnswerElement(UIAnswerElement[] answersElements)
        {
            SetupUIText(answersElements);
            for (int i = 0; i < answersElements.Length; i++)
            {
                if (answersElements[i].go == null) continue;
                if (answersElements[i].correct == null)
                {
                    var trns = answersElements[i].go.transform.FindChild((t) => t.name.ToLower().Contains("correct"));
                    if (trns != null) answersElements[i].correct = trns.gameObject;
                }
                if (answersElements[i].selectedNotCorrect == null)
                {
                    var trns = answersElements[i].go.transform.FindChild((t) => t.name.ToLower().Contains("wrong"));
                    if (trns != null) answersElements[i].selectedNotCorrect = trns.gameObject;
                }
                if (answersElements[i].regular == null)
                {
                    var rndrs = answersElements[i].go.GetComponentsInChildren<Renderer>();
                    for (int j = 0; j < rndrs.Length; j++)
                    {
                        if (rndrs[j].enabled == false) continue;
                        if (rndrs[j].gameObject != answersElements[i].selectedNotCorrect.gameObject &&
                            rndrs[j].gameObject != answersElements[i].correct.gameObject &&
                            rndrs[j].gameObject != answersElements[i].txt.gameObject &&
                            rndrs[j].gameObject != answersElements[i].go)
                        {
                            answersElements[i].regular = rndrs[j];
                            break;
                        }
                    }

                }
                if (answersElements[i].button == null)
                {
                    answersElements[i].button = answersElements[i].go.GetComponentInChildren<ClickAbleButton>();
                }
            }

        }
        private void SetupTimers(UIClockElement[] timersElements)
        {
            for (int i = 0; i < timersElements.Length; i++)
            {
                if (timersElements[i].go != null && timersElements[i].clock != null) continue;
                if (timersElements[i].go) timersElements[i].clock = timersElements[i].go.GetComponentInChildren<Clock>();
                else if (timersElements[i].clock) timersElements[i].go = timersElements[i].clock.gameObject;
            }
        }
        [ContextMenu("Update monos")]
        private void ResetMonos()
        {
            SetupUIText(questionsUi);
            SetupUIText(questionsTrueFalseUi);
            //SetupUIText(questionsSquareTrueFalseUi);

            SetupUIText(infoText);
            SetupUIText(scores);
            //SetupUIText(years);
            //SetupUIText(bestScores);

            SetupUIAnswerElement(answersUi);
            SetupUIAnswerElement(answersTrueFalseUi);
           // SetupUIAnswerElement(answersSquareTrueFalseUi);

            SetupTimers(timers);
           // SetupTimers(squareTimers);
            for (int i = 0; i < videos.Length; i++)
            {
                if (videos[i].go && videos[i].video == null) videos[i].video = videos[i].go.GetComponentInChildren<VideoPlayer>();
                if (videos[i].go && videos[i].videoEvents == null) videos[i].videoEvents = videos[i].go.GetComponentInChildren<VideoEvents>();
            }

#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }


        private void Awake()
        {
            HideAll();

            Show(questionsUi, false);
            Show(questionsTrueFalseUi, false);
          //  Show(questionsSquareTrueFalseUi, false);

            Show(answersUi, false);
            Show(answersTrueFalseUi, false);
         //   Show(answersSquareTrueFalseUi, false);

            Show(timers, false);
          //  Show(squareTimers, false);
        }

        private void SetupUIText(UITextElement[] elemts)
        {
            for (int i = 0; i < elemts.Length; i++)
            {
                if (elemts[i].go != null && elemts[i].txt != null) continue;

                if (elemts[i].go) elemts[i].txt = elemts[i].go.GetComponentInChildren<TextMeshPro>();
                else if (elemts[i].txt) elemts[i].go = elemts[i].txt.gameObject;
            }
        }

        [System.Serializable]
        public class UIElement
        {
            public GameObject go;
        }

        [System.Serializable]
        public class UITextElement : UIElement
        {
            public TextMeshPro txt;
        }
        [System.Serializable]
        public class UIAnswerElement : UITextElement
        {
            public Renderer regular;
            public GameObject correct;
            public GameObject selectedNotCorrect;
            public ClickAbleButton button;
        }
        [System.Serializable]
        public class UIVideoElement : UIElement
        {
            public VideoPlayer video;
            public VideoEvents videoEvents;
        }
        [System.Serializable]
        public class UIClockElement : UIElement
        {
            public Clock clock;
        }

        public void SetTimeLimit(int seconds)
        {
            if (debug) Debug.Log("SetTimeLimit");
            Show(timers, true);
            timers.For(a => a.clock.Reset(seconds));

            if (triviaUi) triviaUi.SetTimeLimit(seconds);
        }
        public void StopTimer()
        {
            if (debug) Debug.Log("StopTimer");
            timers.For(a => a.clock.Stop());

            if (triviaUi) triviaUi.StopTimer();
        }
        public void SetQuestionText(string question)
        {
            if (debug) Debug.Log("SetQuestionText");
            for (int i = 0; i < QnswersUI.Length; i++)
            {
                QnswersUI[i].go.SetActive(true);
                QnswersUI[i].txt.text = question;
            }

            if (triviaUi) triviaUi.SetQuestionText(question);
        }

        public void SetQuestionVoiceOver(AudioClip voiceOver)
        {
            if (debug) Debug.Log("SetQuestionVoiceOver");
            if (qVoiceOver)
            {
                qVoiceOver.clip = voiceOver;
                qVoiceOver.Play();
            }

            if (triviaUi) triviaUi.SetQuestionVoiceOver(voiceOver);
        }

        public void SetInfoText(string txt)
        {
            if (debug) Debug.Log("SetInfoText");
            for (int i = 0; i < infoText.Length; i++)
            {
                infoText[i].go.SetActive(true);
                infoText[i].txt.text = txt;
            }

            if (triviaUi) triviaUi.SetInfoText(txt);
        }

        public void SetInfoVoiceOver(AudioClip voiceOver)
        {
            if (debug) Debug.Log("SetInfoVoiceOver");

            if (qVoiceOver)
            {
                qVoiceOver.clip = voiceOver;
                qVoiceOver.Play();
            }

            if (triviaUi) triviaUi.SetInfoVoiceOver(voiceOver);
        }

     

        public void SetVideo(string videoName)
        {
            if (debug) Debug.Log("SetVideo");

            PrivateShowVideos(true);

            var movieWithSuffix = PreloadStreamingAsset.S.AddSuffixToMovie(name);
            movieWithSuffix = PreloadStreamingAsset.S.GetFullPath(movieWithSuffix);
           
            for (int i = 0; i < videos.Length; i++)
            {
                int index = i;
                videos[index].go.SetActive(true);
                videos[index].videoEvents.PlayWithEvent(movieWithSuffix);
                //PreloadStreamingAsset.S.Load(videoName, videos[index].video, (v) => { videos[index].videoEvents.PlayWithEvent(); });
                

            }

            if (triviaUi) triviaUi.SetVideo(videoName);
        }

        //public void ShowCorrectAnswer(string txt) {

        //    for (int i = 0; i < AnswersUI.Length; i++)
        //    {
        //        if (AnswersUI[i].txt.text == txt)
        //        {
        //            AnswersUI[i].go.SetActive(true);
        //            AnswersUI[i].correct.SetActive(true);
        //            AnswersUI[i].selectedNotCorrect.SetActive(false);
        //            SetAlphaOfAnswer(AnswersUI[i], true);
        //            break;
        //        }
        //    }
        //}
        //public void ShowWrongAnswer(string txt) {
        //    for (int i = 0; i < AnswersUI.Length; i++)
        //    {
        //        if (AnswersUI[i].txt.text == txt)
        //        {
        //            AnswersUI[i].go.SetActive(true);
        //            AnswersUI[i].correct.SetActive(false);
        //            AnswersUI[i].selectedNotCorrect.SetActive(true);
        //            SetAlphaOfAnswer(AnswersUI[i], true);
        //            break;
        //        }
        //    }
        //}
        //public void HideAnswersFrames(string txt) {
        //    for (int i = 0; i < AnswersUI.Length; i++)
        //    {
        //        if (AnswersUI[i].txt.text == txt)
        //        {
        //            AnswersUI[i].go.SetActive(true);
        //            AnswersUI[i].correct.SetActive(false);
        //            AnswersUI[i].selectedNotCorrect.SetActive(true);
        //            SetAlphaOfAnswer(AnswersUI[i], true);
        //            break;
        //        }
        //    }
        //}
        public void InitAnswers()
        {
            //if (debug) Debug.Log("SetAnswerStatus " + status.Length);
           // for (int i = 0; i < AnswersUI.Length; i++)
            //{
            SetAnswerStatus(0, SeeAnswerSeqEvent.AnswerOption.Hide, 1);//Will set all answers
            //SetAnswersText(null);
            AllowClickAbleAnswers(true);

            if (triviaUi) triviaUi.InitAnswers();
        }
        public void SetAnswersStatus(SeeAnswerSeqEvent.AnswerOption[] status)
        {
            if (debug) Debug.Log("SetAnswerStatus " + status.Length);

            for (int i = 0; i < status.Length; i++)
            {
                SetAnswerStatus(i, status[i], status.Length);
            }
            AllowClickAbleAnswers(false);

            if (triviaUi) triviaUi.SetAnswersStatus(status);
        }

        private void SetAlphaOfAnswer(UIAnswerElement elem, bool opaque)
        {
            elem.txt.color = new Color(elem.txt.color.r, elem.txt.color.g, elem.txt.color.b, opaque ? 1 : notAnswerAlpha);
            elem.regular.material.color = new Color(elem.regular.material.color.r, elem.regular.material.color.g, elem.regular.material.color.b, opaque ? 1 : notAnswerAlpha);
        }

        private void SetAnswerStatus(int index, SeeAnswerSeqEvent.AnswerOption status, int answersCount)
        {
            for (; index < AnswersUI.Length; index = index + answersCount)
            {
                switch (status)
                {
                    case SeeAnswerSeqEvent.AnswerOption.Show:
                        AnswersUI[index].go.SetActive(true);
                        AnswersUI[index].regular.SetActive(true);
                        AnswersUI[index].correct.SetActive(false);
                        AnswersUI[index].selectedNotCorrect.SetActive(false);
                        SetAlphaOfAnswer(AnswersUI[index], true);
                        break;

                    case SeeAnswerSeqEvent.AnswerOption.Hide:
                        SetAlphaOfAnswer(AnswersUI[index], true);
                        AnswersUI[index].go.SetActive(false);
                        AnswersUI[index].regular.SetActive(true);
                        AnswersUI[index].correct.SetActive(false);
                        AnswersUI[index].selectedNotCorrect.SetActive(false);
                        break;

                    case SeeAnswerSeqEvent.AnswerOption.MarkCorrect:
                        AnswersUI[index].go.SetActive(true);
                        AnswersUI[index].regular.SetActive(false);
                        AnswersUI[index].correct.SetActive(true);
                        AnswersUI[index].selectedNotCorrect.SetActive(false);
                        SetAlphaOfAnswer(AnswersUI[index], true);
                        break;

                    case SeeAnswerSeqEvent.AnswerOption.SetTransparent:
                        AnswersUI[index].go.SetActive(true);
                        AnswersUI[index].regular.SetActive(true);
                        AnswersUI[index].correct.SetActive(false);
                        AnswersUI[index].selectedNotCorrect.SetActive(false);
                        SetAlphaOfAnswer(AnswersUI[index], false);
                        break;

                    case SeeAnswerSeqEvent.AnswerOption.MarkWrong:
                        AnswersUI[index].go.SetActive(true);
                        AnswersUI[index].regular.SetActive(false);
                        AnswersUI[index].correct.SetActive(false);
                        AnswersUI[index].selectedNotCorrect.SetActive(true);
                        SetAlphaOfAnswer(AnswersUI[index], true);
                        break;

                    default:
                        AnswersUI[index].go.SetActive(true);
                        AnswersUI[index].correct.SetActive(false);
                        AnswersUI[index].selectedNotCorrect.SetActive(false);
                        SetAlphaOfAnswer(AnswersUI[index], true);
                        Debug.LogError("show type not supported " +status);
                        break;
                }
            }
        }

        public void SetAnswersText(string[] answers)
        {
            if (debug) Debug.Log("SetAnswersText");
            for (int i = 0; i < AnswersUI.Length; i++)
            {
                SetAnswerStatus(i, SeeAnswerSeqEvent.AnswerOption.Show, answers == null ? AnswersUI.Length : answers.Length);
                AnswersUI[i].txt.text = answers == null ? "" : answers[i % answers.Length];
            }
            AllowClickAbleAnswers(true);

            if (triviaUi) triviaUi.SetAnswersText(answers);
        }

        private void AllowClickAbleAnswers(bool enableClick)
        {
            if (debug) Debug.Log("AllowClickAbleAnswers");
            for (int i = 0; i < AnswersUI.Length; i++)
            {
                if(enableClick) AnswersUI[i].button.ResetClicks();
                else AnswersUI[i].button.StopClicks();
            }
        }

        //public void SetYear(int year)
        //{
        //    if (debug) Debug.Log("Setyears");
        //    for (int i = 0; i < years.Length; i++)
        //    {
        //        years[i].go.SetActive(true);
        //        years[i].txt.text = year.ToString();
        //    }

        //}
        public void SetScore(int scoreValue)
        {
            if (debug) Debug.Log("SetScore");
            for (int i = 0; i < scores.Length; i++)
            {
                scores[i].go.SetActive(true);
                scores[i].txt.text = scoreValue.ToString();
            }
            if (triviaUi) triviaUi.SetScore(scoreValue);
        }
        public void SetScore(string scoreTxt)
        {
            if (debug) Debug.Log("SetScoreTxt");
            for (int i = 0; i < scores.Length; i++)
            {
                scores[i].go.SetActive(true);
                scores[i].txt.text = scoreTxt;
            }
            if (triviaUi) triviaUi.SetScore(scoreTxt);
        }

        //public void SetBestScore(int scoreValue)
        //{
        //    if (debug) Debug.Log("SetBestScore");
        //    for (int i = 0; i < scores.Length; i++)
        //    {
        //        bestScores[i].go.SetActive(true);
        //        bestScores[i].txt.text = scoreValue.ToString();
        //    }
        //}

        private void Show(UIElement[] arr, bool show)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i].go.SetActive(show);
            }
            
        }

        public void ShowOnly(int mask)
        {
            PrivateShow(mask, true);
            PrivateShow(~mask, false);

            if (triviaUi) triviaUi.ShowOnly(mask);
        }

        public void SetVisible(int mask)
        {
            PrivateShow(mask, true);

            if (triviaUi) triviaUi.SetVisible(mask);
        }

        public void SetHidden(int mask)
        {
            PrivateShow(mask, false);

            if (triviaUi) triviaUi.SetHidden(mask);
        }

        public void Show(int mask, bool show) {
            if (debug) Debug.Log("ShowMask " + mask + " " + show);

            PrivateShow(mask, show);
            if (triviaUi) triviaUi.Show(mask, show);

        }

        private void PrivateShow(int mask, bool show)
        {

            if (IsInMask(mask, GuiElements.QuestionText)) Show(QnswersUI, show);
            if (IsInMask(mask, GuiElements.AnswersText)) PrivateShowAnswers(show);
            if (IsInMask(mask, GuiElements.Score)) Show(scores, show);
           // if (IsInMask(mask, GuiElements.Year)) Show(years, show);
            if (IsInMask(mask, GuiElements.Video)) Show(videos, show);
            if (IsInMask(mask, GuiElements.Timer)) Show(timers, show);
           // if (IsInMask(mask, GuiElements.BestScore)) Show(bestScores, show);
            if (IsInMask(mask, GuiElements.VoiceOver)) PrivateStopVoiceOver();

        }


        private bool IsInMask(int mask, GuiElements compare)
        {
            var maskResult = (uint)mask & (uint)compare;
            // Debug.LogFormat("Mask {0} compare to {1} is {2}", mask, compare, maskResult);
            return maskResult != 0;
        }

        public enum GuiElements : int
        {
            Timer = 1,
            QuestionText = 2,
            AnswersText = 4,
            Score = 8,
            Video = 16,
            NOT_USED_BestScore = 32,
            NOT_USED_Year = 64,
            VoiceOver = 128,
            InfoText = 256,
        }

        public void HideAll() { PrivateHideAll();
            if (triviaUi) triviaUi.HideAll();
        }

        public void ShowQuestion(bool show) { PrivateShowQuestion(show); if (triviaUi) triviaUi.ShowQuestion(show); }
        public void ShowInfoText(bool show) { PrivateShowInfoText(show); if (triviaUi) triviaUi.ShowInfoText(show); }
        public void ShowAnswers(bool show) { PrivateShowAnswers(show); if (triviaUi) triviaUi.ShowAnswers(show); }
        public void ShowScore(bool show) { PrivateShowScore(show); if (triviaUi) triviaUi.ShowScore(show); }
       // public void ShowBestScore(bool show) { Show(bestScores, show); if (debug) Debug.Log("ShowBestScore " + show); }
        //public void ShowYear(bool show) { Show(years, show); if (debug) Debug.Log("ShowYear " + show); }
        public void ShowVideos(bool show) { PrivateShowVideos(show); if (triviaUi) triviaUi.ShowVideos(show); }
        public void ShowTimer(bool show) { PrivateShowTimer(show); if (triviaUi) triviaUi.ShowTimer(show); }

        public void StopVoiceOver() { PrivateStopVoiceOver(); if (triviaUi) triviaUi.StopVoiceOver(); }

        #region private callable
        private void PrivateHideAll() { unchecked { SetHidden((int)uint.MaxValue); } }

       private void PrivateShowQuestion(bool show) { Show(QnswersUI, show); if (debug) Debug.Log("ShowQuestion " + show); }
       private void PrivateShowInfoText(bool show) { Show(infoText, show); if (debug) Debug.Log("ShowInfoText " + show); }
       private void PrivateShowAnswers(bool show) { SetAnswersText(null); Show(AnswersUI, show); if (debug) Debug.Log("ShowAnswers " + show); }
        private void PrivateShowScore(bool show) { Show(scores, show); if (debug) Debug.Log("ShowScore " + show); }
        // public void PrivateShowBestScore(bool show) { Show(bestScores, show); if (debug) Debug.Log("ShowBestScore " + show); }
        //public void PrivateShowYear(bool show) { Show(years, show); if (debug) Debug.Log("ShowYear " + show); }
        private void PrivateShowVideos(bool show) { Show(videos, show); if (debug) Debug.Log("ShowVideos " + show); }
        private void PrivateShowTimer(bool show) { Show(timers, show); if (debug) Debug.Log("ShowTimer " + show); }

        private void PrivateStopVoiceOver() { if(qVoiceOver) qVoiceOver.Stop(); if (debug) Debug.Log("StopVoiceOver "); }
        #endregion

    }
}