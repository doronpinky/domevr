﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
   [DefaultExecutionOrder(-10)]
    public class QuestionState : AQuestionState
    {
        [SerializeField]private QuestionBag qBag;
       // private QuestionBag QBag { get { return qBag; } }

        public override ScriptableSceneSettings.TriviaInfo TriviaInfo
        {
            get { return GameFlow.S.CurrentMovieScene.triviaInfo; }
        }

        private ReadQuestion.QuestionInfo _currentQ;
        public override ReadQuestion.QuestionInfo CurrentQ { get { return _currentQ; } }
        public override void MoveToNextQuestion() {
            Debug.Log("---MoveToNextQuestion");
            if(_currentQ != null)
                qBag.MoveToNextQuestion();
            _currentQ = qBag.CurrentQ;
            _questionCount++;
            _selectedAnswer = null;
        }

        private string _selectedAnswer;
        public override string SelectedAnswer
        {
            get { return _selectedAnswer; }
            set { _selectedAnswer = value; }
        }

        public override bool IsCorrectAnswer()
        {
            return CurrentQ.IsAnswerCorrect(SelectedAnswer);
        }

        public override bool IsTrueFalse { get { return qBag.IsTrueFalse; } }

        public override bool HasMoreQuestions { get { return qBag.HasMoreQuestions; } }

        private int _questionCount;
        public override int QuestionCount { get { return _questionCount; }  }

        private void Start()
        {
            qBag.Init(TriviaInfo.QuestionId);
        }
    }
}