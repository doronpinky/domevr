﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DomeVR
{

    public interface IQuestionState
    {
        ScriptableSceneSettings.TriviaInfo TriviaInfo { get; }
        ReadQuestion.QuestionInfo CurrentQ { get; }
        void MoveToNextQuestion();
        string SelectedAnswer { get; set; }
        bool IsCorrectAnswer();
        bool IsTrueFalse { get; }
        bool HasMoreQuestions { get; }
        int QuestionCount { get; }

    }
    public abstract class AQuestionState : MonoBehaviour, IQuestionState
    {
        public abstract ScriptableSceneSettings.TriviaInfo TriviaInfo { get; }
        public abstract ReadQuestion.QuestionInfo CurrentQ { get; }
        public abstract void MoveToNextQuestion();
        public abstract string SelectedAnswer { get; set; }
        public abstract bool IsCorrectAnswer();
        public abstract bool IsTrueFalse { get; }
        public abstract bool HasMoreQuestions { get; }
        public abstract int QuestionCount { get; }
    }
}