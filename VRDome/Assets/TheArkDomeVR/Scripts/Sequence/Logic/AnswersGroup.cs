﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace DomeVR
{
    public class AnswersGroup : MonoBehaviour
    {
        public GameObject[] answers;
        public TextMeshPro[] answersTxt;

        [ContextMenu("Update TxT refernce")]
        private void SetText()
        {
            answersTxt = new TextMeshPro[answers.Length];
            for (int i = 0; i < answers.Length; i++)
            {
                answersTxt[i] = answers[i].GetComponentInChildren<TextMeshPro>();
            }
        }
    }
}