﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading;

public class ValuesFromArduinoButtons : MonoBehaviour
{
    [SerializeField]
    private string numOfCom;
    private GetValueFromArduino.ArduinoValue arduinoValue;// = new GetValueFromArduino.ArduinoValue(9600, "com3");
    
    public UnityEvent Button1Clicked;
    public UnityEvent Button2Clicked;
    public UnityEvent Button3Clicked;
    public UnityEvent Button4Clicked;

    private void Start()
    {
        arduinoValue = new GetValueFromArduino.ArduinoValue(9600, numOfCom);
        GetValueFromArduino.ArduinoValue.onDataReceived += UseValue;
        arduinoValue.Connect();
        arduinoValue.thread = new Thread(new ThreadStart(arduinoValue.UpdateController));
        arduinoValue.thread.Start();
    }

    public void UseValue(string data)
    {
        Debug.Log(data);
        switch (data)
        {
            case "1":
                if(Button1Clicked != null)
                {
                    Button1Clicked.Invoke();
                }
                break;
            case "2":
                if (Button2Clicked != null)
                {
                    Button2Clicked.Invoke();
                }
                break;
            case "3":
                if (Button3Clicked != null)
                {
                    Button3Clicked.Invoke();
                }
                break;
            case "4":
                if (Button4Clicked != null)
                {
                    Button4Clicked.Invoke();
                }
                break;
            default:
                Debug.LogError("Value Is Not Valid!");
                break;

        }
    }
}
