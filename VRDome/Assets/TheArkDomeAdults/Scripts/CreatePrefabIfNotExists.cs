﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePrefabIfNotExists : MonoBehaviour
{
    public GameObject prefab;

    void Awake()
    {
        var tagged = GameObject.FindGameObjectWithTag(prefab.tag);
        if(tagged == null)
        {
            var pref = Instantiate(prefab);
            pref.transform.position = Vector3.zero;
        }
    }

}