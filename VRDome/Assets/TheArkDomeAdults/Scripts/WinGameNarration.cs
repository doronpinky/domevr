﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DomeVR
{
    public class WinGameNarration : MonoBehaviour
    {
        public Dweiss.EventEmpty win, superWin;

        public void RaiseWin()
        {
            if (ScoreSystem.S.ReduceMoreYearsThanPossible) superWin.Invoke();
            else win.Invoke();

        }

    }
}