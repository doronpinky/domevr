﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DomeVR {
    public class YearNarration : MonoBehaviour
    {
        public int startYear = 2030, endYear = 2051;
        public string fileFormatString = "{0}.wav";
        public string folder = "/Years/";

        private List<AudioWithClip> myClips = new List<AudioWithClip>();

        public bool narrateOnReady = false;

        [System.Serializable]
        public class AudioWithClip
        {
            public AudioClip clip;
            public int id;
        }

        private AudioSource aSource;

        private void Awake()
        {
            aSource = GetComponent<AudioSource>();
        }
        
        void Start()
        {
            for (int curYear = startYear; curYear < endYear; curYear++)
            {
                var id = curYear;
                Dweiss.PreloadStreamingAsset.S.Load(string.Format(folder + fileFormatString, id), 
                    (c) =>
                    {
                        myClips.Add(new AudioWithClip() { clip = c, id = id });
                        if (myClips.Count == (endYear-startYear) && narrateOnReady) Narrate();
                    });

            }
        }

        public void Narrate()
        {
            var years = ScoreSystem.S.CurrentYears;
            var clip = myClips.FirstOrDefault(a => a.id == years);
            if (clip == null) clip = myClips.FirstOrDefault(b => b.id == myClips.Max(a => a.id));
            if (clip != null)
            {
                aSource.clip = clip.clip;
                aSource.Play();
            }
        }

    }
}