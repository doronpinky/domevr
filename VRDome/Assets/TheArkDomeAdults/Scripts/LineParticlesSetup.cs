﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineParticlesSetup : MonoBehaviour
{
    private const float DegreeAngles = 360;

    public GameObject prefab;

    public float distance = 1;
    public int numOfParticleGroups = 8;

    [ContextMenu("Setup now")]
    private void ReSetup()
    {
        var children = transform.GetChildrens();
        foreach(var c in children)
        {
            if (Application.isPlaying)
            {
                Destroy(c.gameObject);
            }
            DestroyImmediate(c.gameObject);
        }

        Setup();
    }
    void Start()
    {
        Setup();
    }
    private void Setup() { 
        for (float i = 0; i < DegreeAngles; i = i + (DegreeAngles/numOfParticleGroups))
        {
            GameObject newPref;
#if UNITY_EDITOR
            if (Application.isPlaying == false)
            {
                newPref = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(prefab);
                newPref.transform.parent = transform;
            }else
            {
                newPref = Instantiate(prefab, transform);
            }
#else
            newPref = Instantiate(prefab, transform);
#endif
            var rot = Quaternion.Euler(0, i, 0);
            var revRot = Quaternion.Euler(0, -i, 0);
            var forward = rot * Vector3.forward;
            newPref.transform.SetPositionAndRotation(forward * distance, revRot);
        }
    }

}
