﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class DissolveAnimation : MonoBehaviour
{
    private Renderer rndr;

    [SerializeField] private bool forwardAnimate = true;
    [SerializeField] private float timeToAnimate = 5;
    [SerializeField] private bool pingPong;

    [SerializeField] private float maxDissolveValue = 0.6f;

    public bool ForwardAnimate { get { return forwardAnimate;  } set { forwardAnimate = value; } }

    // Start is called before the first frame update
    void Awake()
    {
        rndr = GetComponent<Renderer>();
    }

    public void Dissolve()
    {

        StartCoroutine(AnimateDissolve());
    }

    private void OnEnable()
    {
        Dissolve();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
    // Update is called once per frame
    IEnumerator AnimateDissolve()
    {
        yield return AnimateOneDirection(forwardAnimate);

        while (pingPong)
        {
            yield return AnimateOneDirection(!forwardAnimate);
            yield return AnimateOneDirection(forwardAnimate);
        }
    }

    private IEnumerator AnimateOneDirection(bool forward)
    {
        var tStart = Time.time;

        for (float percent = 0; tStart + timeToAnimate > Time.time; percent = (Time.time - tStart) / timeToAnimate)
        {
            rndr.sharedMaterial.SetFloat("_Progress", (forward? percent :(1 - percent)) * maxDissolveValue);
            yield return 0;
        }
        rndr.sharedMaterial.SetFloat("_Progress", forward ? 1f : 0f);
    }
}
